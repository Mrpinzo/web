<?php
/* @var $this YiiEmailController */
/* @var $model YiiEmail */

$this->breadcrumbs=array(
	'Danh sách email'=>array('admin'),
	'Danh sách',
);

$this->menu=array(
	array('label'=>'Tạo mới email', 'url'=>array('create')),
);
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#yii-email-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h4>Dánh sách email</h4>
<?php $form = $this->beginWidget('CActiveForm', array(
    'enableAjaxValidation' => TRUE,
)); ?>
<input id="yt10" type="submit" value="Kích hoạt" name="yt10" style="display:none;" class="search-button">
<?php echo CHtml::ajaxSubmitButton('Xóa', array('YiiEmail/ajaxUpdate', 'act' => 'doDelete'), array('success' => 'reloadGrid', 'beforeSend' => 'function(){
            return confirm("Bạn có chắc chắn muốn xóa những email được chọn?")
        }',)); ?>
<?php 
$cs = Yii::app()->clientScript;
$css = 'ul.yiiPager .first, ul.yiiPager .last {display:inline;}';
$cs->registerCss('show_first_last_buttons', $css);
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'yii-email-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
		'ajaxUpdate'=>false,
		'pager' => array('maxButtonCount' => 4,'pageSize'=>10,'firstPageLabel'=>'<i class="fa fa-angle-double-left"></i>','lastPageLabel'=>'<i class="fa fa-angle-double-right"></i>'
				,'nextPageLabel' => '<i class="fa fa-angle-right"></i>',
				'prevPageLabel'=> '<i class="fa fa-angle-left"></i>',
				'header'=> '',
					
		),
	'columns'=>array(
			array(
					'id'             => 'autoId',
					'class'          => 'CCheckBoxColumn',
					'selectableRows' => '50',
			),
		'id_email',
		'email',
			array(
					'name'   => 'date',
					'value'  => 'date("H:i:s d-m-Y",strtotime($data->date))',
						
			),
		array(
			'class'=>'CButtonColumn',
				'header' => 'Hành động',
				'template'=>'{update}{delete}',
				'buttons'=>array
				(
						
						'update' => array(
								'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'Update')),
								'label' => '<i class="fa fa-pencil-square-o"></i>',
								'imageUrl' => false,
						),
						'delete' => array(
								'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'Delete')),
								'label' => '<i class="fa fa-trash-o"></i>',
								'imageUrl' => false,
						)
				),
		),
	),
)); ?>
<script>
    function reloadGrid(data) {
        $.fn.yiiGridView.update('yii-email-grid');
    }
</script>
<?php $this->endWidget(); ?>