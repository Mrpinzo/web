<?php
/* @var $this YiiEmailController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Yii Emails',
);

$this->menu=array(
	array('label'=>'Create YiiEmail', 'url'=>array('create')),
	array('label'=>'Manage YiiEmail', 'url'=>array('admin')),
);
?>

<h1>Yii Emails</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
