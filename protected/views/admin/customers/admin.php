<?php
/* @var $this CustomersController */
/* @var $model Customers */

$this->breadcrumbs=array(
	'Danh sách khách hàng'=>array('admin'),
	'Danh sách',
);


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#customers-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h4>Danh sách khách hàng</h4>
<?php $form = $this->beginWidget('CActiveForm', array(
    'enableAjaxValidation' => TRUE,
)); ?>
<input id="yt10" type="submit" value="Kích hoạt" name="yt10" style="display:none;">
<?php echo CHtml::ajaxSubmitButton('Kích hoạt', array('customers/ajaxUpdate', 'act' => 'doActive'), array('success' => 'reloadGrid')); ?>
<?php echo CHtml::ajaxSubmitButton('Không kích hoạt', array('customers/ajaxUpdate', 'act' => 'doInactive'), array('success' => 'reloadGrid')); ?>
<?php echo CHtml::ajaxSubmitButton('Xóa', array('customers/ajaxUpdate', 'act' => 'doDelete'), array('success' => 'reloadGrid', 'beforeSend' => 'function(){
            return confirm("Bạn có chắc chắn muốn xóa những khách hàng được chọn?")
        }',)); ?>
<?php
$cs = Yii::app()->clientScript;
$css = 'ul.yiiPager .first, ul.yiiPager .last {display:inline;}';
$cs->registerCss('show_first_last_buttons', $css);
 $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'customers-grid',
 		'ajaxUpdate'=>false,
	'dataProvider'=>$model->search(),
	'filter'=>$model,
		'pager' => array('maxButtonCount' => 4,'pageSize'=>10,'firstPageLabel'=>'<i class="fa fa-angle-double-left"></i>','lastPageLabel'=>'<i class="fa fa-angle-double-right"></i>'
				,'nextPageLabel' => '<i class="fa fa-angle-right"></i>',
				'prevPageLabel'=> '<i class="fa fa-angle-left"></i>',
				'header'=> '',
				
		),
	'columns'=>array(
			array(
					'id'             => 'autoId',
					'class'          => 'CCheckBoxColumn',
					'selectableRows' => '50',
			),
		'cust_id',
		'cust_email',
		'cust_pass',
		'cust_from',
			array(
					'name'   => 'cust_gender',
					'value'  => '$data->cust_gender==1?"Nam":"Nữ"',
					'filter' => array(1 => 'Nam', 0 => 'Nữ'),
			),
		'cust_fullname',
			array(
					'name'   => 'cust_active',
					'value'  => '$data->cust_active==1?"Có":"Không"',
					'filter' => array(1 => 'Có', 0 => 'Không'),
			),
		'cust_phone',
		
			array(
					'name'   => 'cust_birth',
					'value'  => 'date("d-m-Y",strtotime($data->cust_birth ))',
			),
		array(
			'class'=>'CButtonColumn',
				'template'=>'{view}{delete}',
				'buttons'=>array
				(
				'view' => array(
                'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'View')),
                'label' => '<i class="fa fa-search"></i>',
                'imageUrl' => false,
            ),
            'delete' => array(
                'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'Delete')),
                'label' => '<i class="fa fa-trash-o"></i>',
                'imageUrl' => false,
               )
				),
		),
	),
)); ?>

	<script>
    function reloadGrid(data) {
        $.fn.yiiGridView.update('customers-grid');
    }
</script>

<?php $this->endWidget(); ?>