<?php
/* @var $this CustomersController */
/* @var $model Customers */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'cust_id'); ?>
		<?php echo $form->textField($model,'cust_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cust_email'); ?>
		<?php echo $form->textField($model,'cust_email',array('size'=>60,'maxlength'=>150)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cust_pass'); ?>
		<?php echo $form->textField($model,'cust_pass',array('size'=>60,'maxlength'=>150)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cust_from'); ?>
		<?php echo $form->textField($model,'cust_from',array('size'=>60,'maxlength'=>150)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cust_gender'); ?>
		<?php echo $form->textField($model,'cust_gender'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cust_fullname'); ?>
		<?php echo $form->textField($model,'cust_fullname',array('size'=>60,'maxlength'=>80)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cust_active'); ?>
		<?php echo $form->textField($model,'cust_active'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cust_phone'); ?>
		<?php echo $form->textField($model,'cust_phone'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cust_birth'); ?>
		<?php echo $form->textField($model,'cust_birth'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->