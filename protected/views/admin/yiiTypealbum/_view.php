<?php
/* @var $this YiiTypealbumController */
/* @var $data YiiTypealbum */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_typealbum')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_typealbum), array('view', 'id'=>$data->id_typealbum)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_menu')); ?>:</b>
	<?php echo CHtml::encode($data->id_menu); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name_typealbum')); ?>:</b>
	<?php echo CHtml::encode($data->name_typealbum); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('showhide_typealbum')); ?>:</b>
	<?php echo CHtml::encode($data->showhide_typealbum); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('title_seo_album')); ?>:</b>
	<?php echo CHtml::encode($data->title_seo_album); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description_seo_album')); ?>:</b>
	<?php echo CHtml::encode($data->description_seo_album); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('keywords_seo_album')); ?>:</b>
	<?php echo CHtml::encode($data->keywords_seo_album); ?>
	<br />


</div>