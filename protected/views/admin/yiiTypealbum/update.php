<?php
/* @var $this YiiTypealbumController */
/* @var $model YiiTypealbum */

$this->breadcrumbs=array(
	'Loại Album'=>array('admin'),
	$model->name_typealbum,
	'Sửa',
);

$this->menu=array(
	array('label'=>'Loại Album', 'url'=>array('admin')),
);
?>

<h4>Sửa loại Album <?php echo $model->name_typealbum; ?></h4>

<?php echo $this->renderPartial('_form', array('model'=>$model,'YiiMenu' =>$YiiMenu,'join' =>$join,'ImgAlbums'=>$ImgAlbums)); ?>