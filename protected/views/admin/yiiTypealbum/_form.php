
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'yii-typealbum-form',
	'enableAjaxValidation'=>false,
)); ?>

<?php

		if($model->isNewRecord == true)
		    $imgId = rand().$id;
		else
			$imgId = $join;
		?>
<?php echo $form->hiddenField($model,'join_typeAlbum',array('value'=>$imgId)); ?>
	

	<div class="row">
		<?php echo $form->labelEx($model,'name_typealbum'); ?>
		<?php echo $form->textField($model,'name_typealbum'); ?>
		<?php echo $form->error($model,'name_typealbum'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'title_seo_album'); ?>
		<?php echo $form->textField($model,'title_seo_album',array('size'=>60,'maxlength'=>2000)); ?>
		<?php echo $form->error($model,'title_seo_album'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'description_seo_album'); ?>
		<?php echo $form->textField($model,'description_seo_album',array('size'=>60,'maxlength'=>2000)); ?>
		<?php echo $form->error($model,'description_seo_album'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'keywords_seo_album'); ?>
		<?php echo $form->textField($model,'keywords_seo_album',array('size'=>60,'maxlength'=>2000)); ?>
		<?php echo $form->error($model,'keywords_seo_album'); ?>
	</div>
	
		<div class="row">
		<?php echo $form->labelEx($model,'showhide_typealbum'); ?>
		<?php echo $form->checkBox($model, 'showhide_typealbum'); ?>
		<?php echo $form->error($model,'showhide_typealbum'); ?>
	</div>
	
		<div class="row">
		<?php echo $form->labelEx($model,'highlights_typealbum'); ?>
		<?php echo $form->checkBox($model, 'highlights_typealbum'); ?>
		<?php echo $form->error($model,'highlights_typealbum'); ?>
	</div>
<div id="container">
    <div id="filelist"></div>
    <a id="pickfiles" href="javascript:;">Chọn tệp tin ảnh</a>
    <a id="uploadfiles" href="javascript:;">Tải ảnh</a><span>  (File tải lên phải là file ảnh : jpg ,png ,gif - Dung lượng nhỏ hơn 1M)</span>
</div>
<?php if(!$model->isNewRecord){ ?>
<div id="imgupload">
   <ul>
   <?php foreach ($ImgAlbums as $val){ ?>
     <li>
         <div>
          <a href="<?php echo $val->img_album ?>" class="highslide" onclick="return hs.expand(this)">
			<img src="<?php echo $val->img_album ?>"  width="90" height="90" />
		</a>
         </div>
         <span><a href="<?php echo $this->createUrl('/YiiAlbum/update', array('id' => $val->id_album,'sh' => $val->showhide_album,'join' => $val->join_typeAlbum)) ?>">
         <p id="<?php echo $val->id_album?>" class="showhidess" ><?php echo $val->showhide_album == 1?'<i class="fa fa-check-square"></i>':'<i class="fa fa-minus-square"></i>'?></i></p></a>
         <a href="<?php echo $this->createUrl('/YiiAlbum/delete', array('id' => $val->id_album,'join' => $val->join_typeAlbum)) ?>">
         <p id="<?php echo $val->id_album?>" class="deleteimg"><i class="fa fa-times"></i></p>
         </a></span>
      </li>
      <?php }?>
   </ul>
</div>
<?php } ?>
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Tạo mới' : 'Sửa'); ?>
		
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<script type="text/javascript">
    // Custom example logic
    $(function() {
        
        var uploader = new plupload.Uploader({
            runtimes : 'gears,html5,flash,silverlight,browserplus',
            browse_button : 'pickfiles',
            container : 'container',
            max_file_size : '1mb',           
            url: '<?php echo $this->createUrl('yiiAlbum/upload/join/'.$imgId) ?>',
            /* multipart_params : {'YiiAlbum_typeAlbum': typeAlbums,
            	                // 'showhide' : showhide
                               }, */
            flash_swf_url : '/plupload/js/plupload.flash.swf',
            silverlight_xap_url : '/plupload/js/plupload.silverlight.xap',
            filters : [
                {title : "Image files", extensions : "jpg,gif,png"},
                //{title : "Zip files", extensions : "zip"}
            ],
           // resize : {width : 320, height : 240, quality : 90}
        });
        
        //        uploader.bind('Init', function(up, params) {
        //            $('#filelist').html("<div>Current runtime: " + params.runtime + "</div>");
        //        });

        $('#uploadfiles').click(function(e) {     
          //var YiiTypealbum_id_menu = $('#YiiTypealbum_id_menu').val();
          var YiiTypealbum_name_typealbum = $('#YiiTypealbum_name_typealbum').val();
          if(YiiTypealbum_name_typealbum){
            uploader.start();
            e.preventDefault();
          }else{
              alert('Vui lòng nhập các trường bắt buộc.');
              return false;
          }
        });

        uploader.init();

        uploader.bind('FilesAdded', function(up, files) {
            $.each(files, function(i, file) {
                $('#filelist').append(
                '<div id="' + file.id + '" style="float:left" class="dele">' +
                    file.name + ' (' + plupload.formatSize(file.size) + ') <b></b>' +
                    '</div>');
            });

            up.refresh(); // Reposition Flash/Silverlight
        });

        uploader.bind('UploadProgress', function(up, file) {  
            $('#' + file.id+" b").html(file.percent + "%");
        });

        uploader.bind('Error', function(up, err) {
            $('#filelist').append("<div>"+
                "Thông báo: " + err.message +
                (err.file ? ", File: " + err.file.name : "") +
                "</div>"
        );

            up.refresh(); // Reposition Flash/Silverlight
        });

        uploader.bind('FileUploaded', function(up, file) {
            $('#' + file.id + " b").html("100%");
        });
    	
    });
    
   
</script>