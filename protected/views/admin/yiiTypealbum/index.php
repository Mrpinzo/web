<?php
/* @var $this YiiTypealbumController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Yii Typealbums',
);

$this->menu=array(
	array('label'=>'Create YiiTypealbum', 'url'=>array('create')),
	array('label'=>'Manage YiiTypealbum', 'url'=>array('admin')),
);
?>

<h1>Yii Typealbums</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
