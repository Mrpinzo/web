<?php
/* @var $this YiiTypealbumController */
/* @var $model YiiTypealbum */

$this->breadcrumbs=array(
	'Loại Album'=>array('admin'),
	$model->name_typealbum,
);

$this->menu=array(
	
	array('label'=>'Danh sách Album', 'url'=>array('admin')),
);
?>

<h4>Chi tiết Album <?php echo $model->name_typealbum; ?></h4>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_typealbum',
			array(
					'name'  => 'id_menu',
					'value' =>  $model->YiiMenualbum->name_menu,
					'type'  => 'raw',
			),
		'name_typealbum',
			array(
					'name'  => 'showhide_typealbum',
					'value' =>  $model->showhide_typealbum == 0?'Ẩn':'Hiển thị',
					'type'  => 'raw',
			),
		'title_seo_album',
		'description_seo_album',
		'keywords_seo_album',
	),
)); ?>
