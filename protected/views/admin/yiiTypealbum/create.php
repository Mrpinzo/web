<?php
/* @var $this YiiTypealbumController */
/* @var $model YiiTypealbum */

$this->breadcrumbs=array(
	'Loại Album'=>array('admin'),
	'Tạo mới',
);

$this->menu=array(
	array('label'=>'Loại Album', 'url'=>array('admin')),
);
?>

<h4>Tạo mới Album</h4>

<?php echo $this->renderPartial('_form', array('model'=>$model,'YiiMenu' =>$YiiMenu,'id' => $id)); ?>