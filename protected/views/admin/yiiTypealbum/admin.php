<?php
/* @var $this YiiTypealbumController */
/* @var $model YiiTypealbum */

$this->breadcrumbs=array(
	'Loại Album'=>array('admin'),
	'Danh sách',
);

$this->menu=array(
	array('label'=>'Tạo mới', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#yii-typealbum-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h4>Danh sách loại Album</h4>

<?php
$cs = Yii::app()->clientScript;
$css = 'ul.yiiPager .first, ul.yiiPager .last {display:inline;}';
$cs->registerCss('show_first_last_buttons', $css);

 $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'yii-typealbum-grid',
	'dataProvider'=>$model->search(),
 		'ajaxUpdate'=>false,
	'filter'=>$model,
 		'pager' => array('maxButtonCount' => 4,'pageSize'=>10,'firstPageLabel'=>'<i class="fa fa-angle-double-left"></i>','lastPageLabel'=>'<i class="fa fa-angle-double-right"></i>'
 				,'nextPageLabel' => '<i class="fa fa-angle-right"></i>',
 				'prevPageLabel'=> '<i class="fa fa-angle-left"></i>',
 				'header'=> '',
 					
 		),
	'columns'=>array(
		'id_typealbum',
			/* array(
					'name'   => 'id_menu',
					'value'  => 'isset($data->id_menu)?$data->YiiMenualbum->name_menu:""',
					'filter' => CHtml::activeDropDownList($model, 'id_menu', $YiiMenu, array("empty" => "Chọn danh mục")),
			), */
		'name_typealbum',			
		'title_seo_album',
		'description_seo_album',		
		'keywords_seo_album',
			array(
					'name'   => 'showhide_typealbum',
					'value'  => '$data->showhide_typealbum==1?"<i class=\"fa fa-check-square-o csm\"></i>":"<i class=\"fa fa-times csm\"></i>"',
					'filter' => array(1 => 'Hiện', 0 => 'Ẩn'),
					'type'   => 'raw',
			),
			array(
					'name'   => 'highlights_typealbum',
					'value'  => '$data->highlights_typealbum==1?"<i class=\"fa fa-check-square-o csm\"></i>":"<i class=\"fa fa-times csm\"></i>"',
					'filter' => array(1 => 'Nổi bật', 0 => 'Không nổi bật'),
					'type'   => 'raw',
			),
		array(
			'class'=>'CButtonColumn',
				'header' => 'Hành động',
				'template'=>'{update}{delete}',
				'buttons'=>array
				(
						'update' => array(
								'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'Update')),
								'label' => '<i class="fa fa-pencil-square-o"></i>',
								'imageUrl' => false,
						),
						'delete' => array(
								'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'Delete')),
								'label' => '<i class="fa fa-trash-o"></i>',
								'imageUrl' => false,
						)
				),
		),
	),
)); ?>
