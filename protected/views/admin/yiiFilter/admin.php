<?php
/* @var $this YiiMenuController */
/* @var $model YiiFilter */

$this->breadcrumbs=array(
	'Bộ lọc tìm kiếm'=>array('admin'),
	'Danh sách',
);

$this->menu=array(
	array('label'=>'Tạo mới', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#yii-menu-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<?php

Yii::app()->clientScript->registerScript('productform',"
		 
$('.updatesort').click(function(){
		var idmenu = $(this).attr('id');
		var sortmenu = $('#order_menu_'+idmenu).val();
		var numberRegex = /^[+-]?\d+(\.\d+)?([eE][+-]?\d+)?$/;
		if(numberRegex.test(sortmenu)) {
			$.post('".Yii::app()->createAbsoluteUrl('YiiFilter/UpdateAjax')."', {
						'act': 'comfirm_yes_room',
						'idmenu' : + idmenu,
						'sortmenu' : + sortmenu,
			
						}, function(data){
							if(data == '1'){
			 		              $('#order_menu_'+idmenu).val(sortmenu)
						         alert('Cập nhập thứ tự thành công'); 
			 		         }else
			                     alert('Thứ tự đã tồn tại.');
			                     location.reload();
			             })
			 		
			}else
			     alert('Vui lòng nhập số thứ tự!');
			     return false;
			})
");
?>

<h4>Danh sách menu</h4>
<?php $form = $this->beginWidget('CActiveForm', array(
    'enableAjaxValidation' => TRUE,
)); ?>
<?php echo CHtml::ajaxSubmitButton('Hiện', array('YiiFilter/ajaxUpdate', 'act' => 'doActive'), array('success' => 'reloadGrid')); ?>
<?php echo CHtml::ajaxSubmitButton('Ẩn', array('YiiFilter/ajaxUpdate', 'act' => 'doInactive'), array('success' => 'reloadGrid')); ?>
<?php echo CHtml::ajaxSubmitButton('Xóa', array('YiiFilter/ajaxUpdate', 'act' => 'doDelete'), array('success' => 'reloadGrid', 'beforeSend' => 'function(){
            return confirm("Bạn có chắc chắn muốn xóa những menu được chọn?")
        }',)); ?>
        

<?php
$cs = Yii::app()->clientScript;
$css = 'ul.yiiPager .first, ul.yiiPager .last {display:inline;}';
$cs->registerCss('show_first_last_buttons', $css);
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'menu-grid',
     'ajaxUpdate'=>false,		
	'dataProvider'=>$model->search(),
	'filter'=>$model,
		'pager' => array('maxButtonCount' => 4,'pageSize'=>10,'firstPageLabel'=>'<i class="fa fa-angle-double-left"></i>','lastPageLabel'=>'<i class="fa fa-angle-double-right"></i>'
				,'nextPageLabel' => '<i class="fa fa-angle-right"></i>',
				'prevPageLabel'=> '<i class="fa fa-angle-left"></i>',
				'header'=> '',
		
		),
	'columns'=>array(
			array(
					'id'             => 'autoId',
					'class'          => 'CCheckBoxColumn',
					'selectableRows' => '50',
			),
		'id_menu',

			array(
					'name'   => 'name_menu',
					'value'  => '$data->name_menu',
					//'class'  => '$data->level_menu==0?"main_name":""',
					//'itemOptions'=>array('id' => 'first'),
					
			),
	/* 		array(
					'name'   => 'rewrite_url_menu',
					'value'  => '$data->rewrite_url_menu',						
			),
			array(
					'name'   => 'url_menu',
					'value'  => '$data->url_menu',
			), */
			array(
					'name'   => 'showhide_menu',
					'value'  => '$data->showhide_menu==1?"<i class=\'fa fa-check-square-o csm\'></i>":"<i class=\'fa fa-times csm\'></i>"',
					'filter' => array(1 => 'Hiện', 0 => 'Ẩn'),
					'type'  => 'raw'
			),

			array(
					'name'   => 'order_menu',
					'value'=>function($data){
                          return '<input type="Text" class="orders" id="order_menu_'.$data->id_menu.'" value="'.$data->order_menu.'" /><span class="updatesort" id="'.$data->id_menu.'"><i class="fa fa-floppy-o"></i></span>';
                           },
					'type'  =>  'raw',
					
			),
		array(
			'class'=>'CButtonColumn',
				'header' => 'Hành động',
				'template'=>'{update}{delete}',
				'buttons'=>array
				(

						'update' => array(
								'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'Update')),
								'label' => '<i class="fa fa-pencil-square-o"></i>',
								'imageUrl' => false,
						),
						'delete' => array(
								'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'Delete')),
								'label' => '<i class="fa fa-trash-o"></i>',
								'imageUrl' => false,
						)
				),
		),
	),
)); ?>
<script>
    function reloadGrid(data) {
        $.fn.yiiGridView.update('menu-grid');
    }
</script>

<?php $this->endWidget(); ?>