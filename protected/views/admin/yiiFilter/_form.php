<?php
/* @var $this YiiMenuController */
/* @var $model YiiMenu */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'yii-menu-form',
	'enableAjaxValidation'=>false,
)); ?>
<div class="row">
		<?php echo $form->labelEx($model,'level_menu'); ?>
		<?php echo $form->dropDownList($model,'level_menu', $YiiMenue, array("empty" => "---Chọn Lọc cha---")) ?>
		<?php echo $form->error($model,'level_menu'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'name_menu'); ?>
		<?php echo $form->textField($model,'name_menu',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'name_menu'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'order_menu'); ?>
		<?php echo $form->textField($model,'order_menu',array('size'=>60,'maxlength'=>500)); ?>
		<?php echo $form->error($model,'order_menu'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'title_seo_menu'); ?>
		<?php echo $form->textField($model,'title_seo_menu',array('size'=>60,'maxlength'=>500)); ?>
		<?php echo $form->error($model,'title_seo_menu'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'description_seo_menu'); ?>
		<?php echo $form->textField($model,'description_seo_menu',array('size'=>60,'maxlength'=>500)); ?>
		<?php echo $form->error($model,'description_seo_menu'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'keywords_seo'); ?>
		<?php echo $form->textField($model,'keywords_seo',array('size'=>60,'maxlength'=>500)); ?>
		<?php echo $form->error($model,'keywords_seo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'showhide_menu'); ?>
		<?php echo $form->checkBox($model, 'showhide_menu'); ?>
		<?php echo $form->error($model,'showhide_menu'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Tạo mới' : 'Sửa'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->