<?php
/* @var $this AdministratorController */
/* @var $model Administrator */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'ad_id'); ?>
		<?php echo $form->textField($model,'ad_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'username'); ?>
		<?php echo $form->textField($model,'username',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ad_fullname'); ?>
		<?php echo $form->textField($model,'ad_fullname',array('size'=>60,'maxlength'=>70)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ad_phone'); ?>
		<?php echo $form->textField($model,'ad_phone'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ad_email'); ?>
		<?php echo $form->textField($model,'ad_email',array('size'=>60,'maxlength'=>70)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Tìm Kiếm'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->