<?php
/* @var $this AdministratorController */
/* @var $model Administrator */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'administrator-form',
	'enableAjaxValidation'=>false,
)); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'username'); ?>
		<?php echo $form->textField($model,'username',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ad_fullname'); ?>
		<?php echo $form->textField($model,'ad_fullname',array('size'=>60,'maxlength'=>70)); ?>
		<?php echo $form->error($model,'ad_fullname'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ad_phone'); ?>
		<?php echo $form->textField($model,'ad_phone',array('size'=>60,'maxlength'=>12)); ?>
		<?php echo $form->error($model,'ad_phone'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ad_email'); ?>
		<?php echo $form->textField($model,'ad_email',array('size'=>60,'maxlength'=>70)); ?>
		<?php echo $form->error($model,'ad_email'); ?>
	</div>
		
	<div class="row">
		<?php echo $form->labelEx($model,'id_role'); ?>
		<?php echo $form->dropDownList($model, 'id_role', CHtml::listData(YiiRole::model()->findAll(), 'id_role', 'name_role'), array("empty" => "Chọn loại quyền")) ?>
		<?php echo $form->error($model,'id_role'); ?>
	</div>
		
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Tạo mới' : 'Sửa'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->