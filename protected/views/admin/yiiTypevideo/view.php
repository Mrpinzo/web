<?php
/* @var $this YiiTypevideoController */
/* @var $model YiiTypevideo */

$this->breadcrumbs=array(
	'Yii Typevideos'=>array('index'),
	$model->id_typevideo,
);

$this->menu=array(
	array('label'=>'List YiiTypevideo', 'url'=>array('index')),
	array('label'=>'Create YiiTypevideo', 'url'=>array('create')),
	array('label'=>'Update YiiTypevideo', 'url'=>array('update', 'id'=>$model->id_typevideo)),
	array('label'=>'Delete YiiTypevideo', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_typevideo),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage YiiTypevideo', 'url'=>array('admin')),
);
?>

<h1>View YiiTypevideo #<?php echo $model->id_typevideo; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_typevideo',
		'name__typevideo',
		'showhide__typevideo',
		'url_typevideo',
		'title_typevideo',
		'keywords_typevideo',
		'description_typevideo',
	),
)); ?>
