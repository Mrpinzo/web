<?php
/* @var $this YiiTypevideoController */
/* @var $data YiiTypevideo */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_typevideo')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_typevideo), array('view', 'id'=>$data->id_typevideo)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name__typevideo')); ?>:</b>
	<?php echo CHtml::encode($data->name__typevideo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('showhide__typevideo')); ?>:</b>
	<?php echo CHtml::encode($data->showhide__typevideo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('url_typevideo')); ?>:</b>
	<?php echo CHtml::encode($data->url_typevideo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('title_typevideo')); ?>:</b>
	<?php echo CHtml::encode($data->title_typevideo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('keywords_typevideo')); ?>:</b>
	<?php echo CHtml::encode($data->keywords_typevideo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description_typevideo')); ?>:</b>
	<?php echo CHtml::encode($data->description_typevideo); ?>
	<br />


</div>