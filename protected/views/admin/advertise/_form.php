<?php
/* @var $this AdvertiseController */
/* @var $model Advertise */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'advertise-form',
	'enableAjaxValidation'=>false,
		'htmlOptions'          => array(
				'enctype' => 'multipart/form-data',
		),
)); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'adv_link'); ?>
		<?php echo $form->textField($model,'adv_link',array('size'=>60,'maxlength'=>2000)); ?>
		<?php echo $form->error($model,'adv_link'); ?>
	</div>
  
    <div class="row">
		<?php echo $form->labelEx($model,'adv_name_en'); ?>		
		<?php echo CHtml::activeTextArea($model,'adv_name_en',array('rows'=>10, 'cols'=>40)); ?>
		<?php echo $form->error($model,'adv_name_en'); ?>
	</div>
    <div class="row">
		<?php echo $form->labelEx($model,'adv_name_fr'); ?>		
		<?php echo CHtml::activeTextArea($model,'adv_name_fr',array('rows'=>10, 'cols'=>40)); ?>
		<?php echo $form->error($model,'adv_name_fr'); ?>
	</div>
    <div class="row">
		<?php echo $form->labelEx($model,'adv_name_sp'); ?>		
		<?php echo CHtml::activeTextArea($model,'adv_name_sp',array('rows'=>10, 'cols'=>40)); ?>
		<?php echo $form->error($model,'adv_name_sp'); ?>
	</div>
    
	<div class="row">
		<?php echo $form->labelEx($model,'adv_img'); ?>
		<?php echo $form->fileField($model, 'adv_img'); ?>
		<?php echo !$model->isNewRecord ? '<img src="'.$model->adv_img .'" width="50" height="50"/>':'' ?>
		<?php echo $form->error($model,'adv_img'); ?>
	</div>

	
	<div class="row">
		<?php echo $form->labelEx($model,'adv_positions'); ?>
		<?php echo $form->dropDownList($model, 'adv_positions', array('2' => 'Slide',), array("empty" => "---Chọn vị trí---")) ?>

		<?php echo $form->error($model,'adv_positions'); ?>
	</div>
<div class="row">
		<?php echo $form->labelEx($model,'adv_action'); ?>
		<?php echo $form->checkBox($model, 'adv_action'); ?>
		
		<?php echo $form->error($model,'adv_action'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'adv_order'); ?>
		<?php echo $form->textField($model,'adv_order',array('size'=>60,'maxlength'=>2000)); ?>
		<?php echo $form->error($model,'adv_order'); ?>
	</div>
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Tạo mới' : 'Sửa'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->