<?php
/* @var $this AdvertiseController */
/* @var $model Advertise */

$this->breadcrumbs=array(
	'Quảng cáo'=>array('admin'),
	'Tạo mới quảng cáo',
);

$this->menu=array(
	array('label'=>'Danh sách quảng cáo', 'url'=>array('admin')),
);
?>

<h4>Tạo quảng cáo</h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>