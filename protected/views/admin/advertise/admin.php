<?php
/* @var $this AdvertiseController */
/* @var $model Advertise */

$this->breadcrumbs=array(
	'Quảng cáo'=>array('admin'),
	'Danh sách',
);

$this->menu=array(
	array('label'=>'Thêm mới', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#advertise-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h4>Danh sách quảng cáo</h4>
<?php $form = $this->beginWidget('CActiveForm', array(
    'enableAjaxValidation' => TRUE,
)); ?>
<input id="yt10" type="submit" value="Kích hoạt" name="yt10" style="display:none;" class="search-button">
<?php echo CHtml::ajaxSubmitButton('Hiện', array('Advertise/ajaxUpdate', 'act' => 'doActive'), array('success' => 'reloadGrid')); ?>
<?php echo CHtml::ajaxSubmitButton('Ẩn', array('Advertise/ajaxUpdate', 'act' => 'doInactive'), array('success' => 'reloadGrid')); ?>
<?php echo CHtml::ajaxSubmitButton('Xóa', array('Advertise/ajaxUpdate', 'act' => 'doDelete'), array('success' => 'reloadGrid', 'beforeSend' => 'function(){
            return confirm("Bạn có chắc chắn muốn xóa những quảng cáo được chọn?")
        }',)); ?>
<?php
$cs = Yii::app()->clientScript;
$css = 'ul.yiiPager .first, ul.yiiPager .last {display:inline;}';
$cs->registerCss('show_first_last_buttons', $css);
 $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'advertise-grid',
 		'ajaxUpdate'=>false,
	'dataProvider'=>$model->search(),
	'filter'=>$model,
 		'pager' => array('maxButtonCount' => 4,'pageSize'=>10,'firstPageLabel'=>'<i class="fa fa-angle-double-left"></i>','lastPageLabel'=>'<i class="fa fa-angle-double-right"></i>'
 				,'nextPageLabel' => '<i class="fa fa-angle-right"></i>',
 				'prevPageLabel'=> '<i class="fa fa-angle-left"></i>',
 				'header'=> '',
 				
 		),
	'columns'=>array(
			array(
					'id'             => 'autoId',
					'class'          => 'CCheckBoxColumn',
					'selectableRows' => '50',
			),
		'adv_id',
		
			array(
					'name'=>'adv_link',
					'type'=>'raw',
					'value'=>'CHtml::link(CHtml::encode($data->adv_link), $data->adv_link, array("target"=>"_blank"))',
					
			),
				
			array(
					'name'   => 'adv_img',
					'value'  => '"<a href=\"".Yii::app()->request->baseUrl.$data->adv_img."\" class=\"highslide\" onclick=\"return hs.expand(this)\"><img src=\"".Yii::app()->request->baseUrl.$data->adv_img."\" width=\"50\" height=\"50\"/></a>"',
					'type'   => 'raw',
					'filter' => FALSE,
					'sortable'   => FALSE,
			),
			array(
					'name'   => 'adv_action',
					'value'  => '$data->adv_action==1?"<i class=\"fa fa-check-square-o csm\"></i>":"<i class=\"fa fa-times csm\"></i>"',
					'filter' => array(1 => 'Hiện', 0 => 'Ẩn'),
					'type'   => 'raw',
			),
			'adv_order',
			array(
					'name'   => 'adv_positions',					
					'value'  =>function($data){
						if ($data->adv_positions==2)
							return 'Slide';
					
					},
					'filter' => array(1 => 'Left', 2 => 'Slide'),
			),
		array(
			'class'=>'CButtonColumn',
				'header' => 'Hành động',
				'buttons'=>array
				(
						'view' => array(
								'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'View')),
								'label' => '<i class="fa fa-search"></i>',
								'imageUrl' => false,
						),
						'update' => array(
								'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'Update')),
								'label' => '<i class="fa fa-pencil-square-o"></i>',
								'imageUrl' => false,
						),
						'delete' => array(
								'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'Delete')),
								'label' => '<i class="fa fa-trash-o"></i>',
								'imageUrl' => false,
						)
				),
		),
	),
)); ?>
<script>
    function reloadGrid(data) {
        $.fn.yiiGridView.update('advertise-grid');
    }
</script>
<?php $this->endWidget(); ?>