<?php
/* @var $this YiiAlbumController */
/* @var $model YiiAlbum */
/* @var $form CActiveForm */
?>
 <?php
    $imgId = rand();
    ?>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'yii-album-form',
	'enableAjaxValidation'=>false,
		'htmlOptions'          => array(
				'enctype' => 'multipart/form-data',
		),
)); ?>
<?php echo $form->hiddenField($model,'typeAlbum',array('value'=>'')); ?>
	<div class="row">
		<?php echo $form->labelEx($model,'id_typealbum'); ?>
		<span style="color: red">* </span><?php echo $form->dropDownList($model, 'id_typealbum',CHtml::listData(YiiTypealbum::model()->findAll(), "id_typealbum", "name_typealbum"), array("empty" => "Chọn loại tin tức")) ?>
		<div style="color:red" id="erralbum"></div>
	</div>
<div id="container">
    <div id="filelist"></div>
    <a id="pickfiles" href="javascript:;">[Select files]</a>
    <a id="uploadfiles" href="javascript:;">[Upload files]</a>
</div>
	<div class="row">
		<?php echo $form->labelEx($model,'showhide_album',array('for' => 'showhide_album')); ?>
        <input id="showhide_album" type="checkbox" name="showhide_album" disabled>
		<?php echo $form->error($model,'showhide_album'); ?>
	</div>


<?php $this->endWidget(); ?>

</div><!-- form -->
<script type="text/javascript">
    // Custom example logic
    $(function() {
    	//var typeAlbums ='';
    	$('#YiiAlbum_id_typealbum').change(function(){

    		var typeAlbums = $('#YiiAlbum_id_typealbum').val();
		/* if($('#showhide_album').prop( 'checked' ) == true)
			var showhide = 1;
		else
			var showhide = 0; */
    	});
    	var YiiAlbum_typeAlbum = $('#YiiAlbum_typeAlbum').val();
        
        var uploader = new plupload.Uploader({
            runtimes : 'gears,html5,flash,silverlight,browserplus',
            browse_button : 'pickfiles',
            container : 'container',
            max_file_size : '10mb',           
            url: '<?php echo $this->createUrl('yiiAlbum/upload') ?>',
            multipart_params : {'YiiAlbum_typeAlbum': typeAlbums,
            	                // 'showhide' : showhide
                               },
            flash_swf_url : '/plupload/js/plupload.flash.swf',
            silverlight_xap_url : '/plupload/js/plupload.silverlight.xap',
            filters : [
                {title : "Image files", extensions : "jpg,gif,png"},
                {title : "Zip files", extensions : "zip"}
            ],
            resize : {width : 320, height : 240, quality : 90}
        });
        
        //        uploader.bind('Init', function(up, params) {
        //            $('#filelist').html("<div>Current runtime: " + params.runtime + "</div>");
        //        });

        $('#uploadfiles').click(function(e) {     

        	if($('#YiiAlbum_id_typealbum').val()){
            uploader.start();
            e.preventDefault();
        	}else{
            	$('#erralbum').text('Vui lòng nhập loại Album.');
             // alert('Vui lòng nhập loại Album.');
              return false;
        	}
        });

        uploader.init();

        uploader.bind('FilesAdded', function(up, files) {
            $.each(files, function(i, file) {
                $('#filelist').append(
                '<div id="' + file.id + '" style="float:left" class="dele">' +
                    file.name + ' (' + plupload.formatSize(file.size) + ') <b></b>' +
                    '</div>');
            });

            up.refresh(); // Reposition Flash/Silverlight
        });

        uploader.bind('UploadProgress', function(up, file) {  
            $('#' + file.id+" b").html(file.percent + "%");
        });

        uploader.bind('Error', function(up, err) {
            $('#filelist').append("<div>Error: " + err.code +
                ", Message: " + err.message +
                (err.file ? ", File: " + err.file.name : "") +
                "</div>"
        );

            up.refresh(); // Reposition Flash/Silverlight
        });

        uploader.bind('FileUploaded', function(up, file) {
            $('#' + file.id + " b").html("100%");
        });
    	
    });
    
   
</script>