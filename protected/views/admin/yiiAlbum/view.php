<?php
/* @var $this YiiAlbumController */
/* @var $model YiiAlbum */

$this->breadcrumbs=array(
		'Albums'=>array('admin'),
	    $model->id_album,
);

$this->menu=array(
	
	array('label'=>'Danh sách Album', 'url'=>array('admin')),
);
?>

<h4>Chi tiết <?php echo $model->id_album; ?></h4>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_album',
			array(
					'name'  => 'id_typealbum',
					'value' =>  $model->Albums->name_typealbum,
					'type'  => 'raw',
			),
		'img_album',

			array(
					'name'  => 'showhide_album',
					'value' =>  $model->showhide_album == 0?'Ẩn':'Hiển thị',
					'type'  => 'raw',
			),
	),
)); ?>
