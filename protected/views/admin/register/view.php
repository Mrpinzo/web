<?php
/* @var $this RegisterController */
/* @var $model Register */

$this->breadcrumbs=array(
	'Register'=>array('admin'),
	$model->title_register,
);

$this->menu=array(
	
	array('label'=>'Liên hệ', 'url'=>array('admin')),
);
?>

<h4>Chi tiết liên hệ: <?php echo $model->title_register; ?></h4>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_register',
        'namexh_register',
		'fullnam_register',
        'lastnam_register',
        'namenew_register',
		'company_register',
		'email_register',
	   	'address_register',
		'title_register',
		'country_register',
        'areacode_register',
        'phone_register',
        
        'yesmail_register',
        'companyaddress_register',
        'postcode_register',
        'viplabel_register',
        'bannerad_register',
        'topay_register',
			array(
					'name'   => 'dates',
					'value'  => date("H:i:s d-m-Y",strtotime($model->dates)),
			),
	),
)); ?>
