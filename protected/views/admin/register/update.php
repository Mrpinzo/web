<?php
/* @var $this RegisterController */
/* @var $model Register */

$this->breadcrumbs=array(
	'Register'=>array('index'),
	$model->id_register=>array('view','id'=>$model->id_register),
	'Update',
);

$this->menu=array(
	array('label'=>'List Register', 'url'=>array('index')),
	array('label'=>'Create Register', 'url'=>array('create')),
	array('label'=>'View Register', 'url'=>array('view', 'id'=>$model->id_register)),
	array('label'=>'Manage Register', 'url'=>array('admin')),
);
?>

<h1>Update Register <?php echo $model->id_register; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>