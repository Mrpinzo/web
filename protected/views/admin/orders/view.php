<?php
/* @var $this OrdersController */
/* @var $model Orders */

$this->breadcrumbs=array(
	'Danh sách khách hàng'=>array('admin'),
	$model->ord_name,
);

$this->menu=array(
	array('label'=>'Danh sách khách hàng', 'url'=>array('admin')),
);
?>

<h4>Chi tiết khách hàng <?php echo $model->ord_name; ?></h4>
<input type="hidden" name="id_order" id="id_order" value="<?php echo $model->ord_id; ?>" />
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ord_id',			
		'ord_email',
		array(
					'name'   => 'ord_Gender',
					'value'  => $model->ord_Gender==1?"Nam":"Nữ",
			),
		
	
			array(
					'name'   => 'ord_Dateofbirth',
					'value'  => !empty($model->ord_Dateofbirth)?date('d-m-Y',strtotime($model->ord_Dateofbirth )):'',
			),
	
		'ord_name',
		'ord_from',
		'ord_phone',
			
			array(
					'name'   => 'ord_date',
					'value'  => date('d-m-Y',strtotime($model->ord_date )),
			),
		array(
					'name'   => 'ord_status',
					//'value'  => $model->ord_status==1?"<span class=\"hoanthanh\">Hoàn thành</span>":"<span class=\"dangcho\">Đang chờ</span>",
				   	'value' => CHtml::activeDropDownList($model, 'ord_status', array('0'=>'Đang chờ','1'=>'Hoàn thành')),
				'type' => 'raw'
			),
			
			
			'ord_request'
	),
)); ?>
<div id="detail_order">Chi tiết đơn hàng : </div>
  <table id="hh">
  <tr>
    <th class="title_order">Tên</th>
    <th class="title_order">Ảnh</th>
    <th class="title_order">Số lượng</th>
     <th class="title_order">Gía sản phẩm</th>
      <th class="title_order">Tổng</th>
      
  </tr>
  <?php
   foreach ($cart as $val){ ?>
  <tr>
    <td><?php echo $val['pr_name'] ?></td>
    <?php 
                               $criteria2g   = new CDbCriteria();
                                   $criteria2g->condition = 'showhide_yii_imgproduct = 1 AND pr_id="'.$val['pr_joinImg'].'"';
                                   $criteria2g->group = 'pr_id';
                                   $YiiImgproduct = YiiImgproduct::model()->findAll($criteria2g);
                                   
                                   $count = YiiImgproduct::model()->count($criteria2g);
                                   
                                   if($count > 0){
                                   	 foreach ($YiiImgproduct as $valk){
    ?>
   <td><img src="<?php echo Yii::app()->request->baseUrl.$valk->url_yii_imgproduct ?>" width="90" height="90"/></td>
   <?php }?>
     <?php }else{?>
     <td><img src="<?php echo Yii::app()->request->baseUrl ?>/images/home/images/nothumb.png" width="90" height="90"/></td>
     <?php }?>
    <td><?php echo $val['cart_number'] ?></td>
   <td><?php echo number_format($val['pr_gia'],0,"",".").' đ' ?></td>
   <td><?php echo number_format($val['car_summoney'],0,"",".").' đ' ?></td>
  </tr>
  <?php }?>
</table>
<div id="sac"><span id="ttien">Tổng thành tiền :</span> 
<?php 
      $i = 0;
      foreach ($cart as $val){
         $i = $i + $val['car_summoney'];
       }
       echo number_format($i,0,"",".").' đ';
       ?>
</div>
      

