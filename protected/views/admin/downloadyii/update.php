<?php
/* @var $this DownloadyiiController */
/* @var $model Downloadyii */

$this->breadcrumbs=array(
	'Downloadyii'=>array('index'),
	$model->id_downloadyii=>array('view','id'=>$model->id_downloadyii),
	'Update',
);

$this->menu=array(
	array('label'=>'List Downloadyii', 'url'=>array('index')),
	array('label'=>'Create Downloadyii', 'url'=>array('create')),
	array('label'=>'View Downloadyii', 'url'=>array('view', 'id'=>$model->id_downloadyii)),
	array('label'=>'Manage Downloadyii', 'url'=>array('admin')),
);
?>

<h1>Update download <?php echo $model->id_downloadyii; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>