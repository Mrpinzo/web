<?php
/* @var $this DownloadyiiController */
/* @var $data Downloadyii */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_downloadyii')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_downloadyii), array('view', 'id'=>$data->id_downloadyii)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fullnam_downloadyii')); ?>:</b>
	<?php echo CHtml::encode($data->fullnam_downloadyii); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('company_downloadyii')); ?>:</b>
	<?php echo CHtml::encode($data->company_downloadyii); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('phone_downloadyii')); ?>:</b>
	<?php echo CHtml::encode($data->phone_downloadyii); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email_downloadyii')); ?>:</b>
	<?php echo CHtml::encode($data->email_downloadyii); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('address_downloadyii')); ?>:</b>
	<?php echo CHtml::encode($data->address_downloadyii); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('title_downloadyii')); ?>:</b>
	<?php echo CHtml::encode($data->title_downloadyii); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('content_downloadyii')); ?>:</b>
	<?php echo CHtml::encode($data->content_downloadyii); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mobile_downloadyii')); ?>:</b>
	<?php echo CHtml::encode($data->mobile_downloadyii); ?>
	<br />

	*/ ?>

</div>