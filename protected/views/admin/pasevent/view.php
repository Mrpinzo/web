<?php
/* @var $this ProductController */
/* @var $model Product */

$this->breadcrumbs=array(
	'Sản phẩm'=>array('admin'),
	$model->pr_name,
);

$this->menu=array(
	array('label'=>'Danh sách sản phẩm', 'url'=>array('admin')),
);
?>

<h4>Chi tiết <?php echo $model->pr_name; ?></h4>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'pr_id',
			array(
					'name'  => 'id_menu',
					'value' =>  $model->YiiMenus->name_menu_en,
					'type'  => 'raw',
			),
		'pr_name',
			
			array(
					'name'  => 'pr_img',
					'value' => '<img src="'.Yii::app()->request->baseUrl . $model->pr_img.'" width="100" height="100px">',
					'type'  => 'raw',
			),
			array(
					'name'  => 'pr_summary',
					'value' =>  $model->pr_summary,
					'type'  => 'raw',
			),
			array(
					'name'  => 'pr_mota',
					'value' =>  $model->pr_mota,
					'type'  => 'raw',
			),
			array(
					'name'  => 'pr_gia',
					'value' =>  number_format($model->pr_gia) .'đ',
					'type'  => 'raw',
			),
		
			'pr_title_seo',
			'pr_description_seo',
			'pr_keywords_seo',
			array(
					'name'  => 'pr_highlights',
					'value' => $model->highlight($model->pr_highlights),                       
					'type' => 'raw',							
			),
			array(
					'name'  => 'pr_showhide',
					'value' =>  $model->pr_showhide == 0?'Ẩn':'Hiển thị',
					'type'  => 'raw',
			),
			'pr_url_news',
			'pr_width',
			'pr_height',
			'pr_code',
			'pr_madein',
			'pr_number',
			'pr_Manufacturer',
	),
)); ?>
