<?php
/* @var $this NewsController */
/* @var $model News */
/* @var $form CActiveForm */
Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/css/ckeditor/ckeditor.js');
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'news-form',
	'enableAjaxValidation'=>false,
		'htmlOptions'          => array(
				'enctype' => 'multipart/form-data',
		),
)); ?>
<div class="row">
		<?php echo $form->labelEx($model,'id_menu'); ?>
		<?php echo $form->dropDownList($model, 'id_menu',$YiiMenu, array("empty" => "Chọn loại tin tức")) ?>
		<?php echo $form->error($model,'id_menu'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'new_title_en'); ?>
		<?php echo $form->textField($model,'new_title_en',array('size'=>60,'maxlength'=>400)); ?>
		<?php echo $form->error($model,'new_title_en'); ?>
	</div>
    <div class="row">
		<?php echo $form->labelEx($model,'new_title_fr'); ?>
		<?php echo $form->textField($model,'new_title_fr',array('size'=>60,'maxlength'=>400)); ?>
		<?php echo $form->error($model,'new_title_fr'); ?>
	</div>
    <div class="row">
		<?php echo $form->labelEx($model,'new_title_sp'); ?>
		<?php echo $form->textField($model,'new_title_sp',array('size'=>60,'maxlength'=>400)); ?>
		<?php echo $form->error($model,'new_title_sp'); ?>
	</div>

		
	<div class="row">
		<?php echo $form->labelEx($model,'new_img'); ?>
		<?php echo $form->fileField($model, 'new_img'); ?><span id="imgs">Tập tin ảnh có định dạng (jpg, gif, png)</span>
		<?php echo !$model->isNewRecord ? '<img src="'.$model->new_img .'" width="50" height="50"/>':'' ?>
		<?php echo $form->error($model,'new_img'); ?>
	</div>
    
    <div class="row">
		<?php echo $form->labelEx($model,'img_file'); ?>
		<?php echo $form->fileField($model, 'img_file'); ?><span id="imgs">Tập tin download</span>
		<?php echo $form->error($model,'img_file'); ?>
	</div>
    <div class="row">
		<?php echo $form->labelEx($model,'imgfile'); ?>
		<?php echo $form->fileField($model, 'imgfile'); ?><span id="imgs">Tập tin ảnh có định dạng (jpg, gif, png)</span>
		<?php echo !$model->isNewRecord ? '<img src="'.$model->imgfile .'" width="50" height="50"/>':'' ?>
		<?php echo $form->error($model,'imgfile'); ?>
	</div>
    <!------------------>
    <div class="row">
		<?php echo $form->labelEx($model,'yii_date'); ?>
		<?php echo $form->textField($model,'yii_date',array('size'=>60,'maxlength'=>400)); ?>
		<?php echo $form->error($model,'yii_date'); ?>
	</div>
    <div class="row">
		<?php echo $form->labelEx($model,'yii_year'); ?>
		<?php echo $form->textField($model,'yii_year',array('size'=>60,'maxlength'=>400)); ?>
		<?php echo $form->error($model,'yii_year'); ?>
	</div>
    <div class="row">
		<?php echo $form->labelEx($model,'yii_address_en'); ?>
		<?php echo $form->textField($model,'yii_address_en',array('size'=>60,'maxlength'=>400)); ?>
		<?php echo $form->error($model,'yii_address_en'); ?>
	</div>
     <div class="row">
		<?php echo $form->labelEx($model,'yii_address_fr'); ?>
		<?php echo $form->textField($model,'yii_address_fr',array('size'=>60,'maxlength'=>400)); ?>
		<?php echo $form->error($model,'yii_address_fr'); ?>
	</div>
     <div class="row">
		<?php echo $form->labelEx($model,'yii_address_sp'); ?>
		<?php echo $form->textField($model,'yii_address_sp',array('size'=>60,'maxlength'=>400)); ?>
		<?php echo $form->error($model,'yii_address_sp'); ?>
	</div>
  
    <div class="row">
		<?php echo $form->labelEx($model,'yii_overview_en'); ?>
		<?php echo $form->textArea($model,'yii_overview_en',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'yii_overview_en'); ?>
	</div>
    <div class="row">
		<?php echo $form->labelEx($model,'yii_overview_fr'); ?>
		<?php echo $form->textArea($model,'yii_overview_fr',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'yii_overview_fr'); ?>
	</div>
    <div class="row">
		<?php echo $form->labelEx($model,'yii_overview_sp'); ?>
		<?php echo $form->textArea($model,'yii_overview_sp',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'yii_overview_sp'); ?>
	</div>
    
    
    <div class="row">
		<?php echo $form->labelEx($model,'yii_content_en'); ?>
		<?php echo $form->textArea($model,'yii_content_en',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'yii_content_en'); ?>
	</div>
    <div class="row">
		<?php echo $form->labelEx($model,'yii_content_fr'); ?>
		<?php echo $form->textArea($model,'yii_content_fr',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'yii_content_fr'); ?>
	</div>
    <div class="row">
		<?php echo $form->labelEx($model,'yii_content_sp'); ?>
		<?php echo $form->textArea($model,'yii_content_sp',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'yii_content_sp'); ?>
	</div>
    
    
   <div class="row">
		<?php echo $form->labelEx($model,'yii_wsattend_en'); ?>
		<?php echo $form->textArea($model,'yii_wsattend_en',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'yii_wsattend_en'); ?>
	</div>
   <div class="row">
		<?php echo $form->labelEx($model,'yii_wsattend_fr'); ?>
		<?php echo $form->textArea($model,'yii_wsattend_fr',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'yii_wsattend_fr'); ?>
	</div>
     <div class="row">
		<?php echo $form->labelEx($model,'yii_wsattend_sp'); ?>
		<?php echo $form->textArea($model,'yii_wsattend_sp',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'yii_wsattend_sp'); ?>
	</div>
    
    <div class="row">
		<?php echo $form->labelEx($model,'yii_attending_en'); ?>
		<?php echo $form->textArea($model,'yii_attending_en',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'yii_attending_en'); ?>
	</div>
     <div class="row">
		<?php echo $form->labelEx($model,'yii_attending_fr'); ?>
		<?php echo $form->textArea($model,'yii_attending_fr',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'yii_attending_fr'); ?>
	</div>
     <div class="row">
		<?php echo $form->labelEx($model,'yii_attending_sp'); ?>
		<?php echo $form->textArea($model,'yii_attending_sp',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'yii_attending_sp'); ?>
	</div>
    
    <div class="row">
		<?php echo $form->labelEx($model,'yii_director_en'); ?>
		<?php echo $form->textArea($model,'yii_director_en',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'yii_director_en'); ?>
	</div>
    <div class="row">
		<?php echo $form->labelEx($model,'yii_director_fr'); ?>
		<?php echo $form->textArea($model,'yii_director_fr',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'yii_director_fr'); ?>
	</div>
    <div class="row">
		<?php echo $form->labelEx($model,'yii_director_sp'); ?>
		<?php echo $form->textArea($model,'yii_director_sp',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'yii_director_sp'); ?>
	</div>
    <!------------------>
	<div class="row">
		<?php echo $form->labelEx($model,'new_summary_en'); ?>
		<?php echo $form->textArea($model,'new_summary_en',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'new_summary_en'); ?>
	</div>
    <div class="row">
		<?php echo $form->labelEx($model,'new_summary_fr'); ?>
		<?php echo $form->textArea($model,'new_summary_fr',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'new_summary_fr'); ?>
	</div>
    	<div class="row">
		<?php echo $form->labelEx($model,'new_summary_sp'); ?>
		<?php echo $form->textArea($model,'new_summary_sp',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'new_summary_sp'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'new_content_en'); ?>
		<?php echo $form->textArea($model,'new_content_en',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'new_content_en'); ?>
	</div>
    <div class="row">
		<?php echo $form->labelEx($model,'new_content_fr'); ?>
		<?php echo $form->textArea($model,'new_content_fr',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'new_content_fr'); ?>
	</div>
    <div class="row">
		<?php echo $form->labelEx($model,'new_content_sp'); ?>
		<?php echo $form->textArea($model,'new_content_sp',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'new_content_sp'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'new_title_seo'); ?>
		<?php echo $form->textField($model,'new_title_seo',array('size'=>60,'maxlength'=>400)); ?>
				<?php echo $form->error($model,'new_title_seo'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'new_keywords_seo'); ?>
		<?php echo $form->textField($model,'new_keywords_seo',array('size'=>60,'maxlength'=>400)); ?><span style="font-size: 12px;padding-left:5px">Các từ khóa cách nhau bởi dấu phẩy ','</span>
				<?php echo $form->error($model,'new_keywords_seo'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'new_description_seo'); ?>
		<?php echo $form->textArea($model,'new_description_seo',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'new_description_seo'); ?>
	</div>
	
	
	<div class="row">
		<?php echo $form->labelEx($model,'typedetailnews'); ?>
		<?php echo $form->checkBox($model, 'typedetailnews',array('id'=>'typel')); ?>
		<?php echo $form->error($model,'typedetailnews'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'contentnewsd_sp'); ?>
		<?php echo $form->textArea($model,'contentnewsd_sp',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'contentnewsd_sp'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'contentnewsd_fr'); ?>
		<?php echo $form->textArea($model,'contentnewsd_fr',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'contentnewsd_fr'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'contentnewsd_en'); ?>
		<?php echo $form->textArea($model,'contentnewsd_en',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'contentnewsd_en'); ?>
	</div>
	
	<div class="row" style="display: none;">
		<?php echo $form->labelEx($model,'new_highlights'); ?>
		<?php echo $form->checkBox($model, 'new_highlights'); ?>
		<?php echo $form->error($model,'new_highlights'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'new_showhide'); ?>
		<?php echo $form->checkBox($model, 'new_showhide'); ?>
		<?php echo $form->error($model,'new_showhide'); ?>
	</div>
    
    <div class="row">
		<?php echo $form->labelEx($model,'new_highlights'); ?>
		<?php echo $form->checkBox($model, 'new_highlights'); ?>
		<?php echo $form->error($model,'new_highlights'); ?>
	</div>
	
	<div class="row" style="display: none;">
		<?php echo $form->labelEx($model,'agency'); ?>
		<?php echo $form->checkBox($model, 'agency'); ?>
		<?php echo $form->error($model,'agency'); ?>
	</div>
	
	<div class="row" style="display: none;">
		<?php echo $form->labelEx($model,'customer'); ?>
		<?php echo $form->checkBox($model, 'customer'); ?>
		<?php echo $form->error($model,'customer'); ?>
	</div>
	
	<div class="row" >
		<?php echo $form->labelEx($model,'notdownload_register'); ?>
		<?php echo $form->checkBox($model, 'notdownload_register'); ?>
		<?php echo $form->error($model,'notdownload_register'); ?>
	</div>
	
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Tạo mới' : 'Sửa'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
 <script type="text/javascript">
               CKEDITOR.config.allowedContent = true;
               CKEDITOR.replace('News[yii_overview_en]', {
                   'extraPlugins': 'showblocks,div,doksoft_backup,doksoft_stat,iframe',
                   'filebrowserImageBrowseUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgbrowse/imgbrowse.html'?>',
                   'filebrowserImageUploadUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgupload.php'?>',
                 });
               CKEDITOR.replace('News[yii_overview_fr]', {
                   'extraPlugins': 'showblocks,div,doksoft_backup,doksoft_stat,iframe',
                   'filebrowserImageBrowseUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgbrowse/imgbrowse.html'?>',
                   'filebrowserImageUploadUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgupload.php'?>',
                 });
               CKEDITOR.replace('News[yii_overview_sp]', {
                   'extraPlugins': 'showblocks,div,doksoft_backup,doksoft_stat,iframe',
                   'filebrowserImageBrowseUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgbrowse/imgbrowse.html'?>',
                   'filebrowserImageUploadUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgupload.php'?>',
                 });

               CKEDITOR.replace('News[yii_content_en]', {
                   'extraPlugins': 'showblocks,div,doksoft_backup,doksoft_stat,iframe',
                   'filebrowserImageBrowseUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgbrowse/imgbrowse.html'?>',
                   'filebrowserImageUploadUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgupload.php'?>',
                 });
                 CKEDITOR.replace('News[yii_content_fr]', {
                   'extraPlugins': 'showblocks,div,doksoft_backup,doksoft_stat,iframe',
                   'filebrowserImageBrowseUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgbrowse/imgbrowse.html'?>',
                   'filebrowserImageUploadUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgupload.php'?>',
                 });
                  CKEDITOR.replace('News[yii_content_sp]', {
                   'extraPlugins': 'showblocks,div,doksoft_backup,doksoft_stat,iframe',
                   'filebrowserImageBrowseUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgbrowse/imgbrowse.html'?>',
                   'filebrowserImageUploadUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgupload.php'?>',
                 });

               CKEDITOR.replace('News[new_content_en]', {
                   'extraPlugins': 'showblocks,div,doksoft_backup,doksoft_stat,iframe',
                   'filebrowserImageBrowseUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgbrowse/imgbrowse.html'?>',
                   'filebrowserImageUploadUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgupload.php'?>',
                 });
                  CKEDITOR.replace('News[new_content_fr]', {
                   'extraPlugins': 'showblocks,div,doksoft_backup,doksoft_stat,iframe',
                   'filebrowserImageBrowseUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgbrowse/imgbrowse.html'?>',
                   'filebrowserImageUploadUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgupload.php'?>',
                 });
                  CKEDITOR.replace('News[new_content_sp]', {
                   'extraPlugins': 'showblocks,div,doksoft_backup,doksoft_stat,iframe',
                   'filebrowserImageBrowseUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgbrowse/imgbrowse.html'?>',
                   'filebrowserImageUploadUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgupload.php'?>',
                 });

                  CKEDITOR.replace('News[yii_wsattend_en]', {
                      'extraPlugins': 'showblocks,div,doksoft_backup,doksoft_stat,iframe',
                      'filebrowserImageBrowseUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgbrowse/imgbrowse.html'?>',
                      'filebrowserImageUploadUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgupload.php'?>',
                    });
                     CKEDITOR.replace('News[yii_wsattend_fr]', {
                      'extraPlugins': 'showblocks,div,doksoft_backup,doksoft_stat,iframe',
                      'filebrowserImageBrowseUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgbrowse/imgbrowse.html'?>',
                      'filebrowserImageUploadUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgupload.php'?>',
                    });
                     CKEDITOR.replace('News[yii_wsattend_sp]', {
                      'extraPlugins': 'showblocks,div,doksoft_backup,doksoft_stat,iframe',
                      'filebrowserImageBrowseUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgbrowse/imgbrowse.html'?>',
                      'filebrowserImageUploadUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgupload.php'?>',
                    });

                     CKEDITOR.replace('News[yii_attending_en]', {
                         'extraPlugins': 'showblocks,div,doksoft_backup,doksoft_stat,iframe',
                         'filebrowserImageBrowseUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgbrowse/imgbrowse.html'?>',
                         'filebrowserImageUploadUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgupload.php'?>',
                       });
                        CKEDITOR.replace('News[yii_attending_fr]', {
                         'extraPlugins': 'showblocks,div,doksoft_backup,doksoft_stat,iframe',
                         'filebrowserImageBrowseUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgbrowse/imgbrowse.html'?>',
                         'filebrowserImageUploadUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgupload.php'?>',
                       });
                        CKEDITOR.replace('News[yii_attending_sp]', {
                         'extraPlugins': 'showblocks,div,doksoft_backup,doksoft_stat,iframe',
                         'filebrowserImageBrowseUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgbrowse/imgbrowse.html'?>',
                         'filebrowserImageUploadUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgupload.php'?>',
                       });


                        CKEDITOR.replace('News[yii_director_en]', {
                            'extraPlugins': 'showblocks,div,doksoft_backup,doksoft_stat,iframe',
                            'filebrowserImageBrowseUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgbrowse/imgbrowse.html'?>',
                            'filebrowserImageUploadUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgupload.php'?>',
                          });
                           CKEDITOR.replace('News[yii_director_fr]', {
                            'extraPlugins': 'showblocks,div,doksoft_backup,doksoft_stat,iframe',
                            'filebrowserImageBrowseUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgbrowse/imgbrowse.html'?>',
                            'filebrowserImageUploadUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgupload.php'?>',
                          });
                           CKEDITOR.replace('News[yii_director_sp]', {
                            'extraPlugins': 'showblocks,div,doksoft_backup,doksoft_stat,iframe',
                            'filebrowserImageBrowseUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgbrowse/imgbrowse.html'?>',
                            'filebrowserImageUploadUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgupload.php'?>',
                          });
                        
                CKEDITOR.replace('News[new_summary_en]', {
                    'extraPlugins': 'showblocks,div,doksoft_backup,doksoft_stat,iframe',
                    'filebrowserImageBrowseUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgbrowse/imgbrowse.html'?>',
                    'filebrowserImageUploadUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgupload.php'?>',
                  });
                  CKEDITOR.replace('News[new_summary_fr]', {
                    'extraPlugins': 'showblocks,div,doksoft_backup,doksoft_stat,iframe',
                    'filebrowserImageBrowseUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgbrowse/imgbrowse.html'?>',
                    'filebrowserImageUploadUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgupload.php'?>',
                  });
                  CKEDITOR.replace('News[new_summary_sp]', {
                    'extraPlugins': 'showblocks,div,doksoft_backup,doksoft_stat,iframe',
                    'filebrowserImageBrowseUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgbrowse/imgbrowse.html'?>',
                    'filebrowserImageUploadUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgupload.php'?>',
                  });
                  
                
                   CKEDITOR.replace('News[contentnewsd_sp]', {
                       'extraPlugins': 'showblocks,div,doksoft_backup,doksoft_stat,iframe',
                       'filebrowserImageBrowseUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgbrowse/imgbrowse.html'?>',
                       'filebrowserImageUploadUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgupload.php'?>',
                     });
                   CKEDITOR.replace('News[contentnewsd_fr]', {
                       'extraPlugins': 'showblocks,div,doksoft_backup,doksoft_stat,iframe',
                       'filebrowserImageBrowseUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgbrowse/imgbrowse.html'?>',
                       'filebrowserImageUploadUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgupload.php'?>',
                     });
                   CKEDITOR.replace('News[contentnewsd_en]', {
                       'extraPlugins': 'showblocks,div,doksoft_backup,doksoft_stat,iframe',
                       'filebrowserImageBrowseUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgbrowse/imgbrowse.html'?>',
                       'filebrowserImageUploadUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgupload.php'?>',
                     });
                  
                  CKEDITOR.replace('News[contentnewsd]', {
                       'extraPlugins': 'showblocks,div,doksoft_backup,doksoft_stat,iframe',
                       'filebrowserImageBrowseUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgbrowse/imgbrowse.html'?>',
                       'filebrowserImageUploadUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgupload.php'?>',
                     });
       
           
                   CKEDITOR.replace('News[yii_wsattend_eg]', {
                    'extraPlugins': 'showblocks,div,doksoft_backup,doksoft_stat,iframe',
                    'filebrowserImageBrowseUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgbrowse/imgbrowse.html'?>',
                    'filebrowserImageUploadUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgupload.php'?>',
                  });
                   CKEDITOR.replace('News[yii_wsattend_fr]', {
                    'extraPlugins': 'showblocks,div,doksoft_backup,doksoft_stat,iframe',
                    'filebrowserImageBrowseUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgbrowse/imgbrowse.html'?>',
                    'filebrowserImageUploadUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgupload.php'?>',
                  });
                   CKEDITOR.replace('News[yii_wsattend_sp]', {
                    'extraPlugins': 'showblocks,div,doksoft_backup,doksoft_stat,iframe',
                    'filebrowserImageBrowseUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgbrowse/imgbrowse.html'?>',
                    'filebrowserImageUploadUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgupload.php'?>',
                  });
                  
                  
                   CKEDITOR.replace('News[yii_attending_en]', {
                    'extraPlugins': 'showblocks,div,doksoft_backup,doksoft_stat,iframe',
                    'filebrowserImageBrowseUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgbrowse/imgbrowse.html'?>',
                    'filebrowserImageUploadUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgupload.php'?>',
                  });
                   CKEDITOR.replace('News[yii_attending_fr]', {
                    'extraPlugins': 'showblocks,div,doksoft_backup,doksoft_stat,iframe',
                    'filebrowserImageBrowseUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgbrowse/imgbrowse.html'?>',
                    'filebrowserImageUploadUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgupload.php'?>',
                  });
                   CKEDITOR.replace('News[yii_attending_sp]', {
                    'extraPlugins': 'showblocks,div,doksoft_backup,doksoft_stat,iframe',
                    'filebrowserImageBrowseUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgbrowse/imgbrowse.html'?>',
                    'filebrowserImageUploadUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgupload.php'?>',
                  });

                   CKEDITOR.replace('News[yii_director_en]', {
                       'extraPlugins': 'showblocks,div,doksoft_backup,doksoft_stat,iframe',
                       'filebrowserImageBrowseUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgbrowse/imgbrowse.html'?>',
                       'filebrowserImageUploadUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgupload.php'?>',
                     });
                      CKEDITOR.replace('News[yii_director_sp]', {
                       'extraPlugins': 'showblocks,div,doksoft_backup,doksoft_stat,iframe',
                       'filebrowserImageBrowseUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgbrowse/imgbrowse.html'?>',
                       'filebrowserImageUploadUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgupload.php'?>',
                     });
                      CKEDITOR.replace('News[yii_director_sp]', {
                       'extraPlugins': 'showblocks,div,doksoft_backup,doksoft_stat,iframe',
                       'filebrowserImageBrowseUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgbrowse/imgbrowse.html'?>',
                       'filebrowserImageUploadUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgupload.php'?>',
                     });
  
                  
            </script>  