<?php
/* @var $this NewsController */
/* @var $model News */

$this->breadcrumbs=array(
	'Tin tức'=>array('admin'),
	'Tạo mới',
);

$this->menu=array(
	array('label'=>'Danh sách tin tức', 'url'=>array('admin')),
);
?>

<h4>Tạo mới tin tức</h4>

<?php echo $this->renderPartial('_form', array('model'=>$model,'YiiMenu'=>$YiiMenu)); ?>