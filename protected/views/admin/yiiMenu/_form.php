<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/css/ckeditor/ckeditor.js');
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'yii-menu-form',
	'enableAjaxValidation'=>false,
		'htmlOptions'          => array(
				'enctype' => 'multipart/form-data',
		),
)); ?>
<div class="row">
		<?php echo $form->labelEx($model,'level_menu'); ?>
		<?php echo $form->dropDownList($model,'level_menu', $YiiMenue, array("empty" => "---Chọn menu cha---")) ?>
		<?php echo $form->error($model,'level_menu'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'name_menu_en'); ?>
		<?php echo $form->textField($model,'name_menu_en',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'name_menu_en'); ?>
	</div>
    <div class="row">
		<?php echo $form->labelEx($model,'name_menu_fr'); ?>
		<?php echo $form->textField($model,'name_menu_fr',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'name_menu_fr'); ?>
	</div>
    	<div class="row">
		<?php echo $form->labelEx($model,'name_menu_sp'); ?>
		<?php echo $form->textField($model,'name_menu_sp',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'name_menu_sp'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'img_represent'); ?>
		<?php echo $form->fileField($model, 'img_represent'); ?><span class="imgs">Tập tin ảnh có định dạng (jpg, gif, png)</span>
		<?php echo !$model->isNewRecord ? '<img src="'.$model->img_represent .'" width="50" height="50"/>':'' ?>
		<?php echo $form->error($model,'img_represent'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'img_background'); ?>
		<?php echo $form->fileField($model, 'img_background'); ?><span class="imgs imgsbg">Tập tin ảnh có định dạng (jpg, gif, png)</span>
		<?php echo !$model->isNewRecord ? '<img src="'.$model->img_background .'" width="50" height="50"/>':'' ?>
		<?php echo $form->error($model,'img_background'); ?>
	</div>

<div class="row">
		<?php echo $form->labelEx($model,'summary_en'); ?>
		<?php echo $form->textArea($model,'summary_en',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'summary_en'); ?>
	</div>
<div class="row">
		<?php echo $form->labelEx($model,'summary_fr'); ?>
		<?php echo $form->textArea($model,'summary_fr',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'summary_fr'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'summary_sp'); ?>
		<?php echo $form->textArea($model,'summary_sp',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'summary_sp'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'location_menu'); ?>
		<?php echo $form->dropDownList($model, 'location_menu', array('1' => 'Top','2' => 'comnent', '5' => 'None'), array("empty" => "---Chọn vị trí---")) ?>
		<?php echo $form->error($model,'location_menu'); ?>
	</div>
    
	<div class="row">
		<?php echo $form->labelEx($model,'url_menu'); ?>
		<?php echo $form->textField($model,'url_menu',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'url_menu'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'order_menu'); ?>
		<?php echo $form->textField($model,'order_menu',array('size'=>60,'maxlength'=>500)); ?>
		<?php echo $form->error($model,'order_menu'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'title_seo_menu'); ?>
		<?php echo $form->textField($model,'title_seo_menu',array('size'=>60,'maxlength'=>500)); ?>
		<?php echo $form->error($model,'title_seo_menu'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'description_seo_menu'); ?>
		<?php echo $form->textField($model,'description_seo_menu',array('size'=>60,'maxlength'=>500)); ?>
		<?php echo $form->error($model,'description_seo_menu'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'keywords_seo'); ?>
		<?php echo $form->textField($model,'keywords_seo',array('size'=>60,'maxlength'=>500)); ?>
		<?php echo $form->error($model,'keywords_seo'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'highlights_menu'); ?>
		<?php echo $form->checkBox($model, 'highlights_menu'); ?>
		<?php echo $form->error($model,'highlights_menu'); ?>
	</div>
    <div class="row">
		<?php echo $form->labelEx($model,'bntb'); ?>
		<?php echo $form->checkBox($model, 'bntb'); ?>
		<?php echo $form->error($model,'bntb'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'showhide_menu'); ?>
		<?php echo $form->checkBox($model, 'showhide_menu'); ?>
		<?php echo $form->error($model,'showhide_menu'); ?>
	</div>
    <div class="row" style="display: none;">
		<?php echo $form->labelEx($model,'h_rightvd'); ?>
		<?php echo $form->checkBox($model, 'h_rightvd'); ?>
		<?php echo $form->error($model,'h_rightvd'); ?>
	</div>
    <div class="row">
		<?php echo $form->labelEx($model,'lev'); ?>
		<?php echo $form->checkBox($model, 'lev'); ?>
		<?php echo $form->error($model,'lev'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'newwindow_mennu'); ?>
		<?php echo $form->checkBox($model, 'newwindow_mennu'); ?>
		<?php echo $form->error($model,'newwindow_mennu'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'listmenus'); ?>
		<?php echo $form->checkBox($model, 'listmenus'); ?>
		<?php echo $form->error($model,'listmenus'); ?>
	</div>
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Tạo mới' : 'Sửa'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
 <script type="text/javascript">
               CKEDITOR.config.allowedContent = true;
                CKEDITOR.replace('YiiMenu[summary_en]', {
                    'extraPlugins': 'showblocks,div,doksoft_backup,doksoft_stat,iframe',
                    'filebrowserImageBrowseUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgbrowse/imgbrowse.html'?>',
                    'filebrowserImageUploadUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgupload.php'?>',
                  });
                CKEDITOR.replace('YiiMenu[summary_fr]', {
                    'extraPlugins': 'showblocks,div,doksoft_backup,doksoft_stat,iframe',
                    'filebrowserImageBrowseUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgbrowse/imgbrowse.html'?>',
                    'filebrowserImageUploadUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgupload.php'?>',
                  });
                CKEDITOR.replace('YiiMenu[summary_sp]', {
                    'extraPlugins': 'showblocks,div,doksoft_backup,doksoft_stat,iframe',
                    'filebrowserImageBrowseUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgbrowse/imgbrowse.html'?>',
                    'filebrowserImageUploadUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgupload.php'?>',
                  });
                </script>  