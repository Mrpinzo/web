<?php
/* @var $this YiiMenuController */
/* @var $data YiiMenu */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_menu')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_menu), array('view', 'id'=>$data->id_menu)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name_menu_en')); ?>:</b>
	<?php echo CHtml::encode($data->name_menu); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('showhide_menu')); ?>:</b>
	<?php echo CHtml::encode($data->showhide_menu); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('location_menu')); ?>:</b>
	<?php echo CHtml::encode($data->location_menu); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('level_menu')); ?>:</b>
	<?php echo CHtml::encode($data->level_menu); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('order_menu')); ?>:</b>
	<?php echo CHtml::encode($data->order_menu); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('newwindow_mennu')); ?>:</b>
	<?php echo CHtml::encode($data->newwindow_mennu); ?>
	<br />

	*/ ?>

</div>