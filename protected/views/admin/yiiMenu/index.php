<?php
/* @var $this YiiMenuController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Yii Menus',
);

$this->menu=array(
	array('label'=>'Create YiiMenu', 'url'=>array('create')),
	array('label'=>'Manage YiiMenu', 'url'=>array('admin')),
);
?>

<h1>Yii Menus</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
