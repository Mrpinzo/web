<?php
/* @var $this YiiMenuController */
/* @var $model YiiMenu */

$this->breadcrumbs=array(
	'Yii Menus'=>array('index'),
	$model->id_menu,
);

$this->menu=array(
	array('label'=>'List YiiMenu', 'url'=>array('index')),
	array('label'=>'Create YiiMenu', 'url'=>array('create')),
	array('label'=>'Update YiiMenu', 'url'=>array('update', 'id'=>$model->id_menu)),
	array('label'=>'Delete YiiMenu', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_menu),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage YiiMenu', 'url'=>array('admin')),
);
?>

<h1>View YiiMenu #<?php echo $model->id_menu; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_menu',
		'name_menu_en',
		'showhide_menu',
		'location_menu',
		'level_menu',
		'order_menu',
		'newwindow_mennu',
			
			'title_seo_menu',
			'description_seo_menu',
			'keywords_seo',
	),
)); ?>
