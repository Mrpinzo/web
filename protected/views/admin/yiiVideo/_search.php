<?php
/* @var $this YiiVideoController */
/* @var $model YiiVideo */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_video'); ?>
		<?php echo $form->textField($model,'id_video'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'name_video'); ?>
		<?php echo $form->textField($model,'name_video',array('size'=>60,'maxlength'=>500)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'showhide_video'); ?>
		<?php echo $form->textField($model,'showhide_video'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'url_video'); ?>
		<?php echo $form->textField($model,'url_video',array('size'=>60,'maxlength'=>500)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'title_video'); ?>
		<?php echo $form->textField($model,'title_video',array('size'=>60,'maxlength'=>500)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'keywords_video'); ?>
		<?php echo $form->textField($model,'keywords_video',array('size'=>60,'maxlength'=>500)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'description_video'); ?>
		<?php echo $form->textField($model,'description_video',array('size'=>60,'maxlength'=>500)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_typevideo'); ?>
		<?php echo $form->textField($model,'id_typevideo'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->