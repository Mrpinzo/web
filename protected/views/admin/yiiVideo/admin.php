<?php
/* @var $this YiiVideoController */
/* @var $model YiiVideo */

$this->breadcrumbs=array(
	'Video'=>array('admin'),
	'Danh sách',
);

$this->menu=array(
	array('label'=>'Tạo mới', 'url'=>array('create')),
);

?>

<h4>Danh sách Video</h4>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'yii-video-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_video',
			array(
					'name'   => 'id_typevideo',
					'value'  => 'isset($data->id_typevideo)?$data->YiiTypeVideo->name__typevideo:""',
					'filter' => CHtml::activeDropDownList($model, 'id_typevideo', CHtml::listData(YiiTypevideo::model()->findAll(), "id_typevideo", "name__typevideo"), array("empty" => "Chọn loại video")),
			),
		    'name_video',
            //'url_video',
			array(
					'name'   => 'url_video',
					'value'  => '"<a target=\"_blank\" href=\"".$data->url_video."\">".$data->url_video."</iframe>"',
					'type'   => 'raw',
			),
			/* array(
					'name'   => 'url_video',
					'value'  => '"<iframe width=\"160\" height=\"160\" src=\"".$data->url_video."\" frameborder=\"0\" allowfullscreen></iframe>"',
					'type'   => 'raw',
			), */
			array(
					'name'   => 'showhide_video',
					'value'  => '$data->showhide_video==1?"<i class=\"fa fa-check-square-o csm\"></i>":"<i class=\"fa fa-times csm\"></i>"',
					'filter' => array(1 => 'Hiện', 0 => 'Ẩn'),
					'type'   => 'raw',
			),
		'title_video',
		'keywords_video',
		'description_video',
			'date_video',
			array(
					'name'   => 'highlights_video',
					'value'  => '$data->highlights_video==1?"<i class=\"fa fa-check-square-o csm\"></i>":"<i class=\"fa fa-times csm\"></i>"',
					'filter' => array(1 => 'Nổi bật', 0 => 'Không nổi bật'),
					'type'   => 'raw',
			),
		array(
			'class'=>'CButtonColumn',
				'header' => 'Hành động',
				'buttons'=>array
				(
						'view' => array(
								'options' => array('target' => '_blank','rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'View')),
								'label' => '<i class="fa fa-search"></i>',
								'imageUrl' => false,
								'url'=>'"/video/".$data->url_seo_video.".html"',
						),
						'update' => array(
								'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'Update')),
								'label' => '<i class="fa fa-pencil-square-o"></i>',
								'imageUrl' => false,
						),
						'delete' => array(
								'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'Delete')),
								'label' => '<i class="fa fa-trash-o"></i>',
								'imageUrl' => false,
						)
				),
		),
	),
)); ?>
