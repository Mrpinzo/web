<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/media/mediaelement-and-player.min.js');
Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/js/media/mediaelementplayer.min.css');

$this->breadcrumbs=array(
	'Video'=>array('admin'),
	$model->name_video,
);

$this->menu=array(
	array('label'=>'Danh sách Video', 'url'=>array('admin')),
);
?>

<h4>Chi tiết <?php echo $model->name_video; ?></h4>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_video',
		'name_video',		
			array(
					'name'   => 'url_video',
					'value'  => isset($model->url_video)?'<video width="640" height="360" id="player1" preload="none"><source type="video/youtube" src="'.$model->url_video.'" /></video>':"",
					'type'   => 'raw',
			),
		'title_video',
		'keywords_video',
		'description_video',
			array(
					'name'   => 'id_typevideo',
					'value'  => isset($model->id_typevideo)?$model->YiiTypeVideo->name__typevideo:"",
			),
			'date_video',
			array(
					'name'   => 'showhide_video',
					'value'  => $model->showhide_video==1?"<i class=\"fa fa-check-square-o csm\"></i>":"<i class=\"fa fa-times csm\"></i>",
					'type'   => 'raw',
			),
			array(
					'name'   => 'highlights_video',
					'value'  => $model->highlights_video==1?"<i class=\"fa fa-check-square-o csm\"></i>":"<i class=\"fa fa-times csm\"></i>",
					'type'   => 'raw',
			),
	),
)); ?>
<script>

$('video').mediaelementplayer({
	success: function(media, node, player) {
		$('#' + node.id + '-mode').html('mode: ' + media.pluginType);
	}
});

</script>