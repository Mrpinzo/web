<?php
/* @var $this YiiVideoController */
/* @var $model YiiVideo */

$this->breadcrumbs=array(
	'Video'=>array('admin'),
	$model->name_video=>array('view','id'=>$model->id_video),
	'Sửa',
);

$this->menu=array(
	array('label'=>'Danh sách Video', 'url'=>array('admin')),
);
?>

<h4>Sửa Video <?php echo $model->name_video; ?></h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>