<?php
/* @var $this YiiVideoController */
/* @var $data YiiVideo */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_video')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_video), array('view', 'id'=>$data->id_video)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name_video')); ?>:</b>
	<?php echo CHtml::encode($data->name_video); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('showhide_video')); ?>:</b>
	<?php echo CHtml::encode($data->showhide_video); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('url_video')); ?>:</b>
	<?php echo CHtml::encode($data->url_video); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('title_video')); ?>:</b>
	<?php echo CHtml::encode($data->title_video); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('keywords_video')); ?>:</b>
	<?php echo CHtml::encode($data->keywords_video); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description_video')); ?>:</b>
	<?php echo CHtml::encode($data->description_video); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('id_typevideo')); ?>:</b>
	<?php echo CHtml::encode($data->id_typevideo); ?>
	<br />

	*/ ?>

</div>