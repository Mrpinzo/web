<?php
/* @var $this LinksController */
/* @var $data Links */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_links')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_links), array('view', 'id'=>$data->id_links)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name_links')); ?>:</b>
	<?php echo CHtml::encode($data->name_links); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('showhide_links')); ?>:</b>
	<?php echo CHtml::encode($data->showhide_links); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('url_links')); ?>:</b>
	<?php echo CHtml::encode($data->url_links); ?>
	<br />


</div>