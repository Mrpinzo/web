<?php
/* @var $this LinksController */
/* @var $model Links */

$this->breadcrumbs=array(
	'Liên kết'=>array('admin'),
	$model->name_links=>array('view','id'=>$model->id_links),
	'Sửa',
);

?>

<h4>Sửa liên kết  <?php echo $model->name_links; ?></h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>