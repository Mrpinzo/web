<?php
/* @var $this ConfigureController */
/* @var $model Configure */

$this->breadcrumbs=array(
	'Cấu hình'=>array('/configure/view?id=1'),
);
if($count==0)
	$this->menu=array(
	array('label'=>'Tạo mới', 'url'=>array('create'),'itemOptions'=>array('id' => 'configure_ad')),
);else
$this->menu=array(
	array('label'=>'Sửa', 'url'=>array('update', 'id'=>$model->id_configure),'itemOptions'=>array('id' => 'configure_ad')),
);
?>
<h4>Cấu hình</h4>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
			array(
					'name'  => 'logo_configure',
					'value' => !empty($model->logo_configure)?'<a class="highslide" onclick="return hs.expand(this)" href="'.Yii::app()->request->baseUrl . $model->logo_configure.'"><img width="80" height="80" src="'.Yii::app()->request->baseUrl . $model->logo_configure.'" ></a>':'',
					'type'  => 'raw',
			),
			array(
					'name'  => 'paymentgateways',
					'value' => !empty($model->paymentgateways)?'<a class="highslide" onclick="return hs.expand(this)" href="'.Yii::app()->request->baseUrl . $model->paymentgateways.'"><img width="80" height="80" src="'.Yii::app()->request->baseUrl . $model->paymentgateways.'" ></a>':'',
					'type'  => 'raw',
			),
		/* 	array(
					'name'  => 'service',
					'value' => !empty($model->service)?'<a class="highslide" onclick="return hs.expand(this)" href="'.Yii::app()->request->baseUrl . $model->service.'"><img width="80" height="80" src="'.Yii::app()->request->baseUrl . $model->service.'" ></a>':'',
					'type'  => 'raw',
			), */
			array(
					'name'  => 'slogan',
					'value' => !empty($model->slogan)?'<a class="highslide" onclick="return hs.expand(this)" href="'.Yii::app()->request->baseUrl . $model->slogan.'"><img width="80" height="80" src="'.Yii::app()->request->baseUrl . $model->slogan.'" ></a>':'',
					'type'  => 'raw',
			),
           array(
					'name'  => 'imgdl_configure',
					'value' => !empty($model->imgdl_configure)?'<a class="highslide" onclick="return hs.expand(this)" href="'.Yii::app()->request->baseUrl . $model->imgdl_configure.'"><img width="80" height="80" src="'.Yii::app()->request->baseUrl . $model->imgdl_configure.'" ></a>':'',
					'type'  => 'raw',
			),
		//	'latitude_configure',
		//	'longitude_configure',
			
			'title_seo',
			'description_seo',
			'keywords_seo',
			'url_home',
		//	'phone_configure',
			'page_facebook',
			'page_google',
			'page_youtube',
			
		//	'code_google_analytics',
		//	'Verification_google',
		
		//	'ga_profile_id',
			'alexa',
			array(
					'name'  => 'about_configure',
					'value' =>  $model->about_configure,
					'type'  => 'raw',
			),
			array(
					'name'  => 'title_contact',
					'value' =>  $model->title_contact,
					'type'  => 'raw',
			),
			array(
					'name'  => 'ServiceCenter',
					'value' =>  $model->ServiceCenter,
					'type'  => 'raw',
			),
			/* array(
					'name'  => 'Contactstore',
					'value' =>  $model->Contactstore,
					'type'  => 'raw',
			), */
			/* array(
					'name'  => 'contact_top',
					'value' =>  $model->contact_top,
					'type'  => 'raw',
			), */
			//'facebook_configure',
			//'mail_configure',
			//'skype_configure',
			//'yahoo_configure'
	),
)); ?>
