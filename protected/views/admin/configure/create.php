<?php
/* @var $this ConfigureController */
/* @var $model Configure */

$this->breadcrumbs=array(
	'Cấu hình'=>array('index'),
	'Tạo mới',
);
?>

<h4>Tạo mới cấu hình</h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>