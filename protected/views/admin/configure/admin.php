<?php
/* @var $this ConfigureController */
/* @var $model Configure */

$this->breadcrumbs=array(
	'Cấu hình'=>array('admin'),
	'Danh sách',
);
if($count==0)		
		$this->menu=array(
			array('label'=>'Tạo mới', 'url'=>array('create')),
		);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#configure-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h4>Cấu hình</h4>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'configure-grid',
		'ajaxUpdate'=>false,
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
			array(
					'name'   => 'logo_configure',
					'value'  =>  '!empty($data->logo_configure)?"<img src=\"".Yii::app()->request->baseUrl.$data->logo_configure."\" width=\"50\" height=\"50\"/>":""',
					'type'   => 'raw',
					'filter' => FALSE,
					'sortable'   => FALSE,
			),
			
			array(
					'name'   => 'about_configure',
					'value'  => '$data->about_configure',
					'type'   => 'raw',
			),

			'title_seo',
			'description_seo',
			'keywords_seo',
			'url_home',
			
			'latitude_configure',
			'longitude_configure',
			array(
					'name'   => 'title_contact',
					'value'  => '$data->title_contact',
					'type'   => 'raw',
			),
		array(
			'class'=>'CButtonColumn',
				'header' => 'Hành động',
				'template'=>'{update}',
				'buttons'=>array
				(
						'view' => array(
								'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'View')),
								'label' => '<i class="fa fa-search"></i>',
								'imageUrl' => false,
						),
						'update' => array(
								'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'Update')),
								'label' => '<i class="fa fa-pencil-square-o"></i>',
								'imageUrl' => false,
						),
						'delete' => array(
								'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'Delete')),
								'label' => '<i class="fa fa-trash-o"></i>',
								'imageUrl' => false,
						)
				),
		),
	),
)); ?>
