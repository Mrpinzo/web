<?php
/* @var $this cmertiseController */
/* @var $data cmertise */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('cm_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->cm_id), array('view', 'id'=>$data->cm_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cm_link')); ?>:</b>
	<?php echo CHtml::encode($data->cm_link); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cm_img')); ?>:</b>
	<?php echo CHtml::encode($data->cm_img); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cm_action')); ?>:</b>
	<?php echo CHtml::encode($data->cm_action); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cm_positions')); ?>:</b>
	<?php echo CHtml::encode($data->cm_positions); ?>
	<br />


</div>