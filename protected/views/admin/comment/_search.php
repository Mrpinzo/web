<?php
/* @var $this cmertiseController */
/* @var $model cmertise */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'cm_id'); ?>
		<?php echo $form->textField($model,'cm_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cm_link_en'); ?>
		<?php echo $form->textField($model,'cm_link_en',array('size'=>60,'maxlength'=>200)); ?>
	</div>


	<div class="row">
		<?php echo $form->label($model,'cm_action'); ?>
		<?php echo $form->textField($model,'cm_action'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cm_positions'); ?>
		<?php echo $form->textField($model,'cm_positions'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->