<?php
/* @var $this ContactController */
/* @var $model Contact */

$this->breadcrumbs=array(
	'Liên hệ'=>array('admin'),
	$model->title_contact,
);

$this->menu=array(
	
	array('label'=>'Liên hệ', 'url'=>array('admin')),
);
?>

<h4>Chi tiết liên hệ: <?php echo $model->title_contact; ?></h4>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_contact',
		'fullnam_contact',
		'company_contact',
		'phone_contact',
		'email_contact',
		'address_contact',
		'title_contact',
		'content_contact',
		'mobile_contact',
			array(
					'name'   => 'date',
					'value'  => date("H:i:s d-m-Y",strtotime($model->date)),
			),
	),
)); ?>
