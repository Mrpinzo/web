<?php
/* @var $this ContactController */
/* @var $model Contact */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'contact-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'fullnam_contact'); ?>
		<?php echo $form->textField($model,'fullnam_contact',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'fullnam_contact'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'company_contact'); ?>
		<?php echo $form->textField($model,'company_contact',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'company_contact'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'phone_contact'); ?>
		<?php echo $form->textField($model,'phone_contact',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'phone_contact'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email_contact'); ?>
		<?php echo $form->textField($model,'email_contact',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'email_contact'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'address_contact'); ?>
		<?php echo $form->textField($model,'address_contact',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'address_contact'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'title_contact'); ?>
		<?php echo $form->textField($model,'title_contact',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'title_contact'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'content_contact'); ?>
		<?php echo $form->textArea($model,'content_contact',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'content_contact'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'mobile_contact'); ?>
		<?php echo $form->textField($model,'mobile_contact',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'mobile_contact'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->