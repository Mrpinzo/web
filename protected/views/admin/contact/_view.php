<?php
/* @var $this ContactController */
/* @var $data Contact */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_contact')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_contact), array('view', 'id'=>$data->id_contact)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fullnam_contact')); ?>:</b>
	<?php echo CHtml::encode($data->fullnam_contact); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('company_contact')); ?>:</b>
	<?php echo CHtml::encode($data->company_contact); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('phone_contact')); ?>:</b>
	<?php echo CHtml::encode($data->phone_contact); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email_contact')); ?>:</b>
	<?php echo CHtml::encode($data->email_contact); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('address_contact')); ?>:</b>
	<?php echo CHtml::encode($data->address_contact); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('title_contact')); ?>:</b>
	<?php echo CHtml::encode($data->title_contact); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('content_contact')); ?>:</b>
	<?php echo CHtml::encode($data->content_contact); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mobile_contact')); ?>:</b>
	<?php echo CHtml::encode($data->mobile_contact); ?>
	<br />

	*/ ?>

</div>