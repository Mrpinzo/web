<?php
function postEX($str){
	$i = strrpos($str,".");
	if(!$i){
		return "";	
	}
	$l = strlen($str) - $i;
	$ext = substr($str,$i+1,$l);
        return $ext;
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="imagetoolbar" content="no" />
<meta property="og:locale" content="vi_VN" />
<meta property="fb:app_id" content="1797452207203956" />
<meta id="fbTitle" itemprop="headline" property="og:title" content="<?php echo CHtml::encode($this->pageTitle); ?>" />
<meta id="fbDescription" property="og:description" content="<?php echo CHtml::encode($this->description); ?>" />
<meta id="fbImage" itemprop="thumbnailUrl" property="og:image" content="http://pri-qua.com/images/home/images/logo_large.png" />
<meta id="fbUrl" property="og:url" content=<?php echo "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?> />
<meta id="fbSiteName" property="og:site_name" content="" />
<meta id="fbType" property="og:type" content="article" />

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />

<meta name="keywords" content="<?php echo CHtml::encode($this->keywords); ?>" />
<meta name="description" content="<?php echo CHtml::encode($this->description); ?>" />

<title><?php echo CHtml::encode($this->pageTitle); ?></title>

<link href="<?php echo Yii::app()->request->baseUrl; ?>/images/home/images/logo.png" type="images/png" rel="icon"/>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/home/js/jquery-2.1.4.min.js"></script>
<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" rel="stylesheet" type="text/css" />
<link type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/font/font-awesome/css/font-awesome.css" rel="stylesheet"> 
 <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />   
<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/home/csslaptop/style.css" rel="stylesheet" type="text/css" />
 <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/js/home/jslaptop/engine1/style.css" />
<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/home/csslaptop/styledetail.css" rel="stylesheet" type="text/css" />
<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/home/csslaptop/responsive.css" type="text/css" media="screen" rel="stylesheet"> 
 <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/home/csslaptop/jquery.mmenu.all.css" rel="stylesheet">
 <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/home/css/style.facebook.io.css" rel="stylesheet">

<link rel="stylesheet" href="/js/scrollbar/jquery.mCustomScrollbar.css"> 

 <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/home/css/maps.css" />
 <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/home/css/slider_io.css" />
        
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/home/css/demo-slider.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/home/css/styles-slider.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/home/css/responsive-man.css" />
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/home/js/modernizr.custom.53451.js"></script>    
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/home/js/jquery.ground.news.js"></script>      
<?php echo $this->alexas; ?>
</head>
<body>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '1797452207203956',
      xfbml      : true,
      version    : 'v2.7'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>

     <div id="main" style="position: relative;">
     <?php
        $path = 'http://pri-qua.com/images/home/ground_news/images/';
        $title =  CHtml::encode($this->pageTitle);
        $title= str_replace(" ","",$title);
        $title= str_replace("&amp;","",$title);
        $title= str_replace(",","",$title);
        $title = str_replace(".","",$title);
        $title = str_replace("_","",$title);
        $title = strtolower($title);
        $s = false;
        $filepath = $path . $title .'.gif';
        $s = @getimagesize($filepath);
        if($s == false){
        	$filepath = $path . $title .'.jpg';
        	$s = @getimagesize($filepath);
        }
        if($s == false){
        	$filepath = $path . $title .'.png';
        	$s = @getimagesize($filepath);
        }
        if($s == false){
        	$filepath = $path . $title .'.mp4';
        	$s = file_exists($filepath);
        }
     
       	if($s == true){
	        echo ('<div class="ground-web">');
	        if(postEX($filepath)=='mp4'){ ?> <video width = "100%" autoplay loop><source src="<?php echo $filepath; ?>" type="video/mp4"></video> <?php }
else { ?>
<img src="<?php echo $filepath; ?>">
            <?php
	       }
		    echo('</div>');
	   }
 ?>
     
            <?php $this->widget('top'); ?>
    
            <?php echo $content; ?>     
       
            <?php $this->widget('footer'); ?>
    </div>
</body>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/home/main.js"></script> 
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/home/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/home/jquery.mousewheel-3.0.4.pack.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/home/jquery.fancybox-1.3.4.pack.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/home/js/jquery.gallery.js"></script>
 <script src="/js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>

	
	<script>
		(function($){
                        $('#dg-container').gallery({
				autoplay:true
			});
			$(window).load(function(){
				
				$("#content-3").mCustomScrollbar({
					scrollButtons:{enable:true},
					theme:"light-thick",
					scrollbarPosition:"outside"
				});
				
				$("#content-4").mCustomScrollbar({
					theme:"rounded-dots",
					scrollInertia:400
				});
				
				$("#content-5").mCustomScrollbar({
					axis:"x",
					theme:"dark-thin",
					autoExpandScrollbar:true,
					advanced:{autoExpandHorizontalScroll:true}
				});
				
				$("#content-6").mCustomScrollbar({
					axis:"x",
					theme:"light-3",
					advanced:{autoExpandHorizontalScroll:true}
				});
				
				$("#content-7").mCustomScrollbar({
					scrollButtons:{enable:true},
					theme:"3d-thick"
				});
				
				$("#content-8").mCustomScrollbar({
					axis:"yx",
					scrollButtons:{enable:true},
					theme:"3d",
					scrollbarPosition:"outside"
				});
				
				$("#content-9").mCustomScrollbar({
					scrollButtons:{enable:true,scrollType:"stepped"},
					keyboard:{scrollType:"stepped"},
					mouseWheel:{scrollAmount:188},
					theme:"rounded-dark",
					autoExpandScrollbar:true,
					snapAmount:188,
					snapOffset:65
				});
				
			});
		})(jQuery);
	</script>

</html>
