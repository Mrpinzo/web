<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width" />
<title>Không tìm thấy yêu cầu của bạn</title>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/home/error.css"/>
</head>
<body>
    <div id="main">
      <div id="content">
            <?php echo $content; ?>
            </div>
    </div>
</body>

</html>