<?php echo '<!--?xml version="1.0" encoding="UTF-8"?-->' ?>
  
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemalocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
    <url>
        <loc>http://<?php echo $_SERVER['HTTP_HOST'] ?></loc>
        <changefreq>daily</changefreq>
        <priority>1.00</priority>
    </url>
    <url>
        <loc>http://<?php echo $_SERVER['HTTP_HOST'] ?>/chi-tiet-gio-hang.html</loc>
        <changefreq>daily</changefreq>
        <priority>0.9</priority>
    </url>
    <url>
        <loc>http://<?php echo $_SERVER['HTTP_HOST'] ?>/chi-tiet-don-hang.html</loc>
        <changefreq>daily</changefreq>
        <priority>0.9</priority>
    </url>    
      <url>
        <loc>http://<?php echo $_SERVER['HTTP_HOST'] ?>/loai-san-pham.html</loc>
        <changefreq>daily</changefreq>
        <priority>0.9</priority>
    </url>
    <url>
        <loc>http://<?php echo $_SERVER['HTTP_HOST'] ?>/tim-kiem.html</loc>
        <changefreq>daily</changefreq>
        <priority>0.9</priority>
    </url>
    <url>
        <loc>http://<?php echo $_SERVER['HTTP_HOST'] ?>/list-video.html</loc>
        <changefreq>daily</changefreq>
        <priority>0.9</priority>
    </url>
    <url>
        <loc>http://<?php echo $_SERVER['HTTP_HOST'] ?>/thu-vien-anh.html</loc>
        <changefreq>daily</changefreq>
        <priority>0.9</priority>
    </url>    

    <?php foreach ($YiiMenu as $model): ?>
    <url>
        <loc>http://<?php echo $_SERVER['HTTP_HOST'] ?><?php echo $model->url_menu ?></loc>
        <changefreq>daily</changefreq>
        <priority>0.9</priority>
    </url>
<?php endforeach; ?>
  
<?php foreach ($Product as $model): ?>
    <url>
        <loc>http://<?php echo $_SERVER['HTTP_HOST'] ?>/san-pham/<?php echo $model->pr_url_news ?>.html</loc>
        <changefreq>daily</changefreq>
        <priority>0.9</priority>
    </url>
<?php endforeach; ?>

<?php foreach ($YiiTypealbum as $model): ?>
    <url>
        <loc>http://<?php echo $_SERVER['HTTP_HOST'] ?>/chi-tiet-album/<?php echo $model->url_seo_typealbum ?>.html</loc>
        <changefreq>daily</changefreq>
        <priority>0.9</priority>
    </url>
<?php endforeach; ?>

<?php foreach ($News as $model): ?>
    <url>
        <loc>http://<?php echo $_SERVER['HTTP_HOST'] ?>/chi-tiet-tin-tuc/<?php echo $model->rewrite_url_news ?>.html</loc>
        <changefreq>daily</changefreq>
        <priority>0.9</priority>
    </url>
<?php endforeach; ?>

<?php foreach ($YiiTypevideo as $model): ?>
    <url>
        <loc>http://<?php echo $_SERVER['HTTP_HOST'] ?>/chi-tiet-loai-video/<?php echo $model->url_typevideo ?>.html</loc>
        <changefreq>daily</changefreq>
        <priority>0.9</priority>
    </url>
<?php endforeach; ?>

<?php foreach ($YiiVideo as $model): ?>
    <url>
        <loc>http://<?php echo $_SERVER['HTTP_HOST'] ?>/video/<?php echo $model->url_seo_video ?>.html</loc>
        <changefreq>daily</changefreq>
        <priority>0.9</priority>
    </url>
<?php endforeach; ?>
</urlset>