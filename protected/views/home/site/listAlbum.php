<div id="content_type_bicycle">
<div class="items_contents">
                 <div class="divPath">
					<?php $this->widget('application.components.BreadCrumb', array(
					  'crumbs' => array(
					    array('name' => '<i class="fa fa-home"></i>', 'url' => Yii::app()->getHomeUrl()),
					  	array('name' => 'Album ảnh'),
					  ),
					  'delimiter' => ' <i class="fa fa-angle-right"></i> ', // if you want to change it
					)); ?>
				</div>
				<div id="titlealbumsd">Album Ảnh</div>
                     <div id="related" class="mainalbums">
                        
                        <div class="row">
                        <?php foreach ($items as $val){ ?>
						  <div class="col-sm-6 col-md-4">
						    <div class="thumbnail albumsd">
						      <a href="/chi-tiet-album/<?php echo $val['url_seo_typealbum'] ?>"><img src="<?php echo $val['img_album'] ?>" alt="<?php echo $val['name_typealbum'] ?>"></a>
						      <div class="caption">
						        <a href="/chi-tiet-album/<?php echo $val['url_seo_typealbum'] ?>"><h5><?php echo $val['name_typealbum'] ?></h5></a>
						      </div>
						    </div>
						  </div>
						 <?php }?>
						</div>
                        
                        
                         <div>
								<div id="main_paginnation">
							     <div id="pagination">
							    <?php $this->widget('CLinkPager', array(
							    'pages'       => $pages,
							    'htmlOptions' => array('class' => 'pager'),
							    'header'      => '',
							    'nextPageLabel' => '<i class="fa fa-angle-right"></i>',
							    'prevPageLabel'=> '<i class="fa fa-angle-left"></i>',
					    		'firstPageLabel'=>'<i class="fa fa-angle-double-left"></i>',
					    		'lastPageLabel'=>'<i class="fa fa-angle-double-right"></i>'
							));
							    ?>
							</div>
				
						</div>
							</div>
                     </div>
                    
                     
                 </div>
                 </div>