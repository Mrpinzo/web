

<!--------------------------------------------------------------------------------------------------->
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jssor.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jssor.slider.js"></script>
    <div class="slidec">
<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
  
   <?php foreach ($ads as $k=>$val){ 
   	?>
    <div class="item <?php echo ($k == 0)?'active':''; ?>">
      <a href="<?php echo $val->adv_link ?>" target="_blank"><img u=image src="<?php echo Yii::app()->request->baseUrl.$val->adv_img ?>" width="100%" height="437" /></a>
       <?php 
                        if(!empty($_SESSION)) 
                                   $a = $_SESSION['lang'];
                                   else
                                     $a = 'en';
                                 if(!empty($val['adv_name_' .$a])){ 
                            ?>
      <div class="carousel-caption con5">
          <a href="<?php echo $val->adv_link ?>" target="_blank" class="evbd" style="color: #fff;">
                         <?php 
                                 if(!empty($_SESSION)) 
                                   $a = $_SESSION['lang'];
                                   else
                                     $a = 'en';
                                 echo $val['adv_name_' .$a] 
                            ?>
          </a>
      </div>
       <?php }?>
    </div>
   <?php }?>
    
  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
  </a>
</div>
    </div>
    
<div id="bg_mainh">
            <div id="title2"> 
            <div id="titl3">
                 <div><span></span>upcoming events</div>
                 <div><span></span>Prime Quality Training in Global</div>
            </div>
            </div>
<!------------------------>
    <div id="containner" >
         
           
           <div id="event_hot">
         
             <div class="eventh1 content mCustomScrollbar" id="content-1">	
                  <?php 
                                        $criteria = new CDbCriteria;
                		    		     $criteria->condition = 'showhide_menu=1 AND lev=1';
                		    		     $criteria->order = 'order_menu DESC';
                		    		     $menu_left =  YiiMenu::model()->findAll($criteria);
                       foreach ($menu_left as $vals){
                       	   if(!empty($vals->url_menu))
                       	   	 $g = $vals->url_menu;
                       	   else 
                       	   	$g = '/cat/'.$vals->rewrite_url_menu.'.html';
                  ?>
                                                  <div class="eventh1_namew">
                                                      <a class="title_homeeventee"  href="<?php echo $g ?>">
                                                                 <?php 
                                                                     if(!empty($_SESSION)) 
						                                           $a = $_SESSION['lang'];
						                                           else
						                                             $a = 'en';
						                                         echo $vals['name_menu_' .$a] 
                                                                ?>
                                                      </a>
                                                    
                                            </div>                            
					<?php }?> 
             </div>
                     
          </div>
<!-- Get map here -->
<center>
<div class="map">
  <img width="600" height="300" usemap="#planetmap">
  <map name="planetmap" id="planetmap" >
      <!-- Vietnam -->
        <area shape="rect" coords="470,34,490,54" href="#vi"> <!-- coords.x1.y1 load, coords.x2.y2 = coords.x1.y1 + 20 -->
        <a class="touch vie" href="#vie">
          <span class="nation"></span>
          <span class="information">
            
            <span class="address">HaNoi, VietNam</span>
          </span>
        </a>
        <!-- Singapore -->
      <area shape="rect" coords="463,122,483,142" href="#sin">
      <a class="touch sin" href="#sin"><span class="nation"></span><span class="information">
          <span>PQ Singapore</span>
      </span></a>

      <!-- sudan -->
      <area shape="rect" coords="172,62,192,82" href="#shu">
      <a class="touch shu" href="#shu"><span class="nation"></span><span class="information">
          <span>PQ Sudan</span>
          </span></a>

      <!-- Ethiopia -->
      <area shape="rect" coords="208,93,228,113" href="#eth">
      <a class="touch eth" href="#eth"><span class="nation"></span><span class="information">
          <span>PQ Ethiopia</span>
          </span></a>
      <!-- Kenya -->
      <area shape="rect" coords="212,135,222,155" href="#ken">
      <a class="touch ken" href="#ken"><span class="nation"></span><span class="information">
          <span>PQ Kenya</span>
      </span></a>

      <!-- Tazania -->
      <area shape="rect" coords="183,160,203,180" href="#taz">
      <a class="touch taz" href="#taz"><span class="nation"></span><span class="information">
          <span>PQ Tanzania</span>
          </span></a>



      <!-- Mozambique -->
      <area shape="rect" coords="185,207,205,227" href="#moz">
      <a class="touch moz" href="#moz"><span class="nation"></span><span class="information">
          <span>PQ Mozambique</span>
          </span></a>


      <!-- South Africa -->
      <area shape="rect" coords="161,259,181,279" href="#sou">
      <a class="touch sou" href="#sou"><span class="nation"></span><span class="information">
          <span>PQ South-Africa</span>
        </span></a>


      <!-- Namibia -->
      <area shape="rect" coords="117,228,137,248" href="#nam">
      <a class="touch nam" href="#nam"><span class="nation"></span><span class="information">
          <span>PQ Namibia</span>
          </span></a>

      <!-- Nigeria -->
      <area shape="rect" coords="92,92,112,112" href="#nig">
      <a class="touch nig" href="#nig"><span class="nation"></span><span class="information">
          <span>PQ Nigeria</span>
        </span></a>
      <!-- Ghana -->
      <area shape="rect" coords="74,97,94,117" href="#gha">
      <a class="touch gha" href="#gha"><span class="nation"></span><span class="information">
          <span>PQ Ghana</span>
          
          </span></a>
      <!-- Congo -->
      <area shape="rect" coords="146,134,166,154" href="#con">
      <a class="touch con" href="#con"><span class="nation"></span><span class="information">
          <span>PQ Congo</span>
          </span></a>
  </map>
</div>
</center>

    </div>
</div>
<script type="text/javascript">
		
		// World Map action

		var time = 1000;
	    var timer = setInterval(updateTimer,time);
	    var intime = 0;

	    function updateTimer() {
	        var map = $(".map");
	        switch(intime){
	        	case 0:
	        		$(document).ready(function(){
						$('#planetmap .touch information').hide();
	        			$('.touch').hide();
					});
	        		map.css('background-position', '0 0');
	        		map.css('background-image','url("/images/home/images/maps.gif")');
	        		map.css('background-size', '100% auto');
	        		map.css('transition','1s');
	        		break;
	        	case 2:
	        		map.css('background-image','url("/images/home/images/maps.gif")')
	        		map.css('background-size', '230% auto');
	        		map.css('background-position', '-627px -280px');
	        		map.css('transition','1s');
	        		break;
				case 7:
					$(document).ready(function(){
                                                $('.information').css('display','none');
						$('.sin>.information').css('display','block');
					});
					break;
				case 9:
					$(document).ready(function(){$('.information').css('display','none');
						$('.vie>.information').css('display','block');
					});
					break;
					case 11:
					$(document).ready(function(){$('.information').css('display','none');
						$('.shu>.information').css('display','block');
					});
					break;
					case 13:
					$(document).ready(function(){$('.information').css('display','none');
						$('.eth>.information').css('display','block');
					});
					break;
					case 15:
					$(document).ready(function(){$('.information').css('display','none');
						$('.ken>.information').css('display','block');
					});
					break;
				case 17:
					$(document).ready(function(){$('.information').css('display','none');
						$('.taz>.information').css('display','block');
					});
					break;
				case 19:
					$(document).ready(function(){$('.information').css('display','none');
						$('.moz>.information').css('display','block');
					});
					break;
				case 21:
					$(document).ready(function(){$('.information').css('display','none');
						$('.sou>.information').css('display','block');
					});
					break;
				case 23:
					$(document).ready(function(){$('.information').css('display','none');
						$('.nam>.information').css('display','block');
					});
					break;
				case 25:
					$(document).ready(function(){$('.information').css('display','none');
						$('.nig>.information').css('display','block');
					});
					break;
				case 27:
					$(document).ready(function(){$('.information').css('display','none');
						$('.gha>.information').css('display','block');
					});
					break;
				case 29:
					$(document).ready(function(){$('.information').css('display','none');
						$('.con>.information').css('display','block');
					});
					break;
				case 33:
					$(document).ready(function(){$('.information').css('display','none');
						$('.information').css('display','none');
					});
					break;
				case 5:
	        		// on click location
	        		$(document).ready(function(){
	        			$('#planetmap .touch').click(
							function(){
								if($(this).children('.information').hasClass('open')){
									$('#planetmap .touch .information').hide();
									$('#planetmap .touch .information').removeClass('open');
								}
								else{
									$('#planetmap .touch .information').hide();
									$('#planetmap .touch .information').removeClass('open');
									$(this).children('.information').show();
									$(this).children('.information').addClass('open');
								}
							}
						);
	        			$('.touch').show();
					});
					break;
	        }
	        intime++;
	        if(intime>200) window.location = "http://pri-qua.com";
	    }
	</script>
<!-------------------------------------------------------------------------------------->


<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/js/home/jquery.fancybox-1.3.4.css" media="screen" />

<script type="text/javascript">
	$(document).ready(function() {

		$("a[rel=example_group]").fancybox({
			'transitionIn'		: 'none',
			'transitionOut'		: 'none',
			'titlePosition' 	: 'over',
			'titleFormat'		: function(title, currentArray, currentIndex, currentOpts) {
				return '<span id="fancybox-title-over">Image ' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';
			}
		});

	});
</script>

             
      <!-------------------- img ----------------->     
      <div id="row_passevent">
          <div id="img_passevent">
            <div id="passevent_title"><a>past events</a></div>
             <div class="container-demo">
                <section id="dg-container" class="dg-container">
                   <div class="dg-wrapper">
          <!----------------------------------------------------------->
          	<?php
    				$criteria2js = new CDbCriteria();
    				$criteria2js->condition = 'pr_showhide=1 AND pr_highlights=1';
    				$criteria2js->order = 'pr_id DESC';
    				$pro = Pasevent::model()->findAll($criteria2js);
    				foreach ($pro as $vals){
    					$criteria2g            = new CDbCriteria();
    					$criteria2g->condition = 'showhide_yii_imgproduct = 1 AND pr_id="'.$vals->pr_joinImg.'"';
    					$criteria2g->order = 'id_yii_imgproduct DESC';
    					$YiiImgproduct = YiiImgproduct::model()->findAll($criteria2g);
    					
    					$count = YiiImgproduct::model()->count($criteria2g);
    					if($count > 0){
    						
    				?>
                             <?php foreach ($YiiImgproduct as $k=>$valk){
                                 if($k < 8){
                             	?>
                                            <a rel="example_group" href="<?php echo Yii::app()->request->baseUrl.$valk->url_yii_imgproduct ?>" >
                                                <img src="<?php echo Yii::app()->request->baseUrl.$valk->url_yii_imgproduct ?>" alt="<?php echo $vals->pr_name ?>" />
                                              </a>
                                     <?php }?>
                             <?php }?>
                         <?php }?>
                   <?php }?>
            </div>
            <nav> 
              <span class="dg-prev">&lt;</span>
              <span class="dg-next">&gt;</span>
            </nav>
          </section></div>
          </div>
      </div>
     <!-------------------- End img ----------------->       
<script type="text/javascript" src="js/home/js/jquery.gallery.js"></script>
<script type="text/javascript">
			$(function($) {
				$('#dg-container').gallery({
					autoplay	:	true
				});
			});
		</script/>


<!-- Slider IO -->


<script src="/js/slider_io/slider.io.js"></script>
 <div id="mtphr-dnt-11716" class="mtphr-dnt mtphr-dnt-11716 mtphr-dnt-default mtphr-dnt-scroll mtphr-dnt-scroll-right">
 <div class="mtphr-dnt-wrapper mtphr-dnt-clearfix">
 <div class="mtphr-dnt-tick-container">
 <div class="mtphr-dnt-tick-contents">
      <div class="mtphr-dnt-tick mtphr-dnt-default-tick mtphr-dnt-clearfix">
            <img src="<?php  ?>" />
      </div>
</div>
</div>
</div>
</div>
<script>
            jQuery( window ).load( function() {
                            jQuery( '#mtphr-dnt-11716' ).ditty_news_ticker({
                    id : '11716',
                    type : 'scroll',
                    scroll_direction : 'right',
                    scroll_speed : 8,
                    scroll_pause : 0,
                    scroll_spacing : 40,
                    scroll_init : 0,
                    rotate_type : 'slide_right',
                    auto_rotate : 0,
                    rotate_delay : 7,
                    rotate_pause : 0,
                    rotate_speed : 3,
                    rotate_ease : 'linear',
                    nav_reverse : 0,
                    disable_touchswipe : 0,
                    offset : 20,
                    after_load : function( $ticker ) {
                                            },
                    before_change : function( $ticker ) {
                                            },
                    after_change : function( $ticker ) {
                                            }
                });
                        });
        </script> 

  