<?php
$this->pageTitle=Yii::app()->name . ' - Error';
$this->breadcrumbs=array(
	'Error',
);
?>

<h2>Error <?php echo $code; ?></h2>

<div class="error">
<div id="title_err"><?php echo CHtml::encode($message); ?></div>
<a href="<?php echo Yii::app()->getHomeUrl() ?>">GO BACK TO HOME</a>
</div>