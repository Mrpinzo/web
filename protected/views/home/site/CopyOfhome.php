
<!--------------------------------------------------------------------------------------------------->
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jssor.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jssor.slider.js"></script>
    
    <script>
        jQuery(document).ready(function ($) {
        
            var _SlideshowTransitions = [
            //Fade in R
            {$Duration: 1200, x: -0.3, $During: { $Left: [0.3, 0.7] }, $Easing: { $Left: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear }, $Opacity: 2 }
            //Fade out L
            , { $Duration: 1200, x: 0.3, $SlideOut: true, $Easing: { $Left: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear }, $Opacity: 2 }
            ];

            var options = {
                $AutoPlay: true,                                    //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
                $AutoPlaySteps: 1,                                  //[Optional] Steps to go for each navigation request (this options applys only when slideshow disabled), the default value is 1
                $AutoPlayInterval: 4000,                            //[Optional] Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing, default value is 3000
                $PauseOnHover: 1,                               //[Optional] Whether to pause when mouse over if a slider is auto playing, 0 no pause, 1 pause for desktop, 2 pause for touch device, 3 pause for desktop and touch device, 4 freeze for desktop, 8 freeze for touch device, 12 freeze for desktop and touch device, default value is 1

                $ArrowKeyNavigation: true,   			            //[Optional] Allows keyboard (arrow key) navigation or not, default value is false
                $SlideDuration: 500,                                //[Optional] Specifies default duration (swipe) for slide in milliseconds, default value is 500
                $MinDragOffsetToSlide: 20,                          //[Optional] Minimum drag offset to trigger slide , default value is 20
                //$SlideWidth: 600,                                 //[Optional] Width of every slide in pixels, default value is width of 'slides' container
                $SlideHeight: 330,                                //[Optional] Height of every slide in pixels, default value is height of 'slides' container
                $SlideSpacing: 0, 					                //[Optional] Space between each slide in pixels, default value is 0
                $DisplayPieces: 1,                                  //[Optional] Number of pieces to display (the slideshow would be disabled if the value is set to greater than 1), the default value is 1
                $ParkingPosition: 0,                                //[Optional] The offset position to park slide (this options applys only when slideshow disabled), default value is 0.
                $UISearchMode: 1,                                   //[Optional] The way (0 parellel, 1 recursive, default value is 1) to search UI components (slides container, loading screen, navigator container, arrow navigator container, thumbnail navigator container etc).
                $PlayOrientation: 1,                                //[Optional] Orientation to play slide (for auto play, navigation), 1 horizental, 2 vertical, 5 horizental reverse, 6 vertical reverse, default value is 1
                $DragOrientation: 3,                                //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0)

                $SlideshowOptions: {                                //[Optional] Options to specify and enable slideshow or not
                    $Class: $JssorSlideshowRunner$,                 //[Required] Class to create instance of slideshow
                    $Transitions: _SlideshowTransitions,            //[Required] An array of slideshow transitions to play slideshow
                    $TransitionsOrder: 1,                           //[Optional] The way to choose transition to play slide, 1 Sequence, 0 Random
                    $ShowLink: true                                    //[Optional] Whether to bring slide link on top of the slider when slideshow is running, default value is false
                },

                $BulletNavigatorOptions: {                                //[Optional] Options to specify and enable navigator or not
                    $Class: $JssorBulletNavigator$,                       //[Required] Class to create navigator instance
                    $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                    $Lanes: 1,                                      //[Optional] Specify lanes to arrange items, default value is 1
                    $SpacingX: 10,                                   //[Optional] Horizontal space between each item in pixel, default value is 0
                    $SpacingY: 10                                    //[Optional] Vertical space between each item in pixel, default value is 0
                },

                $ArrowNavigatorOptions: {
                    $Class: $JssorArrowNavigator$,              //[Requried] Class to create arrow navigator instance
                    $ChanceToShow: 2,                                //[Required] 0 Never, 1 Mouse Over, 2 Always
                    $AutoCenter: 2                                 //[Optional] Auto center navigator in parent container, 0 None, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
                },

                $ThumbnailNavigatorOptions: {
                    $Class: $JssorThumbnailNavigator$,              //[Required] Class to create thumbnail navigator instance
                    $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                    $ActionMode: 0,                                 //[Optional] 0 None, 1 act by click, 2 act by mouse hover, 3 both, default value is 1
                    $DisableDrag: true                              //[Optional] Disable drag or not, default value is false
                }
            };

            var jssor_sliderb = new $JssorSlider$("sliderb_container", options);
            //responsive code begin
            //you can remove responsive code if you don't want the slider scales while window resizes
            function ScaleSlider() {
                var parentWidth = jssor_sliderb.$Elmt.parentNode.clientWidth;
                if (parentWidth)
                    jssor_sliderb.$ScaleWidth(Math.min(parentWidth, 1349));
                else
                    window.setTimeout(ScaleSlider, 30);
            }
            ScaleSlider();

            $(window).bind("load", ScaleSlider);
            $(window).bind("resize", ScaleSlider);
            $(window).bind("orientationchange", ScaleSlider);
            //responsive code end
        });
    </script>
    
    <div class="slidec">
 <div id="sliderb_container" style="position: relative; top: 0px; left: 0px; width: 1024px;
        height: 435px; overflow: hidden;">

        <!-- Loading Screen -->
        <div u="loading" style="position: absolute; top: 0px; left: 0px;">
            <div style="filter: alpha(opacity=70); opacity:0.7; position: absolute; display: block;
                background: url('../../../images/home/images/hh.png') repeat; top: 0px; left: 0px;width: 100%;height:100%;">
            </div>
            <div style="position: absolute; display: block; background: url(../img/loading.gif) no-repeat center center;
                top: 0px; left: 0px;width: 100%;height:100%;">
            </div>
        </div>

        <!-- Slides Container -->
        <div u="slides" style="cursor: move; position: absolute; left: 0px; top: 0px; width: 1024px; height: 470px;
            overflow: hidden;">
            
              <?php foreach ($ads as $val){ ?>
    	       <div>
                    <a href="<?php echo $val->adv_link ?>" target="_blank"><img u=image src="<?php echo Yii::app()->request->baseUrl.$val->adv_img ?>" width="100%" height="470px" /></a>
                    <div u="thumb"><a class="evbd" style="color: #fff;">
                         <?php 
                                 if(!empty($_SESSION)) 
                                   $a = $_SESSION['lang'];
                                   else
                                     $a = 'en';
                                 echo $val['adv_name_' .$a] 
                            ?>
                        
                    </a></div>
                </div>
           <?php }?>          
        </div>
        <div id="nly" u="thumbnavigator" style="position: absolute; top: 0px; left: 62px; height:330px !important; width:384px;">
            <div style="filter: alpha(opacity=40); opacity:0.4; position: absolute; display: block;
                background: url('../../../images/home/images/hh.png') repeat; top: 0px; left: 0px; width: 100%; height: 100%;">
            </div>
            <!-- Thumbnail Item Skin Begin -->
            <div u="slides">
                <div u="prototype" style="POSITION: absolute; WIDTH: 384px; HEIGHT: 470px; TOP: 0; LEFT: 0;">
                    <div u="thumbnailtemplate" style="WIDTH: 100%; HEIGHT: 100%; TOP: 0; LEFT: 0; color:#fff; padding-left:28px;padding-top: 60px;padding-right: 10px;"></div>
                </div>
            </div>
            <!-- Thumbnail Item Skin End -->
        </div>
        <style>
          
            .jssorb01 {
                position: absolute;
            }
            .jssorb01 div, .jssorb01 div:hover, .jssorb01 .av {
                position: absolute;
                /* size of bullet elment */
                width: 12px;
                height: 12px;
                filter: alpha(opacity=70);
                opacity: .7;
                overflow: hidden;
                cursor: pointer;
                border: #000 1px solid;
            }
            .jssorb01 div { background-color: gray; }
            .jssorb01 div:hover, .jssorb01 .av:hover { background-color: #d3d3d3; }
            .jssorb01 .av { background-color: #fff; }
            .jssorb01 .dn, .jssorb01 .dn:hover { background-color: #555555; }
        </style>
      
        <style>
            .jssora05l{
                display: block;
                position: absolute;
                /* size of arrow element */
                width: 40px;
                height: 52px;
                cursor: pointer;
                background: url('../../images/home/images/left_slide.png') left no-repeat;
                top: 30% !important;
                overflow: hidden;
            }
           .jssora05r {
                display: block;
                position: absolute;
                /* size of arrow element */
                width: 40px;
                height: 52px;
                cursor: pointer;
                top: 30% !important;
                 background: url('../../images/home/images/icon_right.png') left no-repeat;
                overflow: hidden;
            }
        
        </style>
        <!-- Arrow Left -->
        <span u="arrowleft" class="jssora05l" style="bottom: 1px !important; left: 8px;">
        </span>
        <!-- Arrow Right -->
        <span u="arrowright" class="jssora05r" style="bottom: 1px !important; right: 8px;">
        </span>
        <!-- Trigger -->
    </div>
</div>
    
<div id="bg_mainh">
<?php foreach ($Configure as $val){?>
                    <img width="100%" src="<?php echo $val->paymentgateways ?>" />
            <?php }?> 
<!------------------------>
    <div id="containner" >
         
           
           <div id="event_hot">
             <div class="eventh1">	
                  <?php 
                       $criteria2s = new CDbCriteria();
                       $criteria2s->condition = 'new_showhide=1 AND new_highlights=1';
                       $menu_left = News::model()->findAll($criteria2s);
                       foreach ($menu_left as $vals){
                  ?>
                                                  <div class="eventh1_namew">
                                                      <a class="title_homeeventee"  href="<?php echo '/rg/'.$vals->rewrite_url_news.'.html' ?>">
                                                               
                                                                 <?php 
                                                                     if(!empty($_SESSION)) 
                                                                       $a = $_SESSION['lang'];
                                                                       else
                                                                         $a = 'en';
                                                                     echo $vals['new_title_' .$a] 
                                                                ?>
                                                      </a>
                                                      <ul class="eventncd">
                                                            <li class="eventnc0d1">
                                                                 <a href="<?php echo '/rg/'.$vals->rewrite_url_news.'.html' ?>">
                                                                 <?php 
                                                                             if(!empty($_SESSION)) 
                                                                               $a = $_SESSION['lang'];
                                                                               else
                                                                                 $a = 'en';
                                                                             echo $vals['yii_address_' .$a] 
                                                                        ?><br>
                                                                        <?php echo $vals->yii_date ?> <?php echo $vals->yii_year ?>
                                                                         
                                        					     </a>
                                                            </li>
                                                      </ul>
                                            </div>                            
					<?php }?> 
                  </div>
                     
          </div>
    </div>
</div>  

<!-------------------------------------------------------------------------------------->


<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/js/home/jquery.fancybox-1.3.4.css" media="screen" />

<script type="text/javascript">
	$(document).ready(function() {

		$("a[rel=example_group]").fancybox({
			'transitionIn'		: 'none',
			'transitionOut'		: 'none',
			'titlePosition' 	: 'over',
			'titleFormat'		: function(title, currentArray, currentIndex, currentOpts) {
				return '<span id="fancybox-title-over">Image ' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';
			}
		});

	});
</script>

             
      <!-------------------- img ----------------->     
      <div id="row_passevent">
          <div id="img_passevent">
            <div id="passevent_title"><a>past events</a></div>
          <!----------------------------------------------------------->
          	<?php
    				$criteria2js = new CDbCriteria();
    				$criteria2js->condition = 'pr_showhide=1 AND pr_highlights=1';
    				$criteria2js->order = 'pr_id DESC';
                    $criteria2js->limit = '8';
    				$pro = Pasevent::model()->findAll($criteria2js);
    				foreach ($pro as $vals){
    					$criteria2g            = new CDbCriteria();
    					$criteria2g->condition = 'showhide_yii_imgproduct = 1 AND pr_id="'.$vals->pr_joinImg.'"';
    					$criteria2g->group = 'pr_id';
    					$YiiImgproduct = YiiImgproduct::model()->findAll($criteria2g);
    					
    					$count = YiiImgproduct::model()->count($criteria2g);
    					if($count > 0){
    						
    				?>
                             <?php foreach ($YiiImgproduct as $valk){?>
                                     <div id="hi_event">
                                            <a rel="example_group" href="<?php echo Yii::app()->request->baseUrl.$valk->url_yii_imgproduct ?>" > 
                                                 <img src="<?php echo Yii::app()->request->baseUrl.$valk->url_yii_imgproduct ?>" alt="<?php echo $vals->pr_name ?>" />
                                              </a>
                                     </div>
                             <?php }?>
                         <?php }?>
                   <?php }?>
                    <div style="clear: both;"></div>
          </div>
      </div>
     <!-------------------- End img ----------------->                
  
