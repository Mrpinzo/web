<?php 
echo '<table id="giohangs" class="table table-hover">';
echo '<tr><th>Tên</th><th>Ảnh</th><th>Số lượng</th><th>Gía sản phẩm</th><th>Tổng</th></tr>' ;
 foreach ($itemPro as $val){
     
            $criteria2g   = new CDbCriteria();
 	 		$criteria2g->condition = 'showhide_yii_imgproduct = 1 AND pr_id="'.$val['pr_joinImg'].'"';
 	 		$criteria2g->group = 'pr_id';
 	 		$YiiImgproduct = YiiImgproduct::model()->findAll($criteria2g); 	 		
 	 		$count = YiiImgproduct::model()->count($criteria2g);
 	 		if($count > 0){
 	 			foreach ($YiiImgproduct as $valk){
 	 				echo '<tr><td><a target="_blank" href="/san-pham/'.$val['pr_url_news'].'.html">'.$val['tensp'].'</a></td><td><img src="'.
 		     Yii::app()->request->baseUrl.$valk->url_yii_imgproduct
 		     .'" width="100" height="100"/></td><td>'.$val['soluong'].'</td><td>'.number_format($val['gia'],0,"",".")
 		     .' đ</td><td>'.number_format($val['tonggiasp'],0,"",".").' đ</td></tr>';
 	 			};
 	 		}else{
 	 			echo '<tr><td><a target="_blank" href="/san-pham/'.$val['pr_url_news'].'.html">'.$val['tensp'].'</a></td><td><img src="'.
 	 					Yii::app()->request->baseUrl.'/images/home/images/nothumb.png'
 	 					.'" width="100" height="100"/></td><td>'.$val['soluong'].'</td><td>'.number_format($val['gia'],0,"",".")
 	 					.' đ</td><td>'.number_format($val['tonggiasp'],0,"",".").' đ</td></tr>';
 	 			
 	 		}
 	
 }
 echo '</table>';
 echo '<div id="moneys">'.
 		'<span>Tổng thành tiền</span> : </br>'.number_format($total,0,"",".").' đ </div>';
?>


<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'orders-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Trường dấu <span class="required">*</span> là bắt buộc.</p>
	<div class="row">
		<?php echo $form->labelEx($model,'ord_email'); ?>
		<?php echo $form->textField($model,'ord_email',array('size'=>60,'maxlength'=>150,'class'=>'pay form-control')); ?>
		<?php echo $form->error($model,'ord_email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ord_Gender'); ?>
		<?php echo $form->dropDownList($model, 'ord_Gender', array('1' => 'Nam', '0' => 'Nữ'),array("empty" => "---Chọn giới tính---",'class'=>'pay form-control')) ?>
		<?php echo $form->error($model,'ord_Gender'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ord_Dateofbirth'); ?>
		<?php echo $form->textField($model,'ord_Dateofbirth',array('class'=>'dates pay form-control')); ?><i class="fa fa-calendar"></i>(20-10-1986)		
		<?php echo $form->error($model,'ord_Dateofbirth'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ord_name'); ?>
		<?php echo $form->textField($model,'ord_name',array('size'=>60,'maxlength'=>150,'class'=>'pay form-control')); ?>
		<?php echo $form->error($model,'ord_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ord_from'); ?>
		<?php echo $form->textField($model,'ord_from',array('size'=>60,'maxlength'=>150,'class'=>'pay form-control')); ?>
		<?php echo $form->error($model,'ord_from'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ord_phone'); ?>
		<?php echo $form->textField($model,'ord_phone',array('size'=>60,'maxlength'=>150,'class'=>'pay form-control')); ?>
		<?php echo $form->error($model,'ord_phone'); ?>
	</div>

	<div id="captchas">
		   <?php $this->widget('CCaptcha',array(
			'buttonLabel' => 'Lấy mã',
			'clickableImage' => true,
			'imageOptions' => array('id' => 'captchaimg')
			)); ?>
		   <div>
		      <?php echo $form->textField($model,'code',array('size'=>60,'class' => 'lienhe_capcha form-control')); ?>
	      	
		   </div>
		   
		</div>
	<?php echo $form->error($model,'code'); ?>
	<div class="row buttons">
		<?php echo CHtml::submitButton('Đặt hàng'); ?>
		<?php echo CHtml::resetButton('Nhập lại');?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
