<?php

class YiiRoleController extends AdminController
{

	/**
	 * @return array action filters
	 */
	/* public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}
 */
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */

	public function accessRules()
	{
		$items2a = YiiRole::model()->findByPk(Yii::app()->user->getState('role'));
	
		if(!empty($items2a['type_role'])){
				
			$arrs = CJSON::decode($items2a['type_role'],true);
			 
			$a = '';
			foreach ($arrs as $val){
					
				if(in_array('YiiRole', $val)){
					$a .= implode('-',$val).'-';
				}
					
			};
			if(!empty($a)){
				$arrdsd = explode('-', $a);
				$arr1 = array_unique($arrdsd);
				array_shift($arr1);
				array_pop($arr1);
			}else{
				$arr1 = array('');
			}
		}else {
			$arr1 = array('');
		}
		//var_dump($arr1);die;
		return array(
	
				array('allow', // allow authenticated user to perform 'create' and 'update' actions
						'actions'=>$arr1,
						'users'=>array('*'),
				),
	
				array('deny',  // deny all users
						'users'=>array('*'),
				),
		);
	
	
	}
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new YiiRole;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['YiiRole']))
		{
		
			$Configure_view = !empty($_POST['Configure_view'])?$_POST['Configure_view']:null;			
			$Configure_create = !empty($_POST['Configure_create'])?$_POST['Configure_create']:null;
			$Configure_update = !empty($_POST['Configure_update'])?$_POST['Configure_update']:null;					
			
			$News_admin = !empty($_POST['News_admin'])?$_POST['News_admin']:null;
			$News_view = !empty($_POST['News_view'])?$_POST['News_view']:null;
			$News_create = !empty($_POST['News_create'])?$_POST['News_create']:null;
			$News_update = !empty($_POST['News_update'])?$_POST['News_update']:null;
			$News_delete = !empty($_POST['News_delete'])?$_POST['News_delete']:null;
			$News_ajaxUpdate = !empty($_POST['News_ajaxUpdate'])?$_POST['News_ajaxUpdate']:null;
				
			
			$Administrator_admin = !empty($_POST['Administrator_admin'])?$_POST['Administrator_admin']:null;
			$Administrator_view = !empty($_POST['Administrator_view'])?$_POST['Administrator_view']:null;
			$Administrator_create = !empty($_POST['Administrator_create'])?$_POST['Administrator_create']:null;
			$Administrator_update = !empty($_POST['Administrator_update'])?$_POST['Administrator_update']:null;
			$Administrator_delete = !empty($_POST['Administrator_delete'])?$_POST['Administrator_delete']:null;
			$Administrator_ajaxUpdate = !empty($_POST['Administrator_ajaxUpdate'])?$_POST['Administrator_ajaxUpdate']:null;
			
			$YiiRole_admin = !empty($_POST['YiiRole_admin'])?$_POST['YiiRole_admin']:null;
			$YiiRole_view = !empty($_POST['YiiRole_view'])?$_POST['YiiRole_view']:null;
			$YiiRole_create = !empty($_POST['YiiRole_create'])?$_POST['YiiRole_create']:null;
			$YiiRole_update = !empty($_POST['YiiRole_update'])?$_POST['YiiRole_update']:null;
			$YiiRole_delete = !empty($_POST['YiiRole_delete'])?$_POST['YiiRole_delete']:null;
			
			$Product_admin = !empty($_POST['Product_admin'])?$_POST['Product_admin']:null;
			$Product_view = !empty($_POST['Product_view'])?$_POST['Product_view']:null;
			$Product_create = !empty($_POST['Product_create'])?$_POST['Product_create']:null;
			$Product_update = !empty($_POST['Product_update'])?$_POST['Product_update']:null;
			$Product_delete = !empty($_POST['Product_delete'])?$_POST['Product_delete']:null;
			$Product_ajaxUpdate = !empty($_POST['Product_ajaxUpdate'])?$_POST['Product_ajaxUpdate']:null;
            
            $Comment_admin = !empty($_POST['Comment_admin'])?$_POST['Comment_admin']:null;
			$Comment_view = !empty($_POST['Comment_view'])?$_POST['Comment_view']:null;
			$Comment_create = !empty($_POST['Comment_create'])?$_POST['Comment_create']:null;
			$Comment_update = !empty($_POST['Comment_update'])?$_POST['Comment_update']:null;
			$Comment_delete = !empty($_POST['Comment_delete'])?$_POST['Comment_delete']:null;
			$Comment_ajaxUpdate = !empty($_POST['Comment_ajaxUpdate'])?$_POST['Comment_ajaxUpdate']:null;
            
            $Pasevent_admin = !empty($_POST['Pasevent_admin'])?$_POST['Pasevent_admin']:null;
			$Pasevent_view = !empty($_POST['Pasevent_view'])?$_POST['Pasevent_view']:null;
			$Pasevent_create = !empty($_POST['Pasevent_create'])?$_POST['Pasevent_create']:null;
			$Pasevent_update = !empty($_POST['Pasevent_update'])?$_POST['Pasevent_update']:null;
			$Pasevent_delete = !empty($_POST['Pasevent_delete'])?$_POST['Pasevent_delete']:null;
			$Pasevent_ajaxUpdate = !empty($_POST['Pasevent_ajaxUpdate'])?$_POST['Pasevent_ajaxUpdate']:null;
			
			$Advertise_admin = !empty($_POST['Advertise_admin'])?$_POST['Advertise_admin']:null;
			$Advertise_view = !empty($_POST['Advertise_view'])?$_POST['Advertise_view']:null;
			$Advertise_create = !empty($_POST['Advertise_create'])?$_POST['Advertise_create']:null;
			$Advertise_update = !empty($_POST['Advertise_update'])?$_POST['Advertise_update']:null;
			$Advertise_delete = !empty($_POST['Advertise_delete'])?$_POST['Advertise_delete']:null;
			$Advertise_ajaxUpdate = !empty($_POST['Advertise_ajaxUpdate'])?$_POST['Advertise_ajaxUpdate']:null;
			
			$YiiMenu_admin = !empty($_POST['YiiMenu_admin'])?$_POST['YiiMenu_admin']:null;
			$YiiMenu_view = !empty($_POST['YiiMenu_view'])?$_POST['YiiMenu_view']:null;
			$YiiMenu_create = !empty($_POST['YiiMenu_create'])?$_POST['YiiMenu_create']:null;
			$YiiMenu_update = !empty($_POST['YiiMenu_update'])?$_POST['YiiMenu_update']:null;
			$YiiMenu_delete = !empty($_POST['YiiMenu_delete'])?$_POST['YiiMenu_delete']:null;
			$YiiMenu_ajaxUpdate = !empty($_POST['YiiMenu_ajaxUpdate'])?$_POST['YiiMenu_ajaxUpdate']:null;
			$YiiMenu_updateAjax = !empty($_POST['YiiMenu_updateAjax'])?$_POST['YiiMenu_updateAjax']:null;
				
			
			$Links_admin = !empty($_POST['Links_admin'])?$_POST['Links_admin']:null;
			$Links_view = !empty($_POST['Links_view'])?$_POST['Links_view']:null;
			$Links_create = !empty($_POST['Links_create'])?$_POST['Links_create']:null;
			$Links_update = !empty($_POST['Links_update'])?$_POST['Links_update']:null;
			$Links_delete = !empty($_POST['Links_delete'])?$_POST['Links_delete']:null;
			
			$Contact_admin = !empty($_POST['Contact_admin'])?$_POST['Contact_admin']:null;
			$Contact_view = !empty($_POST['Contact_view'])?$_POST['Contact_view']:null;
			$Contact_create = !empty($_POST['Contact_create'])?$_POST['Contact_create']:null;
			$Contact_update = !empty($_POST['Contact_update'])?$_POST['Contact_update']:null;
			$Contact_delete = !empty($_POST['Contact_delete'])?$_POST['Contact_delete']:null;
			$Contact_ajaxUpdate = !empty($_POST['Contact_ajaxUpdate'])?$_POST['Contact_ajaxUpdate']:null;
            
            $Register_admin = !empty($_POST['Register_admin'])?$_POST['Register_admin']:null;
			$Register_view = !empty($_POST['Register_view'])?$_POST['Register_view']:null;
			$Register_create = !empty($_POST['Register_create'])?$_POST['Register_create']:null;
			$Register_update = !empty($_POST['Register_update'])?$_POST['Register_update']:null;
			$Register_delete = !empty($_POST['Register_delete'])?$_POST['Register_delete']:null;
			$Register_ajaxUpdate = !empty($_POST['Register_ajaxUpdate'])?$_POST['Register_ajaxUpdate']:null;
            
            
            $Downloadyii_admin = !empty($_POST['Downloadyii_admin'])?$_POST['Downloadyii_admin']:null;
			$Downloadyii_view = !empty($_POST['Downloadyii_view'])?$_POST['Downloadyii_view']:null;
			$Downloadyii_create = !empty($_POST['Downloadyii_create'])?$_POST['Downloadyii_create']:null;
			$Downloadyii_update = !empty($_POST['Downloadyii_update'])?$_POST['Downloadyii_update']:null;
			$Downloadyii_delete = !empty($_POST['Downloadyii_delete'])?$_POST['Downloadyii_delete']:null;
			$Downloadyii_ajaxUpdate = !empty($_POST['Downloadyii_ajaxUpdate'])?$_POST['Downloadyii_ajaxUpdate']:null;
            
			
			$Orders_admin = !empty($_POST['Orders_admin'])?$_POST['Orders_admin']:null;
			$Orders_view = !empty($_POST['Orders_view'])?$_POST['Orders_view']:null;
			$Orders_create = !empty($_POST['Orders_create'])?$_POST['Orders_create']:null;
			$Orders_update = !empty($_POST['Orders_update'])?$_POST['Orders_update']:null;
			$Orders_delete = !empty($_POST['Orders_delete'])?$_POST['Orders_delete']:null;
			$Orders_ajaxUpdate = !empty($_POST['Orders_ajaxUpdate'])?$_POST['Orders_ajaxUpdate']:null;
			$Orders_StatusOrder = !empty($_POST['Orders_StatusOrder'])?$_POST['Orders_StatusOrder']:null;
				
			$YiiTypealbum_admin = !empty($_POST['YiiTypealbum_admin'])?$_POST['YiiTypealbum_admin']:null;
			$YiiTypealbum_create = !empty($_POST['YiiTypealbum_create'])?$_POST['YiiTypealbum_create']:null;
			$YiiTypealbum_update = !empty($_POST['YiiTypealbum_update'])?$_POST['YiiTypealbum_update']:null;
			$YiiTypealbum_delete = !empty($_POST['YiiTypealbum_delete'])?$_POST['YiiTypealbum_delete']:null;
            $YiiTypealbum_ajaxUpdate = !empty($_POST['YiiTypealbum_ajaxUpdate'])?$_POST['YiiTypealbum_ajaxUpdate']:null;
			
			$YiiTypevideo_admin = !empty($_POST['YiiTypevideo_admin'])?$_POST['YiiTypevideo_admin']:null;
			$YiiTypevideo_create = !empty($_POST['YiiTypevideo_create'])?$_POST['YiiTypevideo_create']:null;
			$YiiTypevideo_update = !empty($_POST['YiiTypevideo_update'])?$_POST['YiiTypevideo_update']:null;
			$YiiTypevideo_delete = !empty($_POST['YiiTypevideo_delete'])?$_POST['YiiTypevideo_delete']:null;
			
			$YiiVideo_admin = !empty($_POST['YiiVideo_admin'])?$_POST['YiiVideo_admin']:null;
			$YiiVideo_view = !empty($_POST['YiiVideo_view'])?$_POST['YiiVideo_view']:null;
			$YiiVideo_create = !empty($_POST['YiiVideo_create'])?$_POST['YiiVideo_create']:null;
			$YiiVideo_update = !empty($_POST['YiiVideo_update'])?$_POST['YiiVideo_update']:null;
			$YiiVideo_delete = !empty($_POST['YiiVideo_delete'])?$_POST['YiiVideo_delete']:null;
			
            $YiiAlbum_admin = !empty($_POST['YiiAlbum_admin'])?$_POST['YiiAlbum_admin']:null;
            $YiiAlbum_create = !empty($_POST['YiiAlbum_create'])?$_POST['YiiAlbum_create']:null;
			$YiiAlbum_update = !empty($_POST['YiiAlbum_update'])?$_POST['YiiAlbum_update']:null;
			$YiiAlbum_delete = !empty($_POST['YiiAlbum_delete'])?$_POST['YiiAlbum_delete']:null;
			
if(empty($Configure_view) && empty($Configure_create)
        && empty($YiiAlbum_admin)
        && empty($YiiAlbum_create)
		&& empty($YiiAlbum_update)
		&& empty($YiiAlbum_delete)		
		&& empty($YiiVideo_admin)
		&& empty($YiiVideo_view)
		&& empty($YiiVideo_create)
		&& empty($YiiVideo_update)
		&& empty($YiiVideo_delete)		
		&& empty($YiiTypevideo_admin)
		&& empty($YiiTypevideo_create)
		&& empty($YiiTypevideo_update)
		&& empty($YiiTypevideo_delete)	
		&& empty($YiiTypealbum_admin)
		&& empty($YiiTypealbum_create)
		&& empty($YiiTypealbum_update)
		&& empty($YiiTypealbum_delete)		
		&& empty($Administrator_ajaxUpdate)
		&& empty($Pasevent_ajaxUpdate)
		&& empty($News_ajaxUpdate)
		&& empty($Orders_StatusOrder)
		&& empty($Orders_ajaxUpdate)
		&& empty($Advertise_ajaxUpdate)
		&& empty($Configure_update)
		&& empty($News_create)
		&& empty($News_update)
		&& empty($News_delete)
		&& empty($News_admin)
		&& empty($News_view)
		&& empty($Administrator_admin)
		&& empty($Administrator_view)
		&& empty($Administrator_create)
		&& empty($Administrator_update)
		&& empty($Administrator_delete)
		&& empty($YiiRole_admin)
		&& empty($YiiRole_view)
		&& empty($YiiRole_create)
		&& empty($YiiRole_update)
		&& empty($YiiRole_delete)
		&& empty($Product_admin)
		&& empty($Product_view)
		&& empty($Product_create)
		&& empty($Product_update)
		&& empty($Product_delete)
        && empty($Product_ajaxUpdate)
        && empty($Comment_admin)
		&& empty($Comment_view)
		&& empty($Comment_create)
		&& empty($Comment_update)
		&& empty($Comment_delete)
        && empty($Comment_ajaxUpdate)
        && empty($Pasevent_admin)
		&& empty($Pasevent_view)
		&& empty($Pasevent_create)
		&& empty($Pasevent_update)
		&& empty($Pasevent_delete)
		&& empty($Advertise_admin)
		&& empty($Advertise_view)
		&& empty($Advertise_create)
		&& empty($Advertise_update)
		&& empty($Advertise_delete)
		&& empty($YiiMenu_admin)
		&& empty($YiiMenu_view)
		&& empty($YiiMenu_update)
		&& empty($YiiMenu_create)
		&& empty($YiiMenu_delete)
		&& empty($Links_admin)
		&& empty($Links_view)
		&& empty($Links_create)
		&& empty($Links_update)
		&& empty($Links_delete)
		&& empty($Contact_admin)
		&& empty($Contact_view)
		&& empty($Contact_create)
		&& empty($Contact_update)
		&& empty($Contact_delete)
        && empty($Contact_ajaxUpdate)
        && empty($Register_admin)
		&& empty($Register_view)
		&& empty($Register_create)
		&& empty($Register_update)
		&& empty($Register_delete)
        && empty($Register_ajaxUpdate)
        && empty($Downloadyii_admin)
		&& empty($Downloadyii_view)
		&& empty($Downloadyii_create)
		&& empty($Downloadyii_update)
		&& empty($Downloadyii_delete)
        && empty($Downloadyii_ajaxUpdate)
       	&& empty($Orders_admin)
		&& empty($Orders_view)
		&& empty($Orders_create)
		&& empty($$Orders_update)
		&& empty($Orders_delete)
		&& empty($YiiMenu_ajaxUpdate)
		&& empty($YiiMenu_updateAjax)
		){
 $roles=null;
}
else {
$str = $Configure_view.','.$Configure_create.','.$Configure_update.','.$News_create.','.$News_update.','.$News_delete.','.$News_admin.','.$News_view.',';
$str .= $Administrator_admin.','.$Administrator_view.','.$Administrator_create.','.$Administrator_update.','.$Administrator_delete.','.$YiiRole_admin.','.$YiiRole_view.','.$YiiRole_create.',';
$str .= $YiiRole_update.','.$YiiRole_delete.','.$Product_admin.','.$Product_view.','.$Product_create.','.$Product_update.','.$Product_delete.','.$Advertise_admin.',';
$str .= $Comment_admin.','.$Comment_view.','.$Comment_create.','.$Comment_update.','.$Comment_delete.',';
$str .= $Pasevent_admin.','.$Pasevent_view.','.$Pasevent_create.','.$Pasevent_update.','.$Pasevent_delete.',';
$str .= $Advertise_view.','.$Advertise_create.','.$Advertise_update.','.$Advertise_delete.','.$YiiMenu_admin.','.$YiiMenu_view.','.$YiiMenu_create.','.$YiiMenu_update.',';
$str .= $YiiMenu_delete.','.$Links_admin.','.$Links_view.','.$Links_create.','.$Links_update.','.$Links_delete.','.$Contact_admin.','.$Contact_view.',';
$str .= $Contact_create.','.$Contact_update.','.$Contact_delete.','.$Orders_admin.','.$Orders_view.','.$Orders_create.','.$Orders_update.','.$Orders_delete.','.$YiiMenu_ajaxUpdate.','.$YiiMenu_updateAjax.',';
$str .= $Contact_ajaxUpdate.','.$Advertise_ajaxUpdate.','.$Orders_ajaxUpdate.','.$Orders_StatusOrder.','.$News_ajaxUpdate.','.$Product_ajaxUpdate.','.$Administrator_ajaxUpdate.',';
$str .= $Register_create.','.$Register_update.','.$Register_delete.','.$Register_admin.','.$Register_view.','.$Register_ajaxUpdate.',';
$str .= $Downloadyii_create.','.$Downloadyii_update.','.$Downloadyii_delete.','.$Downloadyii_admin.','.$Downloadyii_view.','.$Downloadyii_ajaxUpdate.',';
$str .= $YiiTypealbum_admin.','.$YiiTypealbum_create.','.$YiiTypealbum_update.','.$YiiTypealbum_delete.',';
$str .= $YiiTypevideo_admin.','.$YiiTypevideo_create.','.$YiiTypevideo_update.','.$YiiTypevideo_delete.',';
$str .= $YiiVideo_admin.','.$YiiVideo_view.','.$YiiVideo_create.','.$YiiVideo_update.','.$YiiVideo_delete.',';
$str .= $YiiAlbum_admin.','.$YiiAlbum_create.','.$YiiAlbum_update.','.$YiiAlbum_delete;
$arr = explode(',', $str);
			$arrs =array();
			foreach ($arr as $val){
				$arrs[] = explode('_', $val);
			}
			$roles = CJSON::encode($arrs);

}

			$model->attributes=$_POST['YiiRole'];
			$model->type_role=$roles;
			if($model->save())
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		if(!empty($model)){
			if(!empty($model->type_role)){
				$arrs = CJSON::decode($model->type_role,true);
				$a = array();
				foreach ($arrs as $val){
						
					$a[] = implode('_', $val);
						
				};
				$b = array();
				foreach ($a as $val){
					$b[$val] = $val;
				};
				array_unshift($b, $model->name_role);
				$c = array();
				foreach ($b as $key=>$val){
					if($key == '0')
						$c['model'] = $model;
						else 
							$c[$key] = $val;
				 }
			}else{
				$c = array();			
				$c['model'] = $model;
				
			}
		}

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['YiiRole']))
		{
		    $YiiAlbum_admin = !empty($_POST['YiiAlbum_admin'])?$_POST['YiiAlbum_admin']:null;
            $YiiAlbum_create = !empty($_POST['YiiAlbum_create'])?$_POST['YiiAlbum_create']:null;  
			$YiiAlbum_update = !empty($_POST['YiiAlbum_update'])?$_POST['YiiAlbum_update']:null;
			$YiiAlbum_delete = !empty($_POST['YiiAlbum_delete'])?$_POST['YiiAlbum_delete']:null;
			
			$YiiVideo_admin = !empty($_POST['YiiVideo_admin'])?$_POST['YiiVideo_admin']:null;
			$YiiVideo_view = !empty($_POST['YiiVideo_view'])?$_POST['YiiVideo_view']:null;
			$YiiVideo_create = !empty($_POST['YiiVideo_create'])?$_POST['YiiVideo_create']:null;
			$YiiVideo_update = !empty($_POST['YiiVideo_update'])?$_POST['YiiVideo_update']:null;
			$YiiVideo_delete = !empty($_POST['YiiVideo_delete'])?$_POST['YiiVideo_delete']:null;
			
			$YiiTypevideo_admin = !empty($_POST['YiiTypevideo_admin'])?$_POST['YiiTypevideo_admin']:null;
			$YiiTypevideo_create = !empty($_POST['YiiTypevideo_create'])?$_POST['YiiTypevideo_create']:null;
			$YiiTypevideo_update = !empty($_POST['YiiTypevideo_update'])?$_POST['YiiTypevideo_update']:null;
			$YiiTypevideo_delete = !empty($_POST['YiiTypevideo_delete'])?$_POST['YiiTypevideo_delete']:null;
			
			$YiiTypealbum_admin = !empty($_POST['YiiTypealbum_admin'])?$_POST['YiiTypealbum_admin']:null;
			$YiiTypealbum_create = !empty($_POST['YiiTypealbum_create'])?$_POST['YiiTypealbum_create']:null;
			$YiiTypealbum_update = !empty($_POST['YiiTypealbum_update'])?$_POST['YiiTypealbum_update']:null;
			$YiiTypealbum_delete = !empty($_POST['YiiTypealbum_delete'])?$_POST['YiiTypealbum_delete']:null;
			
			$Configure_view = !empty($_POST['Configure_view'])?$_POST['Configure_view']:null;
			$Configure_create = !empty($_POST['Configure_create'])?$_POST['Configure_create']:null;
			$Configure_update = !empty($_POST['Configure_update'])?$_POST['Configure_update']:null;
				
			$News_admin = !empty($_POST['News_admin'])?$_POST['News_admin']:null;
			$News_view = !empty($_POST['News_view'])?$_POST['News_view']:null;
			$News_create = !empty($_POST['News_create'])?$_POST['News_create']:null;
			$News_update = !empty($_POST['News_update'])?$_POST['News_update']:null;
			$News_delete = !empty($_POST['News_delete'])?$_POST['News_delete']:null;
			$News_ajaxUpdate = !empty($_POST['News_ajaxUpdate'])?$_POST['News_ajaxUpdate']:null;
				
			$Administrator_admin = !empty($_POST['Administrator_admin'])?$_POST['Administrator_admin']:null;
			$Administrator_view = !empty($_POST['Administrator_view'])?$_POST['Administrator_view']:null;
			$Administrator_create = !empty($_POST['Administrator_create'])?$_POST['Administrator_create']:null;
			$Administrator_update = !empty($_POST['Administrator_update'])?$_POST['Administrator_update']:null;
			$Administrator_delete = !empty($_POST['Administrator_delete'])?$_POST['Administrator_delete']:null;
			$Administrator_ajaxUpdate = !empty($_POST['Administrator_ajaxUpdate'])?$_POST['Administrator_ajaxUpdate']:null;
				
			$YiiRole_admin = !empty($_POST['YiiRole_admin'])?$_POST['YiiRole_admin']:null;
			$YiiRole_view = !empty($_POST['YiiRole_view'])?$_POST['YiiRole_view']:null;
			$YiiRole_create = !empty($_POST['YiiRole_create'])?$_POST['YiiRole_create']:null;
			$YiiRole_update = !empty($_POST['YiiRole_update'])?$_POST['YiiRole_update']:null;
			$YiiRole_delete = !empty($_POST['YiiRole_delete'])?$_POST['YiiRole_delete']:null;
				
			$Product_admin = !empty($_POST['Product_admin'])?$_POST['Product_admin']:null;
			$Product_view = !empty($_POST['Product_view'])?$_POST['Product_view']:null;
			$Product_create = !empty($_POST['Product_create'])?$_POST['Product_create']:null;
			$Product_update = !empty($_POST['Product_update'])?$_POST['Product_update']:null;
			$Product_delete = !empty($_POST['Product_delete'])?$_POST['Product_delete']:null;
			$Product_ajaxUpdate = !empty($_POST['Product_ajaxUpdate'])?$_POST['Product_ajaxUpdate']:null;
            
            $Comment_admin = !empty($_POST['Comment_admin'])?$_POST['Comment_admin']:null;
			$Comment_view = !empty($_POST['Comment_view'])?$_POST['Comment_view']:null;
			$Comment_create = !empty($_POST['Comment_create'])?$_POST['Comment_create']:null;
			$Comment_update = !empty($_POST['Comment_update'])?$_POST['Comment_update']:null;
			$Comment_delete = !empty($_POST['Comment_delete'])?$_POST['Comment_delete']:null;
			$Comment_ajaxUpdate = !empty($_POST['Comment_ajaxUpdate'])?$_POST['Comment_ajaxUpdate']:null;
            
            $Pasevent_admin = !empty($_POST['Pasevent_admin'])?$_POST['Pasevent_admin']:null;
			$Pasevent_view = !empty($_POST['Pasevent_view'])?$_POST['Pasevent_view']:null;
			$Pasevent_create = !empty($_POST['Pasevent_create'])?$_POST['Pasevent_create']:null;
			$Pasevent_update = !empty($_POST['Pasevent_update'])?$_POST['Pasevent_update']:null;
			$Pasevent_delete = !empty($_POST['Pasevent_delete'])?$_POST['Pasevent_delete']:null;
			$Pasevent_ajaxUpdate = !empty($_POST['Pasevent_ajaxUpdate'])?$_POST['Pasevent_ajaxUpdate']:null;
				
			$Advertise_admin = !empty($_POST['Advertise_admin'])?$_POST['Advertise_admin']:null;
			$Advertise_view = !empty($_POST['Advertise_view'])?$_POST['Advertise_view']:null;
			$Advertise_create = !empty($_POST['Advertise_create'])?$_POST['Advertise_create']:null;
			$Advertise_update = !empty($_POST['Advertise_update'])?$_POST['Advertise_update']:null;
			$Advertise_delete = !empty($_POST['Advertise_delete'])?$_POST['Advertise_delete']:null;
			$Advertise_ajaxUpdate = !empty($_POST['Advertise_ajaxUpdate'])?$_POST['Advertise_ajaxUpdate']:null;
				
				
			$YiiMenu_admin = !empty($_POST['YiiMenu_admin'])?$_POST['YiiMenu_admin']:null;
			$YiiMenu_view = !empty($_POST['YiiMenu_view'])?$_POST['YiiMenu_view']:null;
			$YiiMenu_create = !empty($_POST['YiiMenu_create'])?$_POST['YiiMenu_create']:null;
			$YiiMenu_update = !empty($_POST['YiiMenu_update'])?$_POST['YiiMenu_update']:null;
			$YiiMenu_delete = !empty($_POST['YiiMenu_delete'])?$_POST['YiiMenu_delete']:null;
			$YiiMenu_ajaxUpdate = !empty($_POST['YiiMenu_ajaxUpdate'])?$_POST['YiiMenu_ajaxUpdate']:null;
			$YiiMenu_updateAjax = !empty($_POST['YiiMenu_updateAjax'])?$_POST['YiiMenu_updateAjax']:null;
				
			$Links_admin = !empty($_POST['Links_admin'])?$_POST['Links_admin']:null;
			$Links_view = !empty($_POST['Links_view'])?$_POST['Links_view']:null;
			$Links_create = !empty($_POST['Links_create'])?$_POST['Links_create']:null;
			$Links_update = !empty($_POST['Links_update'])?$_POST['Links_update']:null;
			$Links_delete = !empty($_POST['Links_delete'])?$_POST['Links_delete']:null;
				
			$Contact_admin = !empty($_POST['Contact_admin'])?$_POST['Contact_admin']:null;
			$Contact_view = !empty($_POST['Contact_view'])?$_POST['Contact_view']:null;
			$Contact_create = !empty($_POST['Contact_create'])?$_POST['Contact_create']:null;
			$Contact_update = !empty($_POST['Contact_update'])?$_POST['Contact_update']:null;
			$Contact_delete = !empty($_POST['Contact_delete'])?$_POST['Contact_delete']:null;
			$Contact_ajaxUpdate = !empty($_POST['Contact_ajaxUpdate'])?$_POST['Contact_ajaxUpdate']:null;
				
			$Register_admin = !empty($_POST['Register_admin'])?$_POST['Register_admin']:null;
			$Register_view = !empty($_POST['Register_view'])?$_POST['Register_view']:null;
			$Register_create = !empty($_POST['Register_create'])?$_POST['Register_create']:null;
			$Register_update = !empty($_POST['Register_update'])?$_POST['Register_update']:null;
			$Register_delete = !empty($_POST['Register_delete'])?$_POST['Register_delete']:null;
			$Register_ajaxUpdate = !empty($_POST['Register_ajaxUpdate'])?$_POST['Register_ajaxUpdate']:null;
            
            $Downloadyii_admin = !empty($_POST['Downloadyii_admin'])?$_POST['Downloadyii_admin']:null;
			$Downloadyii_view = !empty($_POST['Downloadyii_view'])?$_POST['Downloadyii_view']:null;
			$Downloadyii_create = !empty($_POST['Downloadyii_create'])?$_POST['Downloadyii_create']:null;
			$Downloadyii_update = !empty($_POST['Downloadyii_update'])?$_POST['Downloadyii_update']:null;
			$Downloadyii_delete = !empty($_POST['Downloadyii_delete'])?$_POST['Downloadyii_delete']:null;
			$Downloadyii_ajaxUpdate = !empty($_POST['Downloadyii_ajaxUpdate'])?$_POST['Downloadyii_ajaxUpdate']:null;
            
			$Orders_admin = !empty($_POST['Orders_admin'])?$_POST['Orders_admin']:null;
			$Orders_view = !empty($_POST['Orders_view'])?$_POST['Orders_view']:null;
			$Orders_create = !empty($_POST['Orders_create'])?$_POST['Orders_create']:null;
			$Orders_update = !empty($_POST['Orders_update'])?$_POST['Orders_update']:null;
			$Orders_delete = !empty($_POST['Orders_delete'])?$_POST['Orders_delete']:null;
			$Orders_ajaxUpdate = !empty($_POST['Orders_ajaxUpdate'])?$_POST['Orders_ajaxUpdate']:null;
			$Orders_StatusOrder = !empty($_POST['Orders_StatusOrder'])?$_POST['Orders_StatusOrder']:null;
				
				
			if(empty($Configure_view) && empty($Configure_create)
					&& empty($YiiAlbum_admin)
                    && empty($YiiAlbum_create)
                    && empty($YiiAlbum_update)
					&& empty($YiiAlbum_delete)
					&& empty($YiiVideo_admin)
					&& empty($YiiVideo_view)
					&& empty($YiiVideo_create)
					&& empty($YiiVideo_update)
					&& empty($YiiVideo_delete)
					&& empty($YiiTypevideo_admin)
					&& empty($YiiTypevideo_create)
					&& empty($YiiTypevideo_update)
					&& empty($YiiTypevideo_delete)
					&& empty($YiiTypealbum_admin)
					&& empty($YiiTypealbum_create)
					&& empty($YiiTypealbum_update)
					&& empty($YiiTypealbum_delete)
					&& empty($Administrator_ajaxUpdate)
					&& empty($News_ajaxUpdate)
					&& empty($Orders_StatusOrder)
					&& empty($Orders_ajaxUpdate)
					&& empty($Advertise_ajaxUpdate)
					&& empty($Contact_ajaxUpdate)						
					&& empty($Configure_update)
					&& empty($News_create)
					&& empty($News_update)
					&& empty($News_delete)
					&& empty($News_admin)
					&& empty($News_view)
					&& empty($Administrator_admin)
					&& empty($Administrator_view)
					&& empty($Administrator_create)
					&& empty($Administrator_update)
					&& empty($Administrator_delete)
					&& empty($YiiRole_admin)
					&& empty($YiiRole_view)
					&& empty($YiiRole_create)
					&& empty($YiiRole_update)
					&& empty($YiiRole_delete)
					&& empty($Product_admin)
					&& empty($Product_view)
					&& empty($Product_create)
					&& empty($Product_update)
					&& empty($Product_delete)
                    && empty($Product_ajaxUpdate)
                    && empty($Comment_admin)
					&& empty($Comment_view)
					&& empty($Comment_create)
					&& empty($Comment_update)
					&& empty($Comment_delete)
                    && empty($Comment_ajaxUpdate)
                    && empty($Pasevent_admin)
					&& empty($Pasevent_view)
					&& empty($Pasevent_create)
					&& empty($Pasevent_update)
					&& empty($Pasevent_delete)
                    && empty($Pasevent_ajaxUpdate)
                   	&& empty($Advertise_admin)
					&& empty($Advertise_view)
					&& empty($Advertise_create)
					&& empty($Advertise_update)
					&& empty($Advertise_delete)
					&& empty($YiiMenu_admin)
					&& empty($YiiMenu_view)
					&& empty($YiiMenu_update)
					&& empty($YiiMenu_create)
					&& empty($YiiMenu_delete)
					&& empty($Links_admin)
					&& empty($Links_view)
					&& empty($Links_create)
					&& empty($Links_update)
					&& empty($Links_delete)
					&& empty($Contact_admin)
					&& empty($Contact_view)
					&& empty($Contact_create)
					&& empty($Contact_update)
					&& empty($Contact_delete)
                    && empty($Register_admin)
            		&& empty($Register_view)
            		&& empty($Register_create)
            		&& empty($Register_update)
            		&& empty($Register_delete)
                    && empty($Register_ajaxUpdate)
                    && empty($Downloadyii_admin)
            		&& empty($Downloadyii_view)
            		&& empty($Downloadyii_create)
            		&& empty($Downloadyii_update)
            		&& empty($Downloadyii_delete)
                    && empty($Downloadyii_ajaxUpdate)
                   	&& empty($Orders_admin)
					&& empty($Orders_view)
					&& empty($Orders_create)
					&& empty($$Orders_update)
					&& empty($Orders_delete)
					&& empty($YiiMenu_ajaxUpdate)
					&& empty($YiiMenu_updateAjax)
			){
				$roles=null;
			}
			else {
				$str = $Configure_view.','.$Configure_create.','.$Configure_update.','.$News_create.','.$News_update.','.$News_delete.','.$News_admin.','.$News_view.',';
				$str .= $Administrator_admin.','.$Administrator_view.','.$Administrator_create.','.$Administrator_update.','.$Administrator_delete.','.$YiiRole_admin.','.$YiiRole_view.','.$YiiRole_create.',';
				$str .= $YiiRole_update.','.$YiiRole_delete.','.$Product_admin.','.$Product_view.','.$Product_create.','.$Product_update.','.$Product_delete.','.$Advertise_admin.',';
                $str .= $Comment_admin.','.$Comment_view.','.$Comment_create.','.$Comment_update.','.$Comment_delete.',';
                $str .= $Pasevent_admin.','.$Pasevent_view.','.$Pasevent_create.','.$Pasevent_update.','.$Pasevent_delete.',';
				$str .= $Advertise_view.','.$Advertise_create.','.$Advertise_update.','.$Advertise_delete.','.$YiiMenu_admin.','.$YiiMenu_view.','.$YiiMenu_create.','.$YiiMenu_update.',';
				$str .= $YiiMenu_delete.','.$Links_admin.','.$Links_view.','.$Links_create.','.$Links_update.','.$Links_delete.','.$Contact_admin.','.$Contact_view.',';
				$str .= $Contact_create.','.$Contact_update.','.$Contact_delete.','.$Orders_admin.','.$Orders_view.','.$Orders_create.','.$Orders_update.','.$Orders_delete.','.$YiiMenu_ajaxUpdate.','.$YiiMenu_updateAjax.',';
				$str .= $Contact_ajaxUpdate.','.$Advertise_ajaxUpdate.','.$Orders_ajaxUpdate.','.$Orders_StatusOrder.','.$News_ajaxUpdate.','.$Product_ajaxUpdate.','.$Administrator_ajaxUpdate.',';
				$str .= $Register_create.','.$Register_update.','.$Register_delete.','.$Register_admin.','.$Register_view.','.$Register_ajaxUpdate.',';
                $str .= $Downloadyii_create.','.$Downloadyii_update.','.$Downloadyii_delete.','.$Downloadyii_admin.','.$Downloadyii_view.','.$Downloadyii_ajaxUpdate.',';
                $str .= $YiiTypealbum_admin.','.$YiiTypealbum_create.','.$YiiTypealbum_update.','.$YiiTypealbum_delete.',';
				$str .= $YiiTypevideo_admin.','.$YiiTypevideo_create.','.$YiiTypevideo_update.','.$YiiTypevideo_delete.',';
				$str .= $YiiVideo_admin.','.$YiiVideo_view.','.$YiiVideo_create.','.$YiiVideo_update.','.$YiiVideo_delete.',';
				$str .= $YiiAlbum_admin.','.$YiiAlbum_create.','.$YiiAlbum_update.','.$YiiAlbum_delete;
				$arr = explode(',', $str);
				$arrs =array();
				foreach ($arr as $val){
					$arrs[] = explode('_', $val);
				}
				$roles = CJSON::encode($arrs);
			
			}
						
			$model->attributes=$_POST['YiiRole'];
			$model->type_role=$roles;
			if($model->save())
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}

		$this->render('update',$c);
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('YiiRole');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new YiiRole('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['YiiRole']))
			$model->attributes=$_GET['YiiRole'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return YiiRole the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=YiiRole::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param YiiRole $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='yii-role-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
