<?php

/**
 * This is the model class for table "yii_menu".
 *
 * The followings are the available columns in table 'yii_menu':
 * @property integer $id_menu
 * @property string $name_menu_en
 * @property integer $showhide_menu
 * @property integer $location_menu
 * @property integer $level_menu
 * @property integer $order_menu
 * @property integer $from_menu
 * @property integer $newwindow_mennu
 */
class YiiMenu extends CActiveRecord
{
	public $menu_img_represent;
	public $menu_img_bg;
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'yii_menu';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{

		return array(
			array('name_menu_en,order_menu','required'),
			array('location_menu','my_required'),
				
			array('order_menu,showhide_menu,newwindow_mennu,level_menu,home_menu,highlights_menu,lev,bntb,h_rightvd,listmenus', 'numerical', 'integerOnly'=>true),
			array('order_menu',$this->isNewRecord ?'ordermenu':'required'),
			array('name_menu_en,name_menu_fr,name_menu_sp,url_menu,title_seo_menu,description_seo_menu,keywords_seo,summary_en,summary_fr,summary_sp', 'length', 'max'=>100000),
			
				array('img_represent', 'file',
						//'allowEmpty'=>$this->isNewRecord ?false:true,
						'allowEmpty'=>true,
						'types'=>'jpg, gif, png',
						'wrongType'=>'Tập tin không hợp lệ',
						'maxSize'=>5 * 1024 * 1024,
						'tooLarge'=>'Dung lượng file < 5MB. Hãy tải lên một tập tin nhỏ hơn.',//Error Message
				),
				
				array('img_background', 'file',
						//'allowEmpty'=>$this->isNewRecord ?false:true,
						'allowEmpty'=>true,
						'types'=>'jpg, gif, png',
						'wrongType'=>'Tập tin không hợp lệ',
						'maxSize'=>5 * 1024 * 1024,
						'tooLarge'=>'Dung lượng file < 5MB. Hãy tải lên một tập tin nhỏ hơn.',//Error Message
				),
				
			array('highlights_menu,id_menu, name_menu_en,rewrite_url_menu,url_menu, showhide_menu, location_menu, level_menu, order_menu,  newwindow_mennu,home_menu', 'safe', 'on'=>'search'),
		);
	}

	public function my_required($attribute_name,$params){
		
		 if(empty($this->location_menu)){
			$this->addError('location_menu',
					'Vui lòng chọn vị trí');
		}

	}

	public function ordermenu($attribute_name,$params){
	
		$Criteria = new CDbCriteria();
		$Criteria->condition = "order_menu='".$this->order_menu."'";
		//$model=Posts::model()->findByPk($id, $Criteria);
		$model=$this->model()->findAll($Criteria);
		if(count($model))
			$this->addError('order_menu',
					'Vị trí đã tồn tại');
	}
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_menu' => 'Id',
			  'name_menu_en' => 'Title',
               'name_menu_fr' => 'Title French',
                'name_menu_sp' => 'Title Spanish',
                
                'showhide_menu' => 'showhide',
                'location_menu' => 'location',
                'order_menu' =>'order',
                'newwindow_mennu' => 'Cửa sổ mới',
                'home_menu' => 'Show home',
                'rewrite_url_menu' => 'Rewrite url',
                'url_menu' => 'Url',
                
                'h_rightvd' => 'Home 2 (Cat new)',
				'title_seo_menu' => 'Tiêu đề Seo',
				'description_seo_menu' => 'Miêu tả Seo',
				'keywords_seo' => 'Từ khóa Seo',
				'highlights_menu' => 'Bottom',
				'img_represent' => 'Ảnh danh sách',
				'img_background' =>'Ảnh menu',
                'lev' =>'Past event big',
                'bntb' => 'Bottom 2',
				'summary_en'=>'Summary en',
				'summary_fr'=>'Summary fr',
				'summary_sp'=>'Summary sp',
				'listmenus'=>'List menu'
		);
	}

		
	protected function beforeSave()
	{		
		$this->menu_img_represent = CUploadedFile::getInstance($this, 'img_represent');
		if (isset($this->menu_img_represent)) {
			if(!$this->isNewRecord){
				if(!empty($this->img_represent)){
					if (file_exists(getcwd().'/'.$this->img_represent)) {
						unlink(getcwd().'/'.$this->img_represent);
					};
				}
			}
			$fileName     = rand().$this->menu_img_represent->name;
			$uploadFolder = Yii::getPathOfAlias('webroot') . DIRECTORY_SEPARATOR . 'upload' . DIRECTORY_SEPARATOR .'imgdaidienmenu'.DIRECTORY_SEPARATOR . date('Y-m-d');
			if (!is_dir($uploadFolder)) {
				mkdir($uploadFolder, 0755, TRUE);
			}
			$uploadFile = $uploadFolder . DIRECTORY_SEPARATOR . $fileName;
			$this->menu_img_represent->saveAs($uploadFile); //Upload file thong qua object CUploadedFile
			$this->img_represent = '/upload/imgdaidienmenu/' . date('Y-m-d') . '/'. $fileName; //Lưu path vào csdl
		}
		
		$this->menu_img_bg = CUploadedFile::getInstance($this, 'img_background');
		if (isset($this->menu_img_bg)) {
			if(!$this->isNewRecord){
				if(!empty($this->img_background)){
					if (file_exists(getcwd().'/'.$this->img_background)) {
						unlink(getcwd().'/'.$this->img_background);
					};
				}
			}
			$fileName     = rand().$this->menu_img_bg->name;
			$uploadFolder = Yii::getPathOfAlias('webroot') . DIRECTORY_SEPARATOR . 'upload' . DIRECTORY_SEPARATOR .'imgbgmenu'.DIRECTORY_SEPARATOR . date('Y-m-d');
			if (!is_dir($uploadFolder)) {
				mkdir($uploadFolder, 0755, TRUE);
			}
			$uploadFile = $uploadFolder . DIRECTORY_SEPARATOR . $fileName;
			$this->menu_img_bg->saveAs($uploadFile); //Upload file thong qua object CUploadedFile
			$this->img_background = '/upload/imgbgmenu/' . date('Y-m-d') . '/'. $fileName; //Lưu path vào csdl
		}
		
		return parent::beforeSave();
	}
	protected function beforeDelete() {
		if(!empty($this->new_img)){
			if (file_exists(getcwd().'/'.$this->new_img)) {
				unlink(getcwd().'/'.$this->new_img);
			}
		}
		
		if(!empty($this->img_represent)){
			if (file_exists(getcwd().'/'.$this->img_represent)) {
				unlink(getcwd().'/'.$this->img_represent);
			}
		}
		
		if(!empty($this->img_background)){
			if (file_exists(getcwd().'/'.$this->img_background)) {
				unlink(getcwd().'/'.$this->img_background);
			}
		}
		
		return parent::beforeDelete();
	}
	
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		
		$criteria->compare('id_menu',$this->id_menu);
		$criteria->compare('name_menu_en',$this->name_menu_en,true);
		$criteria->compare('showhide_menu',$this->showhide_menu);
		$criteria->compare('location_menu',$this->location_menu);
		$criteria->compare('level_menu',$this->level_menu);
		$criteria->compare('order_menu',$this->order_menu);
		$criteria->compare('newwindow_mennu',$this->newwindow_mennu);
		$criteria->compare('home_menu',$this->home_menu);
		$criteria->compare('rewrite_url_menu',$this->rewrite_url_menu,true);
		$criteria->compare('url_menu',$this->url_menu,true);
		$criteria->compare('highlights_menu',$this->highlights_menu);
		$criteria->compare('img_represent',$this->img_represent);
		$criteria->compare('img_background',$this->img_background);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}