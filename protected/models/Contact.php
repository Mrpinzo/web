<?php

/**
 * This is the model class for table "contact".
 *
 * The followings are the available columns in table 'contact':
 * @property integer $id_contact
 * @property string $fullnam_contact
 * @property string $company_contact
 * @property string $phone_contact
 * @property string $email_contact
 * @property string $address_contact
 * @property string $title_contact
 * @property string $content_contact
 * @property string $mobile_contact
 */
class Contact extends CActiveRecord
{
	public $code;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Contact the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'contact';
	}


	public function rules()
	{
		if(!empty($_SESSION['lang']) && $_SESSION['lang']=='fr'){
			$fullnam_contact = "S'il vous plaît entrer votre nom complet";
			$email_contact = "S'il vous plaît entrer votre e-mail";
			$title_contact = "S'il vous plaît entrer un titre";
			$content_contact = "S'il vous plaît saisir du texte";
			$email_contact = "Email Malformed";
			$phone_contact = "S'il vous plaît entrer le numéro de téléphone";
			$code = "Code de confirmation Entrez Pas vrai";
			$codes = "S'il vous plaît entrez le code de confirmation";
		}
			else if(!empty($_SESSION['lang']) && $_SESSION['lang']=='sp'){
				$fullnam_contact = 'Por favor, introduzca su nombre completo';
				$email_contact = 'Por favor, introduzca su correo electrónico';
				$title_contact = "Por favor, introduzca un título";
				$content_contact = "Por favor, introduzca el texto";
				$email_contact = "Correo electrónico con formato incorrecto";
				$phone_contact = "Por favor, introduzca el número de teléfono";
				$code = "Código de confirmación de prohibido la entrada Verdadero";
				$codes = "Por favor, introduzca el código de confirmación";
			}
				else{
					$fullnam_contact = 'Please enter your full name';
					$email_contact = 'Please enter your email';
					$title_contact = 'Please enter a title';
					$content_contact = "Please enter text";
					$email_contact = "Malformed email";
					$phone_contact = "Please enter the phone number";
					$code = "Confirmation code Enter Not True";
					$codes = 'Please enter confirmation code';
				}
		
		if($this->isNewRecord){
		return array(
			array('fullnam_contact', 'required','message'=> $fullnam_contact),
				array('email_contact', 'required','message'=> $email_contact),
				
				array('title_contact', 'required','message'=> $title_contact),
				array('content_contact', 'required','message'=> $content_contact),
				
				array('email_contact','email','message'=> $email_contact),
				array('phone_contact,mobile_contact', 'numerical', 'integerOnly'=>true,'message'=> $phone_contact),
				array('read', 'numerical', 'integerOnly'=>true),
				//array('phone_contact,mobile_contact', 'match', 'pattern'=>'/^(84|0)(1\d{9}|9\d{8})$/','message'=> 'Sai số điện thoại.'),
			array('fullnam_contact, company_contact, phone_contact, email_contact, address_contact, title_contact, mobile_contact,content_contact,date', 'length', 'max'=>200000),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('read,id_contact, fullnam_contact, company_contact, phone_contact, email_contact, address_contact, title_contact, content_contact, mobile_contact', 'safe', 'on'=>'search'),
				array('code', 'required','message'=> $codes),
				array ('code','captcha',
						'allowEmpty'=>!CCaptcha::checkRequirements(),
						'message' => $code
				),
		);
		}else{
			return array(
					array('fullnam_contact', 'required','message'=> 'Vui lòng nhập họ tên đầy đủ.'),
					array('email_contact', 'required','message'=> 'Vui lòng nhập email.'),
					array('email_contact','email','message'=> 'Sai định dạng emai.'),
					array('title_contact', 'required','message'=> 'Vui lòng nhập tiêu đề.'),
					array('content_contact', 'required','message'=> 'Vui lòng nhập nội dung.'),
					array('phone_contact,mobile_contact', 'numerical', 'integerOnly'=>true,'message'=> 'Vui lòng nhập số điện thoại.'),
					array('read', 'numerical', 'integerOnly'=>true),
					//array('phone_contact,mobile_contact', 'match', 'pattern'=>'/^(84|0)(1\d{9}|9\d{8})$/','message'=> 'Sai số điện thoại.'),
					array('fullnam_contact, company_contact, phone_contact, email_contact, address_contact, title_contact, mobile_contact,content_contact,date', 'length', 'max'=>200000),
					// The following rule is used by search().
					// Please remove those attributes that should not be searched.
					array('read,id_contact, fullnam_contact, company_contact, phone_contact, email_contact, address_contact, title_contact, content_contact, mobile_contact', 'safe', 'on'=>'search'),
					
			);
		}
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_contact' => 'Id Contact',
			'fullnam_contact' => 'Họ Tên',
			'company_contact' => 'Địa chỉ công ty',
			'phone_contact' => 'Điện thoại',
			'email_contact' => 'Email',
			'address_contact' => 'Địa chỉ',
			'title_contact' => 'Tiêu đề',
			'content_contact' => 'Nội dung',
			'mobile_contact' => 'Di động',
				'read'=>'Mới'
		);
	}
	
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_contact',$this->id_contact);
		$criteria->compare('fullnam_contact',$this->fullnam_contact,true);
		$criteria->compare('company_contact',$this->company_contact,true);
		$criteria->compare('phone_contact',$this->phone_contact,true);
		$criteria->compare('email_contact',$this->email_contact,true);
		$criteria->compare('address_contact',$this->address_contact,true);
		$criteria->compare('title_contact',$this->title_contact,true);
		$criteria->compare('content_contact',$this->content_contact,true);
		$criteria->compare('mobile_contact',$this->mobile_contact,true);
		$criteria->compare('read',$this->read,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
				'sort'=>array(
						'defaultOrder'=>'id_contact DESC',
				),
		));
	}
}