<?php

/**
 * This is the model class for table "contact".
 *
 * The followings are the available columns in table 'contact':
 * @property integer $id_contact
 * @property string $fullnam_contact
 * @property string $company_contact
 * @property string $phone_contact
 * @property string $email_contact
 * @property string $address_contact
 * @property string $title_contact
 * @property string $content_contact
 * @property string $mobile_contact
 */
class Dangky extends CActiveRecord
{
	public $code;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Contact the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'contact';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('fullnam_contact', 'required','message'=> 'Vui lòng nhập họ tên đầy đủ.'),
				array('email_contact', 'required','message'=> 'Vui lòng nhập email.'),
				array('email_contact','email','message'=> 'Sai định dạng emai.'),
				array('phone_contact,mobile_contact', 'numerical', 'integerOnly'=>true,'message'=> 'Vui lòng nhập số điện thoại.'),
				//array('phone_contact,mobile_contact', 'match', 'pattern'=>'/^(84|0)(1\d{9}|9\d{8})$/','message'=> 'Sai số điện thoại.'),
			array('fullnam_contact, company_contact, phone_contact, email_contact, address_contact, title_contact, mobile_contact,content_contact', 'length', 'max'=>2000),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_contact, fullnam_contact, company_contact, phone_contact, email_contact, address_contact, title_contact, content_contact, mobile_contact', 'safe', 'on'=>'search'),
				array('code', 'required','message'=> 'Vui lòng nhập mã xác nhận.'),
				array ('code','captcha',
						'allowEmpty'=>!CCaptcha::checkRequirements(),
						'message' => 'Mã Xác Nhận Nhập Không Đúng'
				),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_contact' => 'Id Contact',
			'fullnam_contact' => 'Họ Tên',
			'company_contact' => 'Địa chỉ công ty',
			'phone_contact' => 'Điện thoại',
			'email_contact' => 'Email',
			'address_contact' => 'Địa chỉ',
			'title_contact' => 'Tiêu đề',
			'content_contact' => 'Nội dung',
			'mobile_contact' => 'Di động',
		);
	}
	
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_contact',$this->id_contact);
		$criteria->compare('fullnam_contact',$this->fullnam_contact,true);
		$criteria->compare('company_contact',$this->company_contact,true);
		$criteria->compare('phone_contact',$this->phone_contact,true);
		$criteria->compare('email_contact',$this->email_contact,true);
		$criteria->compare('address_contact',$this->address_contact,true);
		$criteria->compare('title_contact',$this->title_contact,true);
		$criteria->compare('content_contact',$this->content_contact,true);
		$criteria->compare('mobile_contact',$this->mobile_contact,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}