<?php

// This is our user model.
// It represents a user that it stored in a database
class User extends CActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
 
    public function tableName()
    {
        return 'admin';
    }
}