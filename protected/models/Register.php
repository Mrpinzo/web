<?php

/**
 * This is the model class for table "Register".
 *
 * The followings are the available columns in table 'Register':
 * @property integer $id_register
 * @property string $fullnam_register
 * @property string $company_register
 * @property string $phone_register
 * @property string $email_register
 * @property string $address_register
 * @property string $title_register
 * @property string $content_register
 * @property string $mobile_register
 */
class Register extends CActiveRecord
{
	public $code;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Register the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'register';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		if($this->isNewRecord){
		return array(
				
				array('namexh_register', 'required','message'=> 'Vui lòng nhập chức danh.'),
				array('lastnam_register', 'required','message'=> 'Vui lòng nhập Họ.'),
				array('title_register', 'required','message'=> 'Vui lòng nhập chức vụ.'),
				array('company_register', 'required','message'=> 'Vui lòng nhập địa chỉ công ty.'),
				array('address_register', 'required','message'=> 'Vui lòng nhập địa chỉ.'),
				array('phone_register', 'required','message'=> 'Vui lòng nhập điện thoại.'),
				array('areacode_register', 'required','message'=> 'Vui lòng nhập mã vùng.'),
				array('country_register', 'required','message'=> 'Vui lòng nhập đất nước.'),
				
			array('fullnam_register', 'required','message'=> 'Vui lòng nhập họ tên đầy đủ.'),
				array('email_register', 'required','message'=> 'Vui lòng nhập email.'),
				array('email_register','email','message'=> 'Sai định dạng emai.'),
				array('read', 'numerical', 'integerOnly'=>true),
				
				array('companyaddress_register', 'required','message'=> 'Vui lòng nhập địa chỉ công ty.'),
				array('city_register', 'required','message'=> 'Vui lòng nhập thành phố.'),
				array('postcode_register', 'required','message'=> 'Vui lòng nhập mã bưu điện.'),
				array('viplabel_register', 'required','message'=> 'Vui lòng nhập mã vip.'),
				array('bannerad_register', 'required','message'=> 'Vui lòng chọn nơi mà bạn biết về chúng tôi.'),
				array('topay_register', 'required','message'=> 'Vui lòng chọn hình thức thanh toán.'),
				
				array('phone_register,mobile_register', 'numerical', 'integerOnly'=>true,'message'=> 'Vui lòng nhập số điện thoại.'),
				//array('phone_register,mobile_register', 'match', 'pattern'=>'/^(84|0)(1\d{9}|9\d{8})$/','message'=> 'Sai số điện thoại.'),
			array('fullnam_register,yesmail_register,companyaddress_register,city_register,postcode_register,viplabel_register,bannerad_register,topay_register,lastnam_register,namenew_register,namexh_register,country_register,areacode_register, company_register,namenew_register,fullnam_register, phone_register, email_register, address_register, title_register, mobile_register,content_register,dates', 'length', 'max'=>200000),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_register, fullnam_register,fullnam_register,namenew_register, company_register, phone_register, email_register, address_register, title_register, content_register, mobile_register', 'safe', 'on'=>'search'),
				array('code', 'required','message'=> 'Vui lòng nhập mã xác nhận.'),
				array ('code','captcha',
						'allowEmpty'=>!CCaptcha::checkRequirements(),
						'message' => 'Mã Xác Nhận Nhập Không Đúng'
				),
		);
		}else{
			return array(
					array('namexh_register', 'required','message'=> 'Vui lòng nhập chức danh.'),
					array('lastnam_register', 'required','message'=> 'Vui lòng nhập Họ.'),
					array('title_register', 'required','message'=> 'Vui lòng nhập chức vụ.'),
					array('company_register', 'required','message'=> 'Vui lòng nhập địa chỉ công ty.'),
					array('address_register', 'required','message'=> 'Vui lòng nhập địa chỉ.'),
					array('phone_register', 'required','message'=> 'Vui lòng nhập điện thoại.'),
					array('areacode_register', 'required','message'=> 'Vui lòng nhập mã vùng.'),
					array('country_register', 'required','message'=> 'Vui lòng nhập đất nước.'),
					
					array('fullnam_register', 'required','message'=> 'Vui lòng nhập họ tên đầy đủ.'),
					array('email_register', 'required','message'=> 'Vui lòng nhập email.'),
					array('email_register','email','message'=> 'Sai định dạng emai.'),
					array('read', 'numerical', 'integerOnly'=>true),
					array('phone_register,mobile_register', 'numerical', 'integerOnly'=>true,'message'=> 'Vui lòng nhập số điện thoại.'),
					//array('phone_register,mobile_register', 'match', 'pattern'=>'/^(84|0)(1\d{9}|9\d{8})$/','message'=> 'Sai số điện thoại.'),
					array('fullnam_register,yesmail_register,companyaddress_register,city_register,postcode_register,viplabel_register,bannerad_register,topay_register,lastnam_register,namenew_register,namexh_register,country_register,areacode_register, company_register,namenew_register,fullnam_register, phone_register, email_register, address_register, title_register, mobile_register,content_register,dates', 'length', 'max'=>200000),
					// The following rule is used by search().
					// Please remove those attributes that should not be searched.
					array('id_register, fullnam_register,fullnam_register,namenew_register, company_register, phone_register, email_register, address_register, title_register, content_register, mobile_register', 'safe', 'on'=>'search'),
				
			);
		}
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_register' => 'Id',
			'fullnam_register' => 'Full name',
            'lastnam_register' => 'Last name',
            'title_register' => 'Proclaimed title',
            'namexh_register' => 'Name',
            'namenew_register' => 'Title file register ',
			'company_register' => 'Company address',
			'phone_register' => 'Phone',
			'email_register' => 'Email',
			'address_register' => 'Address',
          	'country_register' => 'Country',
            'areacode_register' => 'Areacode',
            
            'yesmail_register' => 'Send mail',
            'companyaddress_register' => 'Company Address',
            'city_register' => 'City',
            'postcode_register' => 'Post Code',
            'viplabel_register' => 'Viplabel',
            'bannerad_register' => 'Bannerad',
            'topay_register' => 'Pay',
				'read'=>'Mới',
				'dates'=>'Thời gian'
          
		);
	}
	
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_register',$this->id_register);
		$criteria->compare('fullnam_register',$this->fullnam_register,true);
        $criteria->compare('namenew_register',$this->namenew_register,true);
		$criteria->compare('company_register',$this->company_register,true);
		$criteria->compare('phone_register',$this->phone_register,true);
		$criteria->compare('email_register',$this->email_register,true);
		$criteria->compare('address_register',$this->address_register,true);
		$criteria->compare('title_register',$this->title_register,true);
		$criteria->compare('content_register',$this->content_register,true);
		$criteria->compare('mobile_register',$this->mobile_register,true);
		$criteria->compare('read',$this->read,true);
		$criteria->compare('dates',$this->dates,true);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
				'sort'=>array(
						'defaultOrder'=>'id_register DESC',
				),
		));
	}
}