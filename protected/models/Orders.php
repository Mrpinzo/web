<?php

/**
 * This is the model class for table "orders".
 *
 * The followings are the available columns in table 'orders':
 * @property integer $ord_id
 * @property string $ord_email
 * @property integer $ord_Gender
 * @property string $ord_Dateofbirth
 * @property integer $ord_payment
 * @property string $ord_name
 * @property string $ord_from
 * @property integer $ord_phone
 * @property string $ord_date
 * @property integer $ord_status
 */
class Orders extends CActiveRecord
{
	public $code;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Orders the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'orders';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		if(Yii::app()->controller->action->id != 'StatusOrder'){
			return array(				
					array('ord_name', 'required','message'=> 'Vui lòng nhập họ tên đầy đủ.'),
					array('ord_email', 'required','message'=> 'Vui lòng nhập email.'),
					array('ord_email','email','message'=> 'Sai định dạng emai.'),
					//array('ord_email','my_required'),
					array('ord_phone','required','message'=> 'Vui lòng nhập số điện thoại.'),
					//array('ord_Gender', 'required','message'=> 'Vui lòng chọn giới tính.'),
					array('ord_Gender', 'numerical', 'integerOnly'=>true),
					array('ord_phone', 'match', 'pattern'=>'/^(84|0)(1\d{9}|9\d{8})$/','message'=> 'Sai số điện thoại.'),
					array('ord_from', 'required','message'=> 'Vui lòng nhập địa chỉ giao hàng.'),
					array('ord_Dateofbirth', 'type','type' =>'date',
							'message' => '{attribute} không đúng định dạng ngày!',
							'dateFormat' => 'dd-MM-yyyy'),
					array('code', 'required','message'=> 'Vui lòng nhập mã xác nhận.'),
					array ('code','captcha',
							'allowEmpty'=>!CCaptcha::checkRequirements(),
							'message' => 'Mã Xác Nhận Nhập Không Đúng'
					),
				array('ord_email, ord_name, ord_from,ord_phone,ord_Dateofbirth,ord_request', 'length', 'max'=>150),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array('ord_id, ord_email, ord_Gender, ord_Dateofbirth, ord_name,ord_request, ord_from, ord_phone, ord_date, ord_status', 'safe', 'on'=>'search'),
			
					
			);
		}else {
			return array(
					array('ord_name', 'required','message'=> 'Vui lòng nhập họ tên đầy đủ.'),
					array('ord_email', 'required','message'=> 'Vui lòng nhập email.'),
					array('ord_email','email','message'=> 'Sai định dạng emai.'),
					//array('ord_email','my_required'),
					array('ord_phone','required','message'=> 'Vui lòng nhập số điện thoại.'),
					//array('ord_Gender', 'required','message'=> 'Vui lòng chọn giới tính.'),
					array('ord_Gender', 'numerical', 'integerOnly'=>true),
					array('ord_phone', 'match', 'pattern'=>'/^(84|0)(1\d{9}|9\d{8})$/','message'=> 'Sai số điện thoại.'),
					array('ord_from', 'required','message'=> 'Vui lòng nhập địa chỉ giao hàng.'),
					array('ord_Dateofbirth', 'type','type' =>'date',
							'message' => '{attribute} không đúng định dạng ngày!',
							'dateFormat' => 'dd-MM-yyyy'),
					
					array('ord_email, ord_name, ord_from,ord_phone,ord_Dateofbirth,ord_request', 'length', 'max'=>150),
					// The following rule is used by search().
					// Please remove those attributes that should not be searched.
					array('ord_id, ord_email, ord_Gender, ord_Dateofbirth, ord_name,ord_request, ord_from, ord_phone, ord_date, ord_status', 'safe', 'on'=>'search'),
						
						
			);
		}
	}

	/* public function my_required($attribute_name,$params){
		if(!empty($this->ord_email)){
			$criteria = new CDbCriteria;
			$criteria->compare('cust_email',$this->ord_email);
			$count = Customers::model()->count($criteria);
			if($count != 0)
                 $this->addError('ord_email','Email đã tồn tại');
		}
	} */
	
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ord_id' => 'Id',
			'ord_email' => 'Email',
			'ord_Gender' => 'Giới tính',
			'ord_Dateofbirth' => 'Ngày sinh',
			'ord_name' => 'Tên',
			'ord_from' => 'Địa chỉ',
			'ord_phone' => 'Điện thoại',
			'ord_date' => 'Ngày đặt',
			'ord_status' => 'Tình trạng',
				'ord_request' => 'Yêu cầu',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ord_id',$this->ord_id);
		$criteria->compare('ord_email',$this->ord_email,true);
		$criteria->compare('ord_Gender',$this->ord_Gender);
		$criteria->compare('ord_Dateofbirth',$this->ord_Dateofbirth,true);
		$criteria->compare('ord_name',$this->ord_name,true);
		$criteria->compare('ord_from',$this->ord_from,true);
		$criteria->compare('ord_phone',$this->ord_phone);
		$criteria->compare('ord_date',$this->ord_date,true);
		$criteria->compare('ord_status',$this->ord_status);
		$criteria->compare('ord_request',$this->ord_request);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}