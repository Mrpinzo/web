<?php

/**
 * This is the model class for table "news".
 *
 * The followings are the available columns in table 'news':
 * @property integer $new_id
 * @property string $new_title
 * @property string $new_img
 * @property string $new_summary
 * @property string $new_content_en
 * @property string $new_date
 */
class News extends CActiveRecord
{
	public $new_img_upload;
    public $img_file_upload;
    public $img_filel;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return News the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'news';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('new_title_en', 'required'),
			array('new_title_en,new_title_fr,new_title_sp,new_summary_fr,new_summary_sp,new_content_fr,new_content_sp,yii_address_fr,yii_address_en,yii_address_sp,yii_overview_fr,yii_overview_sp,yii_content_fr,yii_content_sp,yii_content_en,yii_wsattend_fr,yii_wsattend_sp,yii_attending_fr,yii_attending_sp,yii_director_fr,yii_director_sp,new_summary_en, new_content_en,yii_date,yii_year,yii_overview_en,yii_wsattend_en,yii_attending_en,yii_director_en,new_title_seo,new_description_seo,new_keywords_seo,new_title_en,
					contentnewsd_en,contentnewsd_fr,contentnewsd_sp', 'length','min'=>3),
				//array('id_menu','my_required'),
				array('new_showhide,new_highlights,new_ads,id_menu,agency,view,customer,notdownload_register,typedetailnews', 'numerical', 'integerOnly'=>true),
			//array('new_img', 'file', 'types' => 'jpg, gif, png', 'maxSize' => 1 * 1024 * 1024, 'tooLarge'=>'Dung lượng file < 1MB. Hãy tải lên một tập tin nhỏ hơn.', 'allowEmpty' => TRUE),
				
				array('new_img', 'file',
						//'allowEmpty'=>$this->isNewRecord ?false:true,
						'allowEmpty'=>true,
						'types'=>'jpg, gif, png',
						'wrongType'=>'Tập tin không hợp lệ',
						'maxSize'=>6 * 1024 * 1024,
						'tooLarge'=>'Dung lượng file < 6MB. Hãy tải lên một tập tin nhỏ hơn.',//Error Message
				),
                
                array('img_file', 'file',
						//'allowEmpty'=>$this->isNewRecord ?false:true,
						'allowEmpty'=>true,
						'types'=>'jpg, gif, png, rar, pdf, zip',
						'wrongType'=>'Tập tin không hợp lệ',
						'maxSize'=>6 * 1024 * 1024,
						'tooLarge'=>'Dung lượng file < 6MB. Hãy tải lên một tập tin nhỏ hơn.',//Error Message
				),
                
				array('imgfile', 'file',
						//'allowEmpty'=>$this->isNewRecord ?false:true,
						'allowEmpty'=>true,
						'types'=>'jpg, gif, png',
						'wrongType'=>'Tập tin không hợp lệ',
						'maxSize'=>6 * 1024 * 1024,
						'tooLarge'=>'Dung lượng file < 6MB. Hãy tải lên một tập tin nhỏ hơn.',//Error Message
				),
				
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('notdownload_register,agency,customer,new_id,id_menu,rewrite_url_news, new_title_en,img_file, new_img, new_summary_en, new_content_en, new_date,new_highlights,new_showhide,new_ads', 'safe', 'on'=>'search'),
		);
	}

	public function my_required($attribute_name,$params){
		if(empty($this->id_menu)){
			$this->addError('id_menu',
					'Vui lòng chọn danh mục');
		}
	}
	/**
	 * @return array relational rules.
	 */
   public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
				'YiiMenu' => array(self::BELONGS_TO, 'YiiMenu', 'id_menu'),
		);
	}

	protected function beforeSave()
	{
		$this->new_img_upload = CUploadedFile::getInstance($this, 'new_img');
		if (isset($this->new_img_upload)) {
			if(!$this->isNewRecord){
				if(!empty($this->new_img)){
					if (file_exists(getcwd().'/'.$this->new_img)) {
						unlink(getcwd().'/'.$this->new_img);
					};
				}
			}
			$fileName     = rand().$this->new_img_upload->name;
			$uploadFolder = Yii::getPathOfAlias('webroot') . DIRECTORY_SEPARATOR . 'upload' . DIRECTORY_SEPARATOR .'new'.DIRECTORY_SEPARATOR . date('Y-m-d');
			if (!is_dir($uploadFolder)) {
				mkdir($uploadFolder, 0755, TRUE);
			}
			$uploadFile = $uploadFolder . DIRECTORY_SEPARATOR . $fileName;
			$this->new_img_upload->saveAs($uploadFile); //Upload file thong qua object CUploadedFile
			$this->new_img = '/upload/new/' . date('Y-m-d') . '/'. $fileName; //Lưu path vào csdl
       
		}
        
      $this->img_file_upload = CUploadedFile::getInstance($this, 'img_file');
		if (isset($this->img_file_upload)) {
			if(!$this->isNewRecord){
				if(!empty($this->img_file)){
					if (file_exists(getcwd().'/'.$this->img_file)) {
						unlink(getcwd().'/'.$this->img_file);
					};
				}
			}
			$fileName     = rand().$this->img_file_upload->name;
			$uploadFolder = Yii::getPathOfAlias('webroot') . DIRECTORY_SEPARATOR . 'upload' . DIRECTORY_SEPARATOR .'new'.DIRECTORY_SEPARATOR . date('Y-m-d');
			if (!is_dir($uploadFolder)) {
				mkdir($uploadFolder, 0755, TRUE);
			}
			$uploadFile = $uploadFolder . DIRECTORY_SEPARATOR . $fileName;
			$this->img_file_upload->saveAs($uploadFile); //Upload file thong qua object CUploadedFile
			$this->img_file = '/upload/new/' . date('Y-m-d') . '/'. $fileName; //Lưu path vào csdl
       
		}
        
		$this->img_filel = CUploadedFile::getInstance($this, 'imgfile');
		if (isset($this->img_filel)) {
			if(!$this->isNewRecord){
				if(!empty($this->imgfile)){
					if (file_exists(getcwd().'/'.$this->imgfile)) {
						unlink(getcwd().'/'.$this->imgfile);
					};
				}
			}
			$fileName     = rand().$this->img_filel->name;
			$uploadFolder = Yii::getPathOfAlias('webroot') . DIRECTORY_SEPARATOR . 'upload' . DIRECTORY_SEPARATOR .'imgfiles'.DIRECTORY_SEPARATOR . date('Y-m-d');
			if (!is_dir($uploadFolder)) {
				mkdir($uploadFolder, 0755, TRUE);
			}
			$uploadFile = $uploadFolder . DIRECTORY_SEPARATOR . $fileName;
			$this->img_filel->saveAs($uploadFile); //Upload file thong qua object CUploadedFile
			$this->imgfile = '/upload/imgfiles/' . date('Y-m-d') . '/'. $fileName; //Lưu path vào csdl
		    
		}
		
		return parent::beforeSave();
	}
	protected function beforeDelete() {
		if(!empty($this->new_img)){
			if (file_exists(getcwd().'/'.$this->new_img)) {
				unlink(getcwd().'/'.$this->new_img);
			}
		}
		return parent::beforeDelete();
	}
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'new_id' => 'Id',
			'new_title_en' => 'Title',
            'new_title_fr' => 'Title French',
            'new_title_sp' => 'Title Spanish',
            
            'yii_overview_en' => 'Overview',
            'yii_overview_fr' => 'Overview French',
            'yii_overview_sp' => 'Overview Spanish',
            
            'yii_content_en' => 'Content',
            'yii_content_fr' => 'Content French',
            'yii_content_sp' => 'Content Spanish',
            
            'yii_wsattend_en' => 'Who Should Attend',
            'yii_wsattend_fr' => 'Who Should Attend French',
            'yii_wsattend_sp' => 'WsattWho Should Attendend Spanish',
            
            'yii_attending_en' => 'Benefits Of Attending',
            'yii_attending_fr' => 'Benefits Of Attending French',
            'yii_attending_sp' => 'Benefits Of Attending Spanish',
            
            'yii_director_en' => 'Course Director',
            'yii_director_fr' => 'Course Director French',
            'yii_director_sp' => 'Course Director Spanish',
           
            'new_summary_en' => 'Venue and Accommodation',
            'new_summary_fr' => 'Venue and Accommodation French',
            'new_summary_sp' => 'Venue and Accommodation Spanish',
            
            'new_content_en' => 'Course Fees', 
            'new_content_fr' => 'Course Fees French',
            'new_content_sp' => 'Course Fees Spanish',
            
            'yii_address_en' => 'Address',
            'yii_address_fr' => 'Address French',
            'yii_address_sp' => 'Address Spanish',
            
			'new_img' => 'Images',
            'img_file' => 'Download',
            'new_date' => 'Ngày đăng',
			'new_title_seo' =>'Tiêu đề Seo',
			'new_description_seo' =>'Miêu tả Seo',
			'new_keywords_seo' => 'Từ khóa Seo',
			'id_menu' => 'Loại tin tức',
			'new_highlights' => 'Nổi bật',
			'new_showhide' => 'Hiện thị',
			'new_ads'   => 'Quảng cáo',
			'rewrite_url_news'   => 'Định danh đường dẫn',
			'agency' => 'Đại lý',
			'customer' => 'Khách hàng',
            
            'yii_date' => 'Ngày tháng',
            'yii_year' => 'Năm',

             'yii_overview' => 'Overview',
			 'notdownload_register'=>'Hiện thị Register và download',
             'imgfile'=>'Ảnh đại diện file',
			  'contentnewsd_fr'=>'Nội dung chi tiết fr',
			  'typedetailnews'=>'Hiện thị chi tiết',
				'contentnewsd_sp'=>'Nội dung chi tiết sp',
				'contentnewsd_en'=>'Nội dung chi tiết en',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('new_id',$this->new_id);
		$criteria->compare('id_menu',$this->id_menu,true);
		$criteria->compare('new_title_en',$this->new_title_en,true);
		$criteria->compare('new_keywords_seo',$this->new_keywords_seo,true);
		$criteria->compare('new_title_seo',$this->new_title_seo,true);
		$criteria->compare('new_description_seo',$this->new_description_seo,true);
		$criteria->compare('new_highlights',$this->new_highlights,true);
		$criteria->compare('new_showhide',$this->new_showhide,true);
		$criteria->compare('new_date',$this->new_date,true);
		$criteria->compare('new_ads',$this->new_ads,true);
		$criteria->compare('rewrite_url_news',$this->rewrite_url_news,true);
		$criteria->compare('agency',$this->agency);
		$criteria->compare('customer',$this->customer);
		$criteria->compare('notdownload_register',$this->notdownload_register,true);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
				'sort'=>array(
						'defaultOrder'=>'new_id DESC',
				),
		));
	}
}