<?php
/* @var $this AdministratorController */
/* @var $data Administrator */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ad_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ad_id), array('view', 'id'=>$data->ad_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('username')); ?>:</b>
	<?php echo CHtml::encode($data->username); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('password')); ?>:</b>
	<?php echo CHtml::encode($data->password); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ad_fullname')); ?>:</b>
	<?php echo CHtml::encode($data->ad_fullname); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ad_phone')); ?>:</b>
	<?php echo CHtml::encode($data->ad_phone); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ad_email')); ?>:</b>
	<?php echo CHtml::encode($data->ad_email); ?>
	<br />


</div>