<?php
/* @var $this AdministratorController */
/* @var $model Administrator */

$this->breadcrumbs=array(
	'Quản trị'=>array('admin'),
	$model->username=>array('view','id'=>$model->ad_id),
	'Update',
);

$this->menu=array(
	array('label'=>'Danh sách quản trị', 'url'=>array('admin')),
);
?>

<h4>Sửa tài khoản - <?php echo $model->username; ?></h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>