<?php
/* @var $this AdministratorController */
/* @var $model Administrator */

$this->breadcrumbs=array(
	'Quản trị'=>array('admin'),
	'Thêm mới',
);

$this->menu=array(
	array('label'=>'Danh sách quản trị', 'url'=>array('admin')),
);
?>

<h4>Thêm tài khoản</h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>