<?php
/* @var $this LinksController */
/* @var $model Links */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'links-form',
	'enableAjaxValidation'=>false,
		'htmlOptions'          => array(
				'enctype' => 'multipart/form-data',
		),
)); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name_links'); ?>
		<?php echo $form->textField($model,'name_links',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'name_links'); ?>
	</div>
  
	<div class="row">
		<?php echo $form->labelEx($model,'showhide_links'); ?>
		<?php echo $form->checkBox($model, 'showhide_links'); ?>
		<?php echo $form->error($model,'showhide_links'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'url_links'); ?>
		<?php echo $form->textField($model,'url_links',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'url_links'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Tạo mới' : 'Sửa'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->