<?php
/* @var $this LinksController */
/* @var $model Links */

$this->breadcrumbs=array(
	'Liên kết'=>array('admin'),
	'Danh sách',
);

$this->menu=array(
	array('label'=>'Tạo mới liên kết', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#links-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h4>Liên kết</h4>

<?php 

$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'links-grid',
		'ajaxUpdate'=>false,
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_links',
		'name_links',

			array(
					'name'   => 'showhide_links',
					 'value'  => '$data->showhide_links==1?"<i class=\"fa fa-check-square-o csm\"></i>":"<i class=\"fa fa-times csm\"></i>"',
					'filter' => array(1 => 'Hiện', 0 => 'Ẩn'),
					'type'  => 'raw',
			),
		
		'url_links',
		
		array(
			'class'=>'CButtonColumn',
				'header' => 'Hành động',
				'template'=>'{update}{delete}',
				'buttons'=>array
				(
						
						'update' => array(
								'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'Update')),
								'label' => '<i class="fa fa-pencil-square-o"></i>',
								'imageUrl' => false,
						),
						'delete' => array(
								'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'Delete')),
								'label' => '<i class="fa fa-trash-o"></i>',
								'imageUrl' => false,
						)
				),
		),
	),
)); ?>
