
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'yii-role-form',
	'enableAjaxValidation'=>false,
)); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name_role'); ?>
		<?php echo $form->textField($model,'name_role',array('size'=>60,'maxlength'=>500)); ?>
		<?php echo $form->error($model,'name_role'); ?>
	</div>

	<div id="title_roles">Chọn quyền</div>
	<div class="title_roles_items">
	<div id="checkall"><input type="checkbox" name="checkall" id="checkallinput">Tất cả</div>
	<div class="itemsrole">
	 <div>Quản trị</div>
	   <ul>
	      <li><input type="checkbox" class="rolesd" name="Administrator_admin" id="Administrator_admin" value="Administrator_admin">Xem danh sách</li>
	      <li><input type="checkbox" class="rolesd" name="Administrator_view" id="Administrator_view" value="Administrator_view">Xem chi tiết</li>
	      <li><input type="checkbox" class="rolesd" name="Administrator_create" id="Administrator_create" value="Administrator_create">Tạo mới</li>
	      <li><input type="checkbox" class="rolesd" name="Administrator_update" id="Administrator_update" value="Administrator_update">Sửa</li>
	      <li><input type="checkbox" class="rolesd" name="Administrator_delete" id="Administrator_delete" value="Administrator_delete">Xóa</li>
	      <li><input type="checkbox"  class="rolesd" name="Administrator_ajaxUpdate" id="Administrator_ajaxUpdate" value="Administrator_ajaxUpdate">Xóa tất cả</li>
	      
	   </ul>
	   </div>
	   <div class="itemsrole">
	   <div>Cấu hình</div>
	   <ul>
	      <li><input type="checkbox" class="rolesd"  name="Configure_view" id="Configure_view" value="Configure_view">Xem cấu hình</li>
	      <li><input type="checkbox"  class="rolesd" name="Configure_create" id="Configure_create" value="Configure_create">Tạo mới</li>
	      <li><input type="checkbox"  class="rolesd" name="Configure_update" id="Configure_update" value="Configure_update">Sửa</li>
	   </ul>
	   </div>
	   <div class="itemsrole">
	   <div>Nhóm quyền</div>
	   <ul>
	      <li><input type="checkbox"  class="rolesd" name="YiiRole_admin" id="YiiRole_admin" value="YiiRole_admin">Xem danh sách</li>
	      <li><input type="checkbox"  class="rolesd" name="YiiRole_view" id="YiiRole_view" value="YiiRole_view">Xem chi tiết</li>
	      <li><input type="checkbox"  class="rolesd" name="YiiRole_create" id="YiiRole_create" value="YiiRole_create">Tạo mới</li>
	      <li><input type="checkbox"  class="rolesd" name="YiiRole_update" id="YiiRole_update" value="YiiRole_update">Sửa</li>
	      <li><input type="checkbox"  class="rolesd" name="YiiRole_delete" id="YiiRole_delete" value="YiiRole_delete">Xóa</li>
	   </ul>
	   </div>
	   <div class="itemsrole">
	   <div>Tin tức</div>
	   <ul>
	      <li><input type="checkbox"  class="rolesd" name="News_admin" id="News_admin" value="News_admin">Xem danh sách</li>
	      <li><input type="checkbox"  class="rolesd" name="News_view" id="News_view" value="News_view">Xem chi tiết</li>
	      <li><input type="checkbox"  class="rolesd" name="News_create" id="News_create" value="News_create">Tạo mới</li>
	      <li><input type="checkbox"  class="rolesd" name="News_update" id="News_update" value="News_update">Sửa</li>
	      <li><input type="checkbox"  class="rolesd" name="News_delete" id="News_delete" value="News_delete">Xóa</li>
	      <li><input type="checkbox"  class="rolesd" name="News_ajaxUpdate" id="News_ajaxUpdate" value="News_ajaxUpdate">Hiện-Nổi bật-Xóa tất cả</li>
	      
	   </ul>
	   </div>
	   <div class="itemsrole">
	    <div>Sản phẩm</div>
	   <ul>
	      <li><input type="checkbox"  class="rolesd" name="Product_admin" id="Product_admin" value="Product_admin">Xem danh sách</li>
	      <li><input type="checkbox"  class="rolesd" name="Product_view" id="Product_view" value="Product_view">Xem chi tiết</li>
	      <li><input type="checkbox"  class="rolesd" name="Product_create" id="Product_create" value="Product_create">Tạo mới</li>
	      <li><input type="checkbox"  class="rolesd" name="Product_update" id="Product_update" value="Product_update">Sửa</li>
	      <li><input type="checkbox"  class="rolesd" name="Product_delete" id="Product_delete" value="Product_delete">Xóa</li>
	      <li><input type="checkbox"  class="rolesd" name="Product_ajaxUpdate" id="Product_ajaxUpdate" value="Product_ajaxUpdate">Hiện - Ẩn - Xóa tất cả</li>
	      
	   </ul>
	    </div>
        
         <div class="itemsrole">
    	    <div>Pass event</div>
        	   <ul>
        	      <li><input type="checkbox"  class="rolesd" name="Pasevent_admin" id="Pasevent_admin" value="Pasevent_admin">Xem danh sách</li>
        	      <li><input type="checkbox"  class="rolesd" name="Pasevent_view" id="Pasevent_view" value="Pasevent_view">Xem chi tiết</li>
        	      <li><input type="checkbox"  class="rolesd" name="Pasevent_create" id="Pasevent_create" value="Pasevent_create">Tạo mới</li>
        	      <li><input type="checkbox"  class="rolesd" name="Pasevent_update" id="Pasevent_update" value="Pasevent_update">Sửa</li>
        	      <li><input type="checkbox"  class="rolesd" name="Pasevent_delete" id="Pasevent_delete" value="Pasevent_delete">Xóa</li>
        	      <li><input type="checkbox"  class="rolesd" name="Pasevent_ajaxUpdate" id="Pasevent_ajaxUpdate" value="Pasevent_ajaxUpdate">Hiện - Ẩn - Xóa tất cả</li>
        	      
        	   </ul>
    	  </div>
        
	    <div class="itemsrole">
	    <div>Quảng cáo</div>
	   <ul>
	      <li><input type="checkbox"  class="rolesd" name="Advertise_admin" id="Advertise_admin" value="Advertise_admin">Xem danh sách</li>
	      <li><input type="checkbox"  class="rolesd" name="Advertise_view" id="Advertise_view" value="Advertise_view">Xem chi tiết</li>
	      <li><input type="checkbox"  class="rolesd" name="Advertise_create" id="Advertise_create" value="Advertise_create">Tạo mới</li>
	      <li><input type="checkbox"  class="rolesd" name="Advertise_update" id="Advertise_update" value="Advertise_update">Sửa</li>
	      <li><input type="checkbox"  class="rolesd" name="Advertise_delete" id="Advertise_delete" value="Advertise_delete">Xóa</li>
	      <li><input type="checkbox"  class="rolesd" name="Advertise_ajaxUpdate" id="Advertise_ajaxUpdate" value="Advertise_ajaxUpdate">Hiện - Ẩn - Xóa tất cả</li>
	      
	   </ul>
	   </div>
	   <div class="itemsrole">
	   <div>Danh sách menu</div>
	   <ul>
	      <li><input type="checkbox"  class="rolesd" name="YiiMenu_admin" id="YiiMenu_admin" value="YiiMenu_admin">Xem danh sách</li>
	      <li><input type="checkbox"  class="rolesd" name="YiiMenu_view" id="YiiMenu_view" value="YiiMenu_view">Xem chi tiết</li>
	      <li><input type="checkbox"  class="rolesd" name="YiiMenu_create" id="YiiMenu_create" value="YiiMenu_create">Tạo mới</li>
	      <li><input type="checkbox"  class="rolesd" name="YiiMenu_update" id="YiiMenu_update" value="YiiMenu_update">Sửa</li>
	      <li><input type="checkbox"  class="rolesd" name="YiiMenu_delete" id="YiiMenu_delete" value="YiiMenu_delete">Xóa</li>
	       <li><input type="checkbox"  class="rolesd" name="YiiMenu_ajaxUpdate" id="YiiMenu_ajaxUpdate" value="YiiMenu_ajaxUpdate">Hiện - Ẩn - Xóa tất cả</li>
	       <li><input type="checkbox"  class="rolesd" name="YiiMenu_updateAjax" id="YiiMenu_updateAjax" value="YiiMenu_updateAjax">Sắp xếp thứ tự menu</li>
	   </ul>
	   </div>
	   <div class="itemsrole">
	    <div>Liên kết</div>
	   <ul>
	      <li><input type="checkbox"  class="rolesd" name="Links_admin" id="Links_admin" value="Links_admin">Xem danh sách</li>
	      <li><input type="checkbox"  class="rolesd" name="Links_view" id="Links_view" value="Links_view">Xem chi tiết</li>
	      <li><input type="checkbox"  class="rolesd" name="Links_create" id="Links_create" value="Links_create">Tạo mới</li>
	      <li><input type="checkbox"  class="rolesd" name="Links_update" id="Links_update" value="Links_update">Sửa</li>
	      <li><input type="checkbox"  class="rolesd" name="Links_delete" id="Links_delete" value="Links_delete">Xóa</li>
	   </ul>
	   </div>
	   <div class="itemsrole">
	    <div>Liên hệ</div>
	   <ul>
	      <li><input type="checkbox"  class="rolesd" name="Contact_admin" id="Contact_admin" value="Contact_admin">Xem danh sách</li>
	      <li><input type="checkbox"  class="rolesd" name="Contact_view" id="Contact_view" value="Contact_view">Xem chi tiết</li>
	      <li><input type="checkbox"  class="rolesd" name="Contact_create" id="Contact_create" value="Contact_create">Tạo mới</li>
	      <li><input type="checkbox"  class="rolesd" name="Contact_update" id="Contact_update" value="Contact_update">Sửa</li>
	      <li><input type="checkbox"  class="rolesd" name="Contact_delete" id="Contact_delete" value="Contact_delete">Xóa</li>
	      <li><input type="checkbox"  class="rolesd" name="Contact_ajaxUpdate" id="Contact_ajaxUpdate" value="Contact_ajaxUpdate">Xóa tất cả</li>
	      
	   </ul>
	   </div>
	   <div class="itemsrole">
	    <div>Đơn hàng</div>
	   <ul>
	      <li><input type="checkbox"  class="rolesd" name="Orders_admin" id="Orders_admin" value="Orders_admin">Xem danh sách</li>
	      <li><input type="checkbox"  class="rolesd" name="Orders_view" id="Orders_view" value="Orders_view">Xem chi tiết</li>
	      <li><input type="checkbox"  class="rolesd" name="Orders_create" id="Orders_create" value="Orders_create">Tạo mới</li>
	      <li><input type="checkbox"  class="rolesd" name="Orders_update" id="Orders_update" value="Orders_update">Sửa</li>
	      <li><input type="checkbox"  class="rolesd" name="Orders_delete" id="Orders_delete" value="Orders_delete">Xóa</li>
	      	      <li><input type="checkbox"  class="rolesd" name="Orders_ajaxUpdate" id="Orders_ajaxUpdate" value="Orders_ajaxUpdate">Xóa tất cả</li>
	      	      <li><input type="checkbox"  class="rolesd" name="Orders_StatusOrder" id="Orders_StatusOrder" value="Orders_StatusOrder">Thay đổi tình trạng hóa đơn</li>
	      
	   </ul>
	   </div>
	   <div class="itemsrole">
	    <div>Loại Album</div>
	   <ul>
	      <li><input type="checkbox"  class="rolesd" name="YiiTypealbum_admin" id="YiiTypealbum_admin" value="YiiTypealbum_admin">Xem danh sách</li>
	      <li><input type="checkbox"  class="rolesd" name="YiiTypealbum_create" id="YiiTypealbum_create" value="YiiTypealbum_create">Tạo mới</li>
	      <li><input type="checkbox"  class="rolesd" name="YiiTypealbum_update" id="YiiTypealbum_update" value="YiiTypealbum_update">Sửa</li>
	      <li><input type="checkbox"  class="rolesd" name="YiiTypealbum_delete" id="YiiTypealbum_delete" value="YiiTypealbum_delete">Xóa</li>
	   </ul>
	   </div>
	   <div class="itemsrole">
	    <div>Ảnh Album</div>
	   <ul>
	      <li><input type="checkbox"  class="rolesd" name="YiiAlbum_update" id="YiiAlbum_update" value="YiiAlbum_update">Hiện/Ẩn</li>
	      <li><input type="checkbox"  class="rolesd" name="YiiAlbum_delete" id="YiiAlbum_delete" value="YiiAlbum_delete">Xóa</li>
	   </ul>
	   </div>
	   <div class="itemsrole">
	    <div>Loại Video</div>
	   <ul>
	      <li><input type="checkbox"  class="rolesd" name="YiiTypevideo_admin" id="YiiTypevideo_admin" value="YiiTypevideo_admin">Xem danh sách</li>
	      <li><input type="checkbox"  class="rolesd" name="YiiTypevideo_create" id="YiiTypevideo_create" value="YiiTypevideo_create">Tạo mới</li>
	      <li><input type="checkbox"  class="rolesd" name="YiiTypevideo_update" id="YiiTypevideo_update" value="YiiTypevideo_update">Sửa</li>
	      <li><input type="checkbox"  class="rolesd" name="YiiTypevideo_delete" id="YiiTypevideo_delete" value="YiiTypevideo_delete">Xóa</li>
	   </ul>
	   </div>
	   <div class="itemsrole">
	    <div>Video</div>
	   <ul>
	      <li><input type="checkbox"  class="rolesd" name="YiiVideo_admin" id="YiiVideo_admin" value="YiiVideo_admin">Xem danh sách</li>
	      <li><input type="checkbox"  class="rolesd" name="YiiVideo_view" id="YiiVideo_view" value="YiiVideo_view">Xem chi tiết</li>
	      <li><input type="checkbox"  class="rolesd" name="YiiVideo_create" id="YiiVideo_create" value="YiiVideo_create">Tạo mới</li>
	      <li><input type="checkbox"  class="rolesd" name="YiiVideo_update" id="YiiVideo_update" value="YiiVideo_update">Sửa</li>
	      <li><input type="checkbox"  class="rolesd" name="YiiVideo_delete" id="YiiVideo_delete" value="YiiVideo_delete">Xóa</li>	      	  	      
	   </ul>
	</div>
	
	<div class="row buttons rolesd">
		<?php echo CHtml::submitButton('Tạo mới'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->