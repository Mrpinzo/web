<?php
/* @var $this YiiRoleController */
/* @var $model YiiRole */

$this->breadcrumbs=array(
	'Nhóm quyền'=>array('admin'),
	'Danh sách',
);

$this->menu=array(
	array('label'=>'Tạo mới', 'url'=>array('create')),
);
?>

<h4>Danh sách nhóm quyền</h4>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'yii-role-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_role',
		'name_role',
		array(
			'class'=>'CButtonColumn',
				'header' => 'Hành động',
				'template'=>'{update}{delete}',
				'buttons'=>array
				(
						'view' => array(
								'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'View')),
								'label' => '<i class="fa fa-search"></i>',
								'imageUrl' => false,
						),
						'update' => array(
								'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'Update')),
								'label' => '<i class="fa fa-pencil-square-o"></i>',
								'imageUrl' => false,
						),
						'delete' => array(
								'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'Delete')),
								'label' => '<i class="fa fa-trash-o"></i>',
								'imageUrl' => false,
						)
				),
		),
	),
)); ?>
