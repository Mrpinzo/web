<?php
/* @var $this YiiRoleController */
/* @var $model YiiRole */

$this->breadcrumbs=array(
	'Nhóm quyền'=>array('admin'),
	'Tạo mới',
);

$this->menu=array(
	array('label'=>'Danh sách nhóm quyền', 'url'=>array('admin')),
);
?>

<h4>Tạo mới nhóm quyền</h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>