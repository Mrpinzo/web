<?php
/* @var $this YiiRoleController */
/* @var $model YiiRole */

$this->breadcrumbs=array(
	'Yii Roles'=>array('index'),
	$model->id_role,
);

$this->menu=array(
	array('label'=>'List YiiRole', 'url'=>array('index')),
	array('label'=>'Create YiiRole', 'url'=>array('create')),
	array('label'=>'Update YiiRole', 'url'=>array('update', 'id'=>$model->id_role)),
	array('label'=>'Delete YiiRole', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_role),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage YiiRole', 'url'=>array('admin')),
);
?>

<h1>View YiiRole #<?php echo $model->id_role; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_role',
		'name_role',
		'type_role',
	),
)); ?>
