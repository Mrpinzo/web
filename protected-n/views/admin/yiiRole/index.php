<?php
/* @var $this YiiRoleController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Yii Roles',
);

$this->menu=array(
	array('label'=>'Create YiiRole', 'url'=>array('create')),
	array('label'=>'Manage YiiRole', 'url'=>array('admin')),
);
?>

<h1>Yii Roles</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
