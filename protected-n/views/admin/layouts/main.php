<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="language" content="en"/>
    
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css"/>
     <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/admin/main.css"/>
     <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/font/font-awesome/css/font-awesome.min.css"/>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/home/js/jquery-2.1.4.min.js"></script>
     <script src="<?php echo Yii::app()->request->baseUrl?>/js/plupload/plupload.full.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/admin/main.js"></script>
      <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/highslide/highslide-full.js"></script>
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/js/highslide/highslide.css" rel="stylesheet" type="text/css" /> 
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/js/chosen/chosen.css" rel="stylesheet" type="text/css" />  
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-ui.js"></script>
		 <link href="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-ui.css" rel="stylesheet" type="text/css" />
		 
<script type="text/javascript">
hs.graphicsDir = '<?php echo Yii::app()->request->baseUrl; ?>/js/highslide/graphics/';
hs.align = 'center';
hs.transitions = ['expand', 'crossfade'];
hs.outlineType = 'rounded-white';
hs.fadeInOut = true;
//hs.dimmingOpacity = 0.75;

// Add the controlbar
hs.addSlideshow({
	//slideshowGroup: 'group1',
	interval: 5000,
	repeat: false,
	useControls: true,
	fixedControls: 'fit',
	overlayOptions: {
		opacity: 0.75,
		position: 'bottom center',
		hideOnMouseOut: true
	}
});
</script>		
</head>

<body>

<div class="container" id="page">
<div id="header">
        <div id="logo"><img src="/images/admin/logos.png" alt="Admin" width="45" height="33"></img><span id="adminslo">Admin</span> <span id="homelink"><a href="/" target="_blank"><i class="fa fa-home"></i></a></span></div>
       <div id="tk">Xin chào : 
           <?= Yii::app()->user->name ?>
	       <a href="<?php echo $this->createUrl('/configure/logout')?>" title="Thoát">
	               <i class="fa fa-power-off"></i>
	       
	       </a>
       </div>
    </div>
    <!-- header -->
    <div id="mainmenu">
        <?php
        $this->widget('zii.widgets.CMenu', array(
        		'encodeLabel' => false,
        		
            'items' => array(
            //array('label' => '<i class="fa fa-bar-chart"></i>Thống kê truy cập<i class="fa fa-angle-right"></i>', 'url' => array('/site/index')),
            	array('label' => '<i class="fa fa-cog"></i>Cấu hình<i class="fa fa-angle-right"></i>', 'url' => array('/configure/view?id=1'),'itemOptions'=>array('id' => 'configure_ads')),
            	array('label' => '<i class="fa fa-user"></i>Quản Trị<i class="fa fa-angle-right"></i>', 'url' => array('/administrator/admin')),
            	array('label' => '<i class="fa fa-group"></i>Nhóm quyền<i class="fa fa-angle-right"></i>', 'url' => array('/YiiRole/admin')),
            		array('label' => '<i class="fa fa-list-ul"></i>Danh sách menu<i class="fa fa-angle-right"></i>', 'url' => array('/yiiMenu/admin')),
            		//array('label' => '<i class="fa fa-search"></i>Bộ lọc tìm kiếm<i class="fa fa-angle-right"></i>', 'url' => array('/YiiFilter/admin')),
            		//array('label' => '<i class="fa fa-inbox"></i>Sản Phẩm<i class="fa fa-angle-right"></i>', 'url' => array('/product/admin')),
                    array('label' => '<i class="fa fa-inbox"></i>Images Event<i class="fa fa-angle-right"></i>', 'url' => array('/pasevent/admin')),
            	   array('label' => '<i class="fa fa-inbox"></i>Comment<i class="fa fa-angle-right"></i>', 'url' => array('/comment/admin')),
                	//array('label' => '<i class="fa fa-shopping-cart"></i>Đơn Hàng<i class="fa fa-angle-right"></i>', 'url' => array('/orders/admin')),
                    array('label' => '<i class="fa fa-newspaper-o"></i>Tin Tức<i class="fa fa-angle-right"></i>', 'url' => array('/news/admin')),
            		array('label' => '<i class="fa fa-picture-o"></i>Loại Album<i class="fa fa-angle-right"></i>', 'url' => array('/YiiTypealbum/admin')),
            		//array('label' => '<i class="fa fa-film"></i>Loại Video<i class="fa fa-angle-right"></i>', 'url' => array('/YiiTypevideo/admin')),
            		//array('label' => '<i class="fa fa-youtube-play"></i>Video<i class="fa fa-angle-right"></i>', 'url' => array('/YiiVideo/admin')),
            		array('label' => '<i class="fa fa-bullhorn"></i>Slide<i class="fa fa-angle-right"></i>', 'url' => array('/advertise/admin')),
            		array('label' => '<i class="fa fa-street-view"></i>Videos<i class="fa fa-angle-right"></i>', 'url' => array('/links/admin')),           		
            		array('label' => '<i class="fa fa-info-circle"></i>Contact<i class="fa fa-angle-right"></i>', 'url' => array('/Contact/admin')),
            	   array('label' => '<i class="fa fa-inbox"></i>Register<i class="fa fa-angle-right"></i>', 'url' => array('/register/admin')),
                   array('label' => '<i class="fa fa-inbox"></i>Download<i class="fa fa-angle-right"></i>', 'url' => array('/downloadyii/admin')),
                   array('label' => '<i class="fa fa-envelope"></i>Danh sách email<i class="fa fa-angle-right"></i>', 'url' => array('/YiiEmail/admin')),
            		array('label' => 'Album<i class="fa fa-angle-right"></i>', 'url' => array('/YiiAlbum/admin')),
            		
            ),
        ));
        ?>
    </div>
    <!-- mainmenu -->
 <div id="content_main">
    <?php
        $this->widget('zii.widgets.CBreadcrumbs', array(
            'links' => $this->breadcrumbs,
        ));
    ?><!-- breadcrumbs -->
    <div class="container">

	<div class="span-19">
		<div id="content">
		<div id="sidebar">
		<?php
			$this->beginWidget('zii.widgets.CPortlet', array(
				
			));
			$this->widget('zii.widgets.CMenu', array(
				'items'=>$this->menu,
				'htmlOptions'=>array('class'=>'operations'),
			));
			$this->endWidget();
		?>
		</div><!-- sidebar -->
			<?php echo $content; ?>
		</div><!-- content -->
	</div>
	
</div>
</div>
</div>
<!-- page -->
<div id="footers">
<?php $this->widget('Footeradmin'); ?>
</div>
<div id="icon_ajax"><img src="<?php echo Yii::app()->request->baseUrl ?>/images/ajax-loader.gif" alt="Logo" border="0" width="50" height="50" ></div>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/chosen/chosen.jquery.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/chosen/docsupport/prism.js" type="text/javascript" charset="utf-8"></script>
 <script type="text/javascript">
    var config = {
      '.chosen-select'           : {},
    }
    for (var selector in config) {
      $(selector).chosen(config[selector]);
    }
  </script>		 
</body>
</html>