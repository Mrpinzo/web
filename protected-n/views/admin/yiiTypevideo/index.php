<?php
/* @var $this YiiTypevideoController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Yii Typevideos',
);

$this->menu=array(
	array('label'=>'Create YiiTypevideo', 'url'=>array('create')),
	array('label'=>'Manage YiiTypevideo', 'url'=>array('admin')),
);
?>

<h1>Yii Typevideos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
