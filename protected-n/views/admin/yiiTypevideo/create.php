<?php
/* @var $this YiiTypevideoController */
/* @var $model YiiTypevideo */

$this->breadcrumbs=array(
	'Loại Video'=>array('admin'),
	'Tạo mới',
);

$this->menu=array(
	array('label'=>'Danh sách Video', 'url'=>array('admin')),
);
?>

<h4>Tạo mới Video</h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>