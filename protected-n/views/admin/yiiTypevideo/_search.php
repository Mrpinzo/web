<?php
/* @var $this YiiTypevideoController */
/* @var $model YiiTypevideo */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_typevideo'); ?>
		<?php echo $form->textField($model,'id_typevideo'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'name__typevideo'); ?>
		<?php echo $form->textField($model,'name__typevideo',array('size'=>60,'maxlength'=>500)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'showhide__typevideo'); ?>
		<?php echo $form->textField($model,'showhide__typevideo'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'url_typevideo'); ?>
		<?php echo $form->textField($model,'url_typevideo',array('size'=>60,'maxlength'=>500)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'title_typevideo'); ?>
		<?php echo $form->textField($model,'title_typevideo',array('size'=>60,'maxlength'=>500)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'keywords_typevideo'); ?>
		<?php echo $form->textField($model,'keywords_typevideo',array('size'=>60,'maxlength'=>500)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'description_typevideo'); ?>
		<?php echo $form->textField($model,'description_typevideo',array('size'=>60,'maxlength'=>500)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->