<?php
/* @var $this YiiVideoController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Yii Videos',
);

$this->menu=array(
	array('label'=>'Create YiiVideo', 'url'=>array('create')),
	array('label'=>'Manage YiiVideo', 'url'=>array('admin')),
);
?>

<h1>Yii Videos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
