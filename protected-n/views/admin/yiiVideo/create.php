<?php
/* @var $this YiiVideoController */
/* @var $model YiiVideo */

$this->breadcrumbs=array(
	'Video'=>array('admin'),
	'Tạo mới',
);

$this->menu=array(
	array('label'=>'Dánh sách Video', 'url'=>array('admin')),
);
?>

<h4>Tạo mới Video</h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>