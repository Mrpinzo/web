<?php
/* @var $this ConfigureController */
/* @var $model Configure */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_configure'); ?>
		<?php echo $form->textField($model,'id_configure'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'logo_configure'); ?>
		<?php echo $form->textField($model,'logo_configure',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'about_configure'); ?>
		<?php echo $form->textField($model,'about_configure',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->