<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/css/ckeditor/ckeditor.js');

?>
<style type="text/css">
 #configure-form img{
    max-width:3%;
 }
</style>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'configure-form',
	'enableAjaxValidation'=>false,
		'htmlOptions'          => array(
				'enctype' => 'multipart/form-data',
		),
)); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'logo_configure'); ?>
		<?php echo $form->fileField($model, 'logo_configure'); ?><span id="imgs">Tập tin ảnh có định dạng (jpg, gif, png)</span>
		<?php echo !$model->isNewRecord ? '<img src="'.$model->logo_configure .'"/>':'' ?>
		<?php echo $form->error($model,'logo_configure'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'paymentgateways'); ?>
		<?php echo $form->fileField($model, 'paymentgateways'); ?><span id="imgs">Tập tin ảnh có định dạng (jpg, gif, png)</span>
		<?php echo !$model->isNewRecord ? '<img src="'.$model->paymentgateways .'"/>':'' ?>
		<?php echo $form->error($model,'paymentgateways'); ?>
	</div>
		
	<div class="row">
		<?php echo $form->labelEx($model,'slogan'); ?>
		<?php echo $form->fileField($model, 'slogan'); ?><span id="imgs">Tập tin ảnh có định dạng (jpg, gif, png)</span>
		<?php echo !$model->isNewRecord ? '<img src="'.$model->slogan .'"/>':'' ?>
		<?php echo $form->error($model,'slogan'); ?>
	</div>
    <div class="row">
		<?php echo $form->labelEx($model,'imgdl_configure'); ?>
		<?php echo $form->fileField($model, 'imgdl_configure'); ?><span id="imgs">Tập tin ảnh có định dạng (jpg, gif, png)</span>
		<?php echo !$model->isNewRecord ? '<img src="'.$model->imgdl_configure .'"/>':'' ?>
		<?php echo $form->error($model,'imgdl_configure'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'diachi1'); ?>
		<?php echo $form->textField($model,'diachi1',array('size'=>60,'maxlength'=>2000)); ?>
		<?php echo $form->error($model,'diachi1'); ?>
	</div>
    	<div class="row">
		<?php echo $form->labelEx($model,'diachi2'); ?>
		<?php echo $form->textField($model,'diachi2',array('size'=>60,'maxlength'=>2000)); ?>
		<?php echo $form->error($model,'diachi2'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'title_seo'); ?>
		<?php echo $form->textField($model,'title_seo',array('size'=>60,'maxlength'=>2000)); ?>
		<?php echo $form->error($model,'chatyahoo2_configure'); ?>
	</div>	
	<div class="row">
		<?php echo $form->labelEx($model,'description_seo'); ?>
		<?php echo $form->textField($model,'description_seo',array('size'=>60,'maxlength'=>2000)); ?>
		<?php echo $form->error($model,'description_seo'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'keywords_seo'); ?>
		<?php echo $form->textField($model,'keywords_seo',array('size'=>60,'maxlength'=>2000)); ?>
		<?php echo $form->error($model,'keywords_seo'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'url_home'); ?>
		<?php echo $form->textField($model,'url_home',array('size'=>60,'maxlength'=>2000)); ?>
		<?php echo $form->error($model,'url_home'); ?>
	</div>
<!--	
	<div class="row">
		<?php echo $form->labelEx($model,'latitude_configure'); ?>
		<?php echo $form->textField($model,'latitude_configure',array('size'=>60,'maxlength'=>2000)); ?>
		<?php echo $form->error($model,'latitude_configure'); ?>
	</div>	
	
	<div class="row">
		<?php echo $form->labelEx($model,'longitude_configure'); ?>
		<?php echo $form->textField($model,'longitude_configure',array('size'=>60,'maxlength'=>2000)); ?>
		<?php echo $form->error($model,'longitude_configure'); ?>
	</div>	
 -->
	<div class="row">
		<?php echo $form->labelEx($model,'manhung'); ?>
		<?php echo $form->textArea($model,'manhung',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'manhung'); ?>
	</div>
 
    <div class="row">
		<?php echo $form->labelEx($model,'ga_email'); ?>
		<?php echo $form->textField($model,'ga_email',array('size'=>60,'maxlength'=>2000)); ?>
		<?php echo $form->error($model,'ga_email'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'about_configure'); ?>
		<?php echo $form->textArea($model,'about_configure',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'about_configure'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'title_contact'); ?>
		<?php echo $form->textArea($model,'title_contact',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'title_contact'); ?>
	</div>

		
	<div class="row">
		<?php echo $form->labelEx($model,'ServiceCenter'); ?>
		<?php echo $form->textArea($model,'ServiceCenter',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'ServiceCenter'); ?>
	</div>
		
	<div class="row">
		<?php echo $form->labelEx($model,'page_facebook'); ?>
		<?php echo $form->textField($model,'page_facebook',array('size'=>60,'maxlength'=>2000)); ?>
		<?php echo $form->error($model,'page_facebook'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'page_google'); ?>
		<?php echo $form->textField($model,'page_google',array('size'=>60,'maxlength'=>2000)); ?>
		<?php echo $form->error($model,'page_google'); ?>
	</div>
	
    
    
    <div class="row">
		<?php echo $form->labelEx($model,'facebook_configure'); ?>
		<?php echo $form->textField($model,'facebook_configure',array('size'=>60,'maxlength'=>2000)); ?>
		<?php echo $form->error($model,'facebook_configure'); ?>
	</div>
    <div class="row">
		<?php echo $form->labelEx($model,'twitter'); ?>
		<?php echo $form->textField($model,'twitter',array('size'=>60,'maxlength'=>2000)); ?>
		<?php echo $form->error($model,'twitter'); ?>
	</div>
    <div class="row">
		<?php echo $form->labelEx($model,'in_lk'); ?>
		<?php echo $form->textField($model,'in_lk',array('size'=>60,'maxlength'=>2000)); ?>
		<?php echo $form->error($model,'in_lk'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'page_youtube'); ?>
		<?php echo $form->textField($model,'page_youtube',array('size'=>60,'maxlength'=>2000)); ?>
		<?php echo $form->error($model,'page_youtube'); ?>
	</div>
    
    
    
    
	<div class="row">
		<?php echo $form->labelEx($model,'telephone'); ?>
		<?php echo $form->textField($model,'telephone',array('size'=>60,'maxlength'=>2000)); ?>
		<?php echo $form->error($model,'telephone'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'code_google_analytics'); ?>
		<?php echo $form->textField($model,'code_google_analytics',array('size'=>60,'maxlength'=>2000)); ?>
		<?php echo $form->error($model,'code_google_analytics'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'Verification_google'); ?>
		<?php echo $form->textField($model,'Verification_google',array('size'=>60,'maxlength'=>2000)); ?>
		<?php echo $form->error($model,'Verification_google'); ?>
	</div>


	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Tạo mới' : 'Sửa'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script type="text/javascript">
               CKEDITOR.config.allowedContent = true;
                CKEDITOR.replace('Configure[about_configure]', {
                    'extraPlugins': 'showblocks,div,doksoft_backup,doksoft_stat,iframe',
                    'filebrowserImageBrowseUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgbrowse/imgbrowse.html'?>',
                    'filebrowserImageUploadUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgupload.php'?>',
                  });

                 CKEDITOR.replace('Configure[manhung]', {
                    'extraPlugins': 'showblocks,div,doksoft_backup,doksoft_stat,iframe',
                    'filebrowserImageBrowseUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgbrowse/imgbrowse.html'?>',
                    'filebrowserImageUploadUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgupload.php'?>',
                  });
                
                CKEDITOR.replace('Configure[title_contact]', {
                    'extraPlugins': 'showblocks,div,doksoft_backup,doksoft_stat,iframe',
                    'filebrowserImageBrowseUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgbrowse/imgbrowse.html'?>',
                    'filebrowserImageUploadUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgupload.php'?>',
                  });

                
                CKEDITOR.replace('Configure[ServiceCenter]', {
                    'extraPlugins': 'showblocks,div,doksoft_backup,doksoft_stat,iframe',
                    'filebrowserImageBrowseUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgbrowse/imgbrowse.html'?>',
                    'filebrowserImageUploadUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgupload.php'?>',
                  });

                CKEDITOR.replace('Configure[contact_top]', {
                    'extraPlugins': 'showblocks,div,doksoft_backup,doksoft_stat,iframe',
                    'filebrowserImageBrowseUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgbrowse/imgbrowse.html'?>',
                    'filebrowserImageUploadUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgupload.php'?>',
                  });
            </script> 