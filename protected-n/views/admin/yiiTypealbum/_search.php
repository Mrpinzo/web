<?php
/* @var $this YiiTypealbumController */
/* @var $model YiiTypealbum */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_typealbum'); ?>
		<?php echo $form->textField($model,'id_typealbum'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_menu'); ?>
		<?php echo $form->textField($model,'id_menu'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'name_typealbum'); ?>
		<?php echo $form->textField($model,'name_typealbum',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'showhide_typealbum'); ?>
		<?php echo $form->textField($model,'showhide_typealbum'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'title_seo_album'); ?>
		<?php echo $form->textField($model,'title_seo_album',array('size'=>60,'maxlength'=>500)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'description_seo_album'); ?>
		<?php echo $form->textField($model,'description_seo_album',array('size'=>60,'maxlength'=>500)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'keywords_seo_album'); ?>
		<?php echo $form->textField($model,'keywords_seo_album',array('size'=>60,'maxlength'=>500)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->