<?php
/* @var $this DownloadyiiController */
/* @var $model Downloadyii */

$this->breadcrumbs=array(
	'Downloadyii'=>array('admin'),
	'Danh sách',
);
/* 
$this->menu=array(
	array('label'=>'List Downloadyii', 'url'=>array('index')),
	array('label'=>'Create Downloadyii', 'url'=>array('create')),
); */

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#downloadyii-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h4>List Download</h4>
<?php $form = $this->beginWidget('CActiveForm', array(
    'enableAjaxValidation' => TRUE,
)); ?>
<?php echo CHtml::ajaxSubmitButton('Xóa', array('Downloadyii/ajaxUpdate', 'act' => 'doDelete'), array('success' => 'reloadGrid', 'beforeSend' => 'function(){
            return confirm("Bạn có chắc chắn muốn xóa những sản phẩm được chọn?")
        }',)); ?>


<?php 
$cs = Yii::app()->clientScript;
$css = 'ul.yiiPager .first, ul.yiiPager .last {display:inline;}';
$cs->registerCss('show_first_last_buttons', $css);

$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'downloadyii-grid',
		'ajaxUpdate'=>false,
	'dataProvider'=>$model->search(),
	'filter'=>$model,
		'pager' => array('maxButtonCount' => 4,'pageSize'=>10,'firstPageLabel'=>'<i class="fa fa-angle-double-left"></i>','lastPageLabel'=>'<i class="fa fa-angle-double-right"></i>'
				,'nextPageLabel' => '<i class="fa fa-angle-right"></i>',
				'prevPageLabel'=> '<i class="fa fa-angle-left"></i>',
				'header'=> '',
					
		),
	'columns'=>array(
			array(
					'id'             => 'autoId',
					'class'          => 'CCheckBoxColumn',
					'selectableRows' => '50',
			),
		'id_downloadyii',
        'namexh_downloadyii',
		'fullnam_downloadyii',
        'lastnam_downloadyii',
        'namenew_downloadyii',
		'company_downloadyii',
		'email_downloadyii',
	   	'address_downloadyii',
		'title_downloadyii',
		'country_downloadyii',
        'areacode_downloadyii',
        'phone_downloadyii',
		array(
			'class'=>'CButtonColumn',
				'header' => 'Hành động',
				'template'=>'{view}{delete}',
				'buttons'=>array
				(
						'view' => array(
								'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'View')),
								'label' => '<i class="fa fa-search"></i>',
								'imageUrl' => false,
						),
						'delete' => array(
								'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'Delete')),
								'label' => '<i class="fa fa-trash-o"></i>',
								'imageUrl' => false,
						)
				),
		),
	),
)); ?>
	<script>
    function reloadGrid(data) {
        $.fn.yiiGridView.update('downloadyii-grid');
    }
</script>

<?php $this->endWidget(); ?>