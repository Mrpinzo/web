<?php
/* @var $this DownloadyiiController */
/* @var $model Downloadyii */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_downloadyii'); ?>
		<?php echo $form->textField($model,'id_downloadyii'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fullnam_downloadyii'); ?>
		<?php echo $form->textField($model,'fullnam_downloadyii',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'company_downloadyii'); ?>
		<?php echo $form->textField($model,'company_downloadyii',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'phone_downloadyii'); ?>
		<?php echo $form->textField($model,'phone_downloadyii',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'email_downloadyii'); ?>
		<?php echo $form->textField($model,'email_downloadyii',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'address_downloadyii'); ?>
		<?php echo $form->textField($model,'address_downloadyii',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'title_downloadyii'); ?>
		<?php echo $form->textField($model,'title_downloadyii',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'content_downloadyii'); ?>
		<?php echo $form->textArea($model,'content_downloadyii',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'mobile_downloadyii'); ?>
		<?php echo $form->textField($model,'mobile_downloadyii',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->