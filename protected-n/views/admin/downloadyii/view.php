<?php
/* @var $this downloadyiiController */
/* @var $model downloadyii */

$this->breadcrumbs=array(
	'Downloadyii'=>array('admin'),
	$model->title_downloadyii,
);

$this->menu=array(
	
	array('label'=>'Liên hệ', 'url'=>array('admin')),
);
?>

<h4>Chi tiết liên hệ: <?php echo $model->title_downloadyii; ?></h4>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_downloadyii',
        'namexh_downloadyii',
		'fullnam_downloadyii',
        'lastnam_downloadyii',
        'namenew_downloadyii',
		'company_downloadyii',
		'email_downloadyii',
	   	'address_downloadyii',
		'title_downloadyii',
		'country_downloadyii',
        'areacode_downloadyii',
        'phone_downloadyii',
	),
)); ?>
