<?php
/* @var $this YiiAlbumController */
/* @var $model YiiAlbum */

$this->breadcrumbs=array(
	'Albums'=>array('admin'),
	'Tạo mới',
);

$this->menu=array(
	array('label'=>'Danh sách Album', 'url'=>array('admin')),
);
?>

<h4>Tạo mới Album</h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>