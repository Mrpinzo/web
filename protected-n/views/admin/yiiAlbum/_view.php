<?php
/* @var $this YiiAlbumController */
/* @var $data YiiAlbum */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_album')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_album), array('view', 'id'=>$data->id_album)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_typealbum')); ?>:</b>
	<?php echo CHtml::encode($data->id_typealbum); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('img_album')); ?>:</b>
	<?php echo CHtml::encode($data->img_album); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('showhide_album')); ?>:</b>
	<?php echo CHtml::encode($data->showhide_album); ?>
	<br />


</div>