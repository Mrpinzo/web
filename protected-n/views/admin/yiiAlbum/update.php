<?php
/* @var $this YiiAlbumController */
/* @var $model YiiAlbum */

$this->breadcrumbs=array(
	'Albums'=>array('admin'),
	$model->id_typealbum=>array('view','id'=>$model->id_album),
	'Sửa',
);

$this->menu=array(
	array('label'=>'Danh sách Album', 'url'=>array('admin')),
);
?>

<h4>Sửa Album <?php echo $model->id_typealbum; ?></h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>