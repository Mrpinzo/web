<?php
/* @var $this OrdersController */
/* @var $data Orders */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ord_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ord_id), array('view', 'id'=>$data->ord_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ord_email')); ?>:</b>
	<?php echo CHtml::encode($data->ord_email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ord_Gender')); ?>:</b>
	<?php echo CHtml::encode($data->ord_Gender); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ord_Dateofbirth')); ?>:</b>
	<?php echo CHtml::encode($data->ord_Dateofbirth); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ord_payment')); ?>:</b>
	<?php echo CHtml::encode($data->ord_payment); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ord_name')); ?>:</b>
	<?php echo CHtml::encode($data->ord_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ord_from')); ?>:</b>
	<?php echo CHtml::encode($data->ord_from); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('ord_phone')); ?>:</b>
	<?php echo CHtml::encode($data->ord_phone); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ord_date')); ?>:</b>
	<?php echo CHtml::encode($data->ord_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ord_status')); ?>:</b>
	<?php echo CHtml::encode($data->ord_status); ?>
	<br />

	*/ ?>

</div>