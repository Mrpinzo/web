<?php
/* @var $this OrdersController */
/* @var $model Orders */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'orders-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'ord_email'); ?>
		<?php echo $form->textField($model,'ord_email',array('size'=>60,'maxlength'=>150)); ?>
		<?php echo $form->error($model,'ord_email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ord_Gender'); ?>
		<?php echo $form->textField($model,'ord_Gender'); ?>
		<?php echo $form->error($model,'ord_Gender'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ord_Dateofbirth'); ?>
		<?php echo $form->textField($model,'ord_Dateofbirth'); ?>
		<?php echo $form->error($model,'ord_Dateofbirth'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ord_name'); ?>
		<?php echo $form->textField($model,'ord_name',array('size'=>60,'maxlength'=>150)); ?>
		<?php echo $form->error($model,'ord_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ord_from'); ?>
		<?php echo $form->textField($model,'ord_from',array('size'=>60,'maxlength'=>150)); ?>
		<?php echo $form->error($model,'ord_from'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ord_phone'); ?>
		<?php echo $form->textField($model,'ord_phone'); ?>
		<?php echo $form->error($model,'ord_phone'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ord_date'); ?>
		<?php echo $form->textField($model,'ord_date'); ?>
		<?php echo $form->error($model,'ord_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ord_status'); ?>
		<?php echo $form->textField($model,'ord_status'); ?>
		<?php echo $form->error($model,'ord_status'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->