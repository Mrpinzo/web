<?php $this->pageTitle=Yii::app()->name;
   
?>
<div id="thongkex">
<h4>Thống kê truy cập</h4>

<?php $this->widget('ext.gaCounter.ExtGoogleAnalyticsCounter', array(
    'strTotalVisits' => 'Tổng số lần truy cập',
    'strDayVisits' => 'Số lượt xem ngày hôm nay')
);
?>
<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
<div id="datelist">
 <div class="row">
      <input type="text" name="YiiStatistical[datestart]" id="datestart" placeholder='Ngày bắt đầu'>
 </div>
 
 <div class="row">
      <input type="text" name="YiiStatistical[dateend]" id="dateend" placeholder='Ngày kết thúc'>
 </div>
 <div class="row">
		<?php echo CHtml::submitButton('Thống kê',array('id'=>'thongkesubmit')); ?>
	</div>
 </div>
 

<?php $this->endWidget(); ?>
<?php 
$this->widget('ext.gaCounter.ExtGoogleAnalyticsCounter', array(
		'customDateChart' => true,
		'startDate' => $datestart,
		'endDate' => $dateend,
		'typeChart' => 'day',  //day or month. Default is month
		'title' => 'Khoảng thời gian từ : ') //Optional
);
?>
</div>
