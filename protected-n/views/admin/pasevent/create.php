<?php
/* @var $this ProductController */
/* @var $model Product */

$this->breadcrumbs=array(
	'Sản phẩm'=>array('admin'),
	'Tạo mới',
);

$this->menu=array(
	array('label'=>'Danh sách sản phẩm', 'url'=>array('admin')),
);
?>

<h4>Tạo mới sản phẩm</h4>

<?php echo $this->renderPartial('_form', array('model'=>$model,'YiiMenu' =>$YiiMenu,'id'=>$id,'subarrFilter'=>$subarrFilter)); ?>