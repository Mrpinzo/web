<?php
/* @var $this PaseventController */
/* @var $model Pasevent */
/* @var $form CActiveForm */
Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/css/ckeditor/ckeditor.js');

?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'Pasevent-form',
	'enableAjaxValidation'=>false,
		'enableClientValidation'=>true,
		'htmlOptions'          => array(
				'enctype' => 'multipart/form-data',
		),
)); ?>

<?php

		if($model->isNewRecord == true)
		    $imgIdpr = rand().$id;
		else
			$imgIdpr = $join;
		?>
<?php echo $form->hiddenField($model,'pr_joinImg',array('value'=>$imgIdpr)); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'id_menu'); ?>
		<?php echo $form->dropDownList($model, 'id_menu', $YiiMenu, array("empty" => "Chọn chuyên mục")) ?>
		<?php echo $form->error($model,'id_menu'); ?>
		<div class="err" id="err_id_menu"></div>
	</div>

    <div class="row">
		<?php echo $form->labelEx($model,'pr_code'); ?>
		<?php echo $form->textField($model,'pr_code'); ?>
		<?php echo $form->error($model,'pr_code'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'pr_name'); ?>
		<?php echo $form->textField($model,'pr_name',array('size'=>60,'maxlength'=>1000)); ?>
		<?php echo $form->error($model,'pr_name'); ?>
		<div class="err" id="err_pr_name"></div>
	</div>

	 <div class="uploadfilemutil">
	        <div id="container">
			    <div id="filelist"></div>
			    <a id="pickfiles" href="javascript:;">Chọn tệp tin ảnh</a>
			    <span> <a id="uploadfiles" href="javascript:;">Tải ảnh</a><span>  (File tải lên phải là file ảnh : jpg ,png ,gif - Dung lượng nhỏ hơn 1M)</span>
			 </span>
			</div>
	</div>
	
	<?php if(!$model->isNewRecord){ ?>
		<div id="imguploadmutil">
		   <ul>
		   <?php foreach ($imgproduct as $val){ ?>
		     <li>
		         <div>
		          <a href="<?php echo $val['url_yii_imgproduct'] ?>" class="highslide" onclick="return hs.expand(this)">
					<img src="<?php echo $val['url_yii_imgproduct'] ?>"  width="90" height="90" />
				</a>
		         </div>
		         <span>
		         <p id="<?php echo $val['id_yii_imgproduct']?>" class="showhides_pro" ><?php echo $val['showhide_yii_imgproduct'] == 1?'<i class="fa fa-check-square"></i>':'<i class="fa fa-minus-square"></i>'?></i></p>
		        
		         <p id="<?php echo $val['id_yii_imgproduct']?>" class="deleteimg_pro"><i class="fa fa-times"></i></p>
		         </span>
		      </li>
		      <?php }?>
		   </ul>
		</div>
		<?php } ?>
			

	<div class="row">
		<?php echo $form->labelEx($model,'pr_title_seo'); ?>
		<?php echo $form->textArea($model,'pr_title_seo',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'pr_title_seo'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'pr_keywords_seo'); ?>
		<?php echo $form->textArea($model,'pr_keywords_seo',array('rows'=>6, 'cols'=>50)); ?><span style="font-size: 12px;padding-left:5px">Các từ khóa cách nhau bởi dấu phẩy ','</span>
		<?php echo $form->error($model,'pr_keywords_seo'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'pr_description_seo'); ?>
		<?php echo $form->textArea($model,'pr_description_seo',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'pr_description_seo'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'pr_highlights'); ?>
		<?php echo $form->checkBox($model, 'pr_highlights'); ?>
		<?php echo $form->error($model,'pr_highlights'); ?>
	</div>

	
	<div class="row">
		<?php echo $form->labelEx($model,'pr_showhide'); ?>
		<?php echo $form->checkBox($model, 'pr_showhide'); ?>
		<?php echo $form->error($model,'pr_showhide'); ?>
	</div>
	
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Tạo mới' : 'Sửa'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
 <script type="text/javascript">
               CKEDITOR.config.allowedContent = true;
                CKEDITOR.replace('Pasevent[pr_mota]', {
                    'extraPlugins': 'showblocks,div,doksoft_backup,doksoft_stat,iframe',
                    'filebrowserImageBrowseUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgbrowse/imgbrowse.html'?>',
                    'filebrowserImageUploadUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgupload.php'?>',
                  });

                CKEDITOR.replace('Pasevent[pr_summary]', {
                    'extraPlugins': 'showblocks,div,doksoft_backup,doksoft_stat,iframe',
                    'filebrowserImageBrowseUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgbrowse/imgbrowse.html'?>',
                    'filebrowserImageUploadUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgupload.php'?>',
                  });

             

                CKEDITOR.replace('Pasevent[specifications]', {
                    'extraPlugins': 'showblocks,div,doksoft_backup,doksoft_stat,iframe',
                    'filebrowserImageBrowseUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgbrowse/imgbrowse.html'?>',
                    'filebrowserImageUploadUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgupload.php'?>',
                  });

            </script> 
            
            <script type="text/javascript">
    // Custom example logic
    $(function() {
        
        var uploader = new plupload.Uploader({
            runtimes : 'gears,html5,flash,silverlight,browserplus',
            browse_button : 'pickfiles',
            container : 'container',
            max_file_size : '1mb',           
            url: '<?php echo $this->createUrl('Imgproduct/save/join/'.$imgIdpr) ?>',
            /* multipart_params : {'YiiAlbum_typeAlbum': typeAlbums,
            	                // 'showhide' : showhide
                               }, */
            flash_swf_url : '/plupload/js/plupload.flash.swf',
            silverlight_xap_url : '/plupload/js/plupload.silverlight.xap',
            filters : [
                {title : "Image files", extensions : "jpg,gif,png"},
                //{title : "Zip files", extensions : "zip"}
            ],
           // resize : {width : 320, height : 240, quality : 90}
        });
        
        //        uploader.bind('Init', function(up, params) {
        //            $('#filelist').html("<div>Current runtime: " + params.runtime + "</div>");
        //        });

        function checksubmit(){
    		var valid=true;
    		var  Pasevent_pr_name = $('#Pasevent_pr_name').val();
    		var  Pasevent_id_menu = $('#Pasevent_id_menu').val();
    		if(!Pasevent_id_menu)
                {
    			$('#err_id_menu').text('Vui lòng chọn loại sản phẩm');
    			valid=false;
                }else{
          		  $('#err_id_menu').empty();
          	    }		
    		
    		if(!Pasevent_pr_name)
            {
    		$('#err_pr_name').text('Vui lòng nhập tên sản phẩm');
    		valid=false;
            }else{
      		  $('#err_pr_name').empty();
      	    }
         if($('#Pasevent_pr_gia').val()){
    		if(!isFinite($('#Pasevent_pr_gia').val()))
    		{
    			$('#err_pr_gia').text('Vui lòng nhập giá sản phẩm phải là số');
    			valid=false;
    		}else{
    			$('#err_pr_gia').empty();
    		}
         }
    	return valid;
        }
        
        $('#uploadfiles').click(function(e) {     
        	var check=checksubmit();

        	if(check==true){
            uploader.start();
            e.preventDefault();
        	}
        });

        uploader.init();

        uploader.bind('FilesAdded', function(up, files) {
            $.each(files, function(i, file) {
                $('#filelist').append(
                '<div id="' + file.id + '" style="float:left" class="dele">' +
                    file.name + ' (' + plupload.formatSize(file.size) + ') <b></b>' +
                    '</div>');
            });

            up.refresh(); // Reposition Flash/Silverlight
        });

        uploader.bind('UploadProgress', function(up, file) {  
            $('#' + file.id+" b").html(file.percent + "%");
        });

        uploader.bind('Error', function(up, err) {
            $('#filelist').append("<div>"+
                "Thông báo: " + err.message +
                (err.file ? ", File: " + err.file.name : "") +
                "</div>"
        );

            up.refresh(); // Reposition Flash/Silverlight
        });

        uploader.bind('FileUploaded', function(up, file) {
            $('#' + file.id + " b").html("100%");
        });
        uploader.bind('UploadComplete', function(up, file) {
       	 alert("Tải ảnh thành công");
       });
    });
    
   
</script> 