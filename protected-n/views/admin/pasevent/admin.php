<?php
/* @var $this ProductController */
/* @var $model Product */
Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/imageTooltip.js');

$this->breadcrumbs=array(
	'Pass event'=>array('admin'),
	'Danh sách',
);

$this->menu=array(
	array('label'=>'Thêm mới', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#product-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h4>Danh sách</h4>
<?php $form = $this->beginWidget('CActiveForm', array(
    'enableAjaxValidation' => TRUE,
)); ?>

<?php echo CHtml::ajaxSubmitButton('Hiện', array('product/ajaxUpdate', 'act' => 'doActive'), array('success' => 'reloadGrid')); ?>
<?php echo CHtml::ajaxSubmitButton('Ẩn', array('product/ajaxUpdate', 'act' => 'doInactive'), array('success' => 'reloadGrid')); ?>
<?php echo CHtml::ajaxSubmitButton('Xóa', array('product/ajaxUpdate', 'act' => 'doDelete'), array('success' => 'reloadGrid', 'beforeSend' => 'function(){
            return confirm("Bạn có chắc chắn muốn xóa những sản phẩm được chọn?")
        }',)); ?>
<?php 
$cs = Yii::app()->clientScript;
$css = 'ul.yiiPager .first, ul.yiiPager .last {display:inline;}';
$cs->registerCss('show_first_last_buttons', $css);
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'product-grid',
	'ajaxUpdate'=>false,
	'dataProvider'=>$model->search(),
	'filter'=>$model,
		'pager' => array('maxButtonCount' => 4,'pageSize'=>10,'firstPageLabel'=>'<i class="fa fa-angle-double-left"></i>','lastPageLabel'=>'<i class="fa fa-angle-double-right"></i>'
				,'nextPageLabel' => '<i class="fa fa-angle-right"></i>',
				'prevPageLabel'=> '<i class="fa fa-angle-left"></i>',
				'header'=> '',
					
		),
	'columns'=>array(
			array(
					'id'             => 'autoId',
					'class'          => 'CCheckBoxColumn',
					'selectableRows' => '50',
			),
		'pr_id',
			array(
					'name'   => 'id_menu',
					'value'  => 'isset($data->id_menu)?$data->YiiMenus->name_menu:""',
					'filter' => CHtml::activeDropDownList($model, 'id_menu', $YiiMenu, array("empty" => "Chọn danh mục")),
			),

		'pr_name',
			/* array(
					'name'   => 'pr_img',
					'value'  => '!empty($data->pr_img)?"<a href=\"".Yii::app()->request->baseUrl.$data->pr_img."\" class=\"highslide\" onclick=\"return hs.expand(this)\"><img src=\"".$data->pr_img."\" width=\"50\" height=\"50\"/></a>":""',
					'type'   => 'raw',
					'filter' => FALSE,
					'sortable'   => FALSE,
			),
 */
	
			/* array(
					'name'   => 'pr_mota',
					'value'  => '$data->pr_mota',
					'type'   => 'raw',
			), */
		
		/* 'pr_title_seo',
			'pr_description_seo',
			'pr_keywords_seo', */
			//'pr_url_news',
			/* 
			array(
					'name'   => 'id_highlight',
					'value'  => $model->menuhighlight($model->id_highlight),

					'filter' => CHtml::activeDropDownList($model, 'id_highlight', $YiiMenuhl, array("empty" => "Chọn danh mục nổi bật")),
			), */
		
		
			array(
					'name'   => 'pr_highlights',
					'value'  =>  '$data->pr_highlights==1?"<i class=\"fa fa-check-square-o csm\"></i>":"<i class=\"fa fa-times csm\"></i>"',
				
					'filter' => array(1 => 'Có', 0 => 'Không'),
					'type'   => 'raw',
			),
			array(
					'name'   => 'pr_showhide',
					'value'  => '$data->pr_showhide==1?"<i class=\"fa fa-check-square-o csm\"></i>":"<i class=\"fa fa-times csm\"></i>"',
					'filter' => array(1 => 'Hiện', 0 => 'Ẩn'),
					'type'   => 'raw',
			),
			
			array(
					'name'   => 'pr_tinhtrang',
					'value'  => '$data->pr_tinhtrang==1?"<i class=\"fa fa-check-square-o csm\"></i>":"<i class=\"fa fa-times csm\"></i>"',
					'filter' => array(1 => 'Còn hàng', 0 => 'Hết hàng'),
					'type'   => 'raw',
			),
			
		array(
			'class'=>'CButtonColumn',
				'header' => 'Hành động',
				'buttons'=>array
				(
						'view' => array(
								'options' => array('target' => '_blank','rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'View')),
								'label' => '<i class="fa fa-search"></i>',
								'imageUrl' => false,
								'url'=>'"/san-pham/".$data->pr_url_news.".html"',
						),
						'update' => array(
								'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'Update')),
								'label' => '<i class="fa fa-pencil-square-o"></i>',
								'imageUrl' => false,
						),
						'delete' => array(
								'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'Delete')),
								'label' => '<i class="fa fa-trash-o"></i>',
								'imageUrl' => false,
						)
				),
		),
	),
)); ?>
<script>
    function reloadGrid(data) {
        $.fn.yiiGridView.update('product-grid');
    }
</script>
<?php $this->endWidget(); ?>