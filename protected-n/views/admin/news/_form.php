<?php
/* @var $this NewsController */
/* @var $model News */
/* @var $form CActiveForm */
Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/css/ckeditor/ckeditor.js');
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'news-form',
	'enableAjaxValidation'=>false,
		'htmlOptions'          => array(
				'enctype' => 'multipart/form-data',
		),
)); ?>
<div class="row">
		<?php echo $form->labelEx($model,'id_menu'); ?>
		<?php echo $form->dropDownList($model, 'id_menu',$YiiMenu, array("empty" => "Chọn loại tin tức")) ?>
		<?php echo $form->error($model,'id_menu'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'new_title'); ?>
		<?php echo $form->textField($model,'new_title',array('size'=>60,'maxlength'=>400)); ?>
		<?php echo $form->error($model,'new_title'); ?>
	</div>

		
	<div class="row">
		<?php echo $form->labelEx($model,'new_img'); ?>
		<?php echo $form->fileField($model, 'new_img'); ?><span id="imgs">Tập tin ảnh có định dạng (jpg, gif, png)</span>
		<?php echo !$model->isNewRecord ? '<img src="'.$model->new_img .'" width="50" height="50"/>':'' ?>
		<?php echo $form->error($model,'new_img'); ?>
	</div>
    
    <div class="row">
		<?php echo $form->labelEx($model,'img_file'); ?>
		<?php echo $form->fileField($model, 'img_file'); ?><span id="imgs">Tập tin download</span>
		<?php echo $form->error($model,'img_file'); ?>
	</div>
    
    <!------------------>
    <div class="row">
		<?php echo $form->labelEx($model,'yii_date'); ?>
		<?php echo $form->textField($model,'yii_date',array('size'=>60,'maxlength'=>400)); ?>
		<?php echo $form->error($model,'yii_date'); ?>
	</div>
    <div class="row">
		<?php echo $form->labelEx($model,'yii_year'); ?>
		<?php echo $form->textField($model,'yii_year',array('size'=>60,'maxlength'=>400)); ?>
		<?php echo $form->error($model,'yii_year'); ?>
	</div>
    <div class="row">
		<?php echo $form->labelEx($model,'yii_address'); ?>
		<?php echo $form->textField($model,'yii_address',array('size'=>60,'maxlength'=>400)); ?>
		<?php echo $form->error($model,'yii_address'); ?>
	</div>
    
    <div class="row">
		<?php echo $form->labelEx($model,'yii_overview'); ?>
		<?php echo $form->textArea($model,'yii_overview',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'yii_overview'); ?>
	</div>
    <div class="row">
		<?php echo $form->labelEx($model,'yii_content'); ?>
		<?php echo $form->textArea($model,'yii_content',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'yii_content'); ?>
	</div>
    	<div class="row">
		<?php echo $form->labelEx($model,'yii_wsattend'); ?>
		<?php echo $form->textArea($model,'yii_wsattend',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'yii_wsattend'); ?>
	</div>
    	<div class="row">
		<?php echo $form->labelEx($model,'yii_attending'); ?>
		<?php echo $form->textArea($model,'yii_attending',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'yii_attending'); ?>
	</div>
    	<div class="row">
		<?php echo $form->labelEx($model,'yii_director'); ?>
		<?php echo $form->textArea($model,'yii_director',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'yii_director'); ?>
	</div>
    <!------------------>
	<div class="row">
		<?php echo $form->labelEx($model,'new_summary'); ?>
		<?php echo $form->textArea($model,'new_summary',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'new_summary'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'new_content'); ?>
		<?php echo $form->textArea($model,'new_content',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'new_content'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'new_title_seo'); ?>
		<?php echo $form->textField($model,'new_title_seo',array('size'=>60,'maxlength'=>400)); ?>
				<?php echo $form->error($model,'new_title_seo'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'new_keywords_seo'); ?>
		<?php echo $form->textField($model,'new_keywords_seo',array('size'=>60,'maxlength'=>400)); ?><span style="font-size: 12px;padding-left:5px">Các từ khóa cách nhau bởi dấu phẩy ','</span>
				<?php echo $form->error($model,'new_keywords_seo'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'new_description_seo'); ?>
		<?php echo $form->textArea($model,'new_description_seo',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'new_description_seo'); ?>
	</div>
	
	<div class="row" style="display: none;">
		<?php echo $form->labelEx($model,'new_highlights'); ?>
		<?php echo $form->checkBox($model, 'new_highlights'); ?>
		<?php echo $form->error($model,'new_highlights'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'new_showhide'); ?>
		<?php echo $form->checkBox($model, 'new_showhide'); ?>
		<?php echo $form->error($model,'new_showhide'); ?>
	</div>
    
    <div class="row">
		<?php echo $form->labelEx($model,'new_highlights'); ?>
		<?php echo $form->checkBox($model, 'new_highlights'); ?>
		<?php echo $form->error($model,'new_highlights'); ?>
	</div>
	
	<div class="row" style="display: none;">
		<?php echo $form->labelEx($model,'agency'); ?>
		<?php echo $form->checkBox($model, 'agency'); ?>
		<?php echo $form->error($model,'agency'); ?>
	</div>
	
	<div class="row" style="display: none;">
		<?php echo $form->labelEx($model,'customer'); ?>
		<?php echo $form->checkBox($model, 'customer'); ?>
		<?php echo $form->error($model,'customer'); ?>
	</div>
	
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Tạo mới' : 'Sửa'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
 <script type="text/javascript">
               CKEDITOR.config.allowedContent = true;
                CKEDITOR.replace('News[new_summary]', {
                    'extraPlugins': 'showblocks,div,doksoft_backup,doksoft_stat,iframe',
                    'filebrowserImageBrowseUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgbrowse/imgbrowse.html'?>',
                    'filebrowserImageUploadUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgupload.php'?>',
                  });
                CKEDITOR.replace('News[new_content]', {
                    'extraPlugins': 'showblocks,div,doksoft_backup,doksoft_stat,iframe',
                    'filebrowserImageBrowseUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgbrowse/imgbrowse.html'?>',
                    'filebrowserImageUploadUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgupload.php'?>',
                  });
                  //
                   CKEDITOR.replace('News[yii_overview]', {
                    'extraPlugins': 'showblocks,div,doksoft_backup,doksoft_stat,iframe',
                    'filebrowserImageBrowseUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgbrowse/imgbrowse.html'?>',
                    'filebrowserImageUploadUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgupload.php'?>',
                  });
                   CKEDITOR.replace('News[yii_content]', {
                    'extraPlugins': 'showblocks,div,doksoft_backup,doksoft_stat,iframe',
                    'filebrowserImageBrowseUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgbrowse/imgbrowse.html'?>',
                    'filebrowserImageUploadUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgupload.php'?>',
                  });
                   CKEDITOR.replace('News[yii_wsattend]', {
                    'extraPlugins': 'showblocks,div,doksoft_backup,doksoft_stat,iframe',
                    'filebrowserImageBrowseUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgbrowse/imgbrowse.html'?>',
                    'filebrowserImageUploadUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgupload.php'?>',
                  });
                   CKEDITOR.replace('News[yii_attending]', {
                    'extraPlugins': 'showblocks,div,doksoft_backup,doksoft_stat,iframe',
                    'filebrowserImageBrowseUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgbrowse/imgbrowse.html'?>',
                    'filebrowserImageUploadUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgupload.php'?>',
                  });
                   CKEDITOR.replace('News[yii_director]', {
                    'extraPlugins': 'showblocks,div,doksoft_backup,doksoft_stat,iframe',
                    'filebrowserImageBrowseUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgbrowse/imgbrowse.html'?>',
                    'filebrowserImageUploadUrl': '<?= Yii::app()->request->baseUrl.'/css/ckeditor/plugins/imgupload.php'?>',
                  });
                
                  
            </script>  