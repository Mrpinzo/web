<?php
/* @var $this NewsController */
/* @var $model News */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'new_id'); ?>
		<?php echo $form->textField($model,'new_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'new_title'); ?>
		<?php echo $form->textField($model,'new_title',array('size'=>60,'maxlength'=>400)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'new_img'); ?>
		<?php echo $form->textField($model,'new_img',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'new_summary'); ?>
		<?php echo $form->textArea($model,'new_summary',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'new_content'); ?>
		<?php echo $form->textArea($model,'new_content',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'new_date'); ?>
		<?php echo $form->textField($model,'new_date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->