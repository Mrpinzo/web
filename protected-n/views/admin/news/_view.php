<?php
/* @var $this NewsController */
/* @var $data News */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('new_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->new_id), array('view', 'id'=>$data->new_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('new_title')); ?>:</b>
	<?php echo CHtml::encode($data->new_title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('new_img')); ?>:</b>
	<?php echo CHtml::encode($data->new_img); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('new_summary')); ?>:</b>
	<?php echo CHtml::encode($data->new_summary); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('new_content')); ?>:</b>
	<?php echo CHtml::encode($data->new_content); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('new_date')); ?>:</b>
	<?php echo CHtml::encode($data->new_date); ?>
	<br />


</div>