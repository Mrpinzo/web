<?php
/* @var $this NewsController */
/* @var $model News */

$this->breadcrumbs=array(
	'Tin tức'=>array('admin'),
	'Danh sách',
);

$this->menu=array(
	array('label'=>'Thêm mới', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#news-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h4>Tin tức</h4>
<?php $form = $this->beginWidget('CActiveForm', array(
    'enableAjaxValidation' => TRUE,
)); ?>

<?php echo CHtml::ajaxSubmitButton('Hiện', array('news/ajaxUpdate', 'act' => 'doActive'), array('success' => 'reloadGrid')); ?>
<?php echo CHtml::ajaxSubmitButton('Ẩn', array('news/ajaxUpdate', 'act' => 'doInactive'), array('success' => 'reloadGrid')); ?>
<?php echo CHtml::ajaxSubmitButton('Nổi bật', array('news/ajaxUpdate', 'act' => 'highlights'), array('success' => 'reloadGrid')); ?>
<?php echo CHtml::ajaxSubmitButton('Không nổi bật', array('news/ajaxUpdate', 'act' => 'nohighlights'), array('success' => 'reloadGrid')); ?>
<?php echo CHtml::ajaxSubmitButton('Xóa', array('news/ajaxUpdate', 'act' => 'doDelete'), array('success' => 'reloadGrid', 'beforeSend' => 'function(){
            return confirm("Bạn có chắc chắn muốn xóa những menu được chọn?")
        }',)); ?>
<?php

$cs = Yii::app()->clientScript;
$css = 'ul.yiiPager .first, ul.yiiPager .last {display:inline;}';
$cs->registerCss('show_first_last_buttons', $css);
 $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'news-grid',
 		'ajaxUpdate'=>false,
	'dataProvider'=>$model->search(),
	'filter'=>$model,
 		'pager' => array('maxButtonCount' => 4,'pageSize'=>10,'firstPageLabel'=>'<i class="fa fa-angle-double-left"></i>','lastPageLabel'=>'<i class="fa fa-angle-double-right"></i>'
 				,'nextPageLabel' => '<i class="fa fa-angle-right"></i>',
 				'prevPageLabel'=> '<i class="fa fa-angle-left"></i>',
 				'header'=> '',
 				
 		),
	'columns'=>array(
			array(
					'id'             => 'autoId',
					'class'          => 'CCheckBoxColumn',
					'selectableRows' => '50',
			),
		'new_id',
			array(
					'name'   => 'id_menu',
					'value'  => '$data->id_menu != 0?$data->YiiMenu->name_menu:""',
					'filter' => CHtml::activeDropDownList($model, 'id_menu', $YiiMenu, array("empty" => "Chọn danh mục")),
			),
			array(
					'name'   => 'new_title',
					'value'  => '$data->new_title."<i class=\"fa fa-link links\" id=\"/chi-tiet-tin-tuc/".$data->rewrite_url_news.".html\"></i>"',
					'type'   => 'raw',
			),
			array(
					'name'   => 'new_img',
					'value'  =>  '!empty($data->new_img)?"<a href=\"".Yii::app()->request->baseUrl.$data->new_img."\" class=\"highslide\" onclick=\"return hs.expand(this)\"><img src=\"".Yii::app()->request->baseUrl.$data->new_img."\" width=\"50\" height=\"50\"/></a>":""',
					'type'   => 'raw',
					'filter' => FALSE,
					'sortable'   => FALSE,
			),
		
			/* array(
					'name'   => 'new_summary',
					'value'  => 'substr($data->new_summary, 0, 100)."..."',
					'type'   => 'raw',
				
			),
			array(
					'name'   => 'new_content',
					'value'  => 'substr($data->new_content, 0, 100)."..."',
					'type'   => 'raw',
			
			), */
		
	
			array(
					'name'   => 'new_date',
					'value'  => 'date("d-m-Y",strtotime($data->new_date))',
						
			),
			array(
					'name'   => 'new_highlights',
					'value'  => '$data->new_highlights==1?"<i class=\"fa fa-check-square-o csm\"></i>":"<i class=\"fa fa-times csm\"></i>"',
					'filter' => array(1 => 'Nổi bật', 0 => 'Không nổi bật'),
					'type'   => 'raw',
			),
			array(
					'name'   => 'new_showhide',
					'value'  => '$data->new_showhide==1?"<i class=\"fa fa-check-square-o csm\"></i>":"<i class=\"fa fa-times csm\"></i>"',
					'filter' => array(1 => 'Hiện', 0 => 'Ẩn'),
					'type'   => 'raw',
			),
			/* array(
					'name'   => 'new_ads',
					'value'  => '$data->new_ads==1?"<i class=\"fa fa-check-square-o csm\"></i>":"<i class=\"fa fa-times csm\"></i>"',
					'filter' => array(1 => 'Có', 0 => 'Không'),
					'type'   => 'raw',
			), */
			array(
					'name'   => 'agency',
					'value'  => '$data->agency==1?"<i class=\"fa fa-check-square-o csm\"></i>":"<i class=\"fa fa-times csm\"></i>"',
					'filter' => array(1 => 'Có', 0 => 'Không'),
					'type'   => 'raw',
			),
			array(
					'name'   => 'customer',
					'value'  => '$data->customer==1?"<i class=\"fa fa-check-square-o csm\"></i>":"<i class=\"fa fa-times csm\"></i>"',
					'filter' => array(1 => 'Có', 0 => 'Không'),
					'type'   => 'raw',
			),
		array(
			'class'=>'CButtonColumn',
				'header' => 'Hành động',
				'buttons'=>array
				(
						'view' => array(
								'options' => array('target' => '_blank','rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'View')),
								'label' => '<i class="fa fa-search"></i>',
								'imageUrl' => false,
								'url'=>'"/chi-tiet-tin-tuc/".$data->rewrite_url_news.".html"',
						),
						'update' => array(
								'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'Update')),
								'label' => '<i class="fa fa-pencil-square-o"></i>',
								'imageUrl' => false,
						),
						'delete' => array(
								'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'Delete')),
								'label' => '<i class="fa fa-trash-o"></i>',
								'imageUrl' => false,
						)
				),
		),
	),
)); ?>
<script>
    function reloadGrid(data) {
        $.fn.yiiGridView.update('news-grid');
    }
</script>
<?php $this->endWidget(); ?>