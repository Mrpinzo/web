<?php
/* @var $this CustomersController */
/* @var $model Customers */

$this->breadcrumbs=array(
	'Danh sách khách hàng'=>array('admin'),
	$model->cust_fullname=>array('view','id'=>$model->cust_id),
	'Sửa',
);

$this->menu=array(
	array('label'=>'Danh sách khách hàng', 'url'=>array('admin')),
);
?>

<h1>Update Customers <?php echo $model->cust_id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>