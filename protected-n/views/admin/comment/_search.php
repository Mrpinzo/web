<?php
/* @var $this AdvertiseController */
/* @var $model Advertise */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'adv_id'); ?>
		<?php echo $form->textField($model,'adv_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'adv_link'); ?>
		<?php echo $form->textField($model,'adv_link',array('size'=>60,'maxlength'=>200)); ?>
	</div>


	<div class="row">
		<?php echo $form->label($model,'adv_action'); ?>
		<?php echo $form->textField($model,'adv_action'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'adv_positions'); ?>
		<?php echo $form->textField($model,'adv_positions'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->