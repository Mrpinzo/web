<?php
/* @var $this AdvertiseController */
/* @var $model Advertise */

$this->breadcrumbs=array(
	'Quảng cáo'=>array('admin'),
	$model->adv_link,
);

$this->menu=array(
	array('label'=>'Danh sách', 'url'=>array('admin')),
);
?>

<h4>Chi tiết quảng cáo <?php echo $model->adv_link; ?></h4>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'adv_id',
		'adv_link',
		
			array(
					'name'  => 'adv_action',
					'value' =>  $model->adv_action == 0? 'Ẩn':'Hiện',
					'type'  => 'raw',
			),
			array(
					'name'  => 'adv_positions',
					'value' =>  $model->adv_positions == 0? 'Trái':'Phải',
					'type'  => 'raw',
			),
	),
)); ?>
