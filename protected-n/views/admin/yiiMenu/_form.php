<?php
/* @var $this YiiMenuController */
/* @var $model YiiMenu */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'yii-menu-form',
	'enableAjaxValidation'=>false,
		'htmlOptions'          => array(
				'enctype' => 'multipart/form-data',
		),
)); ?>
<div class="row">
		<?php echo $form->labelEx($model,'level_menu'); ?>
		<?php echo $form->dropDownList($model,'level_menu', $YiiMenue, array("empty" => "---Chọn menu cha---")) ?>
		<?php echo $form->error($model,'level_menu'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'name_menu'); ?>
		<?php echo $form->textField($model,'name_menu',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'name_menu'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'img_represent'); ?>
		<?php echo $form->fileField($model, 'img_represent'); ?><span class="imgs">Tập tin ảnh có định dạng (jpg, gif, png)</span>
		<?php echo !$model->isNewRecord ? '<img src="'.$model->img_represent .'" width="50" height="50"/>':'' ?>
		<?php echo $form->error($model,'img_represent'); ?>
	</div>
<!--	
	<div class="row">
		<?php echo $form->labelEx($model,'img_background'); ?>
		<?php echo $form->fileField($model, 'img_background'); ?><span class="imgs imgsbg">Tập tin ảnh có định dạng (jpg, gif, png)</span>
		<?php echo !$model->isNewRecord ? '<img src="'.$model->img_background .'" width="50" height="50"/>':'' ?>
		<?php echo $form->error($model,'img_background'); ?>
	</div>
-->	
	<div class="row">
		<?php echo $form->labelEx($model,'location_menu'); ?>
		<?php echo $form->dropDownList($model, 'location_menu', array('1' => 'Top','2' => 'comnent', '5' => 'None'), array("empty" => "---Chọn vị trí---")) ?>
		<?php echo $form->error($model,'location_menu'); ?>
	</div>
    
	<div class="row">
		<?php echo $form->labelEx($model,'url_menu'); ?>
		<?php echo $form->textField($model,'url_menu',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'url_menu'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'order_menu'); ?>
		<?php echo $form->textField($model,'order_menu',array('size'=>60,'maxlength'=>500)); ?>
		<?php echo $form->error($model,'order_menu'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'title_seo_menu'); ?>
		<?php echo $form->textField($model,'title_seo_menu',array('size'=>60,'maxlength'=>500)); ?>
		<?php echo $form->error($model,'title_seo_menu'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'description_seo_menu'); ?>
		<?php echo $form->textField($model,'description_seo_menu',array('size'=>60,'maxlength'=>500)); ?>
		<?php echo $form->error($model,'description_seo_menu'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'keywords_seo'); ?>
		<?php echo $form->textField($model,'keywords_seo',array('size'=>60,'maxlength'=>500)); ?>
		<?php echo $form->error($model,'keywords_seo'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'highlights_menu'); ?>
		<?php echo $form->checkBox($model, 'highlights_menu'); ?>
		<?php echo $form->error($model,'highlights_menu'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'showhide_menu'); ?>
		<?php echo $form->checkBox($model, 'showhide_menu'); ?>
		<?php echo $form->error($model,'showhide_menu'); ?>
	</div>
    
    <div class="row">
		<?php echo $form->labelEx($model,'h_rightvd'); ?>
		<?php echo $form->checkBox($model, 'h_rightvd'); ?>
		<?php echo $form->error($model,'h_rightvd'); ?>
	</div>
    
	<div class="row">
		<?php echo $form->labelEx($model,'newwindow_mennu'); ?>
		<?php echo $form->checkBox($model, 'newwindow_mennu'); ?>
		<?php echo $form->error($model,'newwindow_mennu'); ?>
	</div>
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Tạo mới' : 'Sửa'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->