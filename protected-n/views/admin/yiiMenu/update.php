<?php
/* @var $this YiiMenuController */
/* @var $model YiiMenu */

$this->breadcrumbs=array(
	'Menu'=>array('admin'),
	$model->name_menu=>array('view','id'=>$model->id_menu),
	'Sửa',
);

$this->menu=array(
	array('label'=>'Danh sách Menu', 'url'=>array('admin')),
);
?>

<h4>Sửa menu <?php echo $model->name_menu; ?></h4>

<?php echo $this->renderPartial('_form', array('model'=>$model,'YiiMenue' =>$YiiMenue)); ?>