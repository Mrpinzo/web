<?php
/* @var $this YiiEmailController */
/* @var $data YiiEmail */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_email')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_email), array('view', 'id'=>$data->id_email)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date')); ?>:</b>
	<?php echo CHtml::encode($data->date); ?>
	<br />


</div>