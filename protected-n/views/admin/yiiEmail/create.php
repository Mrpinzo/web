<?php
/* @var $this YiiEmailController */
/* @var $model YiiEmail */

$this->breadcrumbs=array(
	'Danh sách email'=>array('admin'),
	'Tạo mới',
);

$this->menu=array(
	array('label'=>'Danh sách email', 'url'=>array('admin')),
);
?>

<h4>Tạo mới email</h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>