<?php
/* @var $this AdvertiseController */
/* @var $model Advertise */

$this->breadcrumbs=array(
	'Quảng cáo'=>array('admin'),
	$model->adv_link=>array('view','id'=>$model->adv_id),
	'Sửa',
);

$this->menu=array(
	array('label'=>'Danh sách', 'url'=>array('admin')),
);
?>

<h4>Sửa quảng cáo <?php echo $model->adv_link; ?></h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>