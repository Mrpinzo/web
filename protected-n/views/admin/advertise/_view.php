<?php
/* @var $this AdvertiseController */
/* @var $data Advertise */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('adv_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->adv_id), array('view', 'id'=>$data->adv_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('adv_link')); ?>:</b>
	<?php echo CHtml::encode($data->adv_link); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('adv_img')); ?>:</b>
	<?php echo CHtml::encode($data->adv_img); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('adv_action')); ?>:</b>
	<?php echo CHtml::encode($data->adv_action); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('adv_positions')); ?>:</b>
	<?php echo CHtml::encode($data->adv_positions); ?>
	<br />


</div>