<?php Yii::app()->clientScript->registerMetaTag(CHtml::encode($keywords), 'keywords'); ?>
<?php  Yii::app()->clientScript->registerMetaTag(CHtml::encode($description), 'description'); ?>

<!-------------------------------------------------------------------------------------->
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/home/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/home/jquery.mousewheel-3.0.4.pack.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/home/jquery.fancybox-1.3.4.pack.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/js/home/jquery.fancybox-1.3.4.css" media="screen" />

<script type="text/javascript">
	$(document).ready(function() {

		$("a[rel=example_group]").fancybox({
			'transitionIn'		: 'none',
			'transitionOut'		: 'none',
			'titlePosition' 	: 'over',
			'titleFormat'		: function(title, currentArray, currentIndex, currentOpts) {
				return '<span id="fancybox-title-over">Image ' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';
			}
		});

	});
</script>


 
 <div id="bg_main">
    <div id="containner" >    
       <div class="items_contents">
    
        <div class="tnel"> <div id="catnews_title"><a><?php echo $pros->name_menu ?></a></div></div>

 <!---------------------------------- end title -------------------->
 <div id="newss">
         
                  <!------------------------>
            <div id="list_passevent">
                      <div id="list_img_passevent">
                     <!----------------------------------------------------------->
                      	<?php
                				$criteria2js = new CDbCriteria();
                				$criteria2js->condition = 'pr_showhide=1 AND pr_highlights=1';
                				$criteria2js->order = 'pr_id DESC';
                                $criteria2js->limit = '8';
                				$pro = Pasevent::model()->findAll($criteria2js);
                				foreach ($pro as $vals){
                					$criteria2g            = new CDbCriteria();
                					$criteria2g->condition = 'showhide_yii_imgproduct = 1 AND pr_id="'.$vals->pr_joinImg.'"';
                					$criteria2g->group = 'pr_id';
                					$YiiImgproduct = YiiImgproduct::model()->findAll($criteria2g);
                					
                					$count = YiiImgproduct::model()->count($criteria2g);
                					if($count > 0){
                						
                				?>
                                         <?php foreach ($YiiImgproduct as $valk){?>
                                                            
                                                            <div id="hi_event" style="width: 22.5%;">
                	                                                   
                                                                  <a rel="example_group" href="<?php echo Yii::app()->request->baseUrl.$valk->url_yii_imgproduct ?>" > 
                                                                     <img src="<?php echo Yii::app()->request->baseUrl.$valk->url_yii_imgproduct ?>" alt="<?php echo $vals->pr_name ?>" />
                                                                  </a>
                                                           
                                                            </div>
                                                             
                                          <?php }?>
                                     <?php }?>
                               <?php }?>
                      
                      </div>
                  </div>    
                  
                  
                  <!------------------------> 
</div>
             
             
                 <!-------------------- new ------------->
                    <div class="comment_right" style="padding-top: 2.8% !important;">
                        <div class="comment_bg">
                            <div class="comment_right_title"><a>LIST COMMENTS</a> </div>       
                             <?php 
		                      		                       
                               $comment = new CDbCriteria();
                               $comment->condition = 'adv_action=1';
                               $comment->limit=3;
                               $comments = Comment::model()->findAll($comment);
                               foreach ($comments as $vals){
                           ?>
                           <div class="hi_comment">
                                <div class="nd_comment">
                                        <a><?php echo $vals->adv_name ?> </a>
                                </div>
                                <div class="td_comment">
                                       <a> <?php echo $vals->adv_link ?> </a>
                                </div>
                           </div>     
                         <?php } ?>
                          <div class="clickmore"><a href="/comment.html">Click see more...</a></div>
                        </div>
                       
                       <div class="videoscm">
                            <div class="comment_right_title" style="box-shadow: none;margin-bottom: 1px;"><a>Videos</a> </div>  
                             	 <script>
                                	$(function(){
                                		$('.video1').click(function(){
                                		var bien= $(this).attr('href');//alert(bien);
                                		
                                		$('.list').html(bien);
                                		});
                                		
                                	})
                                </script>
                        <?php 
		                       $video = new CDbCriteria();
                               $video->condition = 'showhide_links=1';
                               $video->order = 'id_links DESC';
                               $video->limit=3;
                               $videos = Links::model()->findAll($video);
                               foreach ($videos as $vc){
                           ?>     
                            <div class="list" style="margin-bottom: 6px;">
                    			<embed  class="video1"
                    			type="application/x-shockwave-flash" 
                    			src="<?php echo Yii::app()->request->baseUrl; ?>/images/home/images/player.swf" 
                    			width="100%" height="249" style="undefined" id="ply" name="ply" quality="high" allowfullscreen="true" allowscriptaccess="always" 
                    			flashvars="width=10&amp;height=249&amp;&amp;file=<?php echo $vc->url_links ?>&amp;feature=plcp"/>
                    		</div>
                          <?php } ?>  
                       </div>
                    </div>
        
           </div>
              
     </div>
</div>

