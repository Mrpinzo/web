<?php Yii::app()->clientScript->registerMetaTag(CHtml::encode($news->new_keywords_seo), 'keywords'); ?>
<?php  Yii::app()->clientScript->registerMetaTag(CHtml::encode($news->new_description_seo), 'description'); ?>
   <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/home/Globalv2.js" type="text/javascript"></script>
  
  
    <script type="text/javascript">
        jQuery(document).ready(function() {
            jQuery(".trainingpageContent2").hide();
            //toggle the componenet with class msg_body
            jQuery(".iirheading").click(function() {
                jQuery(this).next(".trainingpageContent2").slideToggle(800);
                jQuery(this).find('img.a').toggle();
            });
        });
    </script>
 
 <div id="bg_main">
    <div id="containner" >    
       <div class="items_contents">
    
        <div class="tnel"> <div id="catnews_title"><a>
                <?php echo $news->new_title ?>         
        
        </a></div></div>
            <div class="new_detailel_title02">
              <a> <?php echo $news->new_title ?></a>
        </div>

        <div class="new_detailel_dc">
               <a>
                    <?php echo $news->yii_date ?> <?php echo $news->yii_year ?>  <a id="mtgql"><?php echo $news->yii_address ?></a>
               </a>
        </div>        
    	<div id="crumbsContainer-sticky-wrapper" class="sticky-wrapper">
     <div id="crumbsContainer">
    <div id="trainingCrumbs">
    <ul>
    <li><a style="BORDER-LEFT: medium none" class="crumblinks"  href="#overview">Overview</a></li>
    <li><a class="crumblinks"  href="#content">Content</a></li>
    <li><a class="crumblinks"  href="#who">Who Should Attend</a></li>
    <li><a class="crumblinks"  href="#benefits">Benefits of Attending</a></li>
    <!-- Show Only When The Coourse is Certified -->
    <!--<li><a class="crumblinks" onclick="_gaq.push(['_trackPageview', '/Trainings|PageSections/In Association With']);" href="#assoc">In Association With</a></li> -->
    <li><a class="crumblinks" onclick="_gaq.push([&#39;_trackPageview&#39;, &#39;/Trainings|PageSections/Course Director&#39;]);" href="#coursedirector">Course Director</a></li>
    <li><a class="crumblinks" onclick="_gaq.push([&#39;_trackPageview&#39;, &#39;/Trainings|PageSections/Venue and Accommodation&#39;]);" href="#venue">Venue</a></li>
    <li><a class="crumblinks" onclick="_gaq.push([&#39;_trackPageview&#39;, &#39;/Trainings|PageSections/Course Fees]);" href="#fee">Course Fees</a></li>
    <li class="cljt"><a onclick="_gaq.push([&#39;_trackEvent&#39;, &#39;Button&#39;, &#39;CloseNav&#39;, &#39;Training&#39;, Trainings|PageSections, false]);" style="BORDER-RIGHT: medium none" class="closenav">[x] hide</a> </li>
    </ul></div></div></div>
 <!---------------------------------- end title -------------------->
 <div id="newss">
         
                  <!------------------------>
<div class="new_detailel">
    

<!--<div class="2For1Promo"><span classid="b30218a7-77fc-43dd-a844-81935aa9b35e" dynamicclass="PagePropertyPlugin" state="e647d86f-c8d3-4995-8a95-e5cfa452a7d8|Body" hash="J728TNziHAUgx+Ehsyp23GSZKb8Rj4k79QvgxzYpqRI=" disabled="disabled" contentEditable="false" class="dynamiccontent" style="border: 1px solid #111111; background-color: #F8EDA4; padding: 3px;">{DynamicContent:PagePropertyPlugin}</span> </div>-->

<a id="overview" name="overview"></a>
<div class="iirheading">
<div class="headertitles">Overview</div></div>
<div class="trainingpageContent"><div>
        <?php echo $news->yii_overview ?>
	</div> </div><a id="content" name="content"></a><!-- content -->
<div class="iirheading">
<div class="headertitles">Content</div></div>
<div class="trainingpageContent"><div>
		<?php echo $news->yii_content ?>
	</div> </div><a id="who" name="who"></a><!-- who should attend -->
<div class="iirheading">
<div class="headertitles">Who Should Attend</div></div>
<div class="trainingpageContent"><div>
		<?php echo $news->yii_wsattend ?>
	</div> </div>

<a id="benefits" name="benefits"></a><!-- in association with -->
<div class="iirheading">
<div class="headertitles">Benefits Of Attending</div></div>
<div class="trainingpageContent"><div>
		<?php echo $news->yii_attending ?>
	</div> </div>



<a id="coursedirector" name="coursedirector"></a><!-- course director -->
<div class="iirheading">
<div style="FLOAT: left" class="headertitles">Course Director</div>
<div class="clicker"><img class="a" alt="" onclick="_gaq.push([&#39;_trackPageview&#39;, &#39;/Trainings|PageSections/Course Director/View&#39;]);" src="<?php echo Yii::app()->request->baseUrl?>/images/home/images/btn-expand.png"/> <img class="b" alt="" onclick="_gaq.push([&#39;_trackPageview&#39;, &#39;/Trainings|PageSections/Course Director/Close&#39;]);" src="<?php echo Yii::app()->request->baseUrl?>/images/home/images/s.png"/> </div>
<div style="CLEAR: both"></div></div>
<div class="trainingpageContent2" style="display: none;"><div id="ctl00_Content_MainBody_ctl00_ctl10_uxSearchResult_divRepeater">
    <div id="ctl00_Content_MainBody_ctl00_ctl10_uxSearchResult_divTemplates">
       <?php echo $news->yii_director ?>
    </div>
</div>


 </div><a id="venue" name="venue"></a><!-- venue and accomodation -->
<div class="iirheading">
<div style="FLOAT: left" class="headertitles">Venue and Accommodation</div>
<div class="clicker"><img class="a" alt="" onclick="_gaq.push([&#39;_trackPageview&#39;, &#39;/Trainings|PageSections/Venue and Accommodation/View&#39;]);" src="<?php echo Yii::app()->request->baseUrl?>/images/home/images/btn-expand.png"/> <img class="b" alt="" onclick="_gaq.push([&#39;_trackPageview&#39;, &#39;/Trainings|PageSections/Venue and Accommodation/Close&#39;]);" src="<?php echo Yii::app()->request->baseUrl?>/images/home/images/s.png"/> </div>
<div style="CLEAR: both"></div></div>
<div class="trainingpageContent2" style="display: none;">
	
<div id="ctl00_Content_MainBody_ctl00_ctl12_uxSearchResult_divRepeater">
    <div id="ctl00_Content_MainBody_ctl00_ctl12_uxSearchResult_divTemplates">
        
       <?php echo $news->new_summary ?>
        
    </div>
</div>


 </div><a id="fee" name="fee"></a><!-- course fees -->
<div class="iirheading">
<div style="FLOAT: left" class="headertitles">Course Fees</div>
<div class="clicker"><img class="a" alt="" onclick="_gaq.push([&#39;_trackPageview&#39;, &#39;/Trainings|PageSections/Course Fees/View]);" src="<?php echo Yii::app()->request->baseUrl?>/images/home/images/btn-expand.png"/> <img class="b" alt="" onclick="_gaq.push([&#39;_trackPageview&#39;, &#39;/Trainings|PageSections/Course Fees/Close]);" src="<?php echo Yii::app()->request->baseUrl?>/images/home/images/s.png"/> </div>
<div style="CLEAR: both"></div></div>
<div class="trainingpageContent2" style="display: none;">
<?php echo $news->new_content ?>


 </div>
 <!---------- dk duoi ------------------->
<div class="dkvll">
       
        <a href="/register/<?php echo $news->rewrite_url_news ?>.html" class="btn-booknow01">Book Now</a> 
        <a href="/download/<?php echo $news->rewrite_url_news ?>.html" class="btn-moreinfo02">View More Info</a>
        
        <?php foreach ($Configure as $val){ ?>
        <a href="mailto:<?php echo $val->ga_email?>" class="btn-mail03">Get Mail</a>
        <?php } ?>
</div>
<div style="clear: both;"></div>

                        <!-- end #mainContent -->
    </div>
                  
                  
                  
                  <!------------------------> 
             </div>
             
             
                 <!-------------------- new ------------->
                    <div class="comment_right" style="padding-top: 2.8% !important;">
                        <div class="comment_bg">
                            <div class="comment_right_title"><a>LIST COMMENTS</a> </div>       
                             <?php 
		                      		                       
                               $comment = new CDbCriteria();
                               $comment->condition = 'adv_action=1';
                               $comment->limit=3;
                               $comments = Comment::model()->findAll($comment);
                               foreach ($comments as $vals){
                           ?>
                           <div class="hi_comment">
                                <div class="nd_comment">
                                        <a><?php echo $vals->adv_name ?> </a>
                                </div>
                                <div class="td_comment">
                                       <a> <?php echo $vals->adv_link ?> </a>
                                </div>
                           </div>     
                         <?php } ?>
                          <div class="clickmore"><a href="/comment.html">Click see more...</a></div>
                        </div>
                       
                       <div class="videoscm">
                            <div class="comment_right_title" style="box-shadow: none;margin-bottom: 1px;"><a>Videos</a> </div>  
                             	 <script>
                                	$(function(){
                                		$('.video1').click(function(){
                                		var bien= $(this).attr('href');//alert(bien);
                                		
                                		$('.list').html(bien);
                                		});
                                		
                                	})
                                </script>
                        <?php 
		                       $video = new CDbCriteria();
                               $video->condition = 'showhide_links=1';
                               $video->order = 'id_links DESC';
                               $video->limit=3;
                               $videos = Links::model()->findAll($video);
                               foreach ($videos as $vc){
                           ?>     
                            <div class="list" style="margin-bottom: 6px;">
                    			<embed  class="video1"
                    			type="application/x-shockwave-flash" 
                    			src="<?php echo Yii::app()->request->baseUrl; ?>/images/home/images/player.swf" 
                    			width="100%" height="249" style="undefined" id="ply" name="ply" quality="high" allowfullscreen="true" allowscriptaccess="always" 
                    			flashvars="width=10&amp;height=249&amp;&amp;file=<?php echo $vc->url_links ?>&amp;feature=plcp"/>
                    		</div>
                          <?php } ?>  
                       </div>
                    </div>
            <div style="clear: both;"></div>
           </div>
              
     </div>
</div>

<script>
$(".closenav").click(function () {
$("#crumbsContainer").fadeOut(600);
});
</script>
