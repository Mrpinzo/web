<?php
$baseUrl = Yii::app()->request->baseUrl;
?>
        <div id="content_type_bicycle">
<div class="items_contents"> 
        <div class="divPath">
					<?php $this->widget('application.components.BreadCrumb', array(
					  'crumbs' => array(
					    array('name' => '<i class="fa fa-home"></i>', 'url' => Yii::app()->getHomeUrl()),
					  	array('name' => 'Tìm kiếm : '.$_REQUEST['tu-khoa']),
					  ),
					  'delimiter' => ' <i class="fa fa-angle-right"></i> ', // if you want to change it
					)); ?>
				</div>
        <input type="hidden" id="sxep" value="<?php echo Yii::app()->session['sx'] ?>">
                       <div class="title_right">
                           <span class="sub_title">từ khóa tìm kiếm : <span id="keys"><?php echo $_REQUEST['tu-khoa'] ?></span></span>
                           <div id="sxsp_main">
                             <select name="sxsp" id="sxsp">
                                <option value="">Sắp xếp sản phẩm</option>
                                <option value="td">Giá tăng dần</option>
                                <option value="gd">Giá giảm dần</option>
                                <option value="new">Sản phẩm mới</option>
                             </select>
                         </div>
                       </div>
             
                <?php 
                
                if ($count != 0) {
                  ?>
                               
                       <div class="main_right">	
                       
                       <div id="slsp">Có <?php echo $count ?> sản phẩm</div>
                        <?php 
                       foreach ($items as $vals){
                       ?>					  
							 <div class="items_bicycle">
	                                   <?php
	                                   
	                                   $criteria2g            = new CDbCriteria();
	                                   $criteria2g->condition = 'showhide_yii_imgproduct = 1 AND pr_id="'.$vals->pr_joinImg.'"';
	                                   $criteria2g->group = 'pr_id';
	                                   $YiiImgproduct = YiiImgproduct::model()->findAll($criteria2g);
	                                   
	                                   $count = YiiImgproduct::model()->count($criteria2g);
	                                   
	                                   if($count > 0){
	                                   	 foreach ($YiiImgproduct as $valk){
	                                   ?>
	                                   <a href="<?php echo '/san-pham/'.$vals->pr_url_news.'.html' ?>" title="<?php echo $vals->pr_name ?>">
	                                   <img src="<?php echo Yii::app()->request->baseUrl.$valk->url_yii_imgproduct ?>" alt="<?php echo $vals->pr_name ?>" width="295" height="215">
	                                    <div class="specifications">
	                                      <span class="aku">Thông số kỹ thuật</span>
					                         <div><p>Acquy : <label><?php echo $vals->pr_code ?></label></p></div>
					                         <div><p>Xuất xứ : <label><?php echo $vals->pr_madein ?></label></p></div>
					                         <div><p>Công xuất : <label><?php echo $vals->pr_number ?></label></p></div>					                         
					                         <div><p>Quãng đường : <label><?php echo $vals->pr_Manufacturer ?></label></p></div>
					                       
	                                   </div>
	                                   </a>
	                                   <?php }}else{?>
	                                   <a href="<?php echo '/san-pham/'.$vals->pr_url_news.'.html' ?>" title="<?php echo $vals->pr_name ?>">
	                                      <img src="<?php echo Yii::app()->request->baseUrl?>/images/home/images/nothumb.png" alt="<?php echo $vals->pr_name ?>" width="295" height="215">
	                                      <div class="specifications">
	                                      <span class="aku">Thông số kỹ thuật</span>
					                         <div><p>Acquy : <label><?php echo $vals->pr_code ?></label></p></div>
					                         <div><p>Xuất xứ : <label><?php echo $vals->pr_madein ?></label></p></div>
					                         <div><p>Công xuất : <label><?php echo $vals->pr_number ?></label></p></div>					                         
					                         <div><p>Quãng đường : <label><?php echo $vals->pr_Manufacturer ?></label></p></div>
					                       
	                                   </div>
	                                      </a>
	                                      <?php }?>
	                                  
	                                   <div>
	                                       <b><a href="<?php echo '/san-pham/'.$vals->pr_url_news.'.html' ?>" title="<?php echo $vals->pr_name ?>"><?php echo $vals->pr_name ?></a></b>
	                                       <span class="price">
	                                       <?php if($vals->pr_gia_cu !=0){ ?>
	                                       <span class="number_cu"><?php echo number_format($vals->pr_gia_cu,0,"",".").' VNĐ' ?></span>
	                                       <?php }?>
	                                             <span class="number number_new"><?php echo number_format($vals->pr_gia,0,"",".").' VNĐ' ?></span>
	                                       </span>
	                                       <span class="tinhtrangj"><?php echo $vals->pr_tinhtrang == 1?'<span style="color:#2dcc70 !important">Còn hàng</span>':'<span style="color:#FA6C06 !important">Hết hàng</span>' ?></span>
	                                     
	                                         <span class="khuyenmaix"><i class="fa fa-gift"></i><div class="khuyenmaih"><?php echo $vals->pr_khuyenmai ?></div></span>
	                                       
	                                       <a title="<?php echo $vals->pr_name ?>" class="cart" id="<?php echo $vals->pr_id ?>"><i class="fa fa-cart-plus"></i>MUA NGAY</a>
	                                   </div>
	                                  
	                              </div>
							
							<?php }?>
							
						<div id="main_paginnation">
							     <div id="pagination">
							    <?php $this->widget('CLinkPager', array(
							    'pages'       => $pages,
							    'htmlOptions' => array('class' => 'pager'),
							    'header'      => '',
							    'nextPageLabel' => '<i class="fa fa-angle-right"></i>',
							    'prevPageLabel'=> '<i class="fa fa-angle-left"></i>',
					    		'firstPageLabel'=>'<i class="fa fa-angle-double-left"></i>',
					    		'lastPageLabel'=>'<i class="fa fa-angle-double-right"></i>'
							));
							    ?>
							</div>
				
						</div>
								
                       </div>
                       <?php }else{?>    
                        <div class="main_right">	
                           <span id="notitems">Không tồn tại sản phẩm.</span>
                        </div>
                       <?php }?>
                  </div>
                  </div>