<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width" />
<meta name="keywords" content="<?php echo CHtml::encode($this->keywords); ?>" />
<meta name="description" content="<?php echo CHtml::encode($this->description); ?>" />
<title><?php echo CHtml::encode($this->pageTitle); ?></title>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/home/js/jquery-2.1.4.min.js"></script>
<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" rel="stylesheet" type="text/css" />
<link type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/font/font-awesome/css/font-awesome.css" rel="stylesheet"> 
 <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />   
<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/home/csslaptop/style.css" rel="stylesheet" type="text/css" />
 <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/js/home/jslaptop/engine1/style.css" />
<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/home/csslaptop/styledetail.css" rel="stylesheet" type="text/css" />
<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/home/csslaptop/responsive.css" type="text/css" media="screen" rel="stylesheet"> 
 <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/home/csslaptop/jquery.mmenu.all.css" rel="stylesheet">

                                            
 <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-66722689-1', 'auto');
    ga('send', 'pageview');

</script>
 <script> function toggle(id,p){var myChild = document.getElementById(id);if(myChild.style.display!='block')
                        {myChild.style.display='block';document.getElementById(p).className='folderOpen';}else
                        {myChild.style.display='none';document.getElementById(p).className='folder';}}</script>
</head>
<body>
     <div id="main">
            <?php $this->widget('top'); ?>
    
            <?php echo $content; ?>     
       
            <?php $this->widget('footer'); ?>
    </div>

</body>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/home/main.js"></script> 
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/home/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/home/jquery.mousewheel-3.0.4.pack.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/home/jquery.fancybox-1.3.4.pack.js"></script>
 

</html>
