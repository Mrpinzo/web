<?php

/**
 * This is the model class for table "yii_album".
 *
 * The followings are the available columns in table 'yii_album':
 * @property integer $id_album
 * @property integer $id_typealbum
 * @property string $img_album
 * @property integer $showhide_album
 */
class YiiAlbum extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return YiiAlbum the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'yii_album';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title', 'required'),
				/* array('img_album', 'file',
						'allowEmpty'=>$this->isNewRecord ?false:true,
						'types'=>'jpg, gif, png',
						'wrongType'=>'Tập tin không hợp lệ',
						'maxSize'=>1 * 1024 * 1024,
						'tooLarge'=>'Dung lượng file < 1MB. Hãy tải lên một tập tin nhỏ hơn.',//Error Message
				), */
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_album, id_typealbum, img_album, showhide_album', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
				'Albums' => array(self::BELONGS_TO, 'YiiTypealbum', 'id_typealbum'),
		);
	}

	protected function beforeDelete() {
		if (file_exists(getcwd().'/'.$this->img_album)) {
			unlink(getcwd().'/'.$this->img_album);
		}
		return parent::beforeDelete();
	}
	
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_album' => 'Id',
			'id_typealbum' => 'Loại Album',
            'title' => 'Title',
			'img_album' => 'Ảnh',
			'showhide_album' => 'Hiện',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_album',$this->id_album);
		$criteria->compare('id_typealbum',$this->id_typealbum);
		$criteria->compare('img_album',$this->img_album,false);
		$criteria->compare('showhide_album',$this->showhide_album);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}