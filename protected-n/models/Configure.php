<?php

/**
 * This is the model class for table "configure".
 *
 * The followings are the available columns in table 'configure':
 * @property integer $id_configure
 * @property string $logo_configure
 * @property string $about_configure
 */
class Configure extends CActiveRecord
{
	public $upload_logo_configure;
    public $upload_imgdl_configure;
	public $upload_slide_configure;
	public $upload_paymentgateways_configure;
	public $upload_service_configure;
	public $upload_contact_top_configure;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Configure the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'configure';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('about_configure,slogan_configure', 'required'),
				array('ga_email,ga_password,ga_profile_id', 'required'),
			array('longitude_configure,latitude_configure,twitter,in_lk,title_contact,telephone,diachi1,diachi2,about_configure,slogan_configure,manhung,skype_configure,yahoo_configure,title_seo,description_seo,keywords_seo,url_home,phone_configure,page_facebook
					,ServiceCenter,Contactstore,mail_configure,facebook_configure,contact_top,page_google,page_youtube,code_google_analytics,Verification_google,ga_email,ga_password,ga_profile_id', 'length', 'min'=>1),
				//array('phone_configure', 'numerical', 'integerOnly'=>true),
				//array('phone_configure', 'match', 'pattern'=>'/^(84|0)(1\d{9}|9\d{8})$/','message'=> 'Sai định dạng số điện thoại.'),
				array('url_home', 'url', 'defaultScheme' => 'http'),
				array('logo_configure,imgdl_configure,slide_configure,paymentgateways,service,slogan', 'file',
						//'allowEmpty'=>$this->isNewRecord ?false:true,
						'allowEmpty'=>$this->isNewRecord ?false:true,
						'types'=>'jpg, gif, png',
						'wrongType'=>'Tập tin không hợp lệ',
						'maxSize'=>1 * 1024 * 1024,
						'tooLarge'=>'Dung lượng file < 1MB. Hãy tải lên một tập tin nhỏ hơn.',//Error Message
				),
                
            
				
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_configure, logo_configure,imgdl_configure, about_configure,slogan_configure,chatyahoo1_configure,chatyahoo2_configure,title_seo,description_seo,keywords_seo,url_home,page_google,page_youtube,code_google_analytics,Verification_google', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_configure' => 'Id',
			'logo_configure' => 'Logo',
		
				'slogan_configure' => 'Tiêu đề',
				'slide_configure' => 'Ảnh Slide',
                'imgdl_configure' => 'Images dowload',
				'chatyahoo1_configure' => 'Yahoo phòng kỹ thuật',
				'chatyahoo2_configure' => 'Yahoo phòng kinh doanh',
				
				'title_seo' => 'Tiêu đề Seo',
				'description_seo' => 'Miêu tả Seo',
				'keywords_seo' => 'Từ khóa Seo',
				'url_home' => 'Đường dẫn trang chủ',
				
                'diachi1' => 'Địa chỉ 1',
                'diachi2' => 'Địa chỉ 2',
                'paymentgateways' => 'images',
                'slogan' => 'logo fotter',
                'telephone' => 'Telephone',
                'ga_email' =>'Email',
                
               	'latitude_configure' => 'Vĩ độ',
				'longitude_configure' => 'Kinh độ',
				
				'phone_configure' => 'Số điện thoại',
				'page_facebook' => 'Trang facebook',
			
				'Contactstore' => 'Liên hệ cửa hàng',
                
                'manhung' => 'Contact SINGAPORE',
                'ServiceCenter' => 'google map SINGAPORE',
		        'about_configure' => 'Contact VIETNAM',
                 'title_contact' => 'google map VIETNAM', 
				'facebook_configure' => 'Facebook',
                'twitter' => 'Twitter',
                'in_lk' => 'In',
                'page_youtube' => 'Youtube',
                
				'mail_configure' => 'Mail',
				'skype_configure' => 'Skype',
				'yahoo_configure' => 'Yahoo chát foot',
			
				'service' => 'Dịch vụ',
				'contact_top' => 'Thông tin liên hệ Top',
				'page_google' => 'Mail top',
				
				'code_google_analytics' => 'Mã ID google analytics',
				'Verification_google' => 'Mã xác minh google',
			
			
		);
	}

	protected function beforeSave()
	{
		$this->upload_logo_configure = CUploadedFile::getInstance($this, 'logo_configure');
		if (isset($this->upload_logo_configure)) {
			if(!$this->isNewRecord){
				if(!empty($this->logo_configure)){
					if (file_exists(getcwd().'/'.$this->logo_configure)) {
						unlink(getcwd().'/'.$this->logo_configure);
					};
				}
			}
			$fileName     = rand().$this->upload_logo_configure->name;
			$uploadFolder = Yii::getPathOfAlias('webroot') . DIRECTORY_SEPARATOR . 'upload' . DIRECTORY_SEPARATOR .'cauhinh';
			if (!is_dir($uploadFolder)) {
				mkdir($uploadFolder, 0755, TRUE);
			}
			$uploadFile = $uploadFolder . DIRECTORY_SEPARATOR . $fileName;
			$this->upload_logo_configure->saveAs($uploadFile); //Upload file thong qua object CUploadedFile
			$this->logo_configure = '/upload/cauhinh/'. $fileName; //Lưu path vào csdl
		}
		
        $this->upload_imgdl_configure = CUploadedFile::getInstance($this, 'imgdl_configure');
		if (isset($this->upload_imgdl_configure)) {
			if(!$this->isNewRecord){
				if(!empty($this->imgdl_configure)){
					if (file_exists(getcwd().'/'.$this->imgdl_configure)) {
						unlink(getcwd().'/'.$this->imgdl_configure);
					};
				}
			}
			$fileName     = rand().$this->upload_imgdl_configure->name;
			$uploadFolder = Yii::getPathOfAlias('webroot') . DIRECTORY_SEPARATOR . 'upload' . DIRECTORY_SEPARATOR .'cauhinh';
			if (!is_dir($uploadFolder)) {
				mkdir($uploadFolder, 0755, TRUE);
			}
			$uploadFile = $uploadFolder . DIRECTORY_SEPARATOR . $fileName;
			$this->upload_imgdl_configure->saveAs($uploadFile); //Upload file thong qua object CUploadedFile
			$this->imgdl_configure = '/upload/cauhinh/'. $fileName; //Lưu path vào csdl
		}
        
        
        
		$this->upload_slide_configure = CUploadedFile::getInstance($this, 'slide_configure');
		if (isset($this->upload_slide_configure)) {
			if(!$this->isNewRecord){
				if(!empty($this->slide_configure)){
					if (file_exists(getcwd().'/'.$this->slide_configure)) {
						unlink(getcwd().'/'.$this->slide_configure);
					};
				}
			}
			$fileName     = rand().$this->upload_slide_configure->name;
			$uploadFolder = Yii::getPathOfAlias('webroot') . DIRECTORY_SEPARATOR . 'upload' . DIRECTORY_SEPARATOR .'cauhinh';
			if (!is_dir($uploadFolder)) {
				mkdir($uploadFolder, 0755, TRUE);
			}
			$uploadFile = $uploadFolder . DIRECTORY_SEPARATOR . $fileName;
			$this->upload_slide_configure->saveAs($uploadFile); //Upload file thong qua object CUploadedFile
			$this->slide_configure = '/upload/cauhinh/'. $fileName; //Lưu path vào csdl
		}
		
		$this->upload_paymentgateways_configure = CUploadedFile::getInstance($this, 'paymentgateways');
		if (isset($this->upload_paymentgateways_configure)) {
			if(!$this->isNewRecord){
				if(!empty($this->paymentgateways)){
					if (file_exists(getcwd().'/'.$this->paymentgateways)) {
						unlink(getcwd().'/'.$this->paymentgateways);
					};
				}
			}
			$fileName     = rand().$this->upload_paymentgateways_configure->name;
			$uploadFolder = Yii::getPathOfAlias('webroot') . DIRECTORY_SEPARATOR . 'upload' . DIRECTORY_SEPARATOR .'cauhinh';
			if (!is_dir($uploadFolder)) {
				mkdir($uploadFolder, 0755, TRUE);
			}
			$uploadFile = $uploadFolder . DIRECTORY_SEPARATOR . $fileName;
			$this->upload_paymentgateways_configure->saveAs($uploadFile); //Upload file thong qua object CUploadedFile
			$this->paymentgateways = '/upload/cauhinh/'. $fileName; //Lưu path vào csdl
		}
		
		$this->upload_service_configure = CUploadedFile::getInstance($this, 'service');
		if (isset($this->upload_service_configure)) {
			if(!$this->isNewRecord){
				if(!empty($this->service)){
					if (file_exists(getcwd().'/'.$this->service)) {
						unlink(getcwd().'/'.$this->service);
					};
				}
			}
			$fileName     = rand().$this->upload_service_configure->name;
			$uploadFolder = Yii::getPathOfAlias('webroot') . DIRECTORY_SEPARATOR . 'upload' . DIRECTORY_SEPARATOR .'cauhinh';
			if (!is_dir($uploadFolder)) {
				mkdir($uploadFolder, 0755, TRUE);
			}
			$uploadFile = $uploadFolder . DIRECTORY_SEPARATOR . $fileName;
			$this->upload_service_configure->saveAs($uploadFile); //Upload file thong qua object CUploadedFile
			$this->service = '/upload/cauhinh/'. $fileName; //Lưu path vào csdl
		}
		
		$this->upload_contact_top_configure = CUploadedFile::getInstance($this, 'slogan');
		if (isset($this->upload_contact_top_configure)) {
			if(!$this->isNewRecord){
				if(!empty($this->slogan)){
					if (file_exists(getcwd().'/'.$this->slogan)) {
						unlink(getcwd().'/'.$this->slogan);
					};
				}
			}
			$fileName     = rand().$this->upload_contact_top_configure->name;
			$uploadFolder = Yii::getPathOfAlias('webroot') . DIRECTORY_SEPARATOR . 'upload' . DIRECTORY_SEPARATOR .'cauhinh';
			if (!is_dir($uploadFolder)) {
				mkdir($uploadFolder, 0755, TRUE);
			}
			$uploadFile = $uploadFolder . DIRECTORY_SEPARATOR . $fileName;
			$this->upload_contact_top_configure->saveAs($uploadFile); //Upload file thong qua object CUploadedFile
			$this->slogan = '/upload/cauhinh/'. $fileName; //Lưu path vào csdl
		}
		return parent::beforeSave();
	}
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		$criteria->compare('id_configure',$this->id_configure);
		$criteria->compare('logo_configure',$this->logo_configure,true);
		$criteria->compare('about_configure',$this->about_configure,true);
		$criteria->compare('slogan_configure',$this->slogan_configure,true);
		$criteria->compare('chatyahoo1_configure',$this->chatyahoo1_configure,true);
		$criteria->compare('chatyahoo2_configure',$this->chatyahoo2_configure,true);

		$criteria->compare('title_seo',$this->title_seo,true);
		$criteria->compare('description_seo',$this->description_seo,true);
		$criteria->compare('keywords_seo',$this->keywords_seo,true);
		$criteria->compare('url_home',$this->url_home,true);
		
		$criteria->compare('page_google',$this->page_google,true);
		$criteria->compare('page_youtube',$this->page_youtube,true);
		
		$criteria->compare('code_google_analytics',$this->code_google_analytics,true);
		$criteria->compare('Verification_google',$this->Verification_google,true);
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}