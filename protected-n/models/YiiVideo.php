<?php

/**
 * This is the model class for table "yii_video".
 *
 * The followings are the available columns in table 'yii_video':
 * @property integer $id_video
 * @property string $name_video
 * @property integer $showhide_video
 * @property string $url_video
 * @property string $title_video
 * @property string $keywords_video
 * @property string $description_video
 * @property integer $id_typevideo
 */
class YiiVideo extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return YiiVideo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'yii_video';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name_video, url_video, id_typevideo', 'required'),
			array('showhide_video, id_typevideo', 'numerical', 'integerOnly'=>true),
			array('name_video, url_video, title_video, keywords_video, description_video,date_video,highlights_video,url_seo_video', 'length', 'max'=>5000),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('url_seo_video,highlights_video,id_video, name_video, showhide_video, url_video, title_video, keywords_video, description_video, id_typevideo,date_video', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
				'YiiTypeVideo' => array(self::BELONGS_TO, 'YiiTypevideo', 'id_typevideo'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_video' => 'Id',
			'name_video' => 'Tên',
			'showhide_video' => 'Hiện',
			'url_video' => 'Đường dẫn Video',
			'title_video' => 'Tiêu đề Seo',
			'keywords_video' => 'Từ khóa Seo',
			'description_video' => 'Miêu tả Seo',
			'id_typevideo' => 'Loại Video',
			'date_video' => 'Ngày đăng',
				'highlights_video' => 'Nổi bật'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		
		$criteria->compare('id_video',$this->id_video);
		$criteria->compare('name_video',$this->name_video,true);
		$criteria->compare('showhide_video',$this->showhide_video);
		$criteria->compare('url_video',$this->url_video,true);
		$criteria->compare('title_video',$this->title_video,true);
		$criteria->compare('keywords_video',$this->keywords_video,true);
		$criteria->compare('description_video',$this->description_video,true);
		$criteria->compare('id_typevideo',$this->id_typevideo);
		$criteria->compare('date_video',$this->date_video);
		$criteria->compare('highlights_video',$this->highlights_video);
		$criteria->compare('url_seo_video',$this->url_seo_video);
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}