<?php

/**
 * This is the model class for table "admin".
 *
 * The followings are the available columns in table 'admin':
 * @property integer $ad_id
 * @property string $username
 * @property string $password
 * @property string $ad_fullname
 * @property integer $ad_phone
 * @property string $ad_email
 */
class Administrator extends CActiveRecord
{
	public $code;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Administrator the static model class
	 */
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'admin';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('username, password, ad_fullname, ad_phone, ad_email,id_role', 'required'),
				array('username', 'my_required'),
			//array('ad_phone', 'numerical', 'integerOnly'=>true),
			array('ad_phone', 'match', 'pattern'=>'/^(84|0)(1\d{9}|9\d{8})$/'),
			array('username, password', 'length', 'max'=>300),
			array('ad_fullname, ad_email,creat_date,update_date', 'length', 'max'=>300),
			array('ad_email','email'),
				
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ad_id, username, password, ad_fullname, ad_phone, ad_email,creat_date,update_date,id_role', 'safe', 'on'=>'search'),
			/* array ('code','captcha',
				'allowEmpty'=>!CCaptcha::checkRequirements(),
				'message' => 'Mã Xác Nhận Nhập Không Đúng'
				), */
		);
	}

	public function my_required($attribute_name,$params){
	   if($this->isNewRecord){	
		if(!empty($this->username)){
			$criteria1a = new CDbCriteria();
			$criteria1a->condition = 'username="'.$this->username.'"';
			$count = Administrator::model()->count($criteria1a);
			if($count != 0)
			   $this->addError('username','Tên đăng nhập đã tồn tại');
		}
	   }else{
	   	if(!empty($this->username)){
	   		$criteria1a = new CDbCriteria();
	   		$criteria1a->condition = 'username="'.$this->username.'" AND username!="'.Yii::app()->session['username_admin'].'"';
	   		$count = Administrator::model()->count($criteria1a);
	   		if($count != 0)
	   			$this->addError('username','Tên đăng nhập đã tồn tại');
	   	}
	   }
	}
	
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
				'YiiRoles' => array(self::BELONGS_TO, 'YiiRole', 'id_role'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ad_id' => 'Id',
			'username' => 'Tên đăng nhập',
			'password' => 'Mật khẩu',
			'ad_fullname' => 'Họ Tên',
			'ad_phone' => 'Điện thoại',
			'ad_email' => 'Email',
				'creat_date' => 'Ngày tạo',
				'update_date' => 'Ngày sửa',
				'id_role' => 'Loại quyền'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ad_id',$this->ad_id);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('ad_fullname',$this->ad_fullname,true);
		$criteria->compare('ad_phone',$this->ad_phone,true);
		$criteria->compare('ad_email',$this->ad_email,true);
		$criteria->compare('creat_date',$this->creat_date,true);
		$criteria->compare('update_date',$this->update_date,true);
		$criteria->compare('id_role',$this->id_role);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}