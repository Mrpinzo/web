<?php

/**
 * This is the model class for table "downloadyii".
 *
 * The followings are the available columns in table 'downloadyii':
 * @property integer $id_downloadyii
 * @property string $fullnam_downloadyii
 * @property string $company_downloadyii
 * @property string $phone_downloadyii
 * @property string $email_downloadyii
 * @property string $address_downloadyii
 * @property string $title_downloadyii
 * @property string $content_downloadyii
 * @property string $mobile_downloadyii
 */
class Downloadyii extends CActiveRecord
{
	public $code;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return downloadyii the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Downloadyii';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('fullnam_downloadyii', 'required','message'=> 'Vui lòng nhập họ tên đầy đủ.'),
				array('email_downloadyii', 'required','message'=> 'Vui lòng nhập email.'),
				array('email_downloadyii','email','message'=> 'Sai định dạng emai.'),
				array('phone_downloadyii,mobile_downloadyii', 'numerical', 'integerOnly'=>true,'message'=> 'Vui lòng nhập số điện thoại.'),
				//array('phone_downloadyii,mobile_downloadyii', 'match', 'pattern'=>'/^(84|0)(1\d{9}|9\d{8})$/','message'=> 'Sai số điện thoại.'),
			array('fullnam_downloadyii,country_downloadyii,areacode_downloadyii,lastnam_downloadyii,namexh_downloadyii, company_downloadyii,namenew_downloadyii,fullnam_downloadyii, phone_downloadyii, email_downloadyii, address_downloadyii, title_downloadyii, mobile_downloadyii,content_downloadyii', 'length', 'max'=>2000),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_downloadyii, fullnam_downloadyii,lastnam_downloadyii,fullnam_downloadyii,namenew_downloadyii, company_downloadyii, phone_downloadyii, email_downloadyii, address_downloadyii, title_downloadyii, content_downloadyii, mobile_downloadyii', 'safe', 'on'=>'search'),
				array('code', 'required','message'=> 'Vui lòng nhập mã xác nhận.'),
				array ('code','captcha',
						'allowEmpty'=>!CCaptcha::checkRequirements(),
						'message' => 'Mã Xác Nhận Nhập Không Đúng'
				),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_downloadyii' => 'Id',
			'fullnam_downloadyii' => 'Full name',
            'lastnam_downloadyii' => 'Last name',
            'title_downloadyii' => 'Proclaimed title',
            'namexh_downloadyii' => 'Name',
            'namenew_downloadyii' => 'Title file downloadyii ',
			'company_downloadyii' => 'Company address',
			'phone_downloadyii' => 'Phone',
			'email_downloadyii' => 'Email',
			'address_downloadyii' => 'Address',
          	'country_downloadyii' => 'Country',
            'areacode_downloadyii' => 'Areacode',
		);
	}
	
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_downloadyii',$this->id_downloadyii);
		$criteria->compare('fullnam_downloadyii',$this->fullnam_downloadyii,true);
        $criteria->compare('namenew_downloadyii',$this->namenew_downloadyii,true);
        $criteria->compare('lastnam_downloadyii',$this->lastnam_downloadyii,true);
		$criteria->compare('company_downloadyii',$this->company_downloadyii,true);
		$criteria->compare('phone_downloadyii',$this->phone_downloadyii,true);
		$criteria->compare('email_downloadyii',$this->email_downloadyii,true);
		$criteria->compare('address_downloadyii',$this->address_downloadyii,true);
		$criteria->compare('title_downloadyii',$this->title_downloadyii,true);
		$criteria->compare('content_downloadyii',$this->content_downloadyii,true);
		$criteria->compare('mobile_downloadyii',$this->mobile_downloadyii,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}