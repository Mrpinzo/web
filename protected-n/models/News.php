<?php

/**
 * This is the model class for table "news".
 *
 * The followings are the available columns in table 'news':
 * @property integer $new_id
 * @property string $new_title
 * @property string $new_img
 * @property string $new_summary
 * @property string $new_content
 * @property string $new_date
 */
class News extends CActiveRecord
{
	public $new_img_upload;
    public $img_file_upload;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return News the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'news';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('new_title, new_content', 'required'),
			array('new_title,new_summary, new_content,yii_date,yii_year,yii_address,yii_overview,yii_content,yii_wsattend,yii_attending,yii_director,new_title_seo,new_description_seo,new_keywords_seo', 'length','min'=>3),
				//array('id_menu','my_required'),
				array('new_showhide,new_highlights,new_ads,id_menu,agency,view,customer', 'numerical', 'integerOnly'=>true),
			//array('new_img', 'file', 'types' => 'jpg, gif, png', 'maxSize' => 1 * 1024 * 1024, 'tooLarge'=>'Dung lượng file < 1MB. Hãy tải lên một tập tin nhỏ hơn.', 'allowEmpty' => TRUE),
				
				array('new_img', 'file',
						//'allowEmpty'=>$this->isNewRecord ?false:true,
						'allowEmpty'=>true,
						'types'=>'jpg, gif, png',
						'wrongType'=>'Tập tin không hợp lệ',
						'maxSize'=>1 * 1024 * 1024,
						'tooLarge'=>'Dung lượng file < 1MB. Hãy tải lên một tập tin nhỏ hơn.',//Error Message
				),
                
                array('img_file', 'file',
						//'allowEmpty'=>$this->isNewRecord ?false:true,
						'allowEmpty'=>true,
						'types'=>'jpg, gif, png, rar, pdf, zip',
						'wrongType'=>'Tập tin không hợp lệ',
						'maxSize'=>1 * 1024 * 1024,
						'tooLarge'=>'Dung lượng file < 1MB. Hãy tải lên một tập tin nhỏ hơn.',//Error Message
				),
                
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('agency,customer,new_id,id_menu,rewrite_url_news, new_title,img_file, new_img, new_summary, new_content, new_date,new_highlights,new_showhide,new_ads', 'safe', 'on'=>'search'),
		);
	}

	public function my_required($attribute_name,$params){
		if(empty($this->id_menu)){
			$this->addError('id_menu',
					'Vui lòng chọn danh mục');
		}
	}
	/**
	 * @return array relational rules.
	 */
   public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
				'YiiMenu' => array(self::BELONGS_TO, 'YiiMenu', 'id_menu'),
		);
	}

	protected function beforeSave()
	{
		$this->new_img_upload = CUploadedFile::getInstance($this, 'new_img');
		if (isset($this->new_img_upload)) {
			if(!$this->isNewRecord){
				if(!empty($this->new_img)){
					if (file_exists(getcwd().'/'.$this->new_img)) {
						unlink(getcwd().'/'.$this->new_img);
					};
				}
			}
			$fileName     = rand().$this->new_img_upload->name;
			$uploadFolder = Yii::getPathOfAlias('webroot') . DIRECTORY_SEPARATOR . 'upload' . DIRECTORY_SEPARATOR .'new'.DIRECTORY_SEPARATOR . date('Y-m-d');
			if (!is_dir($uploadFolder)) {
				mkdir($uploadFolder, 0755, TRUE);
			}
			$uploadFile = $uploadFolder . DIRECTORY_SEPARATOR . $fileName;
			$this->new_img_upload->saveAs($uploadFile); //Upload file thong qua object CUploadedFile
			$this->new_img = '/upload/new/' . date('Y-m-d') . '/'. $fileName; //Lưu path vào csdl
       
		}
        
      $this->img_file_upload = CUploadedFile::getInstance($this, 'img_file');
		if (isset($this->img_file_upload)) {
			if(!$this->isNewRecord){
				if(!empty($this->img_file)){
					if (file_exists(getcwd().'/'.$this->img_file)) {
						unlink(getcwd().'/'.$this->img_file);
					};
				}
			}
			$fileName     = rand().$this->img_file_upload->name;
			$uploadFolder = Yii::getPathOfAlias('webroot') . DIRECTORY_SEPARATOR . 'upload' . DIRECTORY_SEPARATOR .'new'.DIRECTORY_SEPARATOR . date('Y-m-d');
			if (!is_dir($uploadFolder)) {
				mkdir($uploadFolder, 0755, TRUE);
			}
			$uploadFile = $uploadFolder . DIRECTORY_SEPARATOR . $fileName;
			$this->img_file_upload->saveAs($uploadFile); //Upload file thong qua object CUploadedFile
			$this->img_file = '/upload/new/' . date('Y-m-d') . '/'. $fileName; //Lưu path vào csdl
       
		}
        
        
		return parent::beforeSave();
	}
	protected function beforeDelete() {
		if(!empty($this->new_img)){
			if (file_exists(getcwd().'/'.$this->new_img)) {
				unlink(getcwd().'/'.$this->new_img);
			}
		}
		return parent::beforeDelete();
	}
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'new_id' => 'Id',
			'new_title' => 'Tiêu đề',
			'new_img' => 'Ảnh',
            'img_file' => 'Download',
            'new_date' => 'Ngày đăng',
			'new_title_seo' =>'Tiêu đề Seo',
			'new_description_seo' =>'Miêu tả Seo',
			'new_keywords_seo' => 'Từ khóa Seo',
			'id_menu' => 'Loại tin tức',
			'new_highlights' => 'Nổi bật',
			'new_showhide' => 'Hiện thị',
			'new_ads'   => 'Quảng cáo',
			'rewrite_url_news'   => 'Định danh đường dẫn',
			'agency' => 'Đại lý',
			'customer' => 'Khách hàng',
            
            
            'yii_date' => 'Ngày tháng',
            'yii_year' => 'Năm',
            'yii_address' => 'Địa chỉ',
            'yii_overview' => 'Overview',
            'yii_content' => 'Content',
            'yii_wsattend' => 'Who Should Attend',
            'yii_attending' => 'Benefits Of Attending',
            'yii_director' => 'Course Director',  
            'new_summary' => 'Venue and Accommodation',
			'new_content' => 'Course Fees',  
          
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('new_id',$this->new_id);
		$criteria->compare('id_menu',$this->id_menu,true);
		$criteria->compare('new_title',$this->new_title,true);
		$criteria->compare('new_keywords_seo',$this->new_keywords_seo,true);
		$criteria->compare('new_title_seo',$this->new_title_seo,true);
		$criteria->compare('new_description_seo',$this->new_description_seo,true);
		$criteria->compare('new_highlights',$this->new_highlights,true);
		$criteria->compare('new_showhide',$this->new_showhide,true);
		$criteria->compare('new_date',$this->new_date,true);
		$criteria->compare('new_ads',$this->new_ads,true);
		$criteria->compare('rewrite_url_news',$this->rewrite_url_news,true);
		$criteria->compare('agency',$this->agency);
		$criteria->compare('customer',$this->customer);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}