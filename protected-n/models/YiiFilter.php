<?php

/**
 * This is the model class for table "yii_menu".
 *
 * The followings are the available columns in table 'yii_menu':
 * @property integer $id_menu
 * @property string $name_menu
 * @property integer $showhide_menu
 * @property integer $location_menu
 * @property integer $level_menu
 * @property integer $order_menu
 * @property integer $from_menu
 * @property integer $newwindow_mennu
 */
class YiiFilter extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return YiiMenu the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'yii_filter';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name_menu,order_menu','required'),
			//array('location_menu','my_required'),
				
			array('order_menu,showhide_menu,newwindow_mennu,level_menu,home_menu,highlights_menu,location_menu', 'numerical', 'integerOnly'=>true),
			array('order_menu',$this->isNewRecord ?'ordermenu':'required'),
			array('name_menu,url_menu,title_seo_menu,description_seo_menu,keywords_seo', 'length', 'max'=>50000),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('highlights_menu,id_menu, name_menu,rewrite_url_menu,url_menu, showhide_menu, location_menu, level_menu, order_menu,  newwindow_mennu,home_menu', 'safe', 'on'=>'search'),
		);
	}

	public function my_required($attribute_name,$params){
		
		 if(empty($this->location_menu)){
			$this->addError('location_menu',
					'Vui lòng chọn vị trí');
		}

	}

	public function ordermenu($attribute_name,$params){
	
		$Criteria = new CDbCriteria();
		$Criteria->condition = "order_menu='".$this->order_menu."'";
		//$model=Posts::model()->findByPk($id, $Criteria);
		$model=$this->model()->findAll($Criteria);
		if(count($model))
			$this->addError('order_menu',
					'Vị trí đã tồn tại');
	}
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_menu' => 'Id',
			'name_menu' => 'Tên',
			'showhide_menu' => 'Hiện',
			'location_menu' => 'Vị trí',
				'order_menu' =>'Thứ tự',
				'newwindow_mennu' => 'Cửa sổ mới',
				'home_menu' => 'Hiện thị trang chủ',
				'rewrite_url_menu' => 'Định danh đường dẫn',
				'url_menu' => 'Đường dẫn',
				'level_menu' => 'Lọc cha',
				'title_seo_menu' => 'Tiêu đề Seo',
				'description_seo_menu' => 'Miêu tả Seo',
				'keywords_seo' => 'Từ khóa Seo',
				'highlights_menu' => 'Nổi bật'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		
		$criteria->compare('id_menu',$this->id_menu);
		$criteria->compare('name_menu',$this->name_menu,true);
		$criteria->compare('showhide_menu',$this->showhide_menu);
		$criteria->compare('location_menu',$this->location_menu);
		$criteria->compare('level_menu',$this->level_menu);
		$criteria->compare('order_menu',$this->order_menu);
		$criteria->compare('newwindow_mennu',$this->newwindow_mennu);
		$criteria->compare('home_menu',$this->home_menu);
		$criteria->compare('rewrite_url_menu',$this->rewrite_url_menu,true);
		$criteria->compare('url_menu',$this->url_menu,true);
		$criteria->compare('highlights_menu',$this->highlights_menu);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}