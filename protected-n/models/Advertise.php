<?php

/**
 * This is the model class for table "advertise".
 *
 * The followings are the available columns in table 'advertise':
 * @property integer $adv_id
 * @property string $adv_link
 * @property string $adv_img
 * @property integer $adv_action
 * @property integer $adv_positions
 */
class Advertise extends CActiveRecord
{
	public $adv_img_upload;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Advertise the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'advertise';
	}
	/* <?php echo CHtml::dropDownList($model, 'adv_positions',
			array($model->adv_positions => 'Phải', $model->adv_positions => 'Trái'),
					array('empty' => 'Chọn vị trí'));?> */
	protected function beforeSave()
	{
		$this->adv_img_upload = CUploadedFile::getInstance($this, 'adv_img');
		if (isset($this->adv_img_upload)) {
			if(!$this->isNewRecord){ 
				if (file_exists(getcwd().'/'.$this->adv_img)) {
					unlink(getcwd().'/'.$this->adv_img);
				};
			}
			$fileName     = rand().$this->adv_img_upload->name;
			$uploadFolder = Yii::getPathOfAlias('webroot') . DIRECTORY_SEPARATOR . 'upload' . DIRECTORY_SEPARATOR .'quangcao'.DIRECTORY_SEPARATOR . date('Y-m-d');
			if (!is_dir($uploadFolder)) {
				mkdir($uploadFolder, 0755, TRUE);
			}
			$uploadFile = $uploadFolder . DIRECTORY_SEPARATOR . $fileName;
			$this->adv_img_upload->saveAs($uploadFile); //Upload file thong qua object CUploadedFile
			$this->adv_img = '/upload/quangcao/' . date('Y-m-d') . '/'. $fileName; //Lưu path vào csdl
		}
		return parent::beforeSave();
	}
	protected function beforeDelete() {
		if (file_exists(getcwd().'/'.$this->adv_img)) {
			unlink(getcwd().'/'.$this->adv_img);
		}
		return parent::beforeDelete();
	}
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('adv_link,adv_name, adv_action, adv_positions,adv_order', 'required'),
				array('adv_order', 'numerical', 'integerOnly'=>true),
				//array('adv_order', 'ordermenu'),
			//array('adv_img', 'file', 'types' => 'jpg, gif, png', 'maxSize' => 1 * 1024 * 1024, 'tooLarge'=>'Dung lượng file < 1MB. Hãy tải lên một tập tin nhỏ hơn.', 'allowEmpty' => TRUE),				
			array('adv_link,adv_name', 'length', 'max'=>2000),
			//array('adv_link', 'url', 'defaultScheme' => 'http'),
				array('adv_img', 'file',
						'allowEmpty'=>$this->isNewRecord ?false:true,
						'types'=>'jpg, gif, png',
						'wrongType'=>'Tập tin không hợp lệ',
						'maxSize'=>1 * 1024 * 1024,
						'tooLarge'=>'Dung lượng file < 1MB. Hãy tải lên một tập tin nhỏ hơn.',//Error Message
				),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('adv_id,adv_order, adv_link, adv_img, adv_action, adv_positions', 'safe', 'on'=>'search'),
		);
	}
	
	/* public function ordermenu($attribute_name,$params){
	
		$Criteria = new CDbCriteria();
		$Criteria->condition = "adv_order='".$this->adv_order."'";
		//$model=Posts::model()->findByPk($id, $Criteria);
		$model=$this->model()->findAll($Criteria);
		if(count($model))
			$this->addError('adv_order',
					'Vị trí đã tồn tại');
	} */
	
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'adv_id' => 'Id',
			'adv_link' => 'Link',
            'adv_name' => 'Tiêu đề',
			'adv_img' => 'Ảnh',
			'adv_action' => 'Hiện/Ẩn',
			'adv_positions' => 'Vị trí',
				'adv_order' => 'Thứ tự'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('adv_id',$this->adv_id);
		$criteria->compare('adv_link',$this->adv_link,true);
		$criteria->compare('adv_img',$this->adv_img,true);
		$criteria->compare('adv_action',$this->adv_action);
		$criteria->compare('adv_positions',$this->adv_positions);
		$criteria->compare('adv_order',$this->adv_order);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}