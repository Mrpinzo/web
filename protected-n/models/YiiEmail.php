<?php

/**
 * This is the model class for table "yii_email".
 *
 * The followings are the available columns in table 'yii_email':
 * @property integer $id_email
 * @property string $email
 * @property string $date
 */
class YiiEmail extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return YiiEmail the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'yii_email';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('email, date', 'required'),
				array('email', 'my_required'),
			array('email', 'length', 'max'=>5000),
				array('email','email'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_email, email, date', 'safe', 'on'=>'search'),
		);
	}

	public function my_required($attribute_name,$params){
		if($this->isNewRecord){
			if(!empty($this->email)){
				$criteria1a = new CDbCriteria();
				$criteria1a->condition = 'email="'.$this->email.'"';
				$count = YiiEmail::model()->count($criteria1a);
				if($count != 0)
					$this->addError('email','Email đã tồn tại');
			}
		}else{
			if(!empty($this->email)){
				$criteria1a = new CDbCriteria();
				$criteria1a->condition = 'email="'.$this->email.'" AND email!="'.Yii::app()->session['emailsend'].'"';
				$count = YiiEmail::model()->count($criteria1a);
				if($count != 0)
					$this->addError('email','Email đã tồn tại');
			}
		}
	}
	
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_email' => 'Id',
			'email' => 'Email',
			'date' => 'Ngày nhận',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_email',$this->id_email);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('date',$this->date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}