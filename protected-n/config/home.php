<?php
    return CMap::mergeArray(
        require(dirname(__FILE__) . '/main.php'),
        array(
            'components' => array(
                // uncomment the following to enable URLs in path-format

                'urlManager' => array(
                    'urlFormat' => 'path',
                    'showScriptName' => false,
                    'urlSuffix' => '.html',
                    'rules'     => array(
                        //'gioi-thieu' => array('site/about', 'caseSensitive' => false),
                    	'lien-he' => 'site/Lienhe',
                        'register/<alias>' => 'site/register',
                        'download/<alias>' => 'site/download',
                        'start-download/<alias>' => 'site/startdownload',
                        'notify' => 'site/notify',
                    		'chi-tiet-gio-hang' => 'site/detailcart',
                    		'chi-tiet-don-hang' => 'site/Paymentorders',
                            //'tim-kiem/' => 'site/search',
                            'cat/<alias>' => 'site/listNews',
                    		//'/<alias>' => 'site/detail',
                    		'loai-san-pham' => 'site/listType',
                    		'chi-tiet-album/<alias>' => 'site/detailAlbum',
                    		//'/<alias>' => 'site/listNews',
                    		//'/<alias>' => 'site/detailNews',
                            'rg/<alias>' => 'site/detailNews',
                    		'tim-kiem/' => 'site/search',
                            'search/' => 'site/search_new',
                            'comment/' => 'site/listseemore',
                            'images/<alias>' => 'site/imgevent',
                    		'list-video' => 'site/listvideo',
                    		'chi-tiet-loai-video/<alias>' => 'site/detaillistvideo',
                    		'video/<alias>' => 'site/detailvideo',
                    		'sitemap.xml'=>'site/sitemap',
                        	//'danh-muc-post<id:\d+>' => array('site/listType', 'urlSuffix'=>'.html'),
     
                            'san-pham/<alias>' => 'site/detail',
                    		'thu-vien-anh' => 'site/ListAlbum',
                    		'tim-kiem/' => 'site/search',
                    		'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
                       // '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                    ),
                ),
            ),
        )
    );
?>