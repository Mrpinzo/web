<?php

Yii::import('zii.widgets.CPortlet');

class Right extends CPortlet {
	
    protected function renderContent() {
    	$criteria2 = new CDbCriteria();
    	$criteria2->condition = 'agency=1 AND new_showhide=1';
    	$criteria2->order = 'new_id ASC';
    	$menu_right = News::model()->findAll($criteria2);
    	
    	$criteria3 = new CDbCriteria();
    	$criteria3->condition = 'highlights_video=1 AND showhide_video=1';
    	$criteria3->limit = '1';
    	$video = YiiVideo::model()->findAll($criteria3);
    	
    	$logo = Configure::model()->findAll();
    	
    	$criteria3a = new CDbCriteria();
    	$criteria3a->condition = 'highlights_typealbum=1 AND showhide_typealbum=1';
    	$criteria3a->limit = '1';
    	$typealbum = YiiTypealbum::model()->findAll($criteria3a);
    	foreach ($video as $val){
    	   $a = $this->getYouTubeVideoId($val->url_video);
    	}
        $this->render('home/right',array('menu_right'=>$menu_right,'video'=>$video,'logo'=>$logo,'typealbum'=>$typealbum,'a'=>$a));
    }
    
    protected function getYouTubeVideoId($url)
    {
    	$video_id = false;
    	$url = parse_url($url);
    	if (strcasecmp($url['host'], 'youtu.be') === 0)
    	{
    	#### (dontcare)://youtu.be/<video id>
    		$video_id = substr($url['path'], 1);
    	}
    	elseif (strcasecmp($url['host'], 'www.youtube.com') === 0)
    	{
    	if (isset($url['query']))
    	{
    	parse_str($url['query'], $url['query']);
    	if (isset($url['query']['v']))
    		{
    		#### (dontcare)://www.youtube.com/(dontcare)?v=<video id>
    				$video_id = $url['query']['v'];
    		}
    		}
    			if ($video_id == false)
    			{
    			$url['path'] = explode('/', substr($url['path'], 1));
    			if (in_array($url['path'][0], array('e', 'embed', 'v')))
    			{
    			#### (dontcare)://www.youtube.com/(whitelist)/<video id>
    			$video_id = $url['path'][1];
    			}
    			}
    			}
    			return $video_id;
    			}
}
?>