<?php

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class HomeController extends CController {

    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    public $layout = '//layouts/main';
    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $menu = array();
    /**
     * @var array the breadcrumbs of the current page. The value of this property will
     * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
     * for more details on how to specify this property.
     */
    public $breadcrumbs = array();
    
    public function actions()
    {
    	return array(
    			// captcha action renders the CAPTCHA image displayed on the contact page
    			'captcha'=>array(
    					'class'=>'CCaptchaAction',
    					'backColor'=>0xFFFFFF,
    			),
    	);
    }
    
   public function getYouTubeVideoId($url)
		{
		    $video_id = false;
		    $url = parse_url($url);
		    if (strcasecmp($url['host'], 'youtu.be') === 0)
		    {
		        #### (dontcare)://youtu.be/<video id>
		        $video_id = substr($url['path'], 1);
		    }
		    elseif (strcasecmp($url['host'], 'www.youtube.com') === 0)
		    {
		        if (isset($url['query']))
		        {
		            parse_str($url['query'], $url['query']);
		            if (isset($url['query']['v']))
		            {
		                #### (dontcare)://www.youtube.com/(dontcare)?v=<video id>
		                $video_id = $url['query']['v'];
		            }
		        }
		        if ($video_id == false)
		        {
		            $url['path'] = explode('/', substr($url['path'], 1));
		            if (in_array($url['path'][0], array('e', 'embed', 'v')))
		            {
		                #### (dontcare)://www.youtube.com/(whitelist)/<video id>
		                $video_id = $url['path'][1];
		            }
		        }
		    }
		    return $video_id;
		}
}