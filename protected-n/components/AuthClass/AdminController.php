<?php
class AdminController extends CController
{
    //public $layout='column1';
    public $menu=array();
    public $breadcrumbs=array();
    public function filters()
    {
        return array(
            'accessControl',
        );
    }
 public function accessRules() {
        return array(
            array('allow',
                'users' => array('*'),
                'actions' => array('login'),
            ),
            array('allow',
                'users' => array('@'),
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }

    /* định dạng chuỗi nhập
     tham số $tolower, true nếu bạn muốn nhận chuỗi không dấu dưới dạng viết thường,
     false sẽ trả về chữ không dấu còn viết hoa hay viết thường thì duy trì như chuỗi ban đầu đưa vào. */
  public  function vn2latin($str, $tolower = false)
    {
    	$str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $str);
    	$str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $str);
    	$str = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $str);
    	$str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $str);
    	$str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $str);
    	$str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $str);
    	$str = preg_replace("/(đ)/", 'd', $str);
    	$str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $str);
    	$str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $str);
    	$str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $str);
    	$str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $str);
    	$str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $str);
    	$str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $str);
    	$str = preg_replace("/(Đ)/", 'D', $str);
    	$str = preg_replace(array('/[^a-zA-Z0-9 -]/', '/[ -]+/', '/^-|-$/'),
    			array('', '-', ''), $str);
    	if ($tolower) {
    		return strtolower($str);
    	}
    
    	return $str;
    
    }
    
    
    public function mailsend($to,$from,$from_name,$subject,$message,$cc=array())
    {
      /*   $model=new ContactForm;
        if(isset($_POST['ContactForm']))
        {
            $model->attributes=$_POST['ContactForm'];
            if($model->validate())
            { */
                $name='=?UTF-8?B?'.base64_encode($model->name).'?=';
                $subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
                $headers="From: $name <{$model->email}>\r\n".
                    "Reply-To: {$model->email}\r\n".
                    "MIME-Version: 1.0\r\n".
                    "Content-Type: text/plain; charset=UTF-8";

                // mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);

                $mail = new YiiMailer('contact', array(
                    'message'=>$model->body,
                    'name'=>$model->name,
                    'description'=>'Contact form'
                ));

                $mail->setFrom($model->email, $model->name);
                $mail->setSubject($model->subject);
                $mail->setTo(Yii::app()->params['adminEmail']);

                $mail->isSMTP();
                $mail->host = "smtp.gmail.com";
                $mail->port = 465;
                $mail->Username = "username@gmail.com";
                $mail->Password = "password";

                if($mail->send())
                {
                    Yii::app()->user->setFlash('contact','Gracias por contactarnos, te responderemos tan pronto como podamos.');
                    $this->refresh();
                }

                else
                {
                    Yii::app()->user->setFlash('error','Error enviando correo.');
                    $this->refresh();
                }
          //  }
      //  }
    }
    
    public function getYouTubeVideoId($url)
    {
    	$video_id = false;
    	$url = parse_url($url);
    	if (strcasecmp($url['host'], 'youtu.be') === 0)
    	{
    	#### (dontcare)://youtu.be/<video id>
    		$video_id = substr($url['path'], 1);
    	}
    	elseif (strcasecmp($url['host'], 'www.youtube.com') === 0)
    	{
    	if (isset($url['query']))
    	{
    	parse_str($url['query'], $url['query']);
    	if (isset($url['query']['v']))
    		{
    		#### (dontcare)://www.youtube.com/(dontcare)?v=<video id>
    				$video_id = $url['query']['v'];
    		}
    		}
    			if ($video_id == false)
    			{
    			$url['path'] = explode('/', substr($url['path'], 1));
    			if (in_array($url['path'][0], array('e', 'embed', 'v')))
    			{
    			#### (dontcare)://www.youtube.com/(whitelist)/<video id>
    			$video_id = $url['path'][1];
    			}
    			}
    			}
    			return $video_id;
    			}
    			
    		
}