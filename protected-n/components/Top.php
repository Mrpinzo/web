<?php

Yii::import('zii.widgets.CPortlet');

class Top extends CPortlet {
	
    protected function renderContent() {
    	$criteria = new CDbCriteria;
    	$criteria->compare('location_menu', 1);
    	$criteria->compare('showhide_menu', 1);    
    	$criteria->order = 'order_menu ASC';
    	$meunutop = YiiMenu::model()->findAll($criteria);
    	
    	$criteria = new CDbCriteria;
		$criteria->compare('adv_action', 1);
		$criteria->order = 'adv_id ASC';
		$ads = Advertise::model()->findAll($criteria);
		
    	$logo = Configure::model()->findAll(); 


    	$model = YiiMenu::model()->findByAttributes(array('url_menu' => Yii::app()->request->requestUri));
    	if(!empty($model))
    	   $id_yiimenu = $model->id_menu;
    	else 
    		$id_yiimenu = null;

        $this->render('home/top',array('logo'=>$logo,'meunutop'=>$meunutop,'ads'=>$ads,'id_yiimenu'=>$id_yiimenu));
    }
}
?>