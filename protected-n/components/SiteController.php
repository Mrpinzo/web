<?php

Yii::import('zii.widgets.CPortlet');

class SiteController extends CPortlet {
	
              public function actionPage() {
                if(empty($_GET['view']))
                    $this->actionIndex();
                $model = Spage::model()->findByUrl($_GET['view']);
            // if page is not found, then run a controller with that name
                if ($model === NULL)
                    Yii::app()->runController($_GET['view']);
                else
                    $this->render('pages/spage', array('model'=>$model));
            }
}
?>