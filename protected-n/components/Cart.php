<?php 

   class Cart {
   	
   	  static function addCart($pid,$q=1){
   	  	    $cart = Yii::app()->session['cart'];
   	  	    if(empty($cart)){
   	  	    	$cart[$pid] = $q;
   	  	    }else {
   	  	    	if(array_key_exists($pid, $cart)){
   	  	    	   foreach ($cart as $key=>$item){
   	  	    	   	 if($key == $pid){
   	  	    	   	 	$cart[$key] = ($cart[$key]+$q);
   	  	    	   	 }else {
   	  	    	   	 	$cart[$key] = $cart[$key];
   	  	    	   	 }
   	  	    	   }
   	  	    }else {
   	  	    	$cart[$pid] = $q;
   	  	    }
   	  	   }
   	  	   Yii::app()->session['cart'] = $cart;
   	  	   
   	  }
   	  
   	  static function updateCart($pid,$sl){
   	  	$cart = Yii::app()->session['cart'];   	 
   	  	$cart[$pid] = $sl;	  	
   	  	Yii::app()->session['cart'] = $cart;
   	  	
   	  	$proInfo = Yii::app()->session['cart'];
   	  	$total = 0;
   	  	$items = 0;
   	  	foreach ($proInfo as $key=>$value){
   	  		$items = $items + $value;
   	  		$item = Product::model()->findByPk($key);
   	  		$total = $total + ($item->pr_gia*$value);
   	  	};
   	  	Yii::app()->session['numbers'] = $items;
   	  	Yii::app()->session['total'] = $total;
   	    return true;
   	  }
   	  
   	  static function deleteCart($pid){
   	  	$cart = Yii::app()->session['cart'];
   	  	
   	  	if(isset($cart[$pid])){ 	
   	  			unset($_SESSION['cart'][$pid]);
   	  			$proInfo = Yii::app()->session['cart'];
		   	  	$total = 0;
		   	  	$items = 0;
		   	  	foreach ($proInfo as $key=>$value){
		   	  		$items = $items + $value;
		   	  		$item = Product::model()->findByPk($key);
		   	  		$total = $total + ($item->pr_gia*$value);
		   	  	};
		   	  	Yii::app()->session['numbers'] = $items;
		   	  	Yii::app()->session['total'] = $total;  	  			
   	  			return true;
   	  	   
   	  	}else return false;
   	  }
   }
?>