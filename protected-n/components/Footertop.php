<?php

Yii::import('zii.widgets.CPortlet');

class Footertop extends CPortlet {
	public function getData() {
		$criteria = new CDbCriteria;
		$criteria->limit = 4;
		$criteria->order = 'new_id DESC';
		return News::model()->findAll($criteria);
	}
	
    protected function renderContent() {
        $this->render('home/footertop');
    }
}
?>