<?php

    class SiteController extends HomeController
    {
    	public $ourscript;
    	public $keywords;
    	public $description;
    	public $metagooglesite;
    	
    	public function init(){
    		$logo = Configure::model()->findAll();
    		
    		foreach ($logo as $val)
    			$this->metagooglesite =$val->Verification_google;
                
           
           
        
                
    	}
    	
    	public function actionSitemap() {
    		$YiiMenu = YiiMenu::model()->findAll(array('order' => 'id_menu DESC'));
    		$Product = Product::model()->findAll(array('order' => 'pr_id DESC'));
    		$YiiTypealbum = YiiTypealbum::model()->findAll(array('order' => 'id_typealbum DESC'));
    		$News = News::model()->findAll(array('order' => 'new_id DESC'));
    		$YiiTypevideo = YiiTypevideo::model()->findAll(array('order' => 'id_typevideo DESC'));
    		$YiiVideo = YiiVideo::model()->findAll(array('order' => 'id_video DESC'));
    	
    		header('Content-Type: application/xml');
    		$this->renderPartial('sitemap', array('YiiMenu' => $YiiMenu, 'Product' => $Product,'YiiTypealbum'=>$YiiTypealbum,
    				'News' => $News,'YiiTypevideo' => $YiiTypevideo,'YiiVideo'=>$YiiVideo
    		));
    	}
    	
        public function actionIndex()
        {
          
     
            
        	$Configure = Configure::model()->findAll();
        	foreach ($Configure as $val){
        	    $this->pageTitle =$val->title_seo;
        	    $keywords  = $val->keywords_seo;
        	    $description  = $val->description_seo;
        	}
        	$criteria2 = new CDbCriteria();
        	$criteria2->condition = 'location_menu=2 AND showhide_menu=1';
        	$criteria2->order = 'order_menu ASC';
        	$MenuLeft = YiiMenu::model()->findAll($criteria2);
         	
        	$criteria2s = new CDbCriteria();
        	$criteria2s->condition = 'adv_action=1';
        	$criteria2s->order = 'adv_order ASC';
        	$adv = Advertise::model()->findAll($criteria2s);
        	
        	$criteria2sf = new CDbCriteria();
        	$criteria2sf->condition = 'adv_action=1 AND adv_positions=3';
        	$criteria2sf->order = 'adv_order ASC';
        	$advfooter = Advertise::model()->findAll($criteria2sf);
        	
         
            
        	$criteria2sdt = new CDbCriteria();
        	$criteria2sdt->condition = 'showhide_links=1';
        	$criteria2sdt->order = 'id_links DESC';
        	$crsdt = Links::model()->findAll($criteria2sdt);
        	
        	$criteria2prs = new CDbCriteria();
        	$criteria2prs->condition = 'showhide_menu=1 AND location_menu=4';
        	$criteria2prs->order = 'order_menu ASC';
        	$typeproducthome = YiiMenu::model()->findAll($criteria2prs);
            
            $criteria2k = new CDbCriteria();
        	$criteria2k->condition = 'location_menu=2 AND showhide_menu=1';
        	$criteria2k->order = 'order_menu DESC';
        	$menu_left = YiiMenu::model()->findAll($criteria2k);
            
            $criteria2kj = new CDbCriteria();
        	$criteria2kj->condition = 'h_rightvd=1 AND showhide_menu=1';
        	$criteria2kj->order = 'order_menu DESC';
            $criteria2kj->limit = '1';
        	$menu_home2 = YiiMenu::model()->findAll($criteria2kj);
            
            
            $criteria2x = new CDbCriteria();
        	$criteria2x->condition = 'location_menu=4 AND showhide_menu=1';
        	$criteria2x->order = 'order_menu DESC';
        	$menu_home = YiiMenu::model()->findAll($criteria2x);
            
            $criteria3x = new CDbCriteria();
        	$criteria3x->condition = 'location_menu=4 AND showhide_menu=1';
        	$criteria3x->order = 'order_menu DESC';
            $criteria3x->limit = '1';
        	$menu_home3 = YiiMenu::model()->findAll($criteria3x);
            
            $criteria2a            = new CDbCriteria();
        	$criteria2a->condition = 'showhide_menu=1';
        	$criteria2a->limit = '1,0';
        	$items2a = YiiMenu::model()->findAll($criteria2a);
        	 
        	foreach ($items2a as $val){
        		$a = $val->name_menu;
        		$criteria3a           = new CDbCriteria();
        		$criteria3a->condition = 'level_menu=' . $val->id_menu.' AND showhide_menu=1';
        		$criteria3a->order     = 'order_menu DESC';
        		$items3a = YiiMenu::model()->findAll($criteria3a);
        	}
        	
        	$criteria2            = new CDbCriteria();
        	$criteria2->condition = 'adv_action=1 AND adv_positions=2';
        	$criteria2->order     = 'adv_id DESC';
        	$Advertise = Advertise::model()->findAll($criteria2);
        	
        	$Configure = Configure::model()->findAll();
        	
        	   $ad = new CDbCriteria();
            	$ad->condition = 'adv_positions=2';
            	$ad->order = 'adv_id DESC';
            	$ads = Advertise::model()->findAll($ad);
        
                
         
            $this->render('home', array('Configure'=>$Configure,'ads'=>$ads,'Advertise' => $Advertise,'items2a'=>$a,'items3a'=>$items3a,'menu_home'=>$menu_home,'menu_home3'=>$menu_home3,'menu_home2'=>$menu_home2,'MenuLeft'=>$MenuLeft,'menu_left'=>$menu_left,'adv'=>$adv,'typeproducthome'=>$typeproducthome,
            		'advfooter'=>$advfooter,'crsdt'=>$crsdt
            ));
        }
        
            public function actionSearch_new()
        {
        
          
        	    	
	        $key = isset($_REQUEST['tu-khoa']) ? trim($_REQUEST['tu-khoa']) : '';
	        $this->pageTitle = 'Tim kiem: ' . $key;
	        if (!empty($key)) {
	            $criteria = new CDbCriteria();
	            $criteria->condition = 'new_title like "%' . $key . '%" OR new_summary like "%'.$key.'%" OR new_content like "%'.$key.'%" AND new_showhide=1';
	           
	            $count = News::model()->count($criteria);
	            $pages = new CPagination($count);
	            $pages->pageSize = 12;

	            $pages->applyLimit($criteria);
	            $itemsSeach = News::model()->findAll($criteria);

	            $this->render('search_new', array('items' => $itemsSeach,'pages' => $pages,'count'=>$count
	            ));
	        } else {
	            //throw new CHttpException(400, 'Ban phai nhap tu khoa tim kiem');
	            $this->redirect(Yii::app()->getHomeUrl());
	        }
        }
         public function actionImgevent()
        {
                 	$model = YiiMenu::model()->findByAttributes(array('url_menu' => '/images/'.$_GET['alias'].'.html'));
            	if(empty($model)){
            		$model = YiiMenu::model()->findByAttributes(array('rewrite_url_menu' => $_GET['alias']));
            		
            	}
            
            $this->pageTitle     = $model->title_seo_menu;
            $keywords  = $model->keywords_seo;
            $description  = $model->description_seo_menu;
            
            $criteria = new CDbCriteria();
            $criteria->condition = 'pr_type=1 AND pr_showhide=1';
            $criteria->order = 'pr_id DESC';
            $criteria->limit = 6;
            $selling = Product::model()->findAll($criteria);
            
            $criteria1 = new CDbCriteria();
            $criteria1->condition = 'adv_action=1 AND adv_positions=2';
            $criteria1->order = 'adv_id DESC';
            $ads = Advertise::model()->findAll($criteria1);
            
            $criteria2            = new CDbCriteria();
            $criteria2->condition = 'id_menu=' . $model->id_menu.' AND new_showhide=1';
            $criteria2->order     = 'new_id DESC';
            $count = News::model()->count($criteria2);
            $pages = new CPagination($count);
            $pages->pageSize = 12;
            $pages->applyLimit($criteria2);
            $items = News::model()->findAll($criteria2);
            
            if($count == 1){
            	$news = News::model()->findByAttributes(array('id_menu' => $model->id_menu));
            	$model1               = YiiMenu::model()->findByPk($news['id_menu']);
            	$this->pageTitle     = $news->new_title_seo;
            	
            	$criteria2            = new CDbCriteria();
            	$criteria2->condition = 'id_menu=' . $news->id_menu.' AND new_showhide=1 AND new_id!='.$news->new_id;
            	$criteria2->order     = 'new_id DESC';
            	$count = News::model()->count($criteria2);
            	$pages = new CPagination($count);
            	//results per page
            	$pages->pageSize = 6;
            	$pages->applyLimit($criteria2);
            	$items = News::model()->findAll($criteria2);
            	
            	$this->render('imgevent', array(
            			'news' =>$news,
            			'model' => $model1,
            			'items'  =>$items,
            			'pages' =>$pages,
            			'count' =>$count,

            	));
            }else {
		            $this->render('imgevent', array(
		            		'items' =>$items,
		            		'pages' =>$pages,
		            		'pros' =>$model,
		            		'keywords' =>$keywords,
		            		'description' =>$description,
		            ));
            }
        }
        
        
        public function actionsapxep(){
        	Yii::app()->session['sx'] = $_GET['id'];
        	echo '1';
        }
        
        public function actionListseemore()
        {
                $this->render('Listseemore');
        }
        
        public function actionList()
        {

        	$model = YiiMenu::model()->findByAttributes(array('url_menu' => $_GET['alias'].'.html'));
        	if(empty($model)){
        		$model = YiiMenu::model()->findByAttributes(array('rewrite_url_menu' => $_GET['alias']));
        		
        	}
        	$this->pageTitle     = $model->title_seo_menu;
        	$keywords  = $model->keywords_seo;
        	$description  = $model->description_seo_menu;
        	
        	$criteria2 = new CDbCriteria();
        	$criteria2->condition = 'location_menu=2 AND showhide_menu=1';
        	$criteria2->order = 'order_menu ASC';
        	$MenuLeft = YiiMenu::model()->findAll($criteria2);
        	
        	$criteria2sdt = new CDbCriteria();
        	$criteria2sdt->condition = 'showhide_links=1';
        	$criteria2sdt->order = 'id_links DESC';
        	$crsdt = Links::model()->findAll($criteria2sdt);
        	
            $comment = new CDbCriteria();
        	$comment->condition = 'location_menu=2 AND showhide_menu=1';
        	$comment->order = 'order_menu DESC';
            $comment->limit = '1';
        	$comments = YiiMenu::model()->findAll($comment);
            
        	$criteria2sf = new CDbCriteria();
        	$criteria2sf->condition = 'adv_action=1 AND adv_positions=4';
        	$criteria2sf->order = 'adv_order ASC';
        	$advfooterdt = Advertise::model()->findAll($criteria2sf);
        	
        	$this->render('list', array(
        			'model' =>$model,
        			/* 'items' =>$items,
        			'pages' =>$pages,
        			'count' =>$count, */
                    'comments' =>$comments,
        			'keywords' =>$keywords,
        			'description' =>$description,
        			'MenuLeft'=>$MenuLeft,
        			'crsdt'=>$crsdt,
        			'advfooter'=>$advfooterdt
        		
        	));
        	
        	/* if(Yii::app()->session['sx'] == 'td')
        		$orders = 'pr_gia ASC';
        	else if(Yii::app()->session['sx'] == 'gd')
        		$orders = 'pr_gia DESC';
        	else if(Yii::app()->session['sx'] == 'new')
        		$orders = 'pr_id DESC';
        	else
        		$orders = 'pr_id DESC';
        
        	$model = YiiMenu::model()->findByAttributes(array('url_menu' => '/danh-muc/'.$_GET['alias'].'.html'));
        
        	$this->pageTitle     = $model->title_seo_menu;
        	$keywords  = $model->keywords_seo;
        	$description  = $model->description_seo_menu;
        
        	$criteria2a = new CDbCriteria();
        	$criteria2a->condition = 'showhide_menu=1 AND level_menu='.$model->id_menu;
        	$menu_left = YiiMenu::model()->findAll($criteria2a);
        	$arr = array();
        	foreach ($menu_left as $vlues){
        		$arr[] = $vlues->id_menu;
        	}
        	if (!empty($arr)) {
        		$str = implode($arr, ',');
        		$criteria2 = new CDbCriteria();
        		$criteria2->condition = 'pr_showhide=1 AND id_menu in ('.$str.')';
        		$criteria2->order     = $orders;
        		$count = Product::model()->count($criteria2);
        		$pages = new CPagination($count);
        		$pages->pageSize = 16;
        		$pages->applyLimit($criteria2);
        		$items = Product::model()->findAll($criteria2);
        
        		$this->render('list', array(
        				'model' =>$model->name_menu,
        				'items' =>$items,
        				'pages' =>$pages,
        				'count' =>$count,
        				'keywords' =>$keywords,
        				'description' =>$description,
        		));
        	}
        	else {
        		$menu_left = YiiMenu::model()->findByAttributes(array('url_menu' => '/danh-muc/'.$_GET['alias'].'.html'));
        
        		if (!empty($menu_left)) {
        			$criteria2 = new CDbCriteria();
        			$criteria2->condition = 'pr_showhide=1 AND id_menu ='.$menu_left->id_menu;
        			$criteria2->order     = $orders;
        			$count = Product::model()->count($criteria2);
        			$pages = new CPagination($count);
        			$pages->pageSize = 16;
        			$pages->applyLimit($criteria2);
        			$items = Product::model()->findAll($criteria2);
        
        			$this->render('list', array(
        					'model' =>$model->name_menu,
        					'items' =>$items,
        					'pages' =>$pages,
        					'count' =>$count,
        					'keywords' =>$keywords,
        					'description' =>$description,
        			));
        		}
        	} */
        }
        
        public function actionRegister()
        {
            	$this->pageTitle = 'Register';
        	if(!empty($_GET['alias']))
                $register = $_GET['alias'];
          
            $regisv = News::model()->findByAttributes(array('rewrite_url_news'=>$register));
             
                
        	$Configure = Configure::model()->findAll();
        	
        	$model=new Register;        
        	if(isset($_POST['Register']))
        	{
        	  
        		$model->attributes=$_POST['Register'];
                
        		if($model->save()){
        		      	$ourscript = "alert('Gửi thông tin thành công.');window.location.href='/notify.html';";
        		      	Yii::app()->clientScript->registerScript('hello1script',$ourscript,CClientScript::POS_READY);
                        $this->redirect('/notify.html');
        		}
        	   	
        	}
        
        	$this->render('register',array(
        			'model'=>$model,
	            	'regisv' =>$regisv,
                   	'Configure' =>$Configure,
        			
        	));   
        
        }
        
        
         public function actionDownload()
        {
            	$this->pageTitle = 'Download';
        	if(!empty($_GET['alias']))
                $download = $_GET['alias'];
          
            $regisv = News::model()->findByAttributes(array('rewrite_url_news'=>$download));
           	if(!empty($regisv->new_id)){
            	
                   Yii::app()->session['new_id'] = $_GET['alias'];
            	
        	} 
              
           
        	$Configure = Configure::model()->findAll();
        	
        	$model=new Downloadyii;        
        	if(isset($_POST['Downloadyii']))
        	{
        	  //var_dump($_POST['Downloadyii']['namexh_downloadyii']);die;
        		$model->attributes=$_POST['Downloadyii'];
                
        		if($model->save()){
        		  
        		      	$ourscript = "alert('Gửi thông tin thành công.');window.location.href='/start-download/".$_POST['Downloadyii']['namenew_downloadyii'].".html';";
        		      	Yii::app()->clientScript->registerScript('helloscript',$ourscript,CClientScript::POS_HEAD);
                       // $this->redirect('/start-download.html');
        		}
        	   	
        	}
        
        	$this->render('download',array(
        			'model'=>$model,
	            	'regisv' =>$regisv,
                   	'Configure' =>$Configure,
        			
        	));   
        
        }
        
         public function actionStartdownload()
        {
            	$this->pageTitle = 'Startdownload';
        	
           
           $news = News::model()->findByAttributes(array('new_title' => $_GET['alias']));
            
          
         
          	$Configure = Configure::model()->findAll();	
        
        	$this->render('startdownload',array(
        		        
                       	'Configure' =>$Configure,
            		
        			
        	));
            
            
            
          
                
                
        }
         public function actionNotify()
        {
                $this->render('notify');
        }
        public function actionLienhe()
        {

        	$this->pageTitle = 'Liên hệ';
        	
        	$criteria2a            = new CDbCriteria();
        	$criteria2a->condition = 'showhide_menu=1';
        	$criteria2a->limit = '1,0';
        	$items2a = YiiMenu::model()->findAll($criteria2a);
        	 
        	foreach ($items2a as $val){
        		$a = $val->name_menu;
        		$criteria3a           = new CDbCriteria();
        		$criteria3a->condition = 'level_menu=' . $val->id_menu.' AND showhide_menu=1';
        		$criteria3a->order     = 'order_menu DESC';
        		$items3a = YiiMenu::model()->findAll($criteria3a);
        	}
        	
        	$criteria2            = new CDbCriteria();
        	$criteria2->condition = 'adv_action=1 AND adv_positions=2';
        	$criteria2->order     = 'adv_id DESC';
        	$Advertise = Advertise::model()->findAll($criteria2);
        	
        	$Configure = Configure::model()->findAll();
        	
        	$model=new Contact;        
        	if(isset($_POST['Contact']))
        	{
        		$model->attributes=$_POST['Contact'];
        		if($model->save()){
        			$ourscript = "alert('Gửi liên hệ thành công.');window.location.href='/';";
        			Yii::app()->clientScript->registerScript('helloscript',$ourscript,CClientScript::POS_READY);
        		}
        			//$this->redirect(Yii::app()->getHomeUrl());
        	}
        
        	$this->render('lienhe',array(
        			'model'=>$model,'Advertise' => $Advertise,
	            		'items2a'=>$a,
	            		'items3a'=>$items3a,
        			'Configure' =>$Configure
        			
        	));
        }
        
        public function actionListAlbum(){
        	        	
            $this->pageTitle     = 'Thư viện ảnh';
           
            $criteria = new CDbCriteria();
            $criteria->condition = 'pr_type=1 AND pr_showhide=1';
            $criteria->order = 'pr_id DESC';
            $criteria->limit = 6;
            $selling = Product::model()->findAll($criteria);
            
            $criteria1 = new CDbCriteria();
            $criteria1->condition = 'adv_action=1 AND adv_positions=2';
            $criteria1->order = 'adv_id DESC';
            $ads = Advertise::model()->findAll($criteria1);
            
            $items = Yii::app()->db->createCommand()
            ->select('*')
            ->from('yii_typealbum t')
            ->join('yii_album a', 't.join_typeAlbum=a.join_typeAlbum')
            ->where('showhide_typealbum=1')
            ->group('name_typealbum')
            ->queryAll();
            
            $count = YiiTypealbum::model()->count();
            $pages = new CPagination($count);
            $pages->pageSize = 12;
           // $pages->applyLimit($criteria2);
            
            $this->render('listAlbum', array(
            		'selling' =>$selling,
            		'ads' =>$ads,
            		'items' =>$items,
            		'pages' =>$pages,
            ));
        }
        
        public function actionDetailAlbum()
        {
        	$YiiTypealbum = YiiTypealbum::model()->findByAttributes(array('url_seo_typealbum' => $_GET['alias']));
        	$this->pageTitle     = $YiiTypealbum->title_seo_album;
        	 
        	$criteria = new CDbCriteria();
        	$criteria->condition = 'pr_type=1 AND pr_showhide=1';
        	$criteria->order = 'pr_id DESC';
        	$criteria->limit = 6;
        	$selling = Product::model()->findAll($criteria);
        
        	$criteria1 = new CDbCriteria();
        	$criteria1->condition = 'adv_action=1 AND adv_positions=2';
        	$criteria1->order = 'adv_id DESC';
        	$ads = Advertise::model()->findAll($criteria1);
        	
        	$criteria12e = new CDbCriteria();
        	$criteria12e->condition = 'showhide_album=1 AND join_typeAlbum='.$YiiTypealbum->join_typeAlbum;
        	$criteria12e->order = 'id_album DESC';
        	$album = YiiAlbum::model()->findAll($criteria12e);
        
        	$this->render('detailAlbum', array(
        			'selling' =>$selling,
        			'ads' =>$ads,
        			'YiiTypealbum' =>$YiiTypealbum,
        			'model' =>$album,
        	));
        }
        
        public function actionListvideo(){
        
        	$this->pageTitle     = 'Thư viện video';
        
        	$items = Yii::app()->db->createCommand()
        	->select('*')
        	->from('yii_typevideo t')
        	->join('yii_video a', 't.id_typevideo=a.id_typevideo')
        	->where('showhide__typevideo=1 AND showhide_video=1')
        	->group('name__typevideo')
        	->queryAll();

        	$count = YiiTypealbum::model()->count();
        	$pages = new CPagination($count);
        	$pages->pageSize = 12;
        	// $pages->applyLimit($criteria2);
            $a = array();
        	foreach ($items as $val){
        		$a[$this->getYouTubeVideoId($val['url_video'])] = array('url_seo_video'=>$val['url_seo_video'],'url_typevideo'=>$val['url_typevideo']
        				,'name__typevideo'=>$val['name__typevideo'],'name_video'=>$val['name_video']);
        	}

        	$this->render('listvideo', array(
        			'items' =>$items,
        			'pages' =>$pages,
        			'a' =>$a,
        	));
        }
        
        public function actionDetaillistvideo(){
        
        	$items = Yii::app()->db->createCommand()
        	->select('*')
        	->from('yii_typevideo t')
        	->join('yii_video a', 't.id_typevideo=a.id_typevideo')
        	->where('showhide__typevideo=1 AND showhide_video=1 AND url_typevideo="'.$_GET['alias'].'"')
        	->queryAll();

        	$count = YiiTypealbum::model()->count();
        	$pages = new CPagination($count);
        	$pages->pageSize = 12;
        	// $pages->applyLimit($criteria2);
        	$a = array();
        	foreach ($items as $val){
        		$a[$this->getYouTubeVideoId($val['url_video'])] = array('url_seo_video'=>$val['url_seo_video']
        				,'name_video'=>$val['name_video']);
        		$name__typevideo = $val['name__typevideo'];
        		$keywords = $val['keywords_typevideo'];
        		$description = $val['description_typevideo'];
        		$this->pageTitle = $val['title_typevideo'];
        	}
        
        	$this->render('detaillistvideo', array(
        			'items' =>$items,
        			'pages' =>$pages,
        			'a' =>$a,
        			'name__typevideo'=>$name__typevideo,
        			'keywords' => $keywords,
        	        'description' => $description,
        	));
        }
        
        public function actionDetailvideo()
        {
        
        	$YiiVideo = YiiVideo::model()->findByAttributes(array('url_seo_video' => $_GET['alias']));
        	$criteria2a            = new CDbCriteria();
        	$criteria2a->condition = 'showhide_video=1 AND id_typevideo='.$YiiVideo->id_typevideo.' AND id_video!='.$YiiVideo->id_video;
        	$criteria2a->order     = 'id_video DESC';
        	$model = YiiVideo::model()->findAll($criteria2a);
        	$count = YiiVideo::model()->count($criteria2a);

        	$YiiTypeVideo = YiiTypevideo::model()->findByPk($YiiVideo->id_typevideo);

        	$this->pageTitle     = $YiiVideo->title_video;
        	$keywords  = $YiiVideo->keywords_video;
        	$description  = $YiiVideo->description_video;
        
        	$a = array();
        	foreach ($model as $val){
        		$a[$this->getYouTubeVideoId($val['url_video'])] = array('url_seo_video'=>$val['url_seo_video']
        				,'name_video'=>$val['name_video']);
        	}

        	$this->render('detailvideo', array(
        			'keywords' =>$keywords,
        			'description' =>$description,
        			'items' =>$a,
        			'name__typevideo' => $YiiTypeVideo->name__typevideo,
        			'url_typevideo' => $YiiTypeVideo->url_typevideo,
        			'YiiVideo' => $YiiVideo,
        			'count'  => $count
        	));
        }
        
        public function actionDetailcart()
        {
        
        	$this->pageTitle = 'Giỏ hàng';        
        	
        	$this->render('detailcart');
        }
        
        public function actionPaymentorders()
        {
        	
        		$this->pageTitle = 'Đơn hàng';
        		
        		$this->render('paymentorders');
        	
        }
        
        public function actionSearch()
        {
        	if(Yii::app()->session['sx'] == 'td')
        		$orders = 'pr_gia ASC';
        	else if(Yii::app()->session['sx'] == 'gd')
        		$orders = 'pr_gia DESC';
        	else if(Yii::app()->session['sx'] == 'new')
        		$orders = 'pr_id DESC';
        	else
        		$orders = 'pr_id DESC';
        	
	        $key = isset($_REQUEST['tu-khoa']) ? trim($_REQUEST['tu-khoa']) : '';
	        $this->pageTitle = 'Tim kiem: ' . $key;
	        if (!empty($key)) {
	            $criteria = new CDbCriteria();
	            $criteria->condition = 'pr_name like "%' . $key . '%" OR pr_gia like "%'.$key.'%" OR pr_mota like "%'.$key.'%" AND pr_showhide=1';
	            $criteria->order = $orders;
	            $count = Product::model()->count($criteria);
	            $pages = new CPagination($count);
	            $pages->pageSize = 16;

	            $pages->applyLimit($criteria);
	            $itemsSeach = Product::model()->findAll($criteria);

	            $this->render('search', array('items' => $itemsSeach,'pages' => $pages,'count'=>$count
	            ));
	        } else {
	            //throw new CHttpException(400, 'Ban phai nhap tu khoa tim kiem');
	            $this->redirect(Yii::app()->getHomeUrl());
	        }
        }

       

        public function actionListType()
        {
        	if(Yii::app()->session['sx'] == 'td')
        		$orders = 'pr_gia ASC';
        	else if(Yii::app()->session['sx'] == 'gd')
        		$orders = 'pr_gia DESC';
        	else if(Yii::app()->session['sx'] == 'new')
        		$orders = 'pr_id DESC';
        	else
        		$orders = 'pr_id DESC';
        	
        	$this->pageTitle   = 'Sản phẩm';
        
        	$criteria1a = new CDbCriteria();
        	$criteria1a->condition = 'pr_showhide=1';
        	$criteria1a->order = $orders;
        	$count = Product::model()->count($criteria1a);
        	$pages = new CPagination($count);
        	$pages->pageSize = 12;
        	$pages->applyLimit($criteria1a);
        	$model = Product::model()->findAll($criteria1a);
        
        	$this->render('listType', array(

        			'items' =>$model,
        			'pages' =>$pages,
        			'count' =>$count,
        	
        	));
        }
        
        public function actionDetail()
        {

        	$Product = Product::model()->findByAttributes(array('pr_url_news' => $_GET['alias']));
        	$model               = YiiMenu::model()->findByPk($Product['id_menu']);
        	
            $this->pageTitle     = $Product->pr_title_seo;
            $keywords  = $Product->pr_keywords_seo;
            $description  = $Product->pr_description_seo;
            
            $criteria2            = new CDbCriteria();
            $criteria2->condition = 'id_menu=' . $Product->id_menu.' AND pr_showhide=1 AND pr_id!='.$Product->pr_id;
            $criteria2->order     = 'pr_id DESC';
            $count = Product::model()->count($criteria2);
            $pages = new CPagination($count);
            $pages->pageSize = 8;
            $pages->applyLimit($criteria2);
            $items = Product::model()->findAll($criteria2);
            
            $criteria2g            = new CDbCriteria();
            $criteria2g->condition = 'showhide_yii_imgproduct = 1 AND pr_id="'.$Product['pr_joinImg'].'"';
            $YiiImgproduct = YiiImgproduct::model()->findAll($criteria2g);
            $countimg = YiiImgproduct::model()->count($criteria2g);
            
            $logo = Configure::model()->findAll();
            
            $criteria2 = new CDbCriteria();
        	$criteria2->condition = 'location_menu=2 AND showhide_menu=1';
        	$criteria2->order = 'order_menu ASC';
        	$MenuLeft = YiiMenu::model()->findAll($criteria2);
            
            $this->render('detail', array(
            		'keywords' =>$keywords,
            		'description' =>$description,
            		'Product' =>$Product,
            		'items' =>$items,
            		'count' =>$count,
            		'pages' =>$pages,
            		'pros' =>$model,
            		'YiiImgproduct'=>$YiiImgproduct,
            		'countimg' =>$countimg,
                    'MenuLeft' =>$MenuLeft,
            		'logo' => $logo
            ));
        }

        /* public function actionDetailNews($id)
        {
        	$model               = News::model()->findByPk($id);
        	$this->pageTitle     = $model->new_title;
        	
        	$this->render('detailnews', array(
        			
        			'model' => $model,
        	));
        } */
        
        public function actionPage()
        {
            if (empty($_GET['view']))
                $this->actionIndex();
            $model = Page::model()->findByUrl($_GET['view']);
// if page is not found, then run a controller with that name
            if ($model === NULL)
                throw new CHttpException(400, 'Khong tim thay trang');
            else {
                $this->pageTitle = $model->title;
                $this->render('page', array('model' => $model));
            }
        }

        /**
         * Displays the login page
         */
        public function actionLogin()
        {
        	$model=new LoginFormShop;
        	
        	// collect user input data
        	if(isset($_POST['LoginFormShop']))
        	{
        		$model->attributes=$_POST['LoginFormShop'];
        		// validate user input and redirect to the previous page if valid
        		if($model->validate() && $model->login()){
        			$this->redirect(Yii::app()->user->returnUrl);
        		}
        	}
        	// display the login form
        	$this->render('login',array('model'=>$model));
        }
        
        /**
         * Logs out the current user and redirect to homepage.
         */
        public function actionLogout()
        {
        	Yii::app()->user->logout();
        	$this->redirect(Yii::app()->homeUrl);
        }
        
        public function actionListNews()
        {
        
            	$model = YiiMenu::model()->findByAttributes(array('url_menu' => $_GET['alias'].'.html'));
            	if(empty($model)){
            		$model = YiiMenu::model()->findByAttributes(array('rewrite_url_menu' => $_GET['alias']));
            		
            	}
            
            $this->pageTitle     = $model->title_seo_menu;
            $keywords  = $model->keywords_seo;
            $description  = $model->description_seo_menu;
            
            $criteria = new CDbCriteria();
            $criteria->condition = 'pr_type=1 AND pr_showhide=1';
            $criteria->order = 'pr_id DESC';
            $criteria->limit = 6;
            $selling = Product::model()->findAll($criteria);
            
            $criteria1 = new CDbCriteria();
            $criteria1->condition = 'adv_action=1 AND adv_positions=2';
            $criteria1->order = 'adv_id DESC';
            $ads = Advertise::model()->findAll($criteria1);
            
            $criteria2            = new CDbCriteria();
            $criteria2->condition = 'id_menu=' . $model->id_menu.' AND new_showhide=1';
            $criteria2->order     = 'new_id DESC';
            $count = News::model()->count($criteria2);
            $pages = new CPagination($count);
            $pages->pageSize = 12;
            $pages->applyLimit($criteria2);
            $items = News::model()->findAll($criteria2);
            
            $Configure = Configure::model()->findAll();
            
            if($count == 1){
            	$news = News::model()->findByAttributes(array('id_menu' => $model->id_menu));
            	$model1               = YiiMenu::model()->findByPk($news['id_menu']);
            	$this->pageTitle     = $news->new_title_seo;
            	
            	$criteria2            = new CDbCriteria();
            	$criteria2->condition = 'id_menu=' . $news->id_menu.' AND new_showhide=1 AND new_id!='.$news->new_id;
            	$criteria2->order     = 'new_id DESC';
            	$count = News::model()->count($criteria2);
            	$pages = new CPagination($count);
            	//results per page
            	$pages->pageSize = 6;
            	$pages->applyLimit($criteria2);
            	$items = News::model()->findAll($criteria2);
                
                $Configure = Configure::model()->findAll();
                
            	
            	$this->render('detailNews', array(
            			'news' =>$news,
            			'model' => $model1,
            			'items'  =>$items,
            			'pages' =>$pages,
            			'count' =>$count,
                        'Configure' =>$Configure,

            	));
            }else {
		            $this->render('listNews', array(
		            		'items' =>$items,
		            		'pages' =>$pages,
		            		'pros' =>$model,
                            'count' =>$count,
		            		'keywords' =>$keywords,
		            		'description' =>$description,
                            'Configure' =>$Configure,
		            ));
            }
        }
        
        public function actionDetailNews()
        {
        	
        	$news = News::model()->findByAttributes(array('rewrite_url_news' => $_GET['alias']));
        	
        if(!empty($news)){
        	if(!empty($news->new_id)){
	        	if(Yii::app()->session['new-'.$news->new_id] == null){
	        		$news->view = $news->view + 1;
	        		$news->save();
	        		Yii::app()->session['new-'.$news->new_id] = $_GET['alias'];
	        	}
        	}
        	$model               = YiiMenu::model()->findByPk($news['id_menu']);
        	$this->pageTitle     = $news->new_title_seo;
        
            $criteria2            = new CDbCriteria();
            $criteria2->condition = 'id_menu=' . $news->id_menu.' AND new_showhide=1 AND new_id!='.$news->new_id;
            $criteria2->order     = 'new_id DESC';
            $count = News::model()->count($criteria2);
            $pages = new CPagination($count);
            //results per page
            $pages->pageSize = 6;
            $pages->applyLimit($criteria2);
            $items = News::model()->findAll($criteria2);
            
            $Configure = Configure::model()->findAll();
            
        	$this->render('detailNews', array(
            		'news' =>$news,
        			'model' => $model,
        			'items'  =>$items,
            		'pages' =>$pages,
                    'Configure' =>$Configure,
        			'count' =>$count
            ));
           }else {
           	throw new CHttpException(404, 'Page not found.');
           }
        }
        
        public function actionAjaxlogin()
        {
        	$act = $_GET['act'];
        
        				if ($act == 'login') {
        				    $model=new LoginFormShop;
        				    
				        	// collect user input data
				        	if(isset($_POST['LoginFormShop']))
				        	{
				        		$model->attributes=$_POST['LoginFormShop'];
				        		// validate user input and redirect to the previous page if valid
				        		if($model->validate() && $model->login()){
				        			echo 'ok';
				        			//$this->redirect(Yii::app()->user->returnUrl);
				        		}else 
				        			echo 'no';
				        	}
        				}      			
        		        	
        }
        
        /**
         * This is the action to handle external exceptions.
         */
        public function actionError()
        {
        	$this->layout='error';
            if ($error = Yii::app()->errorHandler->error) {
                if (Yii::app()->request->isAjaxRequest)
                    echo $error['message'];
                else
                    $this->render('error', $error);
            }
        }

    }