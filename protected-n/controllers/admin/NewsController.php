<?php

class NewsController extends AdminController
{

	/**
	 * @return array action filters
	 */
	/* public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	} */

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */

	public function accessRules()
	{
		$items2a = YiiRole::model()->findByPk(Yii::app()->user->getState('role'));
	
		if(!empty($items2a['type_role'])){
				
			$arrs = CJSON::decode($items2a['type_role'],true);
			 
			$a = '';
			foreach ($arrs as $val){
					
				if(in_array('News', $val)){
					$a .= implode('-',$val).'-';
				}
					
			};
			if(!empty($a)){
				$arrdsd = explode('-', $a);
				$arr1 = array_unique($arrdsd);
				array_shift($arr1);
				array_pop($arr1);
			}else{
				$arr1 = array('');
			}
		}else {
			$arr1 = array('');
		}
		//var_dump($arr1);die;
		return array(
	
				array('allow', // allow authenticated user to perform 'create' and 'update' actions
						'actions'=>$arr1,
						'users'=>array('*'),
				),
	
				array('deny',  // deny all users
						'users'=>array('*'),
				),
		);
	
	
	}
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->layout = 'main';
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$this->layout = 'main';
		$model=new News;

	   $criteria = new CDbCriteria();
		$criteria->condition = 'showhide_menu=1';
		$criteria->order = 'order_menu ASC';
		$YiiMenu = YiiMenu::model()->findAll($criteria);
	   $subarr = array();
	foreach ($YiiMenu as $val){

			if($val->level_menu == 0){

				$criteriag = new CDbCriteria();
				$criteriag->condition = 'level_menu='.$val->id_menu.' AND showhide_menu=1';
				$criteriag->order = 'order_menu ASC';
				$YiiMenug = YiiMenu::model()->findAll($criteriag);
				if(!empty($YiiMenug)){
				foreach ($YiiMenug as $key=>$valg){

					$subarr['+ '.$val->name_menu][$valg->id_menu] = '---- '.$valg->name_menu;

				}
			  }else
			  	$subarr[$val->id_menu] = '+ '.$val->name_menu;
			  
			}else{
				$criteriag = new CDbCriteria();
				$criteriag->condition = 'level_menu='.$val->id_menu.' AND showhide_menu=1';
				$criteriag->order = 'order_menu ASC';
				$YiiMenug = YiiMenu::model()->findAll($criteriag);
				if(!empty($YiiMenug)){
					foreach ($YiiMenug as $key=>$valg){
				
						$subarr['+ '.$val->name_menu][$valg->id_menu] = '---- '.$valg->name_menu;
				
					}
				}
			}
		}

		if(isset($_POST['News']))
		{
			$model->attributes=$_POST['News'];
			$model->new_date = gmdate("Y-m-d");
			$model->rewrite_url_news = $this->vn2latin($_POST['News']['new_title'],false).'-new'.rand(1,99999);
			if($model->save())
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}

		$this->render('create',array(
			'model'=>$model,'YiiMenu' =>$subarr,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$this->layout = 'main';
		$model=$this->loadModel($id);

	    $criteria = new CDbCriteria();
		$criteria->condition = 'showhide_menu=1';
		$criteria->order = 'order_menu ASC';
		$YiiMenu = YiiMenu::model()->findAll($criteria);
	    $subarr = array();
	foreach ($YiiMenu as $val){

			if($val->level_menu == 0){

				$criteriag = new CDbCriteria();
				$criteriag->condition = 'level_menu='.$val->id_menu.' AND showhide_menu=1';
				$criteriag->order = 'order_menu ASC';
				$YiiMenug = YiiMenu::model()->findAll($criteriag);
				if(!empty($YiiMenug)){
				foreach ($YiiMenug as $key=>$valg){

					$subarr['+ '.$val->name_menu][$valg->id_menu] = '---- '.$valg->name_menu;

				}
			  }else
			  	$subarr[$val->id_menu] = '+ '.$val->name_menu;
			  
			}else{
				$criteriag = new CDbCriteria();
				$criteriag->condition = 'level_menu='.$val->id_menu.' AND showhide_menu=1';
				$criteriag->order = 'order_menu ASC';
				$YiiMenug = YiiMenu::model()->findAll($criteriag);
				if(!empty($YiiMenug)){
					foreach ($YiiMenug as $key=>$valg){
				
						$subarr['+ '.$val->name_menu][$valg->id_menu] = '---- '.$valg->name_menu;
				
					}
				}
			}
		}

		if(isset($_POST['News']))
		{
			$model->attributes=$_POST['News'];
			$model->rewrite_url_news = $this->vn2latin($_POST['News']['new_title'],false).'-new'.rand(1,99999);
			if($model->save())
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}

		$this->render('update',array(
			'model'=>$model,'YiiMenu' =>$subarr,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('News');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$this->layout = 'main';
		$model=new News('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['News']))
			$model->attributes=$_GET['News'];

		$criteria = new CDbCriteria();
		$criteria->condition = 'showhide_menu=1';
		$criteria->order = 'order_menu ASC';
		$YiiMenu = YiiMenu::model()->findAll($criteria);
		$subarr = array();
	foreach ($YiiMenu as $val){

			if($val->level_menu == 0){

				$criteriag = new CDbCriteria();
				$criteriag->condition = 'level_menu='.$val->id_menu.' AND showhide_menu=1';
				$criteriag->order = 'order_menu ASC';
				$YiiMenug = YiiMenu::model()->findAll($criteriag);
				if(!empty($YiiMenug)){
				foreach ($YiiMenug as $key=>$valg){

					$subarr['+ '.$val->name_menu][$valg->id_menu] = '---- '.$valg->name_menu;

				}
			  }else
			  	$subarr[$val->id_menu] = '+ '.$val->name_menu;
			  
			}
		}
		
		$this->render('admin',array(
			'model'=>$model,
			'YiiMenu' =>$subarr,
		));
	}

	public function actionAjaxUpdate()
	{
		$act = $_GET['act'];

		if(isset($_POST['autoId'])){
			$autoIdAll = $_POST['autoId'];
			if (count($autoIdAll) > 0) {
				foreach ($autoIdAll as $autoId) {
					$model = $this->loadModel($autoId);
					if ($act == 'doDelete') {
						$model->delete();
					} else {
						if ($act == 'doActive')
							$model->new_showhide = 1;
	
						if ($act == 'doInactive')
							$model->new_showhide = 0;
	
						if ($act == 'highlights')
							$model->new_highlights = 1;
						
						if ($act == 'nohighlights')
							$model->new_highlights = 0;
	
						if ($model->save())
							echo 'ok';
						else
							
							throw new Exception("Sorry", 500);
					}
				}
			}
		}
	}
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return News the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=News::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param News $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='news-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	/* định dạng chuỗi nhập
	 tham số $tolower, true nếu bạn muốn nhận chuỗi không dấu dưới dạng viết thường,
	 false sẽ trả về chữ không dấu còn viết hoa hay viết thường thì duy trì như chuỗi ban đầu đưa vào. */
	function vn2latin($str, $tolower = false)
	{
		$str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $str);
		$str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $str);
		$str = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $str);
		$str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $str);
		$str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $str);
		$str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $str);
		$str = preg_replace("/(đ)/", 'd', $str);
		$str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $str);
		$str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $str);
		$str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $str);
		$str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $str);
		$str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $str);
		$str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $str);
		$str = preg_replace("/(Đ)/", 'D', $str);
		$str = preg_replace(array('/[^a-zA-Z0-9 -]/', '/[ -]+/', '/^-|-$/'),
				array('', '-', ''), $str);
		if ($tolower) {
			return strtolower($str);
		}
	
		return $str;
	
	}
}
