<?php

class YiiAlbumController extends AdminController
{

	/**
	 * @return array action filters
	 */
	/* public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	} */

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		$items2a = YiiRole::model()->findByPk(Yii::app()->user->getState('role'));
	
		if(!empty($items2a['type_role'])){
				
			$arrs = CJSON::decode($items2a['type_role'],true);
			 
			$a = '';
			foreach ($arrs as $val){
					
				if(in_array('YiiAlbum', $val)){
					$a .= implode('-',$val).'-';
				}
					
			};
			if(!empty($a)){
				$arrdsd = explode('-', $a);
				$arr1 = array_unique($arrdsd);
				array_shift($arr1);
				array_pop($arr1);
			}else{
				$arr1 = array('');
			}
		}else {
			$arr1 = array('');
		}
		//var_dump($arr1);die;
		return array(
	
				array('allow', // allow authenticated user to perform 'create' and 'update' actions
						'actions'=>$arr1,
						'users'=>array('*'),
				),
				array('allow', // allow authenticated user to perform 'create' and 'update' actions
						'actions'=>array('upload'),
						'users'=>array('*'),
				),
				array('deny',  // deny all users
						'users'=>array('*'),
				),
		);
	
	
	}
	public function actionAjaxUpdate()
	{
		$act = $_GET['act'];
	
		if(isset($_POST['autoId'])){
			$autoIdAll = $_POST['autoId'];
			if (count($autoIdAll) > 0) {
				foreach ($autoIdAll as $autoId) {
					$model = $this->loadModel($autoId);
					if ($act == 'doDelete') {
						$model->delete();
					} else {
						if ($act == 'doActive')
							$model->showhide_album = 1;
	
						if ($act == 'doInactive')
							$model->showhide_album = 0;
	
						if ($model->save())
							echo 'ok';
						else
	
							throw new Exception("Sorry", 500);
					}
				}
			}
		}
	}
	

	public function actionUpload() {
	
		//header('Content-type: text/plain; charset=UTF-8');
		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("Cache-Control: no-store, no-cache, must-revalidate");
		header("Cache-Control: post-check=0, pre-check=0", FALSE);
		header("Pragma: no-cache");
	
		// Settings
		$targetDir = Yii::getPathOfAlias('webroot') . DIRECTORY_SEPARATOR . 'upload' . DIRECTORY_SEPARATOR .'album'.DIRECTORY_SEPARATOR. date('Y-m-d');
	
		if (!is_dir($targetDir)) {
			mkdir($targetDir, 0755, TRUE);
		}
		$cleanupTargetDir = TRUE; // Remove old files
		$maxFileAge = 5 * 3600; // Temp file age in seconds
		// 5 minutes execution time
		@set_time_limit(5 * 60);
	
		// Uncomment this one to fake upload time
		// usleep(5000);
		// Get parameters
		$chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
		$chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;
		$fileName = isset($_REQUEST["name"]) ? $_REQUEST["name"] : '';
	
		// Clean the fileName for security reasons
		$fileName = preg_replace('/[^\w\._]+/', '_', $fileName);
	
		// Make sure the fileName is unique but only if chunking is disabled
		if ($chunks < 2 && file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName)) {
			$ext = strrpos($fileName, '.');
			$fileName_a = substr($fileName, 0, $ext);
			$fileName_b = substr($fileName, $ext);
	
			$count = 1;
			while (file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName_a . '_' . $count . $fileName_b))
				$count++;
	
			$fileName = $fileName_a . '_' . $count .rand(1, 999999) . $fileName_b;
		}
	
		$filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;
	
		// Create target dir
		if (!file_exists($targetDir))
			@mkdir($targetDir);
	
		// Remove old temp files
		if ($cleanupTargetDir && is_dir($targetDir) && ($dir = opendir($targetDir))) {
			while (($file = readdir($dir)) !== FALSE) {
				$tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;
	
				// Remove temp file if it is older than the max age and is not the current file
				if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge) && ($tmpfilePath != "{$filePath}.part")) {
					@unlink($tmpfilePath);
				}
			}
	
			closedir($dir);
		} else
			die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');
	
	
		// Look for the content type header
		if (isset($_SERVER["HTTP_CONTENT_TYPE"]))
			$contentType = $_SERVER["HTTP_CONTENT_TYPE"];
	
		if (isset($_SERVER["CONTENT_TYPE"]))
			$contentType = $_SERVER["CONTENT_TYPE"];
	
		// Handle non multipart uploads older WebKit versions didn't support multipart in HTML5
		if (strpos($contentType, "multipart") !== FALSE) {
			if (isset($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name'])) {
				// Open temp file
				$out = fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
				if ($out) {
					// Read binary input stream and append it to temp file
					$in = fopen($_FILES['file']['tmp_name'], "rb");
	
					if ($in) {
						while ($buff = fread($in, 4096))
							fwrite($out, $buff);
					} else
						die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
					fclose($in);
					fclose($out);
					@unlink($_FILES['file']['tmp_name']);
				} else
					die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
			} else
				die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
		} else {
			// Open temp file
			$out = fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
			if ($out) {
				// Read binary input stream and append it to temp file
				$in = fopen("php://input", "rb");
	
				if ($in) {
					while ($buff = fread($in, 4096))
						fwrite($out, $buff);
				} else
					die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
	
				fclose($in);
				fclose($out);
			} else
				die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
		}
	
		// Check if file has been uploaded
		if (!$chunks || $chunk == $chunks - 1) {
			
			// Strip the temp .part suffix off
			rename("{$filePath}.part", $filePath);
			//Save o day
			$info = pathinfo($filePath);

			$model = new YiiAlbum;
			$model->img_album = '/upload/album/' . date('Y-m-d') . '/' . $info['basename'];
			$model->join_typeAlbum = $_GET['join'];
			$model->save();

			echo json_encode(array(
					'jsonrpc' => '2.0',
					'result' => $filePath,
					//                    'link'   => Yii::app()->params['domain'] . '/file/download/' . $_REQUEST['contentTypeId'] . '/' . $_REQUEST['linkId'] . '/' . $fileId,
			));
			die;
		}
		// Return JSON-RPC response
		die('{"jsonrpc" : "2.0", "result" : null, "id" : "id"}');
	}
	
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new YiiAlbum;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['YiiAlbum']))
		{
			$model->attributes=$_POST['YiiAlbum'];
			if($model->save())
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		//if(isset($_POST['YiiAlbum']))
		//{
		/* if($model->showhide_album==1)
			$model->showhide_album=0;
		else 
			$model->showhide_album=1;
			if($model->save())
				$this->redirect(Yii::getPathOfAlias('application')); */
				//$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
				//echo '1';
		//}
		if(isset($_GET['id']) && isset($_GET['sh']) && isset($_GET['join']))
		{
			if($_GET['sh'] == 1)
				$_GET['sh'] = 0;
			else if($_GET['sh'] == 0)
				$_GET['sh'] = 1;
			
			$YiiTypealbum = YiiTypealbum::model()->findByAttributes(array('join_typeAlbum' => $_GET['join']));

			$model->showhide_album=$_GET['sh'];
			if($model->save())
			    $this->redirect('/admin.php/yiiTypealbum/update?id='.$YiiTypealbum->id_typealbum);
		}
				
		/* $this->render('update',array(
			'model'=>$model,
		)); */
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete()
	{
		

		if(isset($_GET['id']) && isset($_GET['join']))
		{
			$model=$this->loadModel($_GET['id']);
			$YiiTypealbum = YiiTypealbum::model()->findByAttributes(array('join_typeAlbum' => $_GET['join']));

			if($model->delete())
				$this->redirect('/admin.php/yiiTypealbum/update?id='.$YiiTypealbum->id_typealbum);
		}
		
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		//if(!isset($_GET['ajax']))
			//$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
			//echo '1';
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('YiiAlbum');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new YiiAlbum('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['YiiAlbum']))
			$model->attributes=$_GET['YiiAlbum'];
		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return YiiAlbum the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=YiiAlbum::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param YiiAlbum $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='yii-album-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
