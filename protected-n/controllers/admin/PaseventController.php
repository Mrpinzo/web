<?php

class PaseventController extends AdminController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */

	/**
	 * @return array action filters
	 */
	/* public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	} */

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		$items2a = YiiRole::model()->findByPk(Yii::app()->user->getState('role'));
	
		if(!empty($items2a['type_role'])){
				
			$arrs = CJSON::decode($items2a['type_role'],true);
			 
			$a = '';
			foreach ($arrs as $val){
					
				if(in_array('Pasevent', $val)){
					$a .= implode('-',$val).'-';
				}
					
			};
			if(!empty($a)){
				$arrdsd = explode('-', $a);
				$arr1 = array_unique($arrdsd);
				array_shift($arr1);
				array_pop($arr1);
			}else{
				$arr1 = array('');
			}
		}else {
			$arr1 = array('');
		}
		//var_dump($arr1);die;
		return array(
	
				array('allow', // allow authenticated user to perform 'create' and 'update' actions
						'actions'=>$arr1,
						'users'=>array('*'),
				),
	
				array('deny',  // deny all users
						'users'=>array('*'),
				),
		);
	
	
	}
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */

	public function actionView($id)
	{
		$this->layout = 'main';
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$this->layout = 'main';
		$model=new Pasevent;

	    $criteria = new CDbCriteria();
		$criteria->condition = 'showhide_menu=1';
		$criteria->order = 'order_menu ASC';
		$YiiMenu = YiiMenu::model()->findAll($criteria);
	$subarr = array();
	foreach ($YiiMenu as $val){

			if($val->level_menu == 0){

				$criteriag = new CDbCriteria();
				$criteriag->condition = 'level_menu='.$val->id_menu.' AND showhide_menu=1';
				$criteriag->order = 'order_menu ASC';
				$YiiMenug = YiiMenu::model()->findAll($criteriag);
				$subarr[$val->id_menu] = '+ '.$val->name_menu;
				if(!empty($YiiMenug)){
				foreach ($YiiMenug as $key=>$valg){

					$subarr[$valg->id_menu] = '---- '.$valg->name_menu;
					
					$criteriagb = new CDbCriteria();
					$criteriagb->condition = 'level_menu='.$valg->id_menu.' AND showhide_menu=1';
					$criteriagb->order = 'order_menu ASC';
					$YiiMenugb = YiiMenu::model()->findAll($criteriagb);
					if(!empty($YiiMenugb)){
						foreach ($YiiMenugb as $keyb=>$valgb){
					
							$subarr[$valgb->id_menu] = '-------- '.$valgb->name_menu;
						}
					}
				}
			  }
			  			  
			}
		}
	
		$criteriaf = new CDbCriteria();
		$criteriaf->condition = 'showhide_menu=1';
		$criteriaf->order = 'order_menu ASC';
		$YiiFilter = YiiFilter::model()->findAll($criteriaf);
		$subarrFilter = array();
		foreach ($YiiFilter as $val){
		
			if($val->level_menu == 0){
		
				$criteriag = new CDbCriteria();
				$criteriag->condition = 'level_menu='.$val->id_menu.' AND showhide_menu=1';
				$criteriag->order = 'order_menu ASC';
				$YiiMenug = YiiFilter::model()->findAll($criteriag);
				if(!empty($YiiMenug)){
					foreach ($YiiMenug as $key=>$valg){
		
						$subarrFilter['+ '.$val->name_menu][$valg->id_menu] = '---- '.$valg->name_menu;
		
					}
				}else
					$subarrFilter[$val->id_menu] = '+ '.$val->name_menu;
					
			}else{
				$criteriag = new CDbCriteria();
				$criteriag->condition = 'level_menu='.$val->id_menu.' AND showhide_menu=1';
				$criteriag->order = 'order_menu ASC';
				$YiiMenug = YiiFilter::model()->findAll($criteriag);
				if(!empty($YiiMenug)){
					foreach ($YiiMenug as $key=>$valg){
		
						$subarrFilter['+ '.$val->name_menu][$valg->id_menu] = '---- '.$valg->name_menu;
		
					}
				}
			}
		}
		
		
		$commandS = Yii::app()->db->createCommand();
		$id = $commandS->select('pr_id')
		->from('Pasevent')
		->order('pr_id DESC')
		->limit(1,0)
		->queryAll();
		if(!empty($id)){
			$a = $id[0]['pr_id'];
		}else 
			$a = 0;
		if(isset($_POST['Pasevent']))
		{
	
			$model->attributes=$_POST['Pasevent'];
			$model->pr_url_news = $this->vn2latin($_POST['Pasevent']['pr_name'],false).'-sp'.rand(1,99999);
			if(!empty($_POST['Pasevent']['filterpr']))
			   $model->filterpr = implode(',', $_POST['Pasevent']['filterpr']);
             else 
             	$model->filterpr = null;
			if($_POST['Pasevent']['pr_code'] == 0){
				$model->pr_tinhtrang = 0;
			}else if($_POST['Pasevent']['pr_code'] != 0){
				$model->pr_tinhtrang = 1;
			}
			if($model->save())
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
			
		}
		
		$this->render('create',array(
			'model'=>$model,'YiiMenu' =>$subarr,'id'=>$a,'subarrFilter'=>$subarrFilter
		));
	}

	
	public function actionSave()
	{
	
		if(isset($_GET['Pasevent_id_menu'])){
			$Pasevent_id_menu = $_GET['Pasevent_id_menu'];
			$Pasevent_pr_name = $_GET['Pasevent_pr_name'];
			$Pasevent_pr_gia = $_GET['Pasevent_pr_gia'];
			$Pasevent_pr_code = $_GET['Pasevent_pr_code'];
			$Pasevent_pr_madein = $_GET['Pasevent_pr_madein'];
			$Pasevent_pr_number = $_GET['Pasevent_pr_number'];
			$Pasevent_pr_Manufacturer = $_GET['Pasevent_pr_Manufacturer'];
			$cke_Pasevent_pr_summary = $_GET['cke_Pasevent_pr_summary'];
			$cke_Pasevent_pr_mota = $_GET['cke_Pasevent_pr_mota'];
			$Pasevent_pr_title_seo = $_GET['Pasevent_pr_title_seo'];
			$Pasevent_pr_keywords_seo = $_GET['Pasevent_pr_keywords_seo'];
			$Pasevent_pr_description_seo = $_GET['Pasevent_pr_description_seo'];
			$Pasevent_pr_highlights = $_GET['Pasevent_pr_highlights'];
			$Pasevent_pr_showhide = $_GET['Pasevent_pr_showhide'];
				
			$command = Yii::app()->db->createCommand();
			$command->insert('Pasevent', array(
					'id_menu'=>$Pasevent_id_menu,
					'pr_name'=>$Pasevent_pr_name,
						
					'pr_gia'=>$Pasevent_pr_gia,
					'pr_code'=>$Pasevent_pr_gia,
					'pr_madein'=>$Pasevent_pr_madein,
					'pr_number'=>$Pasevent_pr_number,
					'pr_Manufacturer'=>$Pasevent_pr_Manufacturer,
					'pr_summary'=>$cke_Pasevent_pr_summary,
					'pr_title_seo'=>$Pasevent_pr_title_seo,
					'pr_keywords_seo'=>$Pasevent_pr_keywords_seo,
					'pr_description_seo'=>$Pasevent_pr_description_seo,
					'pr_highlights'=>$Pasevent_pr_highlights,
					'pr_showhide'=>$Pasevent_pr_showhide,
					'pr_mota'=>$cke_Pasevent_pr_mota,
					'pr_url_news'=>$this->vn2latin($Pasevent_pr_name,false).'-sp'.rand(1,99999),
			));
			$commandS = Yii::app()->db->createCommand();
			$id = $commandS->select('pr_id')
			->from('Pasevent')
			->order('pr_id DESC')
			->limit(1,0)
			->queryAll();
	
			Yii::app()->session['prid'] = $id[0]['pr_id'];
            echo '1';
				
		}

	}
	
	public function actionEdit()
	{
	
		if(isset($_GET['id'])){
			$pr_id = $_GET['id'];
			$Pasevent_id_menu = $_GET['Pasevent_id_menu'];
			$Pasevent_pr_name = $_GET['Pasevent_pr_name'];
			$Pasevent_pr_gia = $_GET['Pasevent_pr_gia'];
			$Pasevent_pr_code = $_GET['Pasevent_pr_code'];
			$Pasevent_pr_madein = $_GET['Pasevent_pr_madein'];
			$Pasevent_pr_number = $_GET['Pasevent_pr_number'];
			$Pasevent_pr_Manufacturer = $_GET['Pasevent_pr_Manufacturer'];
			$cke_Pasevent_pr_summary = $_GET['cke_Pasevent_pr_summary'];
			$cke_Pasevent_pr_mota = $_GET['cke_Pasevent_pr_mota'];
			$Pasevent_pr_title_seo = $_GET['Pasevent_pr_title_seo'];
			$Pasevent_pr_keywords_seo = $_GET['Pasevent_pr_keywords_seo'];
			$Pasevent_pr_description_seo = $_GET['Pasevent_pr_description_seo'];
			$Pasevent_pr_highlights = $_GET['Pasevent_pr_highlights'];
			$Pasevent_pr_showhide = $_GET['Pasevent_pr_showhide'];
	
			$command = Yii::app()->db->createCommand();
			$command->update('Pasevent', array(
					'id_menu'=>$Pasevent_id_menu,
					'pr_name'=>$Pasevent_pr_name,
	
					'pr_gia'=>$Pasevent_pr_gia,
					'pr_code'=>$Pasevent_pr_gia,
					'pr_madein'=>$Pasevent_pr_madein,
					'pr_number'=>$Pasevent_pr_number,
					'pr_Manufacturer'=>$Pasevent_pr_Manufacturer,
					'pr_summary'=>$cke_Pasevent_pr_summary,
					'pr_title_seo'=>$Pasevent_pr_title_seo,
					'pr_keywords_seo'=>$Pasevent_pr_keywords_seo,
					'pr_description_seo'=>$Pasevent_pr_description_seo,
					'pr_highlights'=>$Pasevent_pr_highlights,
					'pr_showhide'=>$Pasevent_pr_showhide,
					'pr_mota'=>$cke_Pasevent_pr_mota,
					'pr_url_news'=>$this->vn2latin($Pasevent_pr_name,false).'-sp'.rand(1,99999),
			), 'pr_id=:id', array(':id'=>$pr_id));
	
			Yii::app()->session['prid'] = $pr_id;
			echo '1';
	
		}
	
	}
	
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$this->layout = 'main';
		$model=$this->loadModel($id);
		$model->filterpr = explode(',', $model->filterpr);

		$criteria = new CDbCriteria();
		$criteria->condition = 'showhide_menu=1';
		$criteria->order = 'order_menu ASC';
		$YiiMenu = YiiMenu::model()->findAll($criteria);
	$subarr = array();
	foreach ($YiiMenu as $val){

			if($val->level_menu == 0){

				$criteriag = new CDbCriteria();
				$criteriag->condition = 'level_menu='.$val->id_menu.' AND showhide_menu=1';
				$criteriag->order = 'order_menu ASC';
				$YiiMenug = YiiMenu::model()->findAll($criteriag);
				$subarr[$val->id_menu] = '+ '.$val->name_menu;
				if(!empty($YiiMenug)){
				foreach ($YiiMenug as $key=>$valg){

					$subarr[$valg->id_menu] = '---- '.$valg->name_menu;
					
					$criteriagb = new CDbCriteria();
					$criteriagb->condition = 'level_menu='.$valg->id_menu.' AND showhide_menu=1';
					$criteriagb->order = 'order_menu ASC';
					$YiiMenugb = YiiMenu::model()->findAll($criteriagb);
					if(!empty($YiiMenugb)){
						foreach ($YiiMenugb as $keyb=>$valgb){
					
							$subarr[$valgb->id_menu] = '-------- '.$valgb->name_menu;
						}
					}
				}
			  }
			  			  
			}
		}
		
		$criteriaf = new CDbCriteria();
		$criteriaf->condition = 'showhide_menu=1';
		$criteriaf->order = 'order_menu ASC';
		$YiiFilter = YiiFilter::model()->findAll($criteriaf);
		$subarrFilter = array();
		foreach ($YiiFilter as $val){
		
			if($val->level_menu == 0){
		
				$criteriag = new CDbCriteria();
				$criteriag->condition = 'level_menu='.$val->id_menu.' AND showhide_menu=1';
				$criteriag->order = 'order_menu ASC';
				$YiiMenug = YiiFilter::model()->findAll($criteriag);
				if(!empty($YiiMenug)){
					foreach ($YiiMenug as $key=>$valg){
		
						$subarrFilter['+ '.$val->name_menu][$valg->id_menu] = '---- '.$valg->name_menu;
		
					}
				}else
					$subarrFilter[$val->id_menu] = '+ '.$val->name_menu;
					
			}else{
				$criteriag = new CDbCriteria();
				$criteriag->condition = 'level_menu='.$val->id_menu.' AND showhide_menu=1';
				$criteriag->order = 'order_menu ASC';
				$YiiMenug = YiiFilter::model()->findAll($criteriag);
				if(!empty($YiiMenug)){
					foreach ($YiiMenug as $key=>$valg){
		
						$subarrFilter['+ '.$val->name_menu][$valg->id_menu] = '---- '.$valg->name_menu;
		
					}
				}
			}
		}
		
		if(isset($_POST['Pasevent']))
		{
			$model->attributes=$_POST['Pasevent'];
			$model->pr_url_news = $this->vn2latin($_POST['Pasevent']['pr_name'],false).'-sp'.rand(1,99999);
			if(!empty($_POST['Pasevent']['filterpr']))
			   $model->filterpr = implode(',', $_POST['Pasevent']['filterpr']);
             else 
             	$model->filterpr = null;
			
			if($_POST['Pasevent']['pr_code'] == 0){
				$model->pr_tinhtrang = 0;
			}else if($_POST['Pasevent']['pr_code'] != 0){
				$model->pr_tinhtrang = 1;
			}
			if($model->save())
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		
		$commandS = Yii::app()->db->createCommand();
		$imgproduct = $commandS->select('*')
		->from('yii_imgproduct')
        ->where('pr_id=:id', array(':id'=>$model->pr_joinImg))
		->queryAll();
		
		$this->render('update',array(
			'model'=>$model,'YiiMenu' =>$subarr,'imgproduct'=>$imgproduct,'join' => $model->pr_joinImg,'subarrFilter'=>$subarrFilter
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
			
		$yii_imgproduct = Yii::app()->db->createCommand()
		->select('url_yii_imgproduct')
		->from('yii_imgproduct')
		->where('pr_id=:id', array(':id'=>$id))
		->queryAll();

		if(!empty($yii_imgproduct)){
			foreach ($yii_imgproduct as $val){
				if(!empty($val['url_yii_imgproduct'])){
					if (file_exists(getcwd().'/'.$val['url_yii_imgproduct'])) {
						unlink(getcwd().'/'.$val['url_yii_imgproduct']);
					}
				}
			};
		}
		$command = Yii::app()->db->createCommand();
		$command->delete('yii_imgproduct', 'pr_id=:id', array(':id'=>$id));
			
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Pasevent');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$this->layout = 'main';
		$model=new Pasevent('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Pasevent']))
			$model->attributes=$_GET['Pasevent'];

		$criteria = new CDbCriteria();
		$criteria->condition = 'showhide_menu=1';
		$criteria->order = 'order_menu ASC';
		$YiiMenu = YiiMenu::model()->findAll($criteria);
	$subarr = array();
	foreach ($YiiMenu as $val){

			if($val->level_menu == 0){

				$criteriag = new CDbCriteria();
				$criteriag->condition = 'level_menu='.$val->id_menu.' AND showhide_menu=1';
				$criteriag->order = 'order_menu ASC';
				$YiiMenug = YiiMenu::model()->findAll($criteriag);
				$subarr[$val->id_menu] = '+ '.$val->name_menu;
				if(!empty($YiiMenug)){
				foreach ($YiiMenug as $key=>$valg){

					$subarr[$valg->id_menu] = '---- '.$valg->name_menu;
					
					$criteriagb = new CDbCriteria();
					$criteriagb->condition = 'level_menu='.$valg->id_menu.' AND showhide_menu=1';
					$criteriagb->order = 'order_menu ASC';
					$YiiMenugb = YiiMenu::model()->findAll($criteriagb);
					if(!empty($YiiMenugb)){
						foreach ($YiiMenugb as $keyb=>$valgb){
					
							$subarr[$valgb->id_menu] = '-------- '.$valgb->name_menu;
						}
					}
				}
			  }
			  			  
			}
		}
				
		$criteriahl = new CDbCriteria();
		$criteriahl->condition = 'highlights_menu=1 AND showhide_menu=1 AND location_menu=4';
		$criteriahl->order = 'order_menu ASC';
		$YiiMenuhl = YiiMenu::model()->findAll($criteriahl);
		$arrf = array();
		foreach ($YiiMenuhl as $val){
			$arrf[$val->id_menu] = $val->name_menu;
		}
		
		$this->render('admin',array(
			'model'=>$model,
			'YiiMenu' =>$subarr,'YiiMenuhl'=>$arrf
		));
	}

	public function actionAjaxUpdate()
	{
		$act = $_GET['act'];
	
		if(isset($_POST['autoId'])){
			$autoIdAll = $_POST['autoId'];
			if (count($autoIdAll) > 0) {
				foreach ($autoIdAll as $autoId) {
					$model = $this->loadModel($autoId);
					if ($act == 'doDelete') {
						$yii_imgproduct = Yii::app()->db->createCommand()
						->select('url_yii_imgproduct')
						->from('yii_imgproduct')
						->where('pr_id=:id', array(':id'=>$autoId))
						->queryAll();
						
						if(!empty($yii_imgproduct)){
							foreach ($yii_imgproduct as $val){
								if(!empty($val['url_yii_imgproduct'])){
									if (file_exists(getcwd().'/'.$val['url_yii_imgproduct'])) {
										unlink(getcwd().'/'.$val['url_yii_imgproduct']);
									}
								}
							};
						}
						$command = Yii::app()->db->createCommand();
						$command->delete('yii_imgproduct', 'pr_id=:id', array(':id'=>$autoId));
						
						$model->delete();
					} else {
						if ($act == 'doActive')
							$model->pr_showhide = 1;
	
						if ($act == 'doInactive')
							$model->pr_showhide = 0;
	
						if ($act == 'highlights')
							$model->pr_highlights = 1;
	
						if ($act == 'nohighlights')
							$model->pr_highlights = 0;
	
						if ($model->save())
							echo 'ok';
						else
								
							throw new Exception("Sorry", 500);
					}
				}
			}
		}
	}
	
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Pasevent the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Pasevent::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Pasevent $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='Pasevent-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
