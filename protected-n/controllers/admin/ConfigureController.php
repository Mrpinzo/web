<?php

class ConfigureController extends AdminController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/main';

	/**
	 * @return array action filters
	 */
	/* public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	} */

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	
public function accessRules()
	{
		$items2a = YiiRole::model()->findByPk(Yii::app()->user->getState('role'));
		
		if(!empty($items2a['type_role'])){
			
		   $arrs = CJSON::decode($items2a['type_role'],true);
		   
		$a = '';
		foreach ($arrs as $val){
			
			if(in_array('Configure', $val)){
				$a .= implode('-',$val).'-';
			}
			
		};
			if(!empty($a)){
				$arrdsd = explode('-', $a);
				$arr1 = array_unique($arrdsd);
			     array_shift($arr1);
			     array_pop($arr1);	
			}else{
				$arr1 = array('');
			}	
		}else {
          $arr1 = array('');    
        }
        //var_dump($arr1);die;
		return array(
				
				array('allow', // allow authenticated user to perform 'create' and 'update' actions
						'actions'=>$arr1,
						'users'=>array('*'),
				),
		
				array('deny',  // deny all users
						'users'=>array('*'),
				),
		);
		
				
	}


	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$count = Configure::model()->count();
		$this->render('view',array(
			'model'=>$this->loadModel($id),'count'=>$count
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Configure;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Configure']))
		{
			$model->attributes=$_POST['Configure'];
			if($model->save())
				$this->redirect('/admin.php/configure/view?id=1');
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Configure']))
		{
			$model->attributes=$_POST['Configure'];
			if($model->save())
				$this->redirect('/admin.php/configure/view?id=1');
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Configure');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$count = Configure::model()->count();
	
		$model=new Configure('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Configure']))
			$model->attributes=$_GET['Configure'];

		$this->render('admin',array(
			'model'=>$model,'count'=>$count
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Configure the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Configure::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Configure $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='configure-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
