<?php

class SiteController extends AdminController
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		$this->layout = 'main';
		
		if(isset($_GET['YiiStatistical']))
		{
			$this->render('index',array('datestart'=>$_GET['YiiStatistical']['datestart'],'dateend'=>$_GET['YiiStatistical']['dateend']));
		}else {
			$this->render('index',array('datestart'=>date('Y-m-d', strtotime('-1 week')),'dateend'=>date("Y-m-d")));
		}
		
		
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
	    if($error=Yii::app()->errorHandler->error)
	    {
	    	if(Yii::app()->request->isAjaxRequest)
	    		echo $error['message'];
	    	else
	        	$this->render('error', $error);
	    }
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		
		$this->layout = 'login';
		$model=new LoginForm;
		
		// if it is ajax validation request
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
		//var_dump(Yii::app()->user->isGuest);die;
	if(Yii::app()->user->isGuest == true){
		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login()){
				$this->redirect('/admin.php/site/index');
			}
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	 }else $this->redirect(Yii::app()->user->returnUrl);
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		
		Yii::app()->user->logout();
			$this->redirect(Yii::app()->homeUrl);
	}
}