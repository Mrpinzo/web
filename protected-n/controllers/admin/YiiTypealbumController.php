<?php

class YiiTypealbumController extends AdminController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */

	/**
	 * @return array action filters
	 */
	/* public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	} */

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */

	public function accessRules()
	{
		$items2a = YiiRole::model()->findByPk(Yii::app()->user->getState('role'));
	
		if(!empty($items2a['type_role'])){
				
			$arrs = CJSON::decode($items2a['type_role'],true);
			 
			$a = '';
			foreach ($arrs as $val){
					
				if(in_array('YiiTypealbum', $val)){
					$a .= implode('-',$val).'-';
				}
					
			};
			if(!empty($a)){
				$arrdsd = explode('-', $a);
				$arr1 = array_unique($arrdsd);
				array_shift($arr1);
				array_pop($arr1);
			}else{
				$arr1 = array('');
			}
		}else {
			$arr1 = array('');
		}
		//var_dump($arr1);die;
		return array(
	
				array('allow', // allow authenticated user to perform 'create' and 'update' actions
						'actions'=>$arr1,
						'users'=>array('*'),
				),
	
				array('deny',  // deny all users
						'users'=>array('*'),
				),
		);
	
	
	}
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new YiiTypealbum;

	   $criteria = new CDbCriteria();
		$criteria->condition = 'showhide_menu=1';
		$criteria->order = 'order_menu ASC';
		$YiiMenu = YiiMenu::model()->findAll($criteria);
	$subarr = array();
	foreach ($YiiMenu as $val){

			if($val->level_menu == 0){

				$criteriag = new CDbCriteria();
				$criteriag->condition = 'level_menu='.$val->id_menu.' AND showhide_menu=1';
				$criteriag->order = 'order_menu ASC';
				$YiiMenug = YiiMenu::model()->findAll($criteriag);
				if(!empty($YiiMenug)){
				foreach ($YiiMenug as $key=>$valg){

					$subarr[$val->name_menu][$valg->id_menu] = $valg->name_menu;

				}
			  }else
			  	$subarr[$val->id_menu] = $val->name_menu;
			}
		}

		$commandS = Yii::app()->db->createCommand();
		$id = $commandS->select('id_typealbum')
		->from('yii_typealbum')
		->order('id_typealbum DESC')
		->limit(1,0)
		->queryAll();
		
		if(isset($_POST['YiiTypealbum']))
		{
	
			$model->attributes=$_POST['YiiTypealbum'];
			$model->url_seo_typealbum = $this->vn2latin($_POST['YiiTypealbum']['name_typealbum'],false).'-al'.rand(1,99999);
			if($model->save())
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}

		$this->render('create',array(
			'model'=>$model,'YiiMenu' =>$subarr,'id'=>$id[0]['id_typealbum']
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

	$criteria = new CDbCriteria();
		$criteria->condition = 'showhide_menu=1';
		$criteria->order = 'order_menu ASC';
		$YiiMenu = YiiMenu::model()->findAll($criteria);
		$subarr = array();
		foreach ($YiiMenu as $val){
		
			if($val->level_menu == 0){
		
				$criteriag = new CDbCriteria();
				$criteriag->condition = 'level_menu='.$val->id_menu.' AND showhide_menu=1';
				$criteriag->order = 'order_menu ASC';
				$YiiMenug = YiiMenu::model()->findAll($criteriag);
				if(!empty($YiiMenug)){
					foreach ($YiiMenug as $key=>$valg){
		
						$subarr[$val->name_menu][$valg->id_menu] = $valg->name_menu;
		
					}
				}else
					$subarr[$val->id_menu] = $val->name_menu;
			}
		}

		if(isset($_POST['YiiTypealbum']))
		{
			$model->attributes=$_POST['YiiTypealbum'];
			$model->url_seo_typealbum = $this->vn2latin($_POST['YiiTypealbum']['name_typealbum'],false).'-al'.rand(1,99999);
			if($model->save())
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}

		$criteria1 = new CDbCriteria();
		$criteria1->condition = 'join_typeAlbum='.$model->join_typeAlbum;
		$criteria1->order = 'id_album DESC';
		$ImgAlbums = YiiAlbum::model()->findAll($criteria1);

		$this->render('update',array(
			'model'=>$model,'YiiMenu'=>$subarr,'join' => $model->join_typeAlbum,'ImgAlbums' =>$ImgAlbums
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$criteria1 = new CDbCriteria();
		$criteria1->condition = 'id_typealbum='.$id;
		$YiiTypealbum = YiiTypealbum::model()->findAll($criteria1);
		foreach ($YiiTypealbum as $val){
			$join_typeAlbum = $val->join_typeAlbum;
		};
		
		$yii_album = Yii::app()->db->createCommand()
		->select('img_album')
		->from('yii_album')
		->where('join_typeAlbum=:id', array(':id'=>$join_typeAlbum))
		->queryAll();
		if(!empty($yii_album)){
		foreach ($yii_album as $val){
			if(!empty($val['img_album'])){
				if (file_exists(getcwd().'/'.$val['img_album'])) {
					unlink(getcwd().'/'.$val['img_album']);
				}
			}
		};
		}
		$command = Yii::app()->db->createCommand();
		$command->delete('yii_album', 'join_typeAlbum=:id', array(':id'=>$join_typeAlbum));
		$this->loadModel($id)->delete();
		
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('YiiTypealbum');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new YiiTypealbum('search');
		$model->unsetAttributes();  // clear any default values
		
		$criteria = new CDbCriteria();
		$criteria->condition = 'showhide_menu=1';
		$criteria->order = 'order_menu ASC';
		$YiiMenu = YiiMenu::model()->findAll($criteria);
		$subarr = array();
		foreach ($YiiMenu as $val){
		
			if($val->level_menu == 0){
		
				$criteriag = new CDbCriteria();
				$criteriag->condition = 'level_menu='.$val->id_menu.' AND showhide_menu=1';
				$criteriag->order = 'order_menu ASC';
				$YiiMenug = YiiMenu::model()->findAll($criteriag);
				if(!empty($YiiMenug)){
					foreach ($YiiMenug as $key=>$valg){
		
						$subarr[$val->name_menu][$valg->id_menu] = $valg->name_menu;
		
					}
				}else
					$subarr[$val->id_menu] = $val->name_menu;
			}
		}
		
		if(isset($_GET['YiiTypealbum']))
			$model->attributes=$_GET['YiiTypealbum'];

		$this->render('admin',array(
			'model'=>$model,'YiiMenu'=>$subarr
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return YiiTypealbum the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=YiiTypealbum::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	
	/**
	 * Performs the AJAX validation.
	 * @param YiiTypealbum $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='yii-typealbum-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
