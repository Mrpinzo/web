<?php

class YiiMenuController extends AdminController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/main';

	/**
	 * @return array action filters
	 */
	/* public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	} */

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */

	public function accessRules()
	{
		$items2a = YiiRole::model()->findByPk(Yii::app()->user->getState('role'));
	
		if(!empty($items2a['type_role'])){
				
			$arrs = CJSON::decode($items2a['type_role'],true);
			 
			$a = '';
			foreach ($arrs as $val){
					
				if(in_array('YiiMenu', $val)){
					$a .= implode('-',$val).'-';
				}
					
			};
			if(!empty($a)){
				$arrdsd = explode('-', $a);
				$arr1 = array_unique($arrdsd);
				array_shift($arr1);
				array_pop($arr1);
			}else{
				$arr1 = array('');
			}
		}else {
			$arr1 = array('');
		}
		//var_dump($arr1);die;
		return array(
	
				array('allow', // allow authenticated user to perform 'create' and 'update' actions
						'actions'=>$arr1,
						'users'=>array('*'),
				),
	
				array('deny',  // deny all users
						'users'=>array('*'),
				),
		);
	
	
	}
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new YiiMenu;

	    $criteria = new CDbCriteria();
		$criteria->condition = 'showhide_menu=1';
		$criteria->order = 'order_menu ASC';
		$YiiMenue = YiiMenu::model()->findAll($criteria);
	$subarr = array();
	foreach ($YiiMenue as $val){

			if($val->level_menu == 0){

				$criteriag = new CDbCriteria();
				$criteriag->condition = 'level_menu='.$val->id_menu.' AND showhide_menu=1';
				$criteriag->order = 'order_menu ASC';
				$YiiMenug = YiiMenu::model()->findAll($criteriag);
				$subarr[$val->id_menu] = '+ '.$val->name_menu;
				if(!empty($YiiMenug)){
				foreach ($YiiMenug as $key=>$valg){

					$subarr[$valg->id_menu] = '---- '.$valg->name_menu;
					
					$criteriagb = new CDbCriteria();
					$criteriagb->condition = 'level_menu='.$valg->id_menu.' AND showhide_menu=1';
					$criteriagb->order = 'order_menu ASC';
					$YiiMenugb = YiiMenu::model()->findAll($criteriagb);
					if(!empty($YiiMenugb)){
						foreach ($YiiMenugb as $keyb=>$valgb){
					
							$subarr[$valgb->id_menu] = '-------- '.$valgb->name_menu;
						}
					}
				}
			  }
			  			  
			}
		}

		if(isset($_POST['YiiMenu']))
		{
			$model->attributes=$_POST['YiiMenu'];
			$model->rewrite_url_menu = $this->vn2latin($_POST['YiiMenu']['name_menu'],false).'-sp'.rand(1,99999);

			if($model->save())
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
			}

		$this->render('create',array(
			'model'=>$model,'YiiMenue' =>$subarr
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		$criteria = new CDbCriteria();
		$criteria->condition = 'showhide_menu=1';
		$criteria->order = 'order_menu ASC';
		$YiiMenue = YiiMenu::model()->findAll($criteria);
	$subarr = array();
	foreach ($YiiMenue as $val){

			if($val->level_menu == 0){

				$criteriag = new CDbCriteria();
				$criteriag->condition = 'level_menu='.$val->id_menu.' AND showhide_menu=1';
				$criteriag->order = 'order_menu ASC';
				$YiiMenug = YiiMenu::model()->findAll($criteriag);
				$subarr[$val->id_menu] = '+ '.$val->name_menu;
				if(!empty($YiiMenug)){
				foreach ($YiiMenug as $key=>$valg){

					$subarr[$valg->id_menu] = '---- '.$valg->name_menu;
					
					$criteriagb = new CDbCriteria();
					$criteriagb->condition = 'level_menu='.$valg->id_menu.' AND showhide_menu=1';
					$criteriagb->order = 'order_menu ASC';
					$YiiMenugb = YiiMenu::model()->findAll($criteriagb);
					if(!empty($YiiMenugb)){
						foreach ($YiiMenugb as $keyb=>$valgb){
					
							$subarr[$valgb->id_menu] = '-------- '.$valgb->name_menu;
						}
					}
				}
			  }
			  			  
			}
		}

		if(isset($_POST['YiiMenu']))
		{
			$model->attributes=$_POST['YiiMenu'];
			$model->rewrite_url_menu = $this->vn2latin($_POST['YiiMenu']['name_menu'],false).'-sp'.rand(1,99999);
			if($model->save())
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
			}

		$this->render('update',array(
			'model'=>$model,'YiiMenue' =>$subarr
		));
	}

	public function actionUpdateAjax()
	{
		$idmenu = $_POST['idmenu'];
		$sortmenu = $_POST['sortmenu'];

		$model=$this->loadModel($idmenu);
	
		    $criteria2            = new CDbCriteria();
            $criteria2->condition = 'order_menu=' . $sortmenu;
            $count = YiiMenu::model()->count($criteria2);
	
		if(isset($idmenu) && isset($sortmenu))
		{
			if($count == 0){
			$model->order_menu=$sortmenu;
			if($model->save())
				echo '1';
			}else 
				echo '0';
		}

	}
	
	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		$new = Yii::app()->db->createCommand()
		->select('new_img')
		->from('news u')
		->where('id_menu=:id', array(':id'=>$id))
		->queryAll();
		
		foreach ($new as $val){
			if(!empty($val['new_img'])){
				if (file_exists(getcwd().'/'.$val['new_img'])) {
					unlink(getcwd().'/'.$val['new_img']);
				}
			}
		};
		
		$product = Yii::app()->db->createCommand()
		->select('pr_img')
		->from('product')
		->where('id_menu=:id', array(':id'=>$id))
		->queryAll();
		
		foreach ($product as $val){
			if(!empty($val['pr_img'])){
				if (file_exists(getcwd().'/'.$val['pr_img'])) {
					unlink(getcwd().'/'.$val['pr_img']);
				}
			}
		};
		
		$command = Yii::app()->db->createCommand();
		$criteria1 = new CDbCriteria();
		$criteria1->condition = 'id_menu='.$id;
		$YiiTypealbum = YiiTypealbum::model()->findAll($criteria1);
		$join_typeAlbum = array();
		foreach ($YiiTypealbum as $val){
			$join_typeAlbum[] = $val->join_typeAlbum;
		};
		foreach ($join_typeAlbum as $value){
		$yii_album = Yii::app()->db->createCommand()
		->select('img_album')
		->from('yii_album')
		->where('join_typeAlbum=:id', array(':id'=>$value))
		->queryAll();
		if(!empty($yii_album)){
			foreach ($yii_album as $val){
				if(!empty($val['img_album'])){
					if (file_exists(getcwd().'/'.$val['img_album'])) {
						unlink(getcwd().'/'.$val['img_album']);
					}
				}
			};
		 }
		 $command->delete('yii_album', 'join_typeAlbum=:id', array(':id'=>$value));
		}
		
        $command->delete('news', 'id_menu=:id', array(':id'=>$id));
        $command->delete('product', 'id_menu=:id', array(':id'=>$id));
        $command->delete('yii_typealbum', 'id_menu=:id', array(':id'=>$id));
        
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('YiiMenu');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new YiiMenu('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['YiiMenu']))
			$model->attributes=$_GET['YiiMenu'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	public function actionAjaxUpdate()
	{
		$act = $_GET['act'];

		if(isset($_POST['autoId'])){
			$autoIdAll = $_POST['autoId'];
			if (count($autoIdAll) > 0) {
				foreach ($autoIdAll as $autoId) {
					$model = $this->loadModel($autoId);
					if ($act == 'doDelete') {
						
						$new = Yii::app()->db->createCommand()
						->select('new_img')
						->from('news u')
						->where('id_menu=:id', array(':id'=>$autoId))
						->queryAll();
						
						foreach ($new as $val){
							if(!empty($val['new_img'])){
								if (file_exists(getcwd().'/'.$val['new_img'])) {
									unlink(getcwd().'/'.$val['new_img']);
								}
							}
						};
						
						$product = Yii::app()->db->createCommand()
						->select('pr_img')
						->from('product')
						->where('id_menu=:id', array(':id'=>$autoId))
						->queryAll();
						
						foreach ($product as $val){
							if(!empty($val['pr_img'])){
								if (file_exists(getcwd().'/'.$val['pr_img'])) {
									unlink(getcwd().'/'.$val['pr_img']);
								}
							}
						};
						
						$command = Yii::app()->db->createCommand();
						$criteria1 = new CDbCriteria();
						$criteria1->condition = 'id_menu='.$autoId;
						$YiiTypealbum = YiiTypealbum::model()->findAll($criteria1);
						$join_typeAlbum = array();
						foreach ($YiiTypealbum as $val){
							$join_typeAlbum[] = $val->join_typeAlbum;
						};
						foreach ($join_typeAlbum as $value){
							$yii_album = Yii::app()->db->createCommand()
							->select('img_album')
							->from('yii_album')
							->where('join_typeAlbum=:id', array(':id'=>$value))
							->queryAll();
							if(!empty($yii_album)){
								foreach ($yii_album as $val){
									if(!empty($val['img_album'])){
										if (file_exists(getcwd().'/'.$val['img_album'])) {
											unlink(getcwd().'/'.$val['img_album']);
										}
									}
								};
							}
							$command->delete('yii_album', 'join_typeAlbum=:id', array(':id'=>$value));
						}
						
						$command->delete('news', 'id_menu=:id', array(':id'=>$autoId));
						$command->delete('product', 'id_menu=:id', array(':id'=>$autoId));
						$command->delete('yii_typealbum', 'id_menu=:id', array(':id'=>$autoId));
						$model->delete();
					} else {
						if ($act == 'doActive')
							$model->showhide_menu = 1;
	
						if ($act == 'doInactive')
							$model->showhide_menu = 0;
	
						/* if ($act == 'doUpdateorder')
							$model->order_menu = $data; */
						
						if ($model->save())
							echo 'ok';
						else
							throw new Exception("Sorry", 500);
					}
				}
			}
		}
	}
	
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return YiiMenu the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=YiiMenu::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	
	/**
	 * Performs the AJAX validation.
	 * @param YiiMenu $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='menu-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	/* định dạng chuỗi nhập
	 tham số $tolower, true nếu bạn muốn nhận chuỗi không dấu dưới dạng viết thường,
	 false sẽ trả về chữ không dấu còn viết hoa hay viết thường thì duy trì như chuỗi ban đầu đưa vào. */
	function vn2latin($str, $tolower = false)
	{
		$str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $str);
		$str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $str);
		$str = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $str);
		$str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $str);
		$str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $str);
		$str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $str);
		$str = preg_replace("/(đ)/", 'd', $str);
		$str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $str);
		$str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $str);
		$str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $str);
		$str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $str);
		$str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $str);
		$str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $str);
		$str = preg_replace("/(Đ)/", 'D', $str);
		$str = preg_replace(array('/[^a-zA-Z0-9 -]/', '/[ -]+/', '/^-|-$/'),
				array('', '-', ''), $str);
		if ($tolower) {
			return strtolower($str);
		}
	
		return $str;
	
	}
}
