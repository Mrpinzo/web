-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 17, 2015 at 06:51 AM
-- Server version: 5.5.24-log
-- PHP Version: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `tienganhpd`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `ad_id` int(40) NOT NULL AUTO_INCREMENT,
  `username` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `ad_fullname` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `ad_phone` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `ad_email` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `creat_date` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `update_date` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `id_role` int(200) NOT NULL,
  PRIMARY KEY (`ad_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=100 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`ad_id`, `username`, `password`, `ad_fullname`, `ad_phone`, `ad_email`, `creat_date`, `update_date`, `id_role`) VALUES
(98, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin', '0999999999', 'nguyennhan091@gmail.com', '12:30:11 22-07-2015', '21:34:40 09-09-2015', 7);

-- --------------------------------------------------------

--
-- Table structure for table `advertise`
--

CREATE TABLE IF NOT EXISTS `advertise` (
  `adv_id` int(40) NOT NULL AUTO_INCREMENT,
  `adv_link` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `adv_name` text COLLATE utf8_unicode_ci NOT NULL,
  `adv_img` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `adv_action` int(20) NOT NULL DEFAULT '1',
  `adv_positions` int(20) NOT NULL,
  `adv_order` int(200) NOT NULL,
  PRIMARY KEY (`adv_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=40 ;

--
-- Dumping data for table `advertise`
--

INSERT INTO `advertise` (`adv_id`, `adv_link`, `adv_name`, `adv_img`, `adv_action`, `adv_positions`, `adv_order`) VALUES
(35, 'http://tienganhtreemyii.com/', '“The company, with exceptional dedicated teams working on Global Business Intelligence, is providing minute & strategic information, latest technical and cultural skills training to client’s workforce in Africa and Asia Pacific.”', '/upload/quangcao/2015-09-15/8459slide.png', 1, 2, 6),
(39, 'http://tienganhpd.com/', '“The company, with exceptional dedicated teams working on Global Business Intelligence, is providing minute & strategic information, latest technical and cultural skills training to client’s workforce in Africa and Asia Pacific.”', '/upload/quangcao/2015-09-12/22299slide.png', 1, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE IF NOT EXISTS `cart` (
  `car_id` int(40) NOT NULL AUTO_INCREMENT,
  `car_summoney` int(200) NOT NULL,
  `ord_id` int(50) NOT NULL,
  `cart_number` int(60) NOT NULL,
  `pr_id` int(200) NOT NULL,
  PRIMARY KEY (`car_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=110 ;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`car_id`, `car_summoney`, `ord_id`, `cart_number`, `pr_id`) VALUES
(88, 280000, 18, 1, 148),
(100, 34234234, 28, 1, 179),
(99, 90000, 28, 3, 170),
(85, 280000, 16, 1, 148),
(84, 300000, 15, 1, 149),
(83, 280000, 15, 1, 148),
(82, 350000, 14, 1, 147),
(81, 280000, 14, 1, 148),
(80, 280000, 13, 1, 148),
(79, 300000, 12, 1, 149),
(78, 3500000, 11, 10, 147),
(77, 560000, 11, 2, 148),
(76, 300000, 10, 1, 149),
(75, 560000, 10, 2, 148),
(98, 4444, 28, 2, 171),
(97, 342342340, 27, 10, 172),
(96, 4444, 27, 2, 171),
(89, 280000, 19, 1, 148),
(90, 700000, 19, 2, 147),
(91, 300000, 20, 1, 158),
(92, 280000, 21, 1, 148),
(93, 350000, 21, 1, 147),
(94, 280000, 26, 1, 148),
(95, 350000, 26, 1, 147),
(101, 34234234, 28, 1, 172),
(102, 300000, 29, 10, 170),
(103, 4444, 30, 2, 169),
(104, 22220, 31, 10, 168),
(105, 200000, 32, 1, 174),
(106, 342342340, 33, 10, 167),
(107, 30000, 33, 1, 175),
(108, 45435, 34, 1, 258),
(109, 0, 34, 1, 238);

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE IF NOT EXISTS `comment` (
  `adv_id` int(40) NOT NULL AUTO_INCREMENT,
  `adv_link` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `adv_name` text COLLATE utf8_unicode_ci NOT NULL,
  `adv_img` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `adv_action` int(20) NOT NULL DEFAULT '1',
  `adv_positions` int(20) NOT NULL,
  `adv_order` int(200) NOT NULL,
  PRIMARY KEY (`adv_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=47 ;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`adv_id`, `adv_link`, `adv_name`, `adv_img`, `adv_action`, `adv_positions`, `adv_order`) VALUES
(41, 'Head of Group Anti-Bribery and Compliance Vodafone', '“ There''s a lot of very senior people here, General counsels, Deputy General Counsels, Chief compliance officers etc. it''s a really good level of interaction.”', '', 1, 0, 1),
(42, 'Head of Group Anti-Bribery and Compliance Vodafone', '“ There''s a lot of very senior people here, General counsels, Deputy General Counsels, Chief compliance officers etc. it''s a really good level of interaction.”', '', 1, 0, 2),
(43, 'Head of Group Anti-Bribery and Compliance Vodafone', '“ There''s a lot of very senior people here, General counsels, Deputy General Counsels, Chief compliance officers etc. it''s a really good level of interaction.”', '', 1, 0, 3),
(44, 'Head of Group Anti-Bribery and Compliance Vodafone', '“ There''s a lot of very senior people here, General counsels, Deputy General Counsels, Chief compliance officers etc. it''s a really good level of interaction.”', '', 1, 0, 4),
(45, 'Head of Group Anti-Bribery and Compliance Vodafone', '“ There''s a lot of very senior people here, General counsels, Deputy General Counsels, Chief compliance officers etc. it''s a really good level of interaction.”', '', 1, 0, 5),
(46, 'Head of Group Anti-Bribery and Compliance Vodafone', '“ There''s a lot of very senior people here, General counsels, Deputy General Counsels, Chief compliance officers etc. it''s a really good level of interaction.”', '', 1, 0, 6);

-- --------------------------------------------------------

--
-- Table structure for table `configure`
--

CREATE TABLE IF NOT EXISTS `configure` (
  `id_configure` int(50) NOT NULL AUTO_INCREMENT,
  `logo_configure` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `imgdl_configure` varchar(400) COLLATE utf8_unicode_ci NOT NULL,
  `about_configure` text COLLATE utf8_unicode_ci NOT NULL,
  `slogan_configure` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `slide_configure` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `yahoo_configure` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `skype_configure` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `title_seo` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `diachi1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `diachi2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telephone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `manhung` text COLLATE utf8_unicode_ci NOT NULL,
  `description_seo` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `keywords_seo` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `url_home` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `latitude_configure` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `longitude_configure` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `title_contact` text COLLATE utf8_unicode_ci NOT NULL,
  `phone_configure` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `page_facebook` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `ServiceCenter` text COLLATE utf8_unicode_ci NOT NULL,
  `Contactstore` text COLLATE utf8_unicode_ci NOT NULL,
  `facebook_configure` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `mail_configure` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `paymentgateways` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `service` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `slogan` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `contact_top` text COLLATE utf8_unicode_ci NOT NULL,
  `page_google` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `page_youtube` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `twitter` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `in_lk` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `youtube` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code_google_analytics` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `Verification_google` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `ga_email` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `ga_password` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `ga_profile_id` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_configure`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `configure`
--

INSERT INTO `configure` (`id_configure`, `logo_configure`, `imgdl_configure`, `about_configure`, `slogan_configure`, `slide_configure`, `yahoo_configure`, `skype_configure`, `title_seo`, `diachi1`, `diachi2`, `telephone`, `manhung`, `description_seo`, `keywords_seo`, `url_home`, `latitude_configure`, `longitude_configure`, `title_contact`, `phone_configure`, `page_facebook`, `ServiceCenter`, `Contactstore`, `facebook_configure`, `mail_configure`, `paymentgateways`, `service`, `slogan`, `contact_top`, `page_google`, `page_youtube`, `twitter`, `in_lk`, `youtube`, `code_google_analytics`, `Verification_google`, `ga_email`, `ga_password`, `ga_profile_id`) VALUES
(1, '/upload/cauhinh/1968logo.png', '/upload/cauhinh/5252r.jpg', '<div class="dc1sgp" style="width: 45%;float: left;"><a style="color: #bf0000;font-size: 28px;font-family: ROBOTO-BOLDCONDENSED;line-height: 60px;">VIETNAM </a></div>\r\n\r\n<div class="dcrsgp" style="width: 55%;float: right; text-align: right;">\r\n<ul>\r\n	<li><a style="color: #3e3e3e;font-size: 15px;font-family: ROBOTO-CONDENSED">151 Chin Swee Road, Manhattan House</a></li>\r\n	<li><a style="color: #3e3e3e;font-size: 15px;font-family: ROBOTO-CONDENSED">#08-01, Singapore 169876</a></li>\r\n	<li><a style="color: #3e3e3e;font-size: 15px;font-family: ROBOTO-CONDENSED">Tel: +65 6297 8545 Fax: +65 6297 8645</a></li>\r\n</ul>\r\n</div>\r\n', 'bảo vệ giấc ngủ gia đình bạn', '/upload/cauhinh/13753slide.jpg', 'wwwcongtuhathanh_0334www', 'kaka', 'tienganhtreem', 'Tầng 2, Tòa nhà A2 250 Minh Khai , Hai Bà Trưng, Hà Nội', 'Tầng 1, Tòa nhà A2 250 Minh Khai , Hai Bà Trưng, Hà Nội', '04 6253 2244', '<div class="dc1sgp" style="width: 45%;float: left;"><a style="color: #bf0000;font-size: 28px;font-family: ROBOTO-BOLDCONDENSED;line-height: 60px;">SINGAPORE </a></div>\r\n\r\n<div class="dcrsgp" style="width: 55%;float: right; text-align: right;">\r\n<ul>\r\n	<li><a style="color: #3e3e3e;font-size: 15px;font-family: ROBOTO-CONDENSED">151 Chin Swee Road, Manhattan House</a></li>\r\n	<li><a style="color: #3e3e3e;font-size: 15px;font-family: ROBOTO-CONDENSED">#08-01, Singapore 169876</a></li>\r\n	<li><a style="color: #3e3e3e;font-size: 15px;font-family: ROBOTO-CONDENSED">Tel: +65 6297 8545 Fax: +65 6297 8645</a></li>\r\n</ul>\r\n</div>\r\n', 'tienganhtreem', 'tienganhtreem', 'http://tienganhpd.com/', '20.995535', '105.826686', '<p><iframe allowfullscreen="" frameborder="0" height="285" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d29793.996794485138!2d105.8369637!3d21.02269665!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab9bd9861ca1%3A0xe7887f7b72ca17a9!2zSMOgIE5vaSwgSG_DoG4gS2nhur9tLCBIw6AgTuG7mWksIFZp4buHdCBOYW0!5e0!3m2!1svi!2s!4v1442204843956" style="border:0" width="100%"></iframe></p>\r\n', '0964 836 788,098888888', 'https://www.facebook.com/facebook', '<p><iframe allowfullscreen="" frameborder="0" height="285" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d29793.996794485138!2d105.8369637!3d21.02269665!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab9bd9861ca1%3A0xe7887f7b72ca17a9!2zSMOgIE5vaSwgSG_DoG4gS2nhur9tLCBIw6AgTuG7mWksIFZp4buHdCBOYW0!5e0!3m2!1svi!2s!4v1442204843956" style="border:0" width="100%"></iframe></p>\r\n', '<p>GIỜ MỞ CỬA: 7:30 - 21:30<br />\r\nGọi khiếu nại: 0964.836.788<br />\r\n<img alt="" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAAVCAYAAACzK0UYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAD2SURBVEhL5ZTNDYQgEIWthR6owYRWPFCKCY14oBFPlmAbb8HJhD8Ff7Iedkk8iPK+mTczdHhhdS8w8KeQdbaQQkAkj4Sd16rrJ+1aoXu5icteI0ge7afMExASSsXzwOv/NCF21C4DhcXpLpPJrBIwk//i1wLlMtWjLaxrQOggCzGE3ukbB7BhtiAooHhVIfmhFMKiceHToBhUhXir4lq0MoFrCV+/3LJbkNDCefvegNTs4plJo75hFxd3v/CAGdTzwvvC7bUwQzkbNZgnLewxPNVla4Y2JZuOBrY5jCT09WslHisewPiSrGVIZ09mUtwUlzZ+B/IB/VZb7/sWfIEAAAAASUVORK5CYII=" />Địa chỉ: Số 67, Ngõ 20, Đường Mỹ Đình</p>\r\n', 'https://www.facebook.com/', 'admin@gmail.com,abc@gmail.com', '/upload/cauhinh/21337img_slogan.png', '/upload/cauhinh/16479DEMO 2.jpg', '/upload/cauhinh/28158logo_fot.png', '<p><img alt="" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAACUCAYAAAApkxTZAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAFeLSURBVHhe7Z0HoB1Vtf5DSCghdEGfKAj2hqIg4hNBVPQpPNvf8nxWUFTsgg0VqemhBAKhhyY2RB5SlKIgRXpoEZAAoaWQ5CY3t7f9X781Z52zzj5zzsy9uSe5l8zAyT1nZs8ua/b65ltrr733mFAchQQKCRQSGCUSGDNK6llUs5BAIYFCAqEArKITFBIoJDBqJFAA1qh5VEVFCwkUEigAq+gDhQQKCYwaCRSANWoeVVHRQgKFBArAKvpAIYFCAqNGAgVgjZpHVVS0mRL4140Phe0n31L63Bau+Fdrw+JOuvAOTXvo5QtcutZw6OzbXD53hH/V5LI07Fcu55bwxtn3hkUNSrJ6VZfDDdVl7XfhQznEk6d+edJkt2HRvxaEN7p25qtfdhMKwMqWUZHiBS6BKy6/14FMNmj59CfduLQknWolroBfBbRiJbY09UDLQJF0lXIoLr2sxqCQXb96+W4/eXBtqAZ/k+ctYThAqwCsF7gyFs1rLAEPIgkoVBhGLasJoVoZK4pcATE7VwEIAxsDIAOoStnVjK5W4auZWpxPJX0ao0van6d+edJkt6EiPwOoPPXL20/XGLD6+/tDb29v6Ovr0zL5zdHT01OuA9ftGBgY0PQ+rU/v0zZqRKP8rQ5Wpzgfu7e7u1sv+TrlFZxvj7+HvDgsb6tL3nZZffhL/e1+K4Pf/pz/7mUy2Hb49G1tbVW30yYrJy7f2pn2zNPaPFx1XJP2+XtNST3LsXMxI4gZkr/HFLlyTwx8aUBYC2ppbK+agdXeU2FG9U3Z7PqFkJ0mTxtq61cPmIfyDIcEWL7T+e+xspriGnD4zu3Bwu7L25njfNLy55xXGPL2ym1lUsc0AG0kTNJ3dXWVk/j6xEpq6QzY8zwk0nZ0dJST2guhs7OzfI58rVyuW9t8vfKU1SiNl1EM0L4u1MP/9uDKd+pmz3YwwL2m9c++v6Jcnk3VKi45WdrbwiEX3pXiv6ouLY+S1jKPBBAMoAy8fN1qWVAIecqKgTntnjxpYtnUa4P376W9FLKfTXqKIQFW3OnosChYDCT+reyLN+bgGZC9ueuxorj6lFUvf3vbe4Xz31HqmAlam3wbGgnVAwp1to/dg4LGcorZSZ6HRh5W95hxZckgT/710nh5mayMafnfaS+Zei+eNABckzqu6b3pil5r0ngz8aQbF5Yd69V+JV+bChDW8095tlbPt2Pg4MtJA9MKaDQeLPA+MfOfxW3IThM75fFRxeWm+cvqm6uDeY5DAiwrgLcqSuTf6u3t7XqZTm3gk3bdv7GtI/t7shqRlb/lGSuJ1cnXbbBmmzeZyN+zizSljNldVtvi/A0MY/PPl2XtyctSs+pg1z1gxvdQpgd4D9ppTC+NheWtRzPSpftW6vueEqZj1+spoFfW9DQerOqPElYYXWXEMt2/lofBpA8sVNcvT5o0QKtuQxqgDY/DnT4wJMBK81OhKJ4JxB2Mzhp3Ys9ABmMqpLGwtPypg+Ubm1hce/7557WaacCaR0HI04NRDJKxbwy5DYZlGPjHsiEP7zMcjOzytMu/QPx3e0GRR/wsaZuBV+zX8r/9c8hTl2amSVP0mHXF/iwDuXSgGS6wqph52f6rNN9TtdTSGFh9067CluI0teZoLYDGTnk/8lifkeZ/ykMCLA8EVpR1SnuLGoDFyhSbXvy2c4NRvKz8vaOYOlq9Vq5cWSWdmJHkYSgxYMOITIFNwVFMz4hIk7d9sf/NM5N6/jbKGi4Gk+bob1T3eo53A1wE7pl3/u7Z3JSNHO4JUNQLBUgLffBp002zaqd9YxMp3fHfyOEehz5UZJdmRsZtz06Tzu6q70urX+NR18E+4SEDlrEFU5KWlpaqsk1xraMuX768xm909dVXh0022SSMHz8+XH755Xp/rCz1GtQof8snZjPkPXPmzDBmzJjw6le/WhmWMRUUcvXq1bnlZ+3m79577615Tp8+Xevv27Bs2bLcecYJ/b2AggGDgarJ9rbbbtPyN9poo7BixYohlxffaPLzQHjTTTeF888/X5MaW1q0aFF45StfqXWYNm1aVfsBcsuntbVxMOawVTxnRrFJGIc41Isnqo2f8mZQPT9SNvvy1U7zX6WFXNQymtrG14JR7WBDdppa4KkNCakFrLz+tZyPbGgmYWySPfHEE2GzzTYLb37zm7VcOqgHi1WrVoVXvOIVYbfddquq11vf+tawwQYbaEffZZdd9C2cx+melX/sbDd28O9//ztss802YcMNN9QyDzjggKrwCyqXp3wPcr///e81r/e9733lvAywjj766LD55puHCy+8sNzuvCwLYNhiiy3C97///dQXgTE6wOQzn/lMGDduXJg1a1be594wnb2MrB3WXurEcz7iiCPKcuJF9fOf/1xlwIuHv9ddd53mz/PcYYcdwlve8pZyeXlfSMPSkMxMBhuAme6/agRsNsKX7h9KmFqtqWTgUAt+WUGuaQMJ9etXYXl50tRrQ1p4RyVwNmnjcASN8jiHxLD8KBGZ8Fano77tbW+r6SLGAn75y1+GqVOnVl3/2c9+Fk499dTwm9/8Jvz4xz8Oxij8MDg3+Ld0bC5ZhijSnnvuqW/3NLOJPBYuXBh++tOfhttvvz1MmTIlHH/88Xq7N2msbca24roYINvfefPmhcMPPzwsWbJE8/IKedddd4XDDjssLFiwoHzelN+bS8ZgPJjdd999YezYseEnP/lJVf2svd4/NnnyZGWOvm4+Hd99GcbQTJbefI5HO7nX6nXzzTfrc47rNGnSJH22Dz/8sIIXMrF77Br5kPdgfHje5zcUoDNQb/wSqgattGBRk6WBQey/SnNEV4/CpTuikzS1oFSvHKtHNXBUm5b1gjRjQErzweVJE4NWGhDF8mgk08x3SpSgKYAVO5tRFt60b3/728uA8sc//lHfvLAdPvvuu68CE+CBaYV5Y6bWpptuGvbYYw8FmrQRRe57+ctfrp/dd9+9/Kb/1a9+VVZ20pxyyilh4403VqWDkQCSXoEMRADOiRMnhje96U1l3xT1Iv8XvehFZfD5yle+oqAyYcIEPQ/D+cMf/qD5A4aYRzDIX//61yp2G/3zigtgky/58AFIqdPdd9+t+ey3336aN+wFOZx55pmq+IsXL1a5fexjH9O8AYjjjjtO8+AFwLHVVlup6YucMb3JD8D2PjgAxmQCA95+++3DO9/5Tq0DbTZw46WEzMiDz7vf/W4tY/78+cpUOUcZb3jDGwLAZnWi7pSfl1la/zRfI/d5UzgP4CGfp556Spmej1UbrHIU6UeeBJoCWDTT+3LoZFtvvXV4zWteoxKAeVjnx+T54Ac/qErz+te/Xq+j7CgACvm5z30uvP/979ff+Ek47I1pzIDf2223naZ57WtfG37xi1+okvP79NNPr8qTMmB773jHO/Q65g0mqx2WNwBJ+QAD1//+979rHQ0gPv/5z6t5BAh/73vfU4DjN4yCdhqz4NzcuXM1+3gU9c9//rMCM/fC0pARwPTb3/423HrrrWXg/fCHPxy+853vaH2ow6OPPqrmFvX/7Gc/WwaHE044Qc/xMqAsAA7ABLRgeltuuaXK/ZxzzlHGhZz5veuuu+p16sL9ZtrH3ZVnQXswf3mpUIdXvepVes8HPvCBcOCBByp477jjjuGxxx4rgyYvEQOOOIo+TSXiwQqfJi/wPfLII/oC8YNA/hmMPFUsapRHAk0BrHjaDB0QFgEIcBx88MHayf/v//6v3JE/8YlPqIL+5S9/UfOC6wCBvWnxgeF/sje/Nc4cv8YgUHTY1AUXXKB5HHnkkXoPSgt7AIA46MiA18te9rKynLxpCDvifhgPByDH74svvljzIy8U0xToqquuUuXHLIbZADowHkAGP5c3oy0E4Fvf+lZVGaQDOO655x41qyhvp512KtfvkEMO0XN/+tOfAo5uvn/qU58qm8zG6KgjB/JCpg89lMzknz17tt5DvTgAVgDGZHzeeecpQ9trr73K7TI2SJ0xU7kfWQDi+Ob4/YUvfKEMxj/84Q/1HODJwXMB1Dny+AetsXEArzns8zAs8njmmWfCySefnBq8m0cxijQjUwJNASzfVPMpwR4wOfiNQqAY5vymg6PkxogACb6bsxpQeNe73lVmPL7zmxLAHgAlO373u99pHpg8jAaaKQND4Dvl2zk6tx32RsZBT/1gYpQPc8OBznUDk69+9atlkxHgfN3rXldmKE8//bS+4Y3x+LgpykLxkAPX77zzzirfmzcJv/3tb2vVUGBYGGwMExNQBiA/+clPluuOCUy7LrvsMk0PYDGYwYFJd+ONN2p5+A4BHO7H5OQwQADAGAxJ8xmZmYopzUF5sDKAkDrTxmuvvVZBCtnwmzoYYA1mFJb88TleeeWV4frrr9cXGX46WGnWwQsT+VOveNrQUHxhWeUV19eeBJoGWHQUH1yIiYZTnI6Nb4iOjjLCpjALf/CDH6hPCXYBYKFMF110UfkNyb0omzff/NsWwMKvYmVecsklmh4lRxlhGjvvvLMyGFjXoYceGo466ij9PPvss6qgdq+93TGVMKnOOOOMMpvh0eBLAxjwB3GYEsAgARRMNo4TTzxR74c5pbELAzgPmJbXHXfcoWVi3pqiAzScu/TSS9XJT97/8z//oyDKfSeddJJeR26Uh9+QNthBOAL1RgawKmNofmAB5giQWruQscW83XvvvfpcuJ8DsxBQNx8d5wzUYNHIExaKD2swcxytTExQTHBeOpj29BUAMeugPY8//rj6LH0/9DF/WXkU10emBJoCWPFbDOWBnaDQXDOfFG9OO/w9jHrxlsZE4YC90OlRFkw6H2hqoIW/BmcxBwqM2WTKSfkwq5e85CXl65ZvDCTez4RJgVIDLIADCsDB25vzxk6MlVE/C90gH0wv0sF4OLypzD34fABaAIjD++Yw47iXETk7AHbaBHtkYMAzJMqD1ZjJyD2ANOzG5AVj5X6AD5nCagE0M6utTNikHXYvgHP//fdr/gA+LwGLaQNIDOSREbL60Y9+pFkQmsELyrevnHmDL88991z40Ic+VDW6SXI/utooH9gZdfHPNx4MylOPIs3IkkBTAMs30To8JiHOV36byQeA0THp/Dhy6egwC7vufVzmJDe2EZtYZnJa2bAMM384h6+H32ZCEWwJ44JRxMppynfLLbeUBwe23XbbYMGxlI3/B3bBKB8HTACQBYTsgIHYqF3a6Ka187//+79VLuY3g1FgvpE/LM4mUpsfjZFIDuoEo4NNwEzNqW4+rBe/+MVq3tkBMHoQZGSW39QTQPr0pz+tzwA260cyDWj/+c9/ap0OOuggzdJMY8xOvuNoB7BJQx1oE8waxjZYhkX+++yzT1i6NFkgbzCmnI0Swjh9CAf55PWBjSw1LWpjEhgSYJm/wkDD4rDMJ8Rf3v50fgMIfBn2pqXz7b///mUfkvmUGC2kg8GwUHRMKQ46vimCn1pjnRhleOlLX6p+JuvcKDXlm/lCjBB5UBZ+Gv7CykxZrS0eCPmOY560DN1TDzOP8KnAYCzwlTSMmAEephjHHHOMysFMJuQWK4yFbqDkpIX1oPwWpInpavnRFjMJaTNmtPfHWbtMboycEmZgMjEz2XxQmK6Amn9ufLd4OlN2kzPtB+BJg1wY8TMQNX8kz41AVruXF4k3S/OM8hGWgTmIT5Jn+t73vlfZLH+9+dlIjQlmhmH5Oa6F2o9+CQwJsHyz6RAoNmYGTljCBPAbYcrwYQid67CJOXPmVK3bxBuQNNyLg9qYDeyHfOh0mE4oOdd9hLUpMddRAsIIZsyYUU7P2x52AnBZvjjfjz322PC1r31N08LmfICotcsUlL8oP8BDWEN8kB+mD9cxi8y0MlDCmY6PzHxaPv7J8qJumI74qsgLU8+mCXGOEAXkxzmmLwE2DNlzYB5RLnIhLaYn3xkw4CDS3sxYfnMeWcO0DFAAWHtuZgK/5z3vqWqqD1HBrKS9tMteJtSRZ07ZgI2FLnAfoROYjoMBDtjlNddcE6644gr1Wf31r38tf6dP5DkKwMojpdGXZo0By6i+RaPbG9RipPxv++79Cj440RSAv+Zc9/E7Bi6epcTz6/wj8GaE3YNfDNMNBYvBinqZchqL9HVpxA6sHjYqauVhRnLO50eeKLBXYvKOWR718231333bLEyCfL1sY/+NletHVmFMsE/yAAxhT4STADqUZ/45H5bB/fFE63gVBh9v5euah2FRzzj63+STZ3I69xeANfrAKE+N1xiw6EDxSgVe6amEj6GxDucVwRQxbbUBWERah/fg4mN2DIRMMSxPWz2BCGhMKZzydqSN4KX5TOKpHpRFOd5Rb7+9Yvn8ue7baU5kD/zxaJb9tnKsjEYg4Z3TaaBO+7785S+Xg0UtkJfgXJtmFHcgq6O1jfJj31QMsuQxWGe3ATxskwEGBmcwBRl1tXmKWZ27AKwsCY3O60MCrNgP41lUPTHQCU1xvTJ7c8MYlvl60hyk3nkdA45dM+bCb/+mT3s7x6zG8jTAMnCJAxljkEN54/qa49ortQfCGEji+324gZerT2f1i4HDZOjrac57e4FQZ4AAEw9z1KLX7TlYngaQ3hHv2bAxN2uP9wN6uQ3G4U0e+D8JKMadwOAGvk3MxTzHYAEre5uv6vmA9Rbeq59Po/mEjbb7qtxXPUm62OYrTz+oSmNA5YHAA4q9KbkpVqgYAOI0/t6YWcRgkeYb8vlRT+845n4zyaxBMXuKgTQGMn4bw4nXeorr639zn5mNxj7i9nizyeQUM01rn+Xt658GwrEPyZahMVn458M5O291tPabaWv18uzJ/Jn1wNWz0UadDTMaIMbRHgeb5h1tHAxgZa2AkHdbrcYrdjZeWytrmeTqSdKDXWUCaRfbfGmfizth7KNIU3xTKFM2799JY2/efxJ39HpK4EGB8uqxnFjhff5pfpSYpfnJtbEfz+dljnM759mKySw2rdMAgPsMKGPGGr84+B2btmnMizrFz8RYk/nH7D5ff/seM2wDPH/d6pFmfqeBF8yYoFgGNvyzbAR0/lpewMqzzVeezRnyrA0V1z1tQwmfxgNg2hIudi7PNlp5tvDKk2bUbvNlyhEzpXoOZmMExjDit7BXrHqOY3+PKbcHRA+AVi/v5/FK583HGFjS/Ff1zOAYtNPAOI1xxP4gz2BiVuNBOQbYuK4xSHmgNJD0LwDk5PNIeznYubis2DcXP1v/PPI6233bGdEkaJcAUmLG+DBHM8+RF7Cyt/lKM8my9xzMqmPWTjfxUi8VBlZs85Ul25rrspJ5kPG0oDvwsQ1hj/zTJSt2BlnuWF/NwrzkDxthsVthb7esg8R5frAVoCyOwP3c18IFSdgbZFurTtkPT/LhZnF5adI2yaxbTvT1S6IBudAn6131C3MY4CqlsMSyLIMi39j+Qtfb7OuRIrpkS0xZQbSvK6lVm6SQLHTNS8ps8NENvMiQIvjbJl8kT9lrR+pDnZK2LZWLmjf16qbeMnomGetuh+TPXo1yX5C6ywLJgcWZdfdGucb91KWHxCqkJFNdwFluoYzn5aTKTf7RqvRJIm7EH8g2YFKuTMqRdifPQ6XQRUml/CQTsmZzsHb5Qh4tZJDR/tWSTUup+aGbh9lVft6dvcm+i808DAgJ3GXuIGENxL0RSGxxblnl5wOsPNt85VmWeLAmWuNlgytgdodsKXZH1SJ/xTZfWU8+7ToA1SvA0ckmDKUlcBPsSLSjQ76we45cCwNoGB2+J3T0d4SOrnZVnOWasFcVE61q1W8J5HGlp4P7JR8+ovBd/Ul6QadE4UA0wKAPoBPVF6VKfnOPXC/tBSreK0kvyxZ3y4gW3/W+jM9qKVtAUTYvEzBok7+rJctWAQxBWmmDtpM/AsSCnEmeyEQBQX4bICioC9jqhzSSAES1e6QMZCPbc4SlfStURr3SVqAx9ImU2IyWOLdWYAzg7RAgbg3PyvdVmhlAKXVol78dyDnJT2ZxJtc0f9ov39nTkPxUThntB0ntbSNfhYOp7MovqKH0mUHcY8yMaVjM2wSoACxi4mwaU1Z2eQAr3zZfteDiTTUc4fW2oPd78/n6Njbh/G45C8N+0SJ/xTZfWU8+5TrQQX+WkE1Rn17Rwf7AyuV06F4BMtFsuSgputgMlLe/LMKmSosCCchJwlUADfiB8gs+dA3IrjvKluQ314QZhV5R4A7AQnxQpjDCZNAn8CghJwJJA5QjtRJl7+tuk+wA1KRCy+Qf6kWKDr7IjShgo48xRCFuCQ3i5pICK/tS9qRNSwiP/ZbvABaqreZTCbtk/LDEeuRch9yoWEMdE0kC0V3drCdPhgJeZdYmTEqrIE5wOQe0raDENsAneTesljKQBZBGDskK8h0qMWSeMEBprTCv0CNPTBhTZvuTd0yCh0hWHpLeI88o9Cc7fDf7gGV98Ytf1KlFTINiyhPm4d/+9rdcRecBrLzbfNVf3jhZLdSbb/GSyLWjiY3YVbX5WbvyaLHN15DCGtpLiqgMijd334CYL4AULKMrsFiLKTZKBaloMwYmjKlsBknyNjSttMmxYoICFkxJDBxhRiiM4oaAYKswA/KT0wHLhKTd8hf9R410l3i59rzsd9JKplSJskXJlqN/mnemRRQG2mFW8BwUvycs6+3Q8im4Q2DheeE3nZQqrGqxZLkcZCqZddQfoNCKlCrP8oAA+2r5d6n8S9IE8PsV7NWsxlwVgEYAur9z6a1QwvMkzUopB1nJScrRdAamJZt4OWUmhEzllAgpuYFyeQ4ZFmF4TtoHPAJSmNn9XUn7gOI+NcWbewD2rGDBVC0OJssz4Rz/la2mmlWDPICVZ5svKyd9GeRkeeI04Ku3HVgjdmVlxKDX2H9VbPOV1RcSYFBladM3N8r3FBr1tEBVz9OiSPKu7xY1XfasXOlRwFLlUifKCun4ol1LFsiPZ4ADTsqb/xlJlxiGquACBigcZeFTSfxCMBTR0PalcoI11JdL2S1S2irRWzGE0GKtm7AywJTMUNpnF8k/cs/AUqnHUslGfEMNPhiqvdSiVcoQc+p5mkq2kmer1vdpUeDnQljwpLYAMFSWVbLS+NIPdRRtF7eWwCf1l8q0Pi56v1DrF/papKFL1O8E5oMrC5SFktsq+YsU5NO9Ushmi2QFk5UWgzpi/iFPfTH0kF7qQp2WL018YN39mgvXO/CqrXpSvi2SbxiUjduOXLqlJqFd7hlYJW0XkFYXQAJYsLRmHzjoGTywZbOZdoRJyJQtplTlOYYKWNmbktbuDO19TgAYR718YlCytjQyK5O13wHHPP60Wulkb+GVDnrVbRjF23yhvK1GC+jJnHjsKVk+YO+wcszmoWPsy2R7i/8IrZvL4nHHThdFEjVSRtQriieqdNXvw9Ltdwn9YzYJfWMmymerMLDJy2VJTFm+RdZpaisxJWNIWtYqUfvf/j6Ew44JnZtuL/duqfd1SHlh4htC+PTBIfz6AsG+BQm7A+Go11JhRxPfHp4bMz50jN8shA23DAMbjGn46ZDpKW1jNgjLt9g6hEm/EjSeLxPzRFGkjl1yrWfMttLOnUKYsE8It92ZDBRAqKTegApGF+ypRKOE8gnwHClLNY9/i9R5u9ArnzbJ47HtZbL2vNtL/i3J455bw3Ob7SRpXhIGxrw49I1/SeiUdj67zStD56QpCnLLQcV+yW+1yFGiwFdsuYuk31DSjZX6SH6TTlOTWl8jK1tCmDwrtL30DVLnzSTNVlL2KzLb3zNmixDGynLWBx8pWCjllMzDBKqA1rVzsMIqcyHxW/GdOaDnnntursLzAFbWNl8UlGfD0UqMk20oke7Mr01XaUqjXXUqu87k2bg0D2CtZ9t8KXNRhzijX6VRMd7em+4Y2jcUZZ+wgSr2s+NFuU8Uhe/HcQ1bEs9Kv7CWm/4vLB+zUeiWNJ1bjglLx40JK2WWfzjnZFE2fC2JH7uNkUFQ58G7w/Jvfj0smLCV3mOAEzYdKwoIuIwJrbIgYMvmAjCvf0sI/7xPSUxiVa0ObZvsJAAn1u/GY0S55X4Br0af3k0kreQ3IGDRPXWyVGZ56DvmRClLwFHy6B07JiyStdXbBFjCk3eKBGBkYi6BW2p3tUq5iRmFSdVGZabNkLpvLaCR1IF6A7phntRV7T5J+fSd4RkBnz4By56N5Lp8+gRcAdDwnv1DeOgByVdMtT4YrHz+ellYPaFUJ0nXPWZjAX0BLGEoOubQI7I7dnJYLXWlXOQWxm6e2f4+eR598jJp/c53pQkiP7KS59EOw+2CRjb/IByFD1Nz+Mvig6w2wWKLeY48gJUnILTe9lfxTjDZW7hXTMf629NXWlZv55ysINdim6+U3oFiopIgC6DAgBbmWZsoW5ggSiFAxadXOn6YfrQqUDv9XAetJJTh71cJA5N1qEQh+fSgkAIGfeecheGmfqPFoiiM0IX754W2t74/tKJwAhQDki9giAJSBueCgGSQsto5N26j8Mw79gnh8YcEMsjjqfDsptuJoiYg0bXZ+CBrwjT8dFJ/yW9AGEnrCSepo7t72plSx21Cv5wHNPs2ox4Txdz8p4CxrLAA6pjPqCQXUFqss8S8m3y8tFPkQ5slf0Dr2Ykv0npivqlsHn9A6rVNUjdJo+2ZmKRdLGWHmbKBqeQHi1OQ+8vVYdmGm2qeA5sC3FuG9rPn4lIMGIGa5y+mhi6RyQAyMllntH+glK770EP0uSqnKvkDdUBlLR6AFBt+8LF1vvIUnw+wyCl7m68YJOptue5BKw2U0kyzem0xoEyLgC+2+crTA1wa7cDqeO9WR24SZiD6IUoxIMq1dGMxKdQE2TR0ic+B9J2SkFEs2EbXP65Vk0cZljCWMH5DMSMnhHDuHA0bwMkrHhnJV7xH++8beiUN5lyPMKNWQFAUCsDqhaEIayGf1s3lvDAjQElB4cCPiqoJUDDcP+ZVoUWuwVpWjHmRAM0Wes/q7RLwCRuNC91Sd5hFr1wHMLqEYbRMEMY2ZZIypnDSjLB6EwE7aRfgB2Mb2GCj0LfgUaknlBAMEV5Vcr4z9kf4gA4GAOtTjwk9G2ws7RYmOXacglDfhsKIHptfcoJL+scWKLvqHLeJypH2LJc6dclLAPO5Z1sx+ZbdqumXkvNfb1VWtZC2iwwB/r7ZZ8j1pB7qzpsyOfSMFaDUOm8gdZayYZAA/ZgJoXeDzZMXzXgYo5zbYKLIUuolJms46Dsiv0XiF5OACqbr6DMBldfOgZOdpW4wCVkm6Bvf+EZ5FdqsGuQHrKyciusjSQLSQwd/ZAHWEkAI0BgzLnRN+YVosii8UjE+q0LbtX8ShdwuDEi6LkwfNes2CuHMU8RJLWaMaBqm1MAfLw0DL9lBGVXbOJiRsAlRNNjWc+/dO/ScMiO0/NcnhYkI2Ilio3C9G2+qJlTfi3YOz1/1R3Wah5+eICbZz0KY8avQNUlWoZx+lDKx1cJeFOCk7H6Y23tkZ+qp4quaKqxwytQwMPlYWcBd9tgT87dX1nUC3PqlTc0GrAEBpwRcEtl0w/jGbiL13C6s/taXRDLSJoln65XAyn7kJmC7Ws09+ZxxTiZgtU0QgDrs86F78i9C54xJoX/mMcKEZdnjE48IPVN+GVYfc0QIP58mPrKrxcxlxDR5ISUjC833YdksABbxY+0vCyRljmG8Vle93lsA1uD1ejTc0RTA6hTzBADplVU026ZJ5++VgX9Ch2Bl+F6u/6OAylahUxSsU5RNlRL/y9mnaryRBjyK/6Xno19J/DeSV894YSYAC7+//AMZAntc3vyS6QrhYkdPFlATxd18O2FQsAV8WxuFns99VZ6B5LUKjzhhDmJidkgAq4xkLt1o29CBkguj6KVsAYS2r31BIjNakrT46Npk9FI0VZ3Nx05LWM84GFJzGZaCkLAsbauUBRB3jd9A6ilMcLz4rO66Ryq0Kiy78Rq5LmAtMmwhLcB29rmZgNXFgMH8+3QcEvMR07W3tTMZidWfOBDlizwqwIrYVDUv1RxsPmBRB0Drf//3f3WLMps/CQgVgDUaYKV5dWwKYIWNNlGg6RWTMEwXkwqQkg7PoJ06bv96SWjbcKKMtIkiAhqAgLCn7jknapBo6BE1Wfho6NrmTYmZAgCKUsK0ul4s/qgFT6naENP5BMr177tC93avEkBJnMt9wsZgYd3bviJ0zr9X0xKnCfBAuAirWCkOc2VX4gMCCADDrkO+mSgx9SzJnDLa0eTjTlDGg+nYbMDqwmzcbKKaeICkmq+YfSXGFT7xdQGVp8Oya/8gILVZ6EeGG8pAAvU7+/RMwAoMFjzxTwH8ZD0vpU8wW0Gpzp6WkASMyOCmnKb9agVqsJt4FxOH5Vo5WN2Utf4xCVm/jHXzWd00z1EwrDxSGn1pmgNY4qNZJUrWspWMip0gptiqh6WzLxcFaUvm950l5sbWmIRjVRmTEbNxoedsWSYZGYryDNx8lZh6WyegIgwLpeyQkcTuT31EQwZaUSJBlUVEYPdJbJNEReMXUqe/sA7YSZ/4mLqvkFAIQEpjHVA8+cIIwJgdktE6nOsAAUr/tW8kiimV6MLpJjdpMCuVmnKSpl1R8v8004eFOdgl67uv1rZvnMhg4w1Dvzj6deBBQkZabjg39PzzMjW7u8fhpN9UAa7zHNkjMMOH9fCOO8t0kis1ho0GtwNLxLb1rdSBirBUfguyE/ulEe4S0NovAWUDTOlR27C5h58ozVzCr3zlK4ENWvFp5V3toQCs5j6jdZV7cwCrZObBsro3leHxbV8iwLSdMKTtRfleHB7fcduwVJVxnIAFw/iJv6vn3FNknrEAhUzLWf3r0yS9mDsyJG+A0j9uQuiZhk+sM2FA/KMWiozS/eqw0LH5Jjqy1ifOeRzssK32U2dKOslPhjYHlOMJhxL/T9/4lyZsDL+P+NJ6MPO+ISak0gmmdwNVSUS7zvedKn4wTFPYXpNNQphUAtTi4N95F2Gr4xKmuclYlVWXAFrbfvuGcO2F4TmAVhzmhGloyMfcOZmApUC4zZbC2LbWkc5VDCRsKoMNMhjRw0jjT4UVP/9UMropoSUEjOqYMHMR1wJg3XXXXRooyhr1TMVhbXdb152de/IcBWDlkdLoS9MUwFLfy0bJ0DxgA9vi7d+DM17Mv/aNhC2oqYc5tkESJsDvi89Mouj7OkPHScfJOVHETcSsZPRRR7Umho6zJbi0WwBI2I9GBKmDSdY+P+Pk0CrmncYabTA+9KnTXxjW8aJ8AlA6WVhXTegUv43EFm0ggZmq/NQ18Un1HnxQKdaTSHJBQhksYHLOamzEY6aVYriaD1gaZ6YAtVUIn/9Y6Nn97SpD2UooaV8pTipIfRMWtpGatYBc39xshsWI4ooJgJyEZQhYJSOr48SMlvgveUl0fUl8hB2LRRZMvExW2sA0TOZHNt+HRQjDxz/+cfVXsesP+zGyoQi7LrFhR56jAKw8Uhp9aZoCWB3ij2qTIXwisFE8jQGSTy+BpKp4CUAAGN2iJGFjHMpi8p19ss4zVMe7hAF0iy+H6zqKqHFRE0P7WRIPRVxUd8Ky8A0zQafv/LmSdgvxX40NHWJqKgCOlfCKY4UZiZLJJCLVOvVncT9+HAFVgKC/FGoQPv+10hSgEnPrTJZ3UWty8knqQ1IgaTLDWqxmrQTTEmM1W0xqBi42IQwkAfoOgleps5i1stdYWCLMC/9du5xvO/fkTIZF3svkWSQvloSJwjS1XMIbDvmxmvAagSU4T4iEmuA6SGjeveZ2dtbp+ta3vhX+8Y9/aEGwO3b9AczyHAVg5ZHS6EvTFMDCgU5MEwqxWpzrT2y+vbIlVRABriUTk1AFlKVP45pQvnGh/YwzFYSEXwlATJf0TNuBQYxLpp6IT6vjhG9J710twZMy5UYUKom0F0g5RZiFOPkVHAWw1DQcLwzlyKNkPlwSKgEayV41yuJ6lIkBhgIKGn8lwPn1g8oj943iqJSNKXBJ/gvutgE1ffrGRnDsP0NrROE1qHTSVAHrhHVmxmGJ2Uk78Ov1njJL/Ur9bzpAAbxVQjE0vENkQswWI5xdYyXkQwAHU7ht7pRMwMqKw+r56i/FjF4mtRe5ij0MiZWIuCSui9HTtXAAUDjZ7733Xi2N1V+Jci9GCdeC8EdwEU0BLJSZt7ayqPfJduMS5U2kd5j0y9A7VeanfVHm/eE3EqDoEMBS1iKjXD1nn6mhD7oMzUUXiCNc5gqWQh8YDcPHEn70RRFnsorAgPyjFoqEKwx8+3vKMtpEodVM0sh2MXfOP0MARRLivtL4rjUHLG2fAo/U5/E7JU8JqtSRtNJQJHUSv1knsRzyvwZcCgD34CBXH1jjwFHk1oNzn/CGySfI/UINL/u9BrMm04uSGK2VavZOkJHLccnLQL73nDM9E7Cy4rDCb/8qo6qtiRnNgIN8ZAp2Ali6xlZzD1vZ9Mwzzwwf/ehHw+zZs9XhzoYUjBzmOQqGlUdKoy9NUwArSPCm+lo2Fh/JcTIi2CuGm65ZBdMR9b30ktC5dTIK1jVOzDhlQ5L+HJkgjNKzUueDt4ZVm4kZJMCzXOf2ibLDON61h07vIX6oS3zC6hDvkEVb9n63mpvPYxahyDKfsU8YWXjoVgU4aJC50deUYfWMnxBa1JwUhvXUg+rfUVcaLh7qwwcHNWhaCucIU2bq/LwE6BoDls4llDbDSsPRJ0reTKZeFno+fKAyqgTgx4RnJNShF7ASMziJ2RIAPWNmJmBlxWGxICDL3jBmqICF4BStSjFaTe7nfiSQcAY2bWXDWBbwK0YJmyz8EZ59UwCrW0wtTLJ2AZj+409QNkRcD1NzlIncfG1YvCkOXhRvawUspvUQ6a6LVunqAEvD4nf+p0Z8Y0Yxiocid4zfIvSzrndrsq6mrnh6+XlhxVYJU+sdn7AQRihX7LirUIOnE4YjifnL6p5rClhhA0Y9NxbAkpG1Bx/SjDVPqTptZREbJg0TFgGYPYu2TxFfmkyDSSYgNwYsIt1hiJ0EwR4vI6eEcci/ff+6XqcZ4Utj2s4imTieBNYmzviBsVuGcKrEi2WENWTFYfGQWARxhUyT6mVSNivIgr1dLIZTQq4mdmy/fr7fOiwvWFG1wTKs4dnmK97KK1kvKz7qzwUczFZgxTZfubugejGk39abS9gtjmCm3ah/6PiZuqDdSp1sKDdBtv56dWhR/xH+GFHikhM7nHZSgmjyVmcFhM6TxLzRoXYZaSyvoLBxWL3NS2XeoTjfFz8hQ/t/Du377qUAidKukHgkAk3xZ3Ud8XMpk9gvOQDKUtDomgJWu+TdvrHEmu388hDuu0Ed1EByn0SFrdSpyYulLKnbwn/r+lSsS9U7LQEsnbKUAVjtEr4BYPVgEk46UZ/Lss4WnWQdvnB44nCX6x0ik/IkcGl7J/7A007JBKzMOCyJ8CfKH/hdKRCVLDkoB8+m+XilRQFUaTv55AWtwQBW1goIeVZ1yLeNVmW5mmSNq+RTmSiddyuwwa4hj0TX422+sgCLmCANRQCwZHkT4p9w4A5gFrJ65dV/ETNGWIRGbstfVhuANYiDWVVD9EId1YsWhLDTfyoYtZZCD4JMGGbJFZZVWbmhRL2Pe7EsySJBqi+WEUWZI0e8kk5T2XlPmX5yl662CWhonUvBo2sKWOrgxlSVctq23TosGc+ggpiH+NiEdRFr1sko5BbvCuHvwsBgjVNnigksa3IxeTtr8jNAhem4yYTQd7QM40sQK07v0C5oL9vKL3r1q5NJ2zL1Sf9qmANsS4Du7OzJz1lxWF1S/44xMtH6WzKXcpEElFL/5D1CAEkCXk084h12bEG/wRSZF7CGa5uvPFtkVVhcsm5W1s45tDdt04l4q61im6+MnpEFWDiL1S+1gSjQcVMUKTAmkn+lw0tQ4MCEhEVgNmLCKduSlQbUbJRPh45OiQPo5DNC+65vV5bFdBtNJ8qcTP1BqZM5dzANFLF7i83CYy/ZRSbwHidLu7To+i79kpduQEEEu+S95oAlDAdAESbXJ8DcDkCXzLQuDUIdK0xPnOKYjI88pKOe3TOnSbpk1YQswGK0T0Fc/HZ9kwSwxB8G6LIQIGx11QwJcxjLqhJM32HdrkQuyrDyTM3JiMNaKr6xRTLy2Pr970qhrHkhgaOskyNH2u7ZgwGSwaQFqAggJWiUiHcc7sO5RLIHBL8cjIFEsrTL4Lb5qiwHEy+2V7tSaZYs0gGt2OYrS2411zMBSwI3AZBuIrWPk0hzoffgFPcBGP3XXBHaNhNFw4QTpSwvCzNnrhpUpMFtImuMCnoJ6Bz9M43kbhefDSC1Uob0bUE+5iOGCRJGARiUgifDkRIs2rdYFU2d4ULsNBC0xLbWFLA6JCZKTTGJWyI8o59VTMVMo05LLNZMwEwnZD/BJOMVofuEKQmbJHg2g2Hp6CCLGlLGrKkK86GlR+f4qRBX3BsGdniTMro2idVqBwRZHkec7r1zCINovLxMVhyWmq34FL8sI7IdzyaxafyjmNX8qTm2eSvb07/iFa9Qpzvb1Z9wwgnDumuON5P8gnzV61YNbVniGGzqLYEcLwRYUbbGG04kSyYnx2CYmgFz2j15NoytvwKr1af+LkN5Fi7MAqMhOd1XldCHt36LApFo0bKnwvzNXy7AIU5xUd4+UbpFTPc4SswKmafGELmyBOjTlZeGhVvvrCsqhA3xZY0NyzbeQZRzruTEsg6iHWoWJnqippAsNdO+/0fCktLKCgoYKJUABGbRc1tsG5YdcICMks3WNvvdpP1mqzpk3rU03LnxTrrkscZ5MXGYBfK+LKERLCHI3oeSx/MyoqnbhK2Uc7/8aXh+cwkbwFeW8SHynCkvC7YWUHnkfh1ACL88XmLTZNFCnYok0f0yQXzRhlLmo/MUiNpaxbn95AO65HOm/GSN93DeheGpjbZJ5mLC9iTflrGS3+nTte4afCvp2iZNDk9vtZOOUOKcZ/J2Vv3xKS7dVCaSf+dQccfhgSuFkSDYjrUz+ZnnxMTnBx98UJ+lYiYvPmrDM8w48piEw7XNV21VKiBXuztzxXdlPqy0BQHrmXnFNl9ZTz7ler9MnZFNBhNvhvyje+7JOuOrxU8TZFRQ15OSpYV1HfKbbpQEq2Wabbsu4qeU59//CqsnTZe0Yu7M+LmwD1mXaaqEP9x5vwCgbQcmei7EgluSbiqgtfIJCSO4O6yaJv6gmdNlYvWU0HbcUepoDjdLRLRsa6477pQ6NR093glZ/SOyn+LqmeK0P25qaJ16eOg8ReYnHnNs6L7kUtk3VFz0hE1IHuVwBfbyu+VGGQSYKnFRR4Y+eds3+vRM/6mawisnnSqvvyUyeVpCXG/8myy3LLFoRK3PPDb0/eIwGTkUGSxdVAJ98Q89/7hcE5llyI8VJcLTss3qDAHnqceGgWkS6HncjNB/5Imh7b571QzuY48yiUqn3M5JM6R9ku/kY0LH9J9k1j9MOUoZYfd1f9HJ4hbZoIJlV421dPzkJz/RSHfvgOd7vAu339HbqpYHsIZrm69qcXjndoUFxXsZck/9FUjrbQVWbPM1JIYFADGfT5cxwB8rnVi3tmLTgy5Rc5m7p47aNgEZNgLFNGMXGRBOblEAYt9CyadfNwgToJHodVuGifEwTQP2EL4lOsIfGJqyNJsmIl+S/fYkr2QAUvePYQ3wtK3RyyNM5NuF4rFVw3LJG5c2+TDxmqySLbFKMZPKWHpkfZq+LtgfNWl89A4oZ0pGRSE68gvwY6Mv1gYLvSKnbj4CPaVwCHBcHdqyfnyW/JCBjh+wvrqsl98jm7CqDGT3Dp04owxEd0dMYsHYaYi4MNmMtrSJWOMGyH6SA7IeVbeY0CoHfSnJp7xvWJYE1uw6Pqtvf/vbYf/99w+vetWrwgc+8AGNcGe7L+YVpgEWrMufzwNYw7XNV6W16WDF9bSyqn1llVzqO9HTzNNim6/M3kaMTrJnnaiZmlgCMPLmQ7F5+5spp3FS/GYxKg2D6NepMSul9wMuqD7KwEKWSr7QKfkCUOh7HIArhSNoRmh1aYUGAq65rEpJiSx/Ur6x0gSmdNCZATAASzu13AKAUFduYYNVnRKkyMT/LAdcMjuom5ynPrSHdAZk9f6SjvaZ74d88c0pmMhJvnM8J2fUNSSZs59g1yphD1JKlvwQXJfu0div21+0aOjBgNYRubDPdpsAWJe01dpGfdiNKKvuXEfM/FVR6QNMngW8syyXioiH/dtjjz2mvioc7ldffbV+/v73v4fLL79cGRfPMjYLY5Y1VMAayjZfiQA8WNnuORXRpO1TWI9h1dsKzJdRMSPTQcw/lPV+my8Ukrc8HVsWX0nic+RtD4Cw2Sm9vZ+Jw9LjdQ0lItfl7cxInbifFRRQBt1Z2ewNY026c7Scl1D2bgm8bNOJNcnKTIo0oJvu7MC+hSgl6y9IUKVG0UvNhAHFb2AenmdcKDE6qBoOiihyamNKVAKANaQkJeWWriUI3PBD2/o6S4WUlF2FU2KMOhAg35MdqsXbJKgNoGhdQIgs+cGsdGNUYZgJkiiAaRnSlhKm609jitpeCuaTUX9FZkU/birJQVc+RO7k2vyDFw3HDTfcEG666Sbdrv68884LJ510kq5Gmsagfa3yANbwbfPlgzhrwYp6xXsX1oZTWO39VvUaQeiOYpuvoZmE4EKpH6MoynJEgco7HjNKJZoBWyhxLO345eWG2SEaU6jErPhLbLjOIWQ1AMw13QLabELlB3JLh+RYmsic0CvNE8UnGhzW1yXmDId1aHsTm8OWaxiCui2XblUmoCrl9XcmgNhB+crU5LoAYreWmQRkwA8V6kpl1/urLBNQ0LB+2pEwQJY9JevEbINZiXxkyWaWvWlBVrKNfLKCemP59QpoYGGj0m0loIXoqorL7br0evkoPR+13THh5ZNRf+rbI35K3eRVx1elPtxbehlFWjTsP+2Fc/PNN+uSMl/+8pcVqNijkCk6yuZ1pdT6Rx7AyhMQmmebr3ppcKpXRgHTo9jjXXHq7RhtLc0Kci22+UrrEyWdtamB6k8RLdGIaHkL09WlywvmCAyJExwWxq7MyraUcSSKD0NYLWnhT62y2YH6upgwLNfZEj1x5ssHpZb5bcle8zommbz42xMWg4K2Je6nEitI/FgcZgpaMxLgYpUHlqdh+RhYSp+aV8+uXKzXtEyx09i9mRSJ5ycBZP7LMqtk4eGS6cTf5KMgIB/AFbxIAFrKWSVrbskUGDUExc+ERy1LfsrGEIf80e8lPzhmobaP36WXQYJdCItW0u7Eid7oQxsTmBbTUsBUa69AV3oeaX1iGM8ZGE2bNk33JTTfYzvThGhJyihhzKrzARa5rfk2X+nb2CejgdUjgNnTafJsBVZs8zXIztauS7sIiLCHfAmD6NWqPLq0sPw1n1NppK9sggkeyUbyiTMYANIFsCSTVoE9YwkyKtcrigJT4QP0cD/AmJzD1yNnCNYqWYpMgobErFq2Ouy2227hu9/9bhm0rMOXO3p7Eragmy1QXzEx5y2YH3Z88+vCbBm9ZNtXjdsyMKBgZV0JEGR+2kFPYX4yMJFAVeJfYhv4ZEfSHuWLdz3x77DhxM3Cdm94ZejqSFZ2UL9XhvwUn0BoARHd4VlHNql1wkQpzMiQApYGtiX5qspntEG3KjNyWBr04KUgQxkqm7VxAEDXXXddmDlzphbXxgAOskxhVha2Mlin+9poR1HG8EpgSCZh/IaL53cR8DnACg2ypZe+6en8ABSjiwM6zie6VvKa43HWRdMT1cYMs45p+R577LG68uQYiTnaUKajvPWtbw2nny4rO8jBriobyLzEa6+9Vn9Tt9tvv102bt4orFixQs/Zm9mbhY8++qgswzVWlcIcthdddFHYdtttyzFccTvtd5qPLJaBbVVFWrvv0EMP1dgiO/bdd1/dGWbHHXcMkyZNqlJG7jM/jpWXZQYNb9dY97nxHF/72teGD33oQ+G9731v2G+//fQDePEs04DKZJ2fYa37dhY1yC+BIQGWAYMPyESpUFL+6lu8a1lYeO+N4bJL/xgmz5gVfnX0UbKL+snyfUr4w6WXh/nzHy696cVE6lFPcFi5coWaaCi/Kef3vve98NKXvjScdtppZUBiuVwDG6tL3GQfu2OjhKThPPV87rnnFACvueYa7fg25YTvBj4GGAZ6MVCRzoNaa2viJI0ZnZcT1/z0FgNRD0ZctzxMplZO3sm/+bvAyEtpZjxtZ4SQtd15IeGAx/ne6CgAa+Q9z+Gs0ZABy5TX/hqjUKxqWxJ+f85J4Xey/vqd8+aHRUKaALEVQqIWCJm69e6HwulnXxDOm3tBWL5sacK+1HAS6i8hEtbpli1bpqDCQm4+AJSymKZBJ+ZNShoctNSFKRz83lg2r9hll12UQR0gEfDx0rr33XefsjBifhYuXBj22GMPBUHu5fttt92mDOctb3lLePe73x0222wzvYZfBfAkf9JfddVV+jxMDkuWLNF03K+ykLrCqrjv61//usYUcVDfnXbaSZa131TTT58+vfxcH3jggfDGN75R83/Na14Tbr2VNb3SzaHh7AwjJS9WFoXtMn+QWCxisA488MCw55576m9kaiEq/E0D8YJhjZSnObz1GDJgxdUwhV26dGk4bcZR8ja8Tl0n88Uqm3vHynDKFXeF2X++Pcy5vyc8LpYhXOTyv94YTp51amhZsUx+yQif+LNs7Afazx50W265ZVWYgmc0AApgg8Lz9iVuh+/8hbmwtfnmm28ezj77bB1d8uzm8ccfl1WGxylgoQxve9vb9PrTTz8dJsjE7COOOEKbuN1224XXv/71ASCaO3du2GKLLcIhhxyi1z74wQ+qvywGE8Dx8MMP1zrg/KdOACT37bPPPmqqvuhFL9I1yzmIMaKeMAkOTEWUk/oAcG9/+9v1vA0kDG8XGHm5PfXUU2rqPy8zF9gxh2dLDBY76Bgj9rX2zNe+F4A18p7rcNRoyIAVmzZUhjffxRdfHK6/7mpdA+rS+1aFr595W/jihY+HKdc8GS57uCN8Ur4fdtYN4bL7ljNrL1x2zd/CBRderCNyMCwCSU0xzzjjDI10tt/MK9tEVmcAAPh885vf1I0JYD906K9+9avhs5/9bFkugM94WZsLkxKAMmBZvXp12SQE3Dg4xwEI8hYHXPjO5FtYGwfMB1aGbwWzZfLkycqEYocww++UiYy4lzbw/aCDDgof/vCHw7nnnquguHgxo5JJmTC5I488MqxatUrbRts9a7VG5ZlHNxwdY13ngUzMTDb5Uieeadr0nFg+BWCt6yfYnPKHDFje12M+mfnz54ezzjortIgf/ZTb28MXz3kgfPrsR8MnLloezr67I8h2p+GjFz4dvnDug+GgObeGPz6wMiwWE/Hs8y8O8+6/T6d+lEbotbOylMg228iE3tJBOdaJv/SlLymoEBVtDItNC9h0kzTmG8J0M/+XBwAYDwyLoEQA8ec//7kC3stf/nLND98Zx5vf/OZwzDHH6Hcc9Tj9MVk4ACzS+4MyDHQAw7322iswaEDdv/Od76h5c8EFF5RB18xXgPALX/iClsE56sXBfX6wYH0ALFZpgGVZW+2Z86Jg8AX5GqDFfsSCYTUHKEZKrkMGLE/Dzb/EdAp2OVkiqPOxOQ+EA856PHzmosXhA2c8FS5/KNmW6xNz7gr/df6S8JHZ94evnn5T+Lf422+d90j4ze9+K1dZq11igEoxVObDwqTzoEVnPeyww8LBBx+szAllv/7661XhP/OZz5RNSEwvlP+ee+4p5+mdsgAWTl1MOxSBUbz7779fR6UALw7Y0YwZMxQ0KIv8MFfIh2DGnXfeWdPx25tsbJ4AWFEG5XOQJyYeGypg6tphgIQcYRP4rmbNmlW+bkDrgWukdKBm1OOWW25R/94dd9yh2fO86Vu77rpruOSSSxqu1lA43ZvxREZOnkMGLHvrGXDRUfATMVL26+vuCB+f+1R4/5wnw0dOezQcePbC8KdbF8jwWVc45Kx/hD3PWRU+/puW8D+zbwvn3fhYeLa1N8ySbbrYa7Ac+VmSEezmZS97mbIUc66eeOKJZQf4I488ot+vvPJK9XHw3UIcMMFe97rXlUflfHiAjRLiP8I8A3w4AAry+NznPqdMjZADc4hjfgIm+M04CEXAse9HEw28YZrk82pZHdTCKmCEABZADHPExwbIwfa22morBUbKxIeFT42Dyb7/8R//UTWKOXK6T/NqAjAhK2R9/PHHhx122KH8okBGvt+lsc7CJGzes1mXOQ8ZsHylUVI6EArH8eMrl4aPz/pn2PfcxWG3ud3h/51+V7jhtrvUZ/WBMx8NH539QPivMx8L7z15XjjymsdZqTycOEPWzdLo6oSt+KF/QAu2AgDApnB0z5kjW7JLmeZ0N1+UjRISm8Vo35133ql1itcHt9FFTK+TTz5ZmZDFeOEY33vvvfU+fFgAFmUZw+JelAaABhANsDwDWrRokYIbznc7YIT4sDgYAcP/ZW368Y9/XDb9YGQ42rmGc562eROc+9NistJeInHnMkX3I2vG4GIZme/IngX3+onH/jlZGm+iWX72N+9qpRoaI1HtyIhRUgYnVq5cWfViyFKaArCyJDQ6rw8JsKyj+jcdzQcs6GyHX/Jg+PCpD4T/nL0w7H/C7eGEP90ZlkjE9NOS5uNz5oX3nviv8L7TnwwHnvXv8KPf3qMjhtOO/ZnM+5X1H4hYL0Uz+/gpEy8d2Q9lm1IBGp7pkKYq1EJ8S3Z4ZfVK5L+jIP7wSut3cvFpjElxzueFTNLMOau7v2YBkT5f3w7ysvp70LLvcQyZl2EMIGn3WxvimLNGQasxw4nb6mPIBhv8ir+PYFF8WvWAOk31CsAanYCUVeshAVZapnRETDVMwtP/8pj4r54M7zn1iTD7+oXKmu5dHsLhf3wyfGjy9cKyloc9pj0aPnzyfeH0m56ReYTipJ8qC/HJSgWENcRswn6nKZMPMqVefia/V6S0eDEDOOru/URWjim4jSB6QLJzxigMoGKgillLI5bho+N9WabksQO+3sP1gGH1NBZs98QhEj5YlfLsZeTP24siZkzef2d1Ndl6ueQdMMB39a53vUtDQBixZWYDEe+YyqyJlecYLGA1Y5uv7CWB09aLp3XV8xvjCdLWfj+HMbss7sqzDVmeNNX1Sys7XhK6XhvyPEufZsiA5d/A9hYnjghzZr7Yfh+adlt42zF3hpP/viL8/vbnw0en3RTe/MtbwwdPfjjsNfPp8O7pD4dPzboz3CLxD3ffMy9c83vZ0Vem6Ni8VvI3pUrr6J45mILGDMveyKZEjC7ZkcYu7Jz99aDhv8cKa0Djy6ccY1CWPo1leUYVm3TkYfdYGfHImX+YMcuKy01b5cCi871/Ly0S308T8rLzbbI0cTiGpckLWIR7EPCLL5LAXFsXi++cz3MMBrCyVkDIs6pDvjTVNa8ATmVJmnxrv+db+aG6tPV4my8E4VmQKQYjbESlMxp4+o2Lwn5Tbg7vOPqWsO/0e8Mex90b3nny0+Gt054Mex97Z9jvVzeE0/6xXBezu/CCueHhuyUyXBhWadnu8khQGjOi7LRgQWNbXPPKUc/sM0CztPXYj02PiRXFZODrEptk3OtXjjAW2IgdNQI47oud/L7eds3HLln7vGmWZvJ5PxTlGDuLnze/bZ4m31taWrQ5aS8BD+Z5AcvqZvUxUCV/b843Aq68gDXc23w12uTB19eDpGco2Zs81G79VWzzlecV5tJYJ6JzMrpzzfU3hmdk7vMZty4Pe025M+wx45HwruNvD3v96uaw+6T7w/876vJwxt+eCwslBoto9/PnStiCLN+bLIFScZCbGWP+JDNJvIKasqQpDfWpF8MUO5RNsWyUz5zLsWJbfqZEHgQQiaXnr1d48vcKG5uyMVha2pgBNQpxSGNwjXxGBm4efGK/JKzUzsWyoL1xmbTD2Kjd5//m7VoxeA42pCMvYGUvXZxnm688aSotj9fPqphLtYv31a5vVWFK9XfcqZVy7bI19RcDzLNVWaXseMXT2hVQ8+zqk7dfDMkk9CNBcSeGzs+UDQxu/scNYak4r24Xj/qM6xeHH8z5WzjqwtvDjKufCY89L5uxCKDdeOtd4bQ5s8Pzy56TKHfp6KXlWCyS3f4y3878KtYwr/wons3t4x5G5/jYQX05z0igBWry17MQzxD8W9zu8+YdysNUGqufV1yucZ5RSqLyAS0PTl4RfTmxI532kRejol4etNPy8DLwLNHSIwPqwW9jpTwvmzM5ceJEvUZYhwGjyczMZ5tDST1Mftxjbbbrds7uB1i9zPN2SA/6fPe+tphZrznDSlf+wW/zlX8rML/y6CEX3lG1ZlaaYsfn6i0WmLbzThqjWy+3+fLKbW982/hBfU9ti8Ov584KF593SrhvnoQztKxWxztLOC0ToHpo3oPhfIlTuvDcOWHF808pWLECKA73MbKBp+/8BjaEHMSmDb8BK5tA7FkKeXAPB399ngAByofy+2F6/4CtLA9K3jdj5wEl7/8hDw8yPk9jNORdj5mR3q5bPjHwMt3Ij6R6vxbXYvAw4CAf3x7Lw66nsTcDPO41UCU95Vh+yBF5csTPwMDRM88s8Kpnzmfd56/nYVjDt81X/b34qhfx8wxqYdhPt6r3SyrXAl/s50r3t8X5pEsqbaHBGOiy06T5z+JlodP8ZZUdhAbzHOO0Q2JYeQoEFB6WbdX/8Ic/6OghsUwsxsaqBUR64++KO7gpAEriD2NNds4rvJ3zw/1c94ppsU4eWFAkgMwULM3kMGWLy7d6orSm0BajZPegxAYcXPN+NWMN8XI05Oud8Aa6MQhYvh5AYzD3Awi+LtYWS095npHaeZOVl6PVI5atZ60eBK3+9jzz+rDy9K+sNAAWK3p4Fhs/4+Hc5isbSKrNRgPLeISt/uqlicLXOurrbQlWLaH0+lWDSJ40afWrbsNQBgSynmblelMAy3cMlApmkeb/oBqmzHzHDPHKZdU0UMC8MpOGc37krgaJS0GmBi6m5F7pYAUokWcrBiyciwHL2BhtMpOQNJaPAaXdB6B5s9MDjIGVn5bjlctWebB603bygtGRr4GeySuO3/KsyzNMz8DSWCTy8iEJtIUyvD+Oc7A/5GFsy8xF7idf6mnl8t3yzOs0z9+Fa1MiiyeffFJX1/Ampe9r3DXc23ylg0010Jjvp94WX7WhB8lSywYKaUso199lJ5FNBZgrTKi+c79+mgqg1d/l2fKtgFj2rj6DedZNAaw45sdXyCuSZw4+fseU0JTBmzjej8F38ojf6qQH0LhmvhSUBmWxuCKvxHH4AfU1/48HTe8XM9ZmgEh+KK2V61kIiuJjyKhHWiiGgYWXi/e7mYlIPTxD8i8DWyvKZGB+KgNLXy9rWxqLMoDypq/3YRkAGYtiYITpRZbenhn+Mfve6AUzmE6bJy0yYWJ8o6ORw71+XFOFQdSPLapOUy9UwXZ+9tvOV9e31sdWC3TZDGu93+YrT4chjTmbvZOe86as8V+AwxSMdB6IADF7W9IZPVuJ449QZg9IrGGF0vggTn4DYlYHMym9nwbwsbey9+VQdmxmWXn85R7S86EuvlxjJeZ3ox4oOsrs2xSPbnIfQGbszZ6BH030IMZ15EldDESoB/WDHZpTHZlST5iXZ5eWv38erHVmz5W/1NfLxV4wnsV55pW336xpOpO3/a3nrB+ubb6y2Ep9czFhTwnwNfKDVUy3eFedNPYUy68WsGqBMDtNbf1qQ0Jq2VSe+g3meTeFYVEB71NKq5A3Mew6b20UCiDxo2msbBCbdNwTT4XxQ/IGGMawYCdWL/56wDHmZfXwzM78OzHbi5mKAZa12yty7Bfy9eC7Z4nGlgywvAll4EPeBtIG4sZ47LfdTzoP3lZvH8uVxrq8LDzDtfp5c9IGN0zWlt7qy3Vv+g6mgw4lrX9RxuEi1a6J9GBKz5zWZJuvemEH9fxX9YCt2jGeXmcrq9jmawg9xt7UaX6r2PFqYFHPn0KHM/PLMxD7bozFK0nsdPcKa4pjJpNvnq2xZOnjsAMzf6xOmDve1LKRM/KMRyY551lTWoS4sSjPSP3WVla+ydWU0dIQUuIZkQcdM2c9+zGWRb6EJ1Cu9zXxmzYCkD7uzMfdWTvjEUcDYiuvHssZQvca1C2+D8Y+rCSjNd/mi1xisGkUZmAgmGZSVvvB0jdlbVTnekGkMfCmmbx50sTtzG6D35txUI8uNXHTGFa9qpnfiU5PBzLF9dNmzO9hZptnXB4MvNIbAPHXVl7wb1czpewvecbBo2mswo9IeiYSsxIU3ZsiVnffXvInTWyycD7NxAWIfBwUwGH3mp/QAMEHvto5Y3mYn3YfDMzMNPOJIW9kEQdrGvBbyIKBkMmEv8jTAzV19qZ+mpzWvNs2ziF2QXiTvNllF/k3VwJNASzPouK3Wpp55B3xNJff8UijiSEtItw6pB/Kj8/FnTb+7YHJl2X19+XaW9tAwjMxqwNp4gBP74OzMrx8vGnqI9TjNnNvXAcDkxh07LyBZZocY3+Zz98/mzRQ9WzP+4x8t7Xz8VzLZnZt8x3GjD7+3cw6FHkPvwSaAljDX80ix0IChQQKCUjgcyGEQgKFBAoJjBYJFIA1Wp5UUc9CAoUECoZV9IFCAoUERo8ECoY1ep5VUdNCAuu9BArAWu+7QCGAQgKjRwIFYI2eZ1XUtJDAei+BArDW+y5QCKCQwOiRQAFYo+dZFTUtJLDeS6AArPW+CxQCKCQweiRQANboeVZFTQsJrPcSGBRgLViwIBSfQgZFHyj6wLrqA4MCrPUe3gsBFBIoJLBOJVAA1joVf1F4IYFCAoORQAFYg5FWkbaQQCGBdSqBArDWqfiLwgsJFBIYjAQKwBqMtIq0hQQKCaxTCRSAtU7FXxReSGAES+DKgyvbth185YioaAFY9R7DozPD7rI+PGuS7z7z0RHxsIpKFBJY2xJ4dObuYYRglTa9AKysHsBbZi0+MTpIAZJZD2XkXLfntTb7yNpqfbltu88MI+WVvR4B1pXh4BJj8rsT6/d6D0RY1sHDwq4eDTN3h60dHPIQ66Sj5Etb3XkHV87a6vijqxyT4ZhaZmEmkvUX/b17mHllwsabz8TX4vPVttEHS3qzFl/ajfpLAVgGYk19i6ytjra2yhldEDTY2tZjTVcevK5dBMXzXQ8BK2Yuxrzcee9sVECTt2jEicudOpefy3U05xurYVENy3V5VKXz7clZzmA1eH1LX35GXraD7Sd5nleaYCsML7EEBvt800CtVHd9KadcL/UnzxDj/l1NsIbatjXvSAVgGeUtM6w6pqNjYPamrTEt69LmuBMmb2r9lO/JKjdPHnnSrHmneeHnYHJ0LyoDsWF9XrEk6z0/A608zzcLsEIwMLKmJP250tZ6/bsCaHnq0Zxesh4ClgOLsk+rlkFVxB29WctvX3+PpamXT+UBlx+65VPXFI3f6Cl5xD6V8tvT+VMyy2lOxxrtucaDH7GS17ZvKM8ryiWF2Rl4JOCSpx9lA5YgVjICrplG6VP7S5xnnr7YnB5QAFYZtOqZigZwpesGEhGbatyhU97YNczOHnDMtOK3awpQlkFvMOU0p0O9cHLNMKPKDV2T5xVJK8U0Sx9UydMHUszZuJ/o78ipXuOW8C94K3fd9bP1ELBqR9+q3mJVPib/sIYDsBp0oqxy03wPNaCX4+36wkGUprekbCqljQIOy/NKB6z6IRJ5nm+eNGYWpoxwDgqwGoFicx5PAVgiVw9YNXFQMU1fI5Ow/gPOLLcArOZoQKNcq5S32twfnueVbRJWl5MHjCrmmhkBqfFUVYCb4qtrOGqepx7NeVzrIWCl+bDcaEzdN0wFbIbudG/wRsosN08nyZOmOR3phZmrM/diBR6W5zVUp3tjZlO3f1a1wTnOo7Zl37/u+lkBWClhC/6B4SRP808NOawh9n3UGX2sLTdPJ8mT5oUJLc1qVaPYq8b9ZOjPohowPLPLm2e1X+3gK70/zg0nleLK0kzQGtBKBbvCJGxWvyvyLSRQSKDGAk2mgY2QIPZcz2c9Yli55FEkKiQwSAmkBJQOMod1kzyNra2bmgym1AKwBiOtIm0hgRoJjD7A8u6M0cSuEH0BWIUKFhIoJDBqJFAA1qh5VEVFCwkUEigAq+gDhQQKCYwaCRSANWoeVVHRQgKFBArAKvpAIYFCAqNGAgVgjZpHVVS0kEAhgVEAWA+HT971g7Br+XN8eCjluT279Iawj0u3z7zTw7N1nu/sB493+f0gzH7y4dSUedPFNz/05G+r8qfuaWWM1HRp9arIn2dxRPjz0kWjUHvmh5bdXhVWlD8HhK6UVvTOu8KlkfSf+H7ordPa1h8eUJW29U/za1LW5Ldbern+xrZZ3y/lm52W+7r+NKu6ztLGtLqM9HRZnWpEA1YMQvVAq76CxeAWg18FCA9/5AYnq7zpasX750dOrwErq7cHrZGc7oUIWLWgYcBVDQhpCp0AXAwcMfhVgHDlrCvKHSNvub4nVdchG7Aq4ObBOPnuQWukp8sCK66PaMAyhlNhSxUg+eSDvy21b1E4fN4RChJ2zgOdB6IKSFSArLaMEPKmqxVwpS6VOo/Gc2ldJ032ebrYyEhTZkJltlQBnJYfzipV8rmw8hN7qqLbOQ84HojSGFBtGSFUGFgCPPXyq0gpHwuspK/UucIER+O5fP1kBANWRUHSQMcAwYOTZzC1QFQBjmo2FQsqb7rGSt0YKBu3bde7DFDXVbratlXM43STPF93W1epKiCQCjolEPNg4plJLRBVAMHnV9u6tHJrQdHfF5uYtcwuLiWjbWVmONLT5esbIxaw6gFRxVxJFKdeupglVae7tsov5oEub7p08TZmU2msMI2Jrft01a3zJmI9f1++7rZuUtUDoorpVct+0k2ptHSXVfnF0vxG9cy9OK2vT2tuH1ZjNpXGHtOY2LpPl69vjGLAShy/WQzLHMT1/WGJH8sUMW+6+uKt5//K608bKemshWkgnK9zjZRU2YC1Z2ib91yVuZbKsHarTVdx4Ff8R/VAy7OnWmZWYUDcPzinez1/Wl6/20hJl91jRj1ghVABiDQzMR2wDBRqlbEasOqnqy/aSp5+ZK121HKkp0taWO+FkN21Rk6KvIAVglP8FDNxRSpgmbKnMR0vA3c9xYlvYGZMZ3CAFeddAs+a0c2Rni67z7wAACuExqNatUzM+5cq9w4uXZZJuO7NujUxTyutG92+q6Qd+QErPTygwqJqGZZnShWTLkmXftQCW2yacl9+wErziY1GMzEbrEgxigGr2nSqBq3jw5/KsVC1vq5mAVa2362xGdsIPL3vqNnpKl2ndgQ2X7caWamyAatRaMMBoa0c49R4pC8fYHlQTICt1tFeG55Qz7mf3bbG5m5c53WVX94eM2IBy5t6jUYJ6zXUnO6NQyI8O7NgyPTh+xgk0sp9oQHWC8EcTJ5TvlHCen2pzHYahkTUAlFe5S8Aqx4brX0iIxiwQsgXh5UGMOnhABXzxsAp3aE82HRpoJg2+pcWrjAy0yUdJQ9I530zrut0+eKw0mKz0sGuAjJm/qWZYbU+sRCyfF2JpOqZhI3akRqHlRLWMDLT5eshIxqw8ka614sar3V0149grx6uz5eucdCpn06UfE+PzRqZ6eg+aQG0+brVyEuVN+K8bjR4jQO7fqS7HyWsHzmfPnXGJJcfsDy4NTYlG0W6pwfFrt388vSaEQ1YSQPyzSWMQavi9I7FEI/O1ZsXl50uDbCqmUkFjEbTXEKTWL325elYIzNNvijyWLErMUpxq+JRt3Rn+3DOJUyLpqdWI32OYN76ZfWbUQBYWU0orhcSKCSwvkigAKz15UkX7Swk8AKQQAFYL4CHWDShkMD6IoECsNaXJ120s5DAC0ACBWC9AB5i0YRCAuuLBArAWl+edNHOQgIvAAn8f3C9zPPTr7B1AAAAAElFTkSuQmCC" />&nbsp;&nbsp;&nbsp;</p>\r\n', 'info@go-english.vn', 'https://www.youtube.com/watch?v=EzJJS5myzws', 'https://twitter.com/', '', '', 'UA-66722689-1', 'CbODlJoQBCDkf8WJeLiqYPcVOYQPZu57Jss0X-ea86E', 'nguyennhan091@gmail.com', 'rgte4tg4eg', '107419211');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE IF NOT EXISTS `contact` (
  `id_contact` int(11) NOT NULL AUTO_INCREMENT,
  `fullnam_contact` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `company_contact` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `phone_contact` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `email_contact` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `address_contact` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `title_contact` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `content_contact` text COLLATE utf8_unicode_ci NOT NULL,
  `mobile_contact` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_contact`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=17 ;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id_contact`, `fullnam_contact`, `company_contact`, `phone_contact`, `email_contact`, `address_contact`, `title_contact`, `content_contact`, `mobile_contact`) VALUES
(14, 'sadasd', '', '', 'nguyennhan091@gmail.com', '', 'asdsadasdas', 'dsfsdfds', ''),
(15, 'sadasd', '', '', 'nguyennhan091@gmail.com', '', 'asdsadasdas', 'dsfsdfds', ''),
(16, 'sadasd', '', '', 'nguyennhan091@gmail.com', '', 'asdsadasdas', 'dsfsdfds', '');

-- --------------------------------------------------------

--
-- Table structure for table `downloadyii`
--

CREATE TABLE IF NOT EXISTS `downloadyii` (
  `id_downloadyii` int(11) NOT NULL AUTO_INCREMENT,
  `new_id` int(200) NOT NULL,
  `namexh_downloadyii` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fullnam_downloadyii` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `lastnam_downloadyii` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `namenew_downloadyii` varchar(400) COLLATE utf8_unicode_ci NOT NULL,
  `company_downloadyii` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `phone_downloadyii` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `country_downloadyii` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `areacode_downloadyii` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_downloadyii` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `address_downloadyii` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `title_downloadyii` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `content_downloadyii` text COLLATE utf8_unicode_ci NOT NULL,
  `mobile_downloadyii` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_downloadyii`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=91 ;

--
-- Dumping data for table `downloadyii`
--

INSERT INTO `downloadyii` (`id_downloadyii`, `new_id`, `namexh_downloadyii`, `fullnam_downloadyii`, `lastnam_downloadyii`, `namenew_downloadyii`, `company_downloadyii`, `phone_downloadyii`, `country_downloadyii`, `areacode_downloadyii`, `email_downloadyii`, `address_downloadyii`, `title_downloadyii`, `content_downloadyii`, `mobile_downloadyii`) VALUES
(90, 0, 'Ms.', 'Nhan', 'Nguyen', 'tittle of box this  image', 'tau', '21312', 'Việt Nam323', '321', 'nguyennhan091@gmail.com', 'AUSTRALIA', 'fasdasdasdsad', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `i18n`
--

CREATE TABLE IF NOT EXISTS `i18n` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `locale` varchar(6) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `model` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `foreign_key` int(10) NOT NULL,
  `field` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `locale` (`locale`),
  KEY `model` (`model`),
  KEY `row_id` (`foreign_key`),
  KEY `field` (`field`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8087 ;

-- --------------------------------------------------------

--
-- Table structure for table `links`
--

CREATE TABLE IF NOT EXISTS `links` (
  `id_links` int(11) NOT NULL AUTO_INCREMENT,
  `name_links` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `showhide_links` int(11) NOT NULL DEFAULT '1',
  `url_links` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `logo_links` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_links`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `links`
--

INSERT INTO `links` (`id_links`, `name_links`, `showhide_links`, `url_links`, `logo_links`) VALUES
(3, 'cong ty hn', 1, 'https://www.youtube.com/watch?v=Rf9ApGr4_TU', '/upload/doitac/2015-07-20/9128example-slide-1.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `new_id` int(20) NOT NULL AUTO_INCREMENT,
  `id_rn` int(50) NOT NULL,
  `new_title` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `new_img` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `img_file` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `new_summary` text COLLATE utf8_unicode_ci NOT NULL,
  `new_content` text COLLATE utf8_unicode_ci NOT NULL,
  `new_date` date NOT NULL,
  `id_menu` int(50) NOT NULL,
  `new_title_seo` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `new_description_seo` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `new_keywords_seo` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `new_highlights` int(2) NOT NULL DEFAULT '0',
  `new_showhide` int(2) NOT NULL DEFAULT '1',
  `new_ads` int(2) NOT NULL DEFAULT '0',
  `rewrite_url_news` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `agency` int(2) NOT NULL DEFAULT '0',
  `view` int(200) NOT NULL DEFAULT '0',
  `customer` int(2) NOT NULL DEFAULT '0',
  `yii_date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `yii_year` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `yii_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `yii_overview` text COLLATE utf8_unicode_ci NOT NULL,
  `yii_content` text COLLATE utf8_unicode_ci NOT NULL,
  `yii_wsattend` text COLLATE utf8_unicode_ci NOT NULL,
  `yii_attending` text COLLATE utf8_unicode_ci NOT NULL,
  `yii_director` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`new_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=134 ;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`new_id`, `id_rn`, `new_title`, `new_img`, `img_file`, `new_summary`, `new_content`, `new_date`, `id_menu`, `new_title_seo`, `new_description_seo`, `new_keywords_seo`, `new_highlights`, `new_showhide`, `new_ads`, `rewrite_url_news`, `agency`, `view`, `customer`, `yii_date`, `yii_year`, `yii_address`, `yii_overview`, `yii_content`, `yii_wsattend`, `yii_attending`, `yii_director`) VALUES
(129, 0, 'bIG iDEAS AND FUNDAMENTALS SO YOU CAN MASTER COMPLEX SUBJECTS AT SPEED.', '/upload/new/2015-09-09/18590new1.png', '/upload/new/2015-09-16/10085Bearings_And_Lubrication_Technology_And_Application.pdf', '', '<p>ádasdasdasdsada</p>\r\n', '2015-09-09', 26, '', '', '', 1, 1, 0, 'bIG-iDEAS-AND-FUNDAMENTALS-SO-YOU-CAN-MASTER-COMPLEX-SUBJECTS-AT-SPEED-new81530', 0, 5, 0, '23 - 25 october', '2015', 'SINGAPOR', '<p>"Be yourself, everyone else is already taken" is a famous quote by Oscar Wilde and captures the essence of this programme. Are you living a life that is in tune with your authentic self, or your ''fictional self'' (who you think the world wants you to be)? Your authentic self is the ''you'' not defined by your job title, function or role. It is the composite of all your skills, talents and personality. It is all of the things that are uniquely yours and need expression - rather than what you think you are supposed to be and do. But how we behave and communicate is influenced by many things: our upbringing, values, culture and beliefs shape us all to communicate in different ways.</p>\r\n\r\n<p>How well we are able to communicate is of great influence on our ability to adapt and survive, and on our ability to resolve almost any issue we encounter. So what if you could learn to better ''read'' and understand the intentions and needs of the people you work with? How can you establish trust so other people will willingly work with or follow you?</p>\r\n\r\n<p>Drawing from the science and practice behind Emotional Intelligence (EI), this training will allow delegates to advance in non-violent communication, discovering and connecting with purpose, the use of empathy and understanding and responding to emotional expressions of others.</p>\r\n', '<p>Self-awareness Part I<br />\r\nSelf-awareness Part II<br />\r\nBringing It All Together</p>\r\n\r\n<p> </p>\r\n', '<p>This course will benefit all business professionals, burgeoning leaders in business, professionals from NGOs and public sector.</p>\r\n\r\n<p>This course is for those who would like to supercharge their self-awareness, self-esteem and, as a result, promote their careers or businesses.</p>\r\n', '<p>Learn how to connect with your ''opposite'' personality types<br />\r\nUnderstand why and how people sub-consciously communicate in different ways<br />\r\nBecome aware of your values, your purpose and what determines who you are<br />\r\nIdentify barriers and learn to overcome these<br />\r\nIncrease your self-awareness and self-esteem 6. Present yourself more convincingly</p>\r\n', '<p>sadsadsadsadsad</p>\r\n'),
(130, 0, 'tittle of box this  image', '/upload/new/2015-09-09/20479new2.png', '', '<p>sadasdasvsavasvas</p>\r\n', '<p>asavasvasdsad</p>\r\n', '2015-09-09', 26, '', '', '', 1, 1, 0, 'tittle-of-box-this-image-new49606', 0, 1, 0, '23 - 25 october', '2015', 'SINGAPOR', '<p style="line-height: 20.8px;">"Be yourself, everyone else is already taken" is a famous quote by Oscar Wilde and captures the essence of this programme. Are you living a life that is in tune with your authentic self, or your ''fictional self'' (who you think the world wants you to be)? Your authentic self is the ''you'' not defined by your job title, function or role. It is the composite of all your skills, talents and personality. It is all of the things that are uniquely yours and need expression - rather than what you think you are supposed to be and do. But how we behave and communicate is influenced by many things: our upbringing, values, culture and beliefs shape us all to communicate in different ways.</p>\r\n\r\n<p style="line-height: 20.8px;">How well we are able to communicate is of great influence on our ability to adapt and survive, and on our ability to resolve almost any issue we encounter. So what if you could learn to better ''read'' and understand the intentions and needs of the people you work with? How can you establish trust so other people will willingly work with or follow you?</p>\r\n\r\n<p style="line-height: 20.8px;">Drawing from the science and practice behind Emotional Intelligence (EI), this training will allow delegates to advance in non-violent communication, discovering and connecting with purpose, the use of empathy and understanding and responding to emotional expressions of others.</p>\r\n', '<p style="line-height: 20.8px;">Self-awareness Part I<br />\r\nSelf-awareness Part II<br />\r\nBringing It All Together</p>\r\n', '<p style="line-height: 20.8px;">This course will benefit all business professionals, burgeoning leaders in business, professionals from NGOs and public sector.</p>\r\n\r\n<p style="line-height: 20.8px;">This course is for those who would like to supercharge their self-awareness, self-esteem and, as a result, promote their careers or businesses.</p>\r\n', '<p><span style="line-height: 20.8px;">Learn how to connect with your ''opposite'' personality types</span><br style="line-height: 20.8px;" />\r\n<span style="line-height: 20.8px;">Understand why and how people sub-consciously communicate in different ways</span><br style="line-height: 20.8px;" />\r\n<span style="line-height: 20.8px;">Become aware of your values, your purpose and what determines who you are</span><br style="line-height: 20.8px;" />\r\n<span style="line-height: 20.8px;">Identify barriers and learn to overcome these</span><br style="line-height: 20.8px;" />\r\n<span style="line-height: 20.8px;">Increase your self-awareness and self-esteem 6. Present yourself more convincingly</span></p>\r\n', '<p><span style="line-height: 20.8px;">sadsadsadsadsad</span></p>\r\n'),
(131, 0, 'tittle of box this  image', '/upload/new/2015-09-09/25809new2.png', '', '', '<p>sấccsacas</p>\r\n', '2015-09-09', 26, '', '', '', 1, 1, 0, 'tittle-of-box-this-image-new59439', 0, 0, 0, '23 - 25 october', '2015', 'SINGAPOR', '', '', '', '', ''),
(132, 0, 'tittle of box this  image', '/upload/new/2015-09-09/10807new3.png', '', '', '<p>dsvsavasva</p>\r\n', '2015-09-09', 26, '', '', '', 1, 1, 0, 'tittle-of-box-this-image-new83825', 0, 1, 0, '23 - 25 october', '2015', 'SINGAPOR', '', '', '', '', ''),
(133, 0, 'tittle of box this  image', '/upload/new/2015-09-09/13252new5.png', '', '', '<p>ádadada</p>\r\n', '2015-09-09', 26, '', '', '', 1, 1, 0, 'tittle-of-box-this-image-new5924', 0, 5, 0, '23 - 25 october', '2015', 'SINGAPOR', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `ord_id` int(20) NOT NULL AUTO_INCREMENT,
  `ord_email` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `ord_Gender` int(11) NOT NULL,
  `ord_Dateofbirth` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ord_name` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `ord_from` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `ord_phone` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `ord_date` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `ord_status` int(20) NOT NULL,
  `ord_request` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ord_id`),
  FULLTEXT KEY `ord_name` (`ord_name`),
  FULLTEXT KEY `ord_name_2` (`ord_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=35 ;

-- --------------------------------------------------------

--
-- Table structure for table `pasevent`
--

CREATE TABLE IF NOT EXISTS `pasevent` (
  `pr_id` int(20) NOT NULL AUTO_INCREMENT,
  `id_menu` int(200) NOT NULL,
  `pr_name` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `pr_img` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `pr_gia` int(50) NOT NULL,
  `pr_mota` text COLLATE utf8_unicode_ci NOT NULL,
  `pr_number` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `pr_title_seo` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `pr_description_seo` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `pr_keywords_seo` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `pr_highlights` int(50) DEFAULT NULL,
  `pr_showhide` int(2) NOT NULL DEFAULT '1',
  `pr_url_news` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `pr_type` int(2) DEFAULT NULL,
  `pr_width` int(200) NOT NULL,
  `pr_height` int(200) NOT NULL,
  `pr_summary` text COLLATE utf8_unicode_ci NOT NULL,
  `pr_code` int(200) NOT NULL,
  `pr_madein` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `pr_Manufacturer` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `pr_joinImg` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `pr_gia_cu` int(200) NOT NULL,
  `pr_tinhtrang` int(2) NOT NULL DEFAULT '1',
  `pr_khuyenmai` text COLLATE utf8_unicode_ci NOT NULL,
  `specifications` text COLLATE utf8_unicode_ci NOT NULL,
  `imgvideopr` text COLLATE utf8_unicode_ci NOT NULL,
  `filterpr` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`pr_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=277 ;

--
-- Dumping data for table `pasevent`
--

INSERT INTO `pasevent` (`pr_id`, `id_menu`, `pr_name`, `pr_img`, `pr_gia`, `pr_mota`, `pr_number`, `pr_title_seo`, `pr_description_seo`, `pr_keywords_seo`, `pr_highlights`, `pr_showhide`, `pr_url_news`, `pr_type`, `pr_width`, `pr_height`, `pr_summary`, `pr_code`, `pr_madein`, `pr_Manufacturer`, `pr_joinImg`, `pr_gia_cu`, `pr_tinhtrang`, `pr_khuyenmai`, `specifications`, `imgvideopr`, `filterpr`) VALUES
(268, 27, 'hình ảnh 1', '', 0, '', '', '', '', '', 1, 1, 'hinh-anh-1-sp62509', NULL, 0, 0, '', 1, '', '', '50', 1000000, 1, '', '', '', ''),
(269, 27, 'hình ảnh 1', '', 0, '', '', '', '', '', 1, 1, 'hinh-anh-1-sp33985', NULL, 0, 0, '', 1, '', '', '30898268', 0, 1, '', '', '', ''),
(270, 27, '2', '', 0, '', '', '', '', '', 1, 1, '2-sp86233', NULL, 0, 0, '', 1, '', '', '7787269', 0, 1, '', '', '', ''),
(271, 27, '3', '', 0, '', '', '', '', '', 1, 1, '3-sp97055', NULL, 0, 0, '', 3, '', '', '15750270', 0, 1, '', '', '', ''),
(272, 27, '4', '', 0, '', '', '', '', '', 1, 1, '4-sp6791', NULL, 0, 0, '', 4, '', '', '28276271', 0, 1, '', '', '', ''),
(273, 27, '6', '', 0, '', '', '', '', '', 1, 1, '6-sp98339', NULL, 0, 0, '', 6, '', '', '11282272', 0, 1, '', '', '', ''),
(275, 27, '32', '', 0, '', '', '', '', '', 1, 1, '32-sp41971', NULL, 0, 0, '', 7, '', '', '20886273', 0, 1, '', '', '', ''),
(276, 27, 'hình ảnh 1', '', 0, '', '', '', '', '', 1, 1, 'hinh-anh-1-sp14283', NULL, 0, 0, '', 1, '', '', '29931275', 0, 1, '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `pr_id` int(20) NOT NULL AUTO_INCREMENT,
  `id_menu` int(200) NOT NULL,
  `pr_name` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `pr_img` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `pr_gia` int(50) NOT NULL,
  `pr_mota` text COLLATE utf8_unicode_ci NOT NULL,
  `pr_number` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `pr_title_seo` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `pr_description_seo` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `pr_keywords_seo` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `pr_highlights` int(50) DEFAULT NULL,
  `pr_showhide` int(2) NOT NULL DEFAULT '1',
  `pr_url_news` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `pr_type` int(2) DEFAULT NULL,
  `pr_width` int(200) NOT NULL,
  `pr_height` int(200) NOT NULL,
  `pr_summary` text COLLATE utf8_unicode_ci NOT NULL,
  `pr_code` int(200) NOT NULL,
  `pr_madein` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `pr_Manufacturer` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `pr_joinImg` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `pr_gia_cu` int(200) NOT NULL,
  `pr_tinhtrang` int(2) NOT NULL DEFAULT '1',
  `pr_khuyenmai` text COLLATE utf8_unicode_ci NOT NULL,
  `specifications` text COLLATE utf8_unicode_ci NOT NULL,
  `imgvideopr` text COLLATE utf8_unicode_ci NOT NULL,
  `filterpr` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`pr_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=269 ;

-- --------------------------------------------------------

--
-- Table structure for table `register`
--

CREATE TABLE IF NOT EXISTS `register` (
  `id_register` int(11) NOT NULL AUTO_INCREMENT,
  `new_id` int(200) NOT NULL,
  `fullnam_register` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `lastnam_register` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `namenew_register` varchar(400) COLLATE utf8_unicode_ci NOT NULL,
  `company_register` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `phone_register` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `email_register` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `address_register` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `title_register` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `content_register` text COLLATE utf8_unicode_ci NOT NULL,
  `mobile_register` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `namexh_register` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country_register` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `areacode_register` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `yesmail_register` varchar(400) COLLATE utf8_unicode_ci NOT NULL,
  `companyaddress_register` varchar(400) COLLATE utf8_unicode_ci NOT NULL,
  `city_register` varchar(400) COLLATE utf8_unicode_ci NOT NULL,
  `postcode_register` varchar(400) COLLATE utf8_unicode_ci NOT NULL,
  `viplabel_register` varchar(400) COLLATE utf8_unicode_ci NOT NULL,
  `bannerad_register` varchar(400) COLLATE utf8_unicode_ci NOT NULL,
  `topay_register` varchar(400) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_register`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=32 ;

--
-- Dumping data for table `register`
--

INSERT INTO `register` (`id_register`, `new_id`, `fullnam_register`, `lastnam_register`, `namenew_register`, `company_register`, `phone_register`, `email_register`, `address_register`, `title_register`, `content_register`, `mobile_register`, `namexh_register`, `country_register`, `areacode_register`, `yesmail_register`, `companyaddress_register`, `city_register`, `postcode_register`, `viplabel_register`, `bannerad_register`, `topay_register`) VALUES
(28, 0, 'hhhhhhhhhhhh', 'hhhhhhhhhhh', 'tittle of box this  image', 'tau', '123123', 'nguyennhan091@gmail.com', '', 'nhan dai hiep', '', '', '', 'Việt Nam', '2323', '', 'Phường mễ trì - Quận nam từ liên - hà nội', 'Hà nội', '234234', '324324', '', ''),
(29, 0, 'Nhan', 'Nguyen', 'tittle of box this  image', 'tau', '1231', 'nguyennhan091@gmail.com', 'ARUBA', 'nhan dai hiep', '', '', 'Mrs.', 'Việt Nam', '232', '', 'Phường mễ trì - Quận nam từ liên - hà nội', 'Hà nội', '234234', '324324', 'Newsgroup post', 'Electronic bank transfer'),
(30, 0, 'Nhan', 'Nguyen', 'tittle of box this  image', 'tau', '213123213213', 'nguyennhan091@gmail.com', 'Select Country *', 'sadasd', '', '', 'Ms.', 'Việt Nam', '12312', 'yes', 'Phường mễ trì - Quận nam từ liên - hà nội', 'Hà nội', '123', '324324', 'Where did you hear about us? *', 'Secure online credit card payment'),
(31, 0, 'vvvvvvvvvv', 'vvvvvvvvvvvvv', 'tittle of box this  image', 'vvvvvvvvvvvvvv', '3324324', 'nguyennhan091@gmail.com', 'Select Country *', 'vvvvvvvvvvvv', '', '', 'Mr.', 'Việt Nam', '321', 'yes', 'Phường mễ trì - Quận nam từ liên - hà nội', 'Hà nội', '234234', '324324', 'Phone invitation', 'Secure online credit card payment');

-- --------------------------------------------------------

--
-- Table structure for table `yii_album`
--

CREATE TABLE IF NOT EXISTS `yii_album` (
  `id_album` int(200) NOT NULL AUTO_INCREMENT,
  `join_typeAlbum` int(200) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `img_album` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `showhide_album` int(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_album`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `yii_email`
--

CREATE TABLE IF NOT EXISTS `yii_email` (
  `id_email` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `fname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jobtitle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `companyname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `emailaddress` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quocgia` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `areacode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phonenumber` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id_email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `yii_email`
--

INSERT INTO `yii_email` (`id_email`, `email`, `fname`, `lname`, `jobtitle`, `companyname`, `emailaddress`, `quocgia`, `country`, `areacode`, `phonenumber`, `date`) VALUES
(3, 'hc@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-25 14:32:48');

-- --------------------------------------------------------

--
-- Table structure for table `yii_filter`
--

CREATE TABLE IF NOT EXISTS `yii_filter` (
  `id_menu` int(100) NOT NULL AUTO_INCREMENT,
  `name_menu` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `showhide_menu` int(2) NOT NULL,
  `location_menu` int(2) NOT NULL,
  `level_menu` int(50) NOT NULL DEFAULT '0',
  `order_menu` int(50) NOT NULL,
  `newwindow_mennu` int(10) NOT NULL DEFAULT '0',
  `home_menu` int(11) NOT NULL DEFAULT '0',
  `rewrite_url_menu` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `url_menu` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `title_seo_menu` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `description_seo_menu` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `keywords_seo` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `highlights_menu` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_menu`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Dumping data for table `yii_filter`
--

INSERT INTO `yii_filter` (`id_menu`, `name_menu`, `showhide_menu`, `location_menu`, `level_menu`, `order_menu`, `newwindow_mennu`, `home_menu`, `rewrite_url_menu`, `url_menu`, `title_seo_menu`, `description_seo_menu`, `keywords_seo`, `highlights_menu`) VALUES
(1, 'CPU', 1, 0, 0, 1, 0, 0, 'CPU', '', '', '', '', 0),
(2, 'Core i3', 1, 0, 1, 2, 0, 0, 'Core-i3', '', '', '', '', 0),
(3, 'Core i5', 1, 0, 1, 3, 0, 0, 'Core-i5', '', '', '', '', 0),
(4, 'Hãng', 1, 0, 0, 5, 0, 0, 'Hang', '', '', '', '', 0),
(5, 'Panasonic', 1, 0, 4, 6, 0, 0, 'Panasonic', '', '', '', '', 0),
(6, 'Sony', 1, 0, 4, 4, 0, 0, 'Sony', '', '', '', '', 0),
(7, 'Kích thước màn hình', 1, 0, 0, 7, 0, 0, 'Kich-thuoc-man-hinh', '', '', '', '', 0),
(8, '32"', 1, 0, 7, 8, 0, 0, '32', '', '', '', '', 0),
(9, '50"', 1, 0, 7, 9, 0, 0, '50', '', '', '', '', 0),
(10, '100"', 1, 0, 7, 10, 0, 0, '100', '', '', '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `yii_imgproduct`
--

CREATE TABLE IF NOT EXISTS `yii_imgproduct` (
  `id_yii_imgproduct` int(200) NOT NULL AUTO_INCREMENT,
  `url_yii_imgproduct` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `showhide_yii_imgproduct` int(2) NOT NULL DEFAULT '1',
  `pr_id` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_yii_imgproduct`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=94 ;

--
-- Dumping data for table `yii_imgproduct`
--

INSERT INTO `yii_imgproduct` (`id_yii_imgproduct`, `url_yii_imgproduct`, `showhide_yii_imgproduct`, `pr_id`) VALUES
(60, '/upload/sanpham/2015-08-08/diendanbaclieu.net_3_.jpg', 1, '16921257'),
(61, '/upload/sanpham/2015-08-08/diendanbaclieu.net_2_.jpg', 1, '16921257'),
(63, '/upload/sanpham/2015-08-08/diendanbaclieu.net_4_.jpg', 1, '16921257'),
(64, '/upload/sanpham/2015-08-17/hotline.jpg', 1, '15022258'),
(65, '/upload/sanpham/2015-08-17/hotline_1377411.jpg', 1, '15022258'),
(66, '/upload/sanpham/2015-08-17/hotline_1201020.jpg', 1, '30282258'),
(67, '/upload/sanpham/2015-08-17/hotline_1240967.jpg', 1, '30282258'),
(68, '/upload/sanpham/2015-08-20/xe_dien_xmen_khang_dinh_dang_cap_2_.jpg', 1, '2364258'),
(69, '/upload/sanpham/2015-08-20/xe_dien_xmen_khang_dinh_dang_cap_5_.jpg', 1, '2364258'),
(70, '/upload/sanpham/2015-08-25/diendanbaclieu.net_3_.jpg', 1, '21517261'),
(71, '/upload/sanpham/2015-08-27/vforum.vn_1_.jpg', 1, '275600'),
(72, '/upload/sanpham/2015-08-27/vforum.vn_1__1336151.jpg', 1, '3398263'),
(73, '/upload/sanpham/2015-08-27/vforum.vn_1__1961486.jpg', 1, '3976263'),
(74, '/upload/sanpham/2015-08-27/diendanbaclieu.net_3_.jpg', 1, '3976263'),
(75, '/upload/sanpham/2015-08-27/vforum.vn_1__1788086.jpg', 1, '4835263'),
(76, '/upload/sanpham/2015-09-10/event2.png', 1, '16470'),
(77, '/upload/sanpham/2015-09-10/event4.png', 1, '16470'),
(78, '/upload/sanpham/2015-09-10/event2_1904663.png', 1, '50'),
(82, '/upload/sanpham/2015-09-11/event2.png', 1, '26182268'),
(83, '/upload/sanpham/2015-09-11/event2_1745025.png', 1, '9524268'),
(84, '/upload/sanpham/2015-09-11/event2_116236.png', 1, '5209268'),
(86, '/upload/sanpham/2015-09-11/event2_1482666.png', 1, '7787269'),
(87, '/upload/sanpham/2015-09-11/event2_1874115.png', 1, '11282272'),
(88, '/upload/sanpham/2015-09-11/event4.png', 1, '15750270'),
(89, '/upload/sanpham/2015-09-11/event2_1119873.png', 1, '28276271'),
(90, '/upload/sanpham/2015-09-11/new3.png', 1, '10047273'),
(91, '/upload/sanpham/2015-09-11/vent.png', 1, '30898268'),
(92, '/upload/sanpham/2015-09-11/slide.png', 1, '20886273'),
(93, '/upload/sanpham/2015-09-14/event4.png', 1, '29931275');

-- --------------------------------------------------------

--
-- Table structure for table `yii_menu`
--

CREATE TABLE IF NOT EXISTS `yii_menu` (
  `id_menu` int(100) NOT NULL AUTO_INCREMENT,
  `name_menu` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `showhide_menu` int(2) NOT NULL,
  `location_menu` int(2) NOT NULL,
  `level_menu` int(50) NOT NULL DEFAULT '0',
  `order_menu` int(50) NOT NULL,
  `newwindow_mennu` int(10) NOT NULL DEFAULT '0',
  `home_menu` int(11) NOT NULL DEFAULT '0',
  `rewrite_url_menu` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `url_menu` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `title_seo_menu` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `description_seo_menu` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `keywords_seo` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `highlights_menu` int(2) NOT NULL DEFAULT '0',
  `h_rightvd` int(2) NOT NULL DEFAULT '0',
  `img_represent` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `img_background` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_menu`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=49 ;

--
-- Dumping data for table `yii_menu`
--

INSERT INTO `yii_menu` (`id_menu`, `name_menu`, `showhide_menu`, `location_menu`, `level_menu`, `order_menu`, `newwindow_mennu`, `home_menu`, `rewrite_url_menu`, `url_menu`, `title_seo_menu`, `description_seo_menu`, `keywords_seo`, `highlights_menu`, `h_rightvd`, `img_represent`, `img_background`) VALUES
(25, 'Home', 1, 1, 0, 1, 0, 0, 'Home-sp39255', 'http://tienganhpd.com/', '', '', '', 0, 0, '/upload/imgdaidienmenu/2015-09-09/18188iconhome.png', ''),
(26, 'Events', 1, 1, 0, 2, 0, 0, 'Events-sp58505', '', '', '', '', 0, 0, '/upload/imgdaidienmenu/2015-09-09/15584icon_event.png', ''),
(27, 'Past Events', 1, 1, 0, 3, 0, 0, 'Past-Events-sp36133', '/images/past-events.html', '', '', '', 0, 0, '/upload/imgdaidienmenu/2015-09-09/20062icon_pas.png', ''),
(28, 'Admininstration', 1, 5, 0, 4, 0, 0, 'Admininstration-sp89361', '', '', '', '', 1, 0, '', ''),
(29, 'Audit', 1, 5, 0, 6, 0, 0, 'Audit-sp99743', '', '', '', '', 1, 0, '', ''),
(30, 'Business', 1, 5, 0, 7, 0, 0, 'Business-sp90982', '', '', '', '', 1, 0, '', ''),
(31, 'Leadership & Self Development', 1, 5, 0, 8, 0, 0, 'Leadership-Self-Development-sp84405', '', '', '', '', 1, 0, '', ''),
(32, 'Education', 1, 5, 0, 9, 0, 0, 'Education-sp47250', '', '', '', '', 1, 0, '', ''),
(33, 'Food', 1, 5, 0, 10, 0, 0, 'Food-sp83276', '', '', '', '', 1, 0, '', ''),
(34, 'Goverment', 1, 5, 0, 12, 0, 0, 'Goverment-sp77298', '', '', '', '', 1, 0, '', ''),
(35, 'Heathcare', 1, 5, 0, 13, 0, 0, 'Heathcare-sp49091', '', '', '', '', 1, 0, '', ''),
(36, 'Human Resources', 1, 5, 0, 14, 0, 0, 'Human-Resources-sp18958', '', '', '', '', 1, 0, '', ''),
(37, 'ICT & Telecom', 1, 5, 0, 15, 0, 0, 'ICT-Telecom-sp47794', '', '', '', '', 1, 0, '', ''),
(38, 'Industrial', 1, 5, 0, 16, 0, 0, 'Industrial-sp91162', '', '', '', '', 1, 0, '', ''),
(39, 'Legal', 1, 5, 0, 17, 0, 0, 'Legal-sp99343', '', '', '', '', 1, 0, '', ''),
(40, 'Marketing, Sales & PR', 1, 5, 0, 18, 0, 0, 'Marketing-Sales-PR-sp64593', '', '', '', '', 1, 0, '', ''),
(41, 'Oil & Gas', 1, 5, 0, 19, 0, 0, 'Oil-Gas-sp81491', '', '', '', '', 1, 0, '', ''),
(42, 'Projects Management', 1, 5, 0, 20, 0, 0, 'Projects-Management-sp34265', '', '', '', '', 1, 0, '', ''),
(43, 'Real Estate & Construction', 1, 5, 0, 21, 0, 0, 'Real-Estate-Construction-sp84161', '', '', '', '', 1, 0, '', ''),
(44, 'Shipping', 1, 5, 0, 22, 0, 0, 'Shipping-sp45639', '', '', '', '', 1, 0, '', ''),
(45, 'Supply Chain Management', 1, 5, 0, 23, 0, 0, 'Supply-Chain-Management-sp32642', '', '', '', '', 1, 0, '', ''),
(46, 'Finance', 1, 5, 0, 24, 0, 0, 'Finance-sp83108', '', '', '', '', 1, 0, '', ''),
(47, 'Engineering & Maintenance', 1, 5, 0, 25, 0, 0, 'Engineering-Maintenance-sp7441', '', '', '', '', 1, 0, '', ''),
(48, 'LIST COMMENTS', 1, 2, 0, 26, 0, 0, 'LIST-COMMENTS-sp14280', '', '', '', '', 0, 0, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `yii_role`
--

CREATE TABLE IF NOT EXISTS `yii_role` (
  `id_role` int(200) NOT NULL AUTO_INCREMENT,
  `name_role` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `type_role` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_role`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `yii_role`
--

INSERT INTO `yii_role` (`id_role`, `name_role`, `type_role`) VALUES
(7, 'admin', '[["Configure","view"],["Configure","create"],["Configure","update"],["News","create"],["News","update"],["News","delete"],["News","admin"],["News","view"],["Administrator","admin"],["Administrator","view"],["Administrator","create"],["Administrator","update"],["Administrator","delete"],["YiiRole","admin"],["YiiRole","view"],["YiiRole","create"],["YiiRole","update"],["YiiRole","delete"],["Product","admin"],["Product","view"],["Product","create"],["Product","update"],["Product","delete"],["Advertise","admin"],["Comment","admin"],["Comment","view"],["Comment","create"],["Comment","update"],["Comment","delete"],["Pasevent","admin"],["Pasevent","view"],["Pasevent","create"],["Pasevent","update"],["Pasevent","delete"],["Advertise","view"],["Advertise","create"],["Advertise","update"],["Advertise","delete"],["YiiMenu","admin"],["YiiMenu","view"],["YiiMenu","create"],["YiiMenu","update"],["YiiMenu","delete"],["Links","admin"],["Links","view"],["Links","create"],["Links","update"],["Links","delete"],["Contact","admin"],["Contact","view"],["Contact","create"],["Contact","update"],["Contact","delete"],["Orders","admin"],["Orders","view"],["Orders","create"],["Orders","update"],["Orders","delete"],["YiiMenu","ajaxUpdate"],["YiiMenu","updateAjax"],["Contact","ajaxUpdate"],["Advertise","ajaxUpdate"],["Orders","ajaxUpdate"],["Orders","StatusOrder"],["News","ajaxUpdate"],["Product","ajaxUpdate"],["Administrator","ajaxUpdate"],["Register","create"],["Register","update"],["Register","delete"],["Register","admin"],["Register","view"],["Register","ajaxUpdate"],["Downloadyii","create"],["Downloadyii","update"],["Downloadyii","delete"],["Downloadyii","admin"],["Downloadyii","view"],["Downloadyii","ajaxUpdate"],["YiiTypealbum","admin"],["YiiTypealbum","create"],["YiiTypealbum","update"],["YiiTypealbum","delete"],["YiiTypevideo","admin"],["YiiTypevideo","create"],["YiiTypevideo","update"],["YiiTypevideo","delete"],["YiiVideo","admin"],["YiiVideo","view"],["YiiVideo","create"],["YiiVideo","update"],["YiiVideo","delete"],["YiiAlbum","admin"],["YiiAlbum","create"],["YiiAlbum","update"],["YiiAlbum","delete"]]');

-- --------------------------------------------------------

--
-- Table structure for table `yii_typealbum`
--

CREATE TABLE IF NOT EXISTS `yii_typealbum` (
  `id_typealbum` int(200) NOT NULL AUTO_INCREMENT,
  `id_menu` int(200) NOT NULL,
  `name_typealbum` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `showhide_typealbum` int(2) NOT NULL DEFAULT '1',
  `title_seo_album` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `description_seo_album` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `keywords_seo_album` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `join_typeAlbum` int(200) NOT NULL,
  `url_seo_typealbum` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `highlights_typealbum` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_typealbum`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `yii_typealbum`
--

INSERT INTO `yii_typealbum` (`id_typealbum`, `id_menu`, `name_typealbum`, `showhide_typealbum`, `title_seo_album`, `description_seo_album`, `keywords_seo_album`, `join_typeAlbum`, `url_seo_typealbum`, `highlights_typealbum`) VALUES
(1, 0, 'album1', 1, '', '', '', 9947, 'album1-al49771', 0),
(4, 0, 'test', 1, 'gdv', 'ssa', 'đá', 165491, 'test-al74664', 0);

-- --------------------------------------------------------

--
-- Table structure for table `yii_typevideo`
--

CREATE TABLE IF NOT EXISTS `yii_typevideo` (
  `id_typevideo` int(200) NOT NULL AUTO_INCREMENT,
  `name__typevideo` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `showhide__typevideo` int(2) NOT NULL DEFAULT '1',
  `url_typevideo` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `title_typevideo` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `keywords_typevideo` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `description_typevideo` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `date_typevideo` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_typevideo`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `yii_typevideo`
--

INSERT INTO `yii_typevideo` (`id_typevideo`, `name__typevideo`, `showhide__typevideo`, `url_typevideo`, `title_typevideo`, `keywords_typevideo`, `description_typevideo`, `date_typevideo`) VALUES
(1, 'jtjtnjthgh', 1, 'jtjtnjthgh', 'hhhhhhhh', 'hhhhfh', 'trnth', ''),
(2, 'tại sao vì thế vì nó là thế vậy', 1, 'tai-sao-vi-the-vi-no-la-the-vay', 'tại sao vì thế vì nó là thế vậy', 'tại sao vì thế vì nó là thế vậy', 'tại sao vì thế vì nó là thế vậy', '28-07-2015');

-- --------------------------------------------------------

--
-- Table structure for table `yii_video`
--

CREATE TABLE IF NOT EXISTS `yii_video` (
  `id_video` int(200) NOT NULL AUTO_INCREMENT,
  `name_video` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `showhide_video` int(2) NOT NULL DEFAULT '1',
  `url_video` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `title_video` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `keywords_video` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `description_video` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `id_typevideo` int(200) NOT NULL,
  `date_video` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `highlights_video` int(2) NOT NULL DEFAULT '0',
  `url_seo_video` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_video`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `yii_video`
--

INSERT INTO `yii_video` (`id_video`, `name_video`, `showhide_video`, `url_video`, `title_video`, `keywords_video`, `description_video`, `id_typevideo`, `date_video`, `highlights_video`, `url_seo_video`) VALUES
(1, 'em làm gì đây', 1, 'https://www.youtube.com/embed/43sCaavC5kM', 'dsf', 'sdfsdf', 'dsfsdf', 2, '28-07-2015', 0, 'em-lam-gi-day'),
(2, 'Những Ca Khúc Buồn', 1, 'https://www.youtube.com/watch?v=2jPj_JmKEPc', '', '', '', 1, '28-07-2015', 1, 'Nhung-Ca-Khuc-Buon'),
(3, 'GƯƠNG MẶT THÂN QUEN', 1, 'https://www.youtube.com/watch?v=qyT1sdF1fPI', '', '', '', 2, '29-07-2015', 0, 'GUONG-MAT-THAN-QUEN'),
(4, 'Những Ca Khúc Buồn Và Tâm Trạng', 1, 'https://www.youtube.com/watch?v=9JsgrIe84-8', '', '', '', 2, '29-07-2015', 0, 'Nhung-Ca-Khuc-Buon-Va-Tam-Trang'),
(5, 'Hồ Việt Trung MV ', 1, 'https://www.youtube.com/watch?v=5enKLOapKuY', '', '', '', 2, '29-07-2015', 0, 'Ho-Viet-Trung-MV'),
(6, 'Nối vòng tay lớn Thúy Khanh', 1, 'https://www.youtube.com/watch?v=yFIejwp1TG0', '', '', '', 2, '29-07-2015', 0, 'Noi-vong-tay-lon-Thuy-Khanh'),
(7, 'Taylor Swift', 1, 'https://www.youtube.com/watch?v=QcIy9NiNbmo', '', '', '', 2, '31-07-2015', 0, 'Taylor-Swift');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
