$(function(){	
	
	 $( "#datestart" ).datepicker({
	        defaultDate: "+1w",
	        numberOfMonths: 2,
	        dateFormat : 'yy-mm-dd',
	        showOn: "button",
		      buttonImage: "/js/admin/calendar.gif",
		      buttonImageOnly: true,
		      buttonText: "Select date",
			changeMonth: true,
			changeYear: true,
	        onClose: function( selectedDate ) {
	          $( "#dateend" ).datepicker( "option", "minDate", selectedDate );
	        }
	      });
	      $( "#dateend" ).datepicker({
	        defaultDate: "+1w",
	        dateFormat : 'yy-mm-dd',
	        showOn: "button",
		      buttonImage: "/js/admin/calendar.gif",
		      buttonImageOnly: true,
		      buttonText: "Select date",
			changeMonth: true,
			changeYear: true,
	        numberOfMonths: 2,
	        onClose: function( selectedDate ) {
	          $( "#datestart" ).datepicker( "option", "maxDate", selectedDate );
	        }
	      });
	      
	$("input[id=LoginForm_username]").focus();
	
	$('#Orders_ord_status').change(function(){
		$('#icon_ajax').show();
		var status_order = $('#Orders_ord_status').val();
		var id_order = $('#id_order').val();
		 $.post('/admin.php/Orders/StatusOrder/status_order/'+status_order+'/id_order/'+id_order,function(data){
				if(data == 1){
			       alert('Thực hiện thành công');
			       $('#icon_ajax').hide();
				}
		  })
	})
	
	$('#sendEmail').click(function(){
		$('#icon_ajax').show();
		$.post('admin.php?r=Orders/Email',function(data){
			if(data == 1){
		       alert('Gửi email thành công');
			$('#icon_ajax').hide();
			}
	  })
	})
	
	$('.deleteimg').click(function(){
		var confirms = confirm("Bạn có muốn xóa ảnh được chọn?")
		if(confirms == true){
			return true;
		}else
			return false;
	});
	/*$('.deleteimg').click(function(){
		
		var ids = $(this).attr('id');
		var confirms = confirm("Bạn có muốn xóa ảnh được chọn?")
		if(confirms == true){
			$('#icon_ajax').show();
		$.post('/admin.php/YiiAlbum/Delete/id/'+ids,function(data){
			if(data == 1){
		       alert('Xóa thành công');
			$('#icon_ajax').hide();
			location.reload();
			}
	     })
		}
	})
	 
	$('.showhidess').click(function(){
		
		var ids = $(this).attr('id');

			$('#icon_ajax').show();
		  $.post('/admin.php/YiiAlbum/Update/id/'+ids,function(data){
			if(data == 1){
			$('#icon_ajax').hide();
			location.reload();
			}
	     })	
	})*/
	
$('.deleteimg_pro').click(function(){
		
		var ids = $(this).attr('id');
		var confirms = confirm("Bạn có muốn xóa ảnh được chọn?")
		if(confirms == true){
			$('#icon_ajax').show();
		$.post('/admin.php/Imgproduct/Delete/id/'+ids,function(data){
			if(data == 1){
		       alert('Xóa thành công');
			$('#icon_ajax').hide();
			location.reload();
			}
	     })
		}
	})
	 
	$('.showhides_pro').click(function(){
		
		var ids = $(this).attr('id');

			$('#icon_ajax').show();
		  $.post('/admin.php/Imgproduct/Update/id/'+ids,function(data){
			if(data == 1){
			$('#icon_ajax').hide();
			location.reload();
			}
	     })	
	})
	
	$('i.links').click(function(){
		var value = $(this).attr('id');
		prompt("Đường dẫn tin tức", value);
		return;
	})
	
	$("#checkallinput").click(function(){
		if($('#checkallinput').prop('checked')==true)
		    $('.rolesd').prop('checked',true);
		else
			 $('.rolesd').prop('checked',false);
	})
	
	if($('#configure_input').val() == 'configure-view')
		$("#configure_ads").addClass('active');
		
})