<?php
class LanguageSelector extends Controller
{
    public function run()
    { 
        $currentLang = Yii::app()->language;
        $languages = Yii::app()->params->languages;
        
   
        
    /* foreach($languages as $key=>$lang) {
            echo CHtml::hiddenField(
                $key, 
                $this->createMultilanguageReturnUrl($key));
        }
        die; */
        $this->render('languageSelector', array('currentLang' => $currentLang, 'languages'=>$languages));
    }
}
?>