<?php
$news = $this->getData();
?> 
<div id="news">	
<div id="slider2">
		<a class="buttons prev" href="#">&#60;</a>
		<div class="viewport">
			<ul class="overview">
				<li><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/home/images/logo.jpg" /></li>
				<li><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/home/images/logo.jpg" /></li>
				<li><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/home/images/logo.jpg" /></li>
				<li><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/home/images/logo.jpg" /></li>
				<li><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/home/images/logo.jpg" /></li>
				<li><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/home/images/logo.jpg" /></li>
				<li><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/home/images/logo.jpg" /></li>
				<li><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/home/images/logo.jpg" /></li>
				<li><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/home/images/logo.jpg" /></li>
				<li><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/home/images/logo.jpg" /></li>
				<li><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/home/images/logo.jpg" /></li>
			</ul>
		</div>
		<a class="buttons next" href="#">&#62;</a>
	</div>
		<div class="block-news block-left">
		<h4><span>SẢN PHẨM BÁN CHẠY</span></h4>
		
			<ul>	
				<li>
					<a href="#"><img class="thumb" src="<?php echo Yii::app()->request->baseUrl; ?>/images/home/images/1.jpg"></a>
					<div class="block-content-left">
							<h5><a href="#">Tiêu đề sản phẩm 1</a></h5>
							<div class="price">
								<span class="new-block">12.000.000$</span>
							</div>
							<div class="rating"><img src="http://www.kenshoping.com/wp-content/themes/kenshop/images/stars.png"></div>
					</div>
				</li>
				
				
				<li>
					<a href="#"><img class="thumb" src="<?php echo Yii::app()->request->baseUrl; ?>/images/home/images/2.jpg"></a>
					<div class="block-content-left">
							<h5><a href="#">Tiêu đề sản phẩm 2</a></h5>
							<div class="price">
								<span class="new-block">12.000.000$</span>
							</div>
							<div class="rating"><img src="http://www.kenshoping.com/wp-content/themes/kenshop/images/stars.png"></div>
					</div>
				</li>
				
				
				<li>
					<a href="#"><img class="thumb" src="<?php echo Yii::app()->request->baseUrl; ?>/images/home/images/3.jpg"></a>
					<div class="block-content-left">
							<h5><a href="#">Tiêu đề sản phẩm 3</a></h5>
							<div class="price">
								<span class="new-block">12.000.000$</span>
							</div>
							<div class="rating"><img src="http://www.kenshoping.com/wp-content/themes/kenshop/images/stars.png"></div>
					</div>
				</li>
				
				<li>
					<a href="#"><img class="thumb" src="<?php echo Yii::app()->request->baseUrl; ?>/images/home/images/4.jpg"></a>
					<div class="block-content-left">
							<h5><a href="#">Tiêu đề sản phẩm 4</a></h5>
							<div class="price">
								<span class="new-block">12.000.000$</span>
							</div>
							<div class="rating"><img src="http://www.kenshoping.com/wp-content/themes/kenshop/images/stars.png"></div>
					</div>
				</li>
				
			</ul>
		<a class="more" href="#">» Xem thêm</a>
		</div>
		
		<div class="block-news block-left">
		<h4><span>TIN TỨC</span></h4>
		   
			<ul>	
			  <?php
				 foreach ($news as $key=>$value) {
				 	if($key == 0){
				 ?>	
				 
				<li class="imgactive">
					<a class="imgactive" href="<?php echo Yii::app()->urlManager->createUrl('/site/detailnews', array('id' => $value->new_id)) ?>"><img class="thumb imgactive" src="<?php echo Yii::app()->request->baseUrl.$value->new_img; ?>" width="150" height="150"></a>
					<div class="block-content-left">
							<h5><a href="<?php echo Yii::app()->urlManager->createUrl('/site/detailnews', array('id' => $value->new_id)) ?>"><?php echo $value->new_title ?></a></h5>
							<p class="info"><?php echo $value->new_summary ?></p>
							<span class="date"><?php echo date("d-m-Y",strtotime($value->new_date)) ?></span>
							
					</div>
				</li>
				  <?php }else{ ?>
				      <li>
						<a href="<?php echo Yii::app()->urlManager->createUrl('/site/detailnews', array('id' => $value->new_id)) ?>"><img class="thumb" src="<?php echo Yii::app()->request->baseUrl.$value->new_img; ?>" width="84" height="84"></a>
						<div class="block-content-left">
								<h5><a href="<?php echo Yii::app()->urlManager->createUrl('/site/detailnews', array('id' => $value->new_id)) ?>"><?php echo $value->new_title ?></a></h5>
								<div style="margin-top: 15px;"><span class="date"><?php echo date("d-m-Y",strtotime($value->new_date)) ?></span></div>
								
						  </div>
				     </li>
				  <?php }?>
               <?php } ?>
			</ul>
		
		</div>
		
		<div class="block-news block-right">
		<h4><span>VỀ TÔI</span></h4>
		<div class="huykira">
			<a style="float: left;" href="http://huykira.net/"><img class="avt" src="http://huykira.net/wp-content/themes/Huy/images/huykira.jpg"></a>
			<div class="hk"> <p >Pro hóa học, thích kinh tế, đậu sư phạm, làm design, tối về mò code... :))  </p> </div>
		</div>
		<h4><span>LIÊN HỆ</span></h4>
		<div class="contact">
			<ul>
				<li> <p> <b class="b"><i class="d fa fa-map-marker"></i>&nbsp;  Địa chỉ:</b> Số 02 Quang trung, Đà Nẵng, Việt Nam</p> </li>
				<li> <p> <b class="b"><i class="d fa fa-phone-square"></i>&nbsp;  Số điện thoại:</b> 01234567810</p> </li>
				<li> <p> <b class="b"><i class="d fa fa-envelope-o"></i>&nbsp;  Thư điện tử:</b> huykira@gmail.com</p> </li>
				<li> <p> <b class="b"><i class="d fa fa-bar-chart"></i>&nbsp;  website:</b> http://huykira.net/</p> </li>
			</ul>
		</div>
		
		<ul class="social-block ">
		<li class="facebook">
			<a class="_blank" href="https://www.facebook.com/huykra" target="_blank"><i class="fa fa-facebook"></i></a>
		</li>		
		<li class="twitter">
			<a class="_blank" href="https://twitter.com/huykira" target="_blank"><i class="fa fa-twitter"></i></a>
		</li>		
		<li class="rss">
			<a class="_blank" href="#" target="_blank"><i class="fa fa-rss"></i></a>
		</li>		
		<li class="youtube">
			<a class="_blank" href="#" target="_blank"><i class="fa fa-youtube"></i></a>
		</li>		
		<li class="google_plus">
			<a class="_blank" href="https://plus.google.com/+huykira" target="_blank"><i class="fa fa-google"></i></a>
		</li>		
		<li class="pinterest">
			<a class="_blank" href="#" target="_blank"><i class="fa fa-pinterest"></i></a>
		</li>					
		</ul>
		
		</div>
		
		
	</div>