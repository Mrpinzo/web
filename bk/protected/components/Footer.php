<?php

Yii::import('zii.widgets.CPortlet');

class Footer extends CPortlet {
	
    protected function renderContent() {
    	$criteria2            = new CDbCriteria();
    	$criteria2->condition = 'location_menu=3 AND showhide_menu=1';
    	$criteria2->order     = 'order_menu ASC';
    	$items = YiiMenu::model()->findAll($criteria2);
    	
    	$Configure = Configure::model()->findAll();
        $this->render('home/footer',array('Configure'=>$Configure,'items'=>$items));
    }
}
?>

