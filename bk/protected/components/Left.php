<?php

Yii::import('zii.widgets.CPortlet');

class Left extends CPortlet {
	
    protected function renderContent() {
    	$criteria2 = new CDbCriteria();
    	$criteria2->condition = 'location_menu=2 AND showhide_menu=1';
    	$criteria2->order = 'order_menu DESC';
    	$menu_left = YiiMenu::model()->findAll($criteria2);
    	
    	$doitac = new CDbCriteria();
    	$doitac->condition = 'showhide_links=1';
    	$doitac->order = 'id_links DESC';
    	$doitacs = Links::model()->findAll($doitac);

        $this->render('home/left',array('menu_left'=>$menu_left,'doitacs'=>$doitacs));
    }
}
?>