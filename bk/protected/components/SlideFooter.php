 <?php

Yii::import('zii.widgets.CPortlet');

class SlideFooter extends CPortlet {
	
    protected function renderContent() {
    	$criteria2s = new CDbCriteria();
        $criteria2s->condition = 'pr_showhide=1 AND pr_highlights=1';
        $pr_highlights = Product::model()->findAll($criteria2s);
    	  	
        $this->render('home/slideFooter',array('pr_highlights'=>$pr_highlights));
    }
}
?>               