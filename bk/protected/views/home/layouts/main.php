<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width" />
<meta name="keywords" content="<?php echo CHtml::encode($this->keywords); ?>" />
<meta name="description" content="<?php echo CHtml::encode($this->description); ?>" />
<title><?php echo CHtml::encode($this->pageTitle); ?></title>
<link href="<?php echo Yii::app()->request->baseUrl; ?>/images/home/images/logo.png" type="images/png" rel="icon"/>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/home/js/jquery-2.1.4.min.js"></script>
<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" rel="stylesheet" type="text/css" />
<link type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/font/font-awesome/css/font-awesome.css" rel="stylesheet"> 
 <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />   
<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/home/csslaptop/style.css" rel="stylesheet" type="text/css" />
 <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/js/home/jslaptop/engine1/style.css" />
<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/home/csslaptop/styledetail.css" rel="stylesheet" type="text/css" />
<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/home/csslaptop/responsive.css" type="text/css" media="screen" rel="stylesheet"> 
 <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/home/csslaptop/jquery.mmenu.all.css" rel="stylesheet">

<link rel="stylesheet" href="/js/scrollbar/jquery.mCustomScrollbar.css">           
<?php echo $this->alexas; ?>
</head>
<body>


     <div id="main" style="position: relative;">
    
     
            <?php $this->widget('top'); ?>
    
            <?php echo $content; ?>     
       
            <?php $this->widget('footer'); ?>
    </div>

</body>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/home/main.js"></script> 
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/home/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/home/jquery.mousewheel-3.0.4.pack.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/home/jquery.fancybox-1.3.4.pack.js"></script>
 
 <script src="/js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
	
	<script>
		(function($){
			$(window).load(function(){
				
				$("#content-3").mCustomScrollbar({
					scrollButtons:{enable:true},
					theme:"light-thick",
					scrollbarPosition:"outside"
				});
				
				$("#content-4").mCustomScrollbar({
					theme:"rounded-dots",
					scrollInertia:400
				});
				
				$("#content-5").mCustomScrollbar({
					axis:"x",
					theme:"dark-thin",
					autoExpandScrollbar:true,
					advanced:{autoExpandHorizontalScroll:true}
				});
				
				$("#content-6").mCustomScrollbar({
					axis:"x",
					theme:"light-3",
					advanced:{autoExpandHorizontalScroll:true}
				});
				
				$("#content-7").mCustomScrollbar({
					scrollButtons:{enable:true},
					theme:"3d-thick"
				});
				
				$("#content-8").mCustomScrollbar({
					axis:"yx",
					scrollButtons:{enable:true},
					theme:"3d",
					scrollbarPosition:"outside"
				});
				
				$("#content-9").mCustomScrollbar({
					scrollButtons:{enable:true,scrollType:"stepped"},
					keyboard:{scrollType:"stepped"},
					mouseWheel:{scrollAmount:188},
					theme:"rounded-dark",
					autoExpandScrollbar:true,
					snapAmount:188,
					snapOffset:65
				});
				
			});
		})(jQuery);
	</script>

</html>
