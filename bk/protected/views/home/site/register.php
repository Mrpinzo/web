 <div id="bg_main">
    <div id="containner" >    
       <div class="items_contents">
            <div class="tnel"> <div id="catnews_title"><a>Register</a></div></div>
     
   <div id="newss">
               
<input type="hidden" name="id_menus_register" id="id_menus_register" value="menu_register"/>			

<div class="content_lienhe">

 
<div class="nnvcl">
  <div class="form_regi iirForm">

    <?php $form=$this->beginWidget('CActiveForm', array(
    	'id'=>'register-form',
    	'enableAjaxValidation'=>false,
    )); ?>
    
<div id="formPrefix">
 <div id="prefix" class="formBlockME iirForm">   
    <select name="Register[namexh_register]" id="ctl00_Content_ctl00_data_Title">
    	<option value="">Prefix *</option>
    	<option value="Mr.">Mr.</option>
    	<option value="Mrs.">Mrs.</option>
    	<option value="Ms.">Ms.</option>
    	<option value="Miss">Miss</option>
    	<option value="Eng.">Eng.</option>
    	<option value="Dr.">Dr.</option>
    	<option value="Prof.">Prof.</option>
    	<option value="Sheikh">Sheikh</option>
    	<option value="Sheikha">Sheikha</option>
    
    </select>
  </div> 
</div>    
    <div id="formFirstName">
		<?php echo $form->textField($model,'namenew_register',array('value'=>$regisv['new_title_' . $_SESSION['lang']],'size'=>60,'maxlength'=>200,'class' => 'register','readonly'=>'readonly')); ?>
		<?php echo $form->error($model,'namenew_register'); ?>
	</div>
    
	<div id="formFirstName">
	    <?php echo $form->textField($model,'fullnam_register',array('size'=>60,'maxlength'=>200,'class' => 'register','placeholder'=>($this->{$_SESSION['lang']}['firstname']))); ?>
		<?php echo $form->error($model,'fullnam_register'); ?>
	</div>
    <div id="formLastName">
	    <?php echo $form->textField($model,'lastnam_register',array('size'=>60,'maxlength'=>200,'class' => 'register','placeholder'=>($this->{$_SESSION['lang']}['lastname']) )); ?>
		<?php echo $form->error($model,'lastnam_register'); ?>
	</div>
    
   <div id="formJobTitle">
		<?php echo $form->textField($model,'title_register',array('size'=>60,'maxlength'=>200,'class' => 'register','placeholder'=>($this->{$_SESSION['lang']}['jobtitle']) )); ?>
		<?php echo $form->error($model,'title_register'); ?>
	</div>
    <div id="formCompany">
	    <?php echo $form->textField($model,'company_register',array('size'=>60,'maxlength'=>200,'class' => 'register','placeholder'=>($this->{$_SESSION['lang']}['company']) )); ?>
		<?php echo $form->error($model,'company_register'); ?>
	</div>
	<div id="formEmail">
	    <?php echo $form->textField($model,'email_register',array('size'=>60,'maxlength'=>200,'class' => 'register','placeholder'=>($this->{$_SESSION['lang']}['emailaddress']) )); ?>
		<?php echo $form->error($model,'email_register'); ?>
	</div>
    
    <div id="formCountry">
        <div id="country" class="formBlockME iirForm">
            <select name="Register[address_register]" id="ctl00_Content_ctl00_data_Country">
            	<option value=""><?php print_r($this->{$_SESSION['lang']}['selectcountry']); ?></option>
            	<option value="AFGHANISTAN">AFGHANISTAN</option>
            	<option value="ALBANIA">ALBANIA</option>
            	<option value="ALGERIA">ALGERIA</option>
            	<option value="ANDORRA">ANDORRA</option>
            	<option value="ANGOLA">ANGOLA</option>
            	<option value="ANGUILLA">ANGUILLA</option>
            	<option value="ANTARCTICA">ANTARCTICA</option>
            	<option value="ARGENTINA">ARGENTINA</option>
            	<option value="ARMENIA">ARMENIA</option>
            	<option value="ARUBA">ARUBA</option>
            	<option value="AUSTRALIA">AUSTRALIA</option>
            	<option value="AUSTRIA">AUSTRIA</option>
            	<option value="AZERBAIJAN">AZERBAIJAN</option>
            	<option value="BAHAMAS">BAHAMAS</option>
            	<option value="BAHRAIN">BAHRAIN</option>
            	<option value="BANGLADESH">BANGLADESH</option>
            	<option value="BARBADOS">BARBADOS</option>
            	<option value="BELARUS">BELARUS</option>
            	<option value="BELGIUM">BELGIUM</option>
            	<option value="BELIZE">BELIZE</option>
            	<option value="BENIN">BENIN</option>
            	<option value="BERMUDA">BERMUDA</option>
            	<option value="BHUTAN">BHUTAN</option>
            	<option value="BOLIVIA">BOLIVIA</option>
            	<option value="BOTSWANA">BOTSWANA</option>
            	<option value="BOUVET ISLAND">BOUVET ISLAND</option>
            	<option value="BRAZIL">BRAZIL</option>
            	<option value="BRUNEI DARUSSALAM">BRUNEI DARUSSALAM</option>
            	<option value="BULGARIA">BULGARIA</option>
            	<option value="BURKINA FASO">BURKINA FASO</option>
            	<option value="BURUNDI">BURUNDI</option>
            	<option value="CAMBODIA">CAMBODIA</option>
            	<option value="CAMEROON">CAMEROON</option>
            	<option value="CANADA">CANADA</option>
            	<option value="CAPE VERDE">CAPE VERDE</option>
            	<option value="CHAD">CHAD</option>
            	<option value="CHILE">CHILE</option>
            	<option value="CHINA">CHINA</option>
            	<option value="COLOMBIA">COLOMBIA</option>
            	<option value="COMOROS">COMOROS</option>
            	<option value="COOK ISLANDS">COOK ISLANDS</option>
            	<option value="COSTA RICA">COSTA RICA</option>
            	<option value="CÔTE D'IVOIRE">CÔTE D'IVOIRE</option>
            	<option value="CROATIA">CROATIA</option>
            	<option value="CUBA">CUBA</option>
            	<option value="CYPRUS">CYPRUS</option>
            	<option value="CZECH REPUBLIC">CZECH REPUBLIC</option>
            	<option value="DENMARK">DENMARK</option>
            	<option value="DJIBOUTI">DJIBOUTI</option>
            	<option value="DOMINICAN REPUBLIC">DOMINICAN REPUBLIC</option>
            	<option value="ECUADOR">ECUADOR</option>
            	<option value="EGYPT">EGYPT</option>
            	<option value="EL SALVADOR">EL SALVADOR</option>
            	<option value="ERITREA">ERITREA</option>
            	<option value="ESTONIA">ESTONIA</option>
            	<option value="ETHIOPIA">ETHIOPIA</option>
            	<option value="FAROE ISLANDS">FAROE ISLANDS</option>
            	<option value="FIJI">FIJI</option>
            	<option value="FINLAND">FINLAND</option>
            	<option value="FRANCE">FRANCE</option>
            	<option value="GABON">GABON</option>
            	<option value="GAMBIA">GAMBIA</option>
            	<option value="GEORGIA">GEORGIA</option>
            	<option value="GERMANY">GERMANY</option>
            	<option value="GHANA">GHANA</option>
            	<option value="GIBRALTAR">GIBRALTAR</option>
            	<option value="GREECE">GREECE</option>
            	<option value="GREENLAND">GREENLAND</option>
            	<option value="GRENADA">GRENADA</option>
            	<option value="GUADELOUPE">GUADELOUPE</option>
            	<option value="GUAM">GUAM</option>
            	<option value="GUATEMALA">GUATEMALA</option>
            	<option value="GUINEA">GUINEA</option>
            	<option value="GUINEA-BISSAU">GUINEA-BISSAU</option>
            	<option value="GUYANA">GUYANA</option>
            	<option value="HAITI">HAITI</option>
            	<option value="HONDURAS">HONDURAS</option>
            	<option value="HONG KONG">HONG KONG</option>
            	<option value="HUNGARY">HUNGARY</option>
            	<option value="ICELAND">ICELAND</option>
            	<option value="INDIA">INDIA</option>
            	<option value="INDONESIA">INDONESIA</option>
            	<option value="IRAN">IRAN</option>
            	<option value="IRAQ">IRAQ</option>
            	<option value="IRELAND">IRELAND</option>
            	<option value="ITALY">ITALY</option>
            	<option value="JAMAICA">JAMAICA</option>
            	<option value="JAPAN">JAPAN</option>
            	<option value="JORDAN">JORDAN</option>
            	<option value="KAZAKHSTAN">KAZAKHSTAN</option>
            	<option value="KENYA">KENYA</option>
            	<option value="KIRIBATI">KIRIBATI</option>
            	<option value="SOUTH KOREA">SOUTH KOREA</option>
            	<option value="KUWAIT">KUWAIT</option>
            	<option value="KYRGYZSTAN">KYRGYZSTAN</option>
            	<option value="LATVIA">LATVIA</option>
            	<option value="LEBANON">LEBANON</option>
            	<option value="LESOTHO">LESOTHO</option>
            	<option value="LIBERIA">LIBERIA</option>
            	<option value="LIBYA">LIBYA</option>
            	<option value="LIECHTENSTEIN">LIECHTENSTEIN</option>
            	<option value="LITHUANIA">LITHUANIA</option>
            	<option value="LUXEMBOURG">LUXEMBOURG</option>
            	<option value="MACAO">MACAO</option>
            	<option value="MADAGASCAR">MADAGASCAR</option>
            	<option value="MALAWI">MALAWI</option>
            	<option value="MALAYSIA">MALAYSIA</option>
            	<option value="MALDIVES">MALDIVES</option>
            	<option value="MALI">MALI</option>
            	<option value="MALTA">MALTA</option>
            	<option value="MARSHALL ISLANDS">MARSHALL ISLANDS</option>
            	<option value="MARTINIQUE">MARTINIQUE</option>
            	<option value="MAURITANIA">MAURITANIA</option>
            	<option value="MAURITIUS">MAURITIUS</option>
            	<option value="MAYOTTE">MAYOTTE</option>
            	<option value="MEXICO">MEXICO</option>
            	<option value="MONACO">MONACO</option>
            	<option value="MONGOLIA">MONGOLIA</option>
            	<option value="MONTSERRAT">MONTSERRAT</option>
            	<option value="MOROCCO">MOROCCO</option>
            	<option value="MOZAMBIQUE">MOZAMBIQUE</option>
            	<option value="MYANMAR">MYANMAR</option>
            	<option value="NAMIBIA">NAMIBIA</option>
            	<option value="NAURU">NAURU</option>
            	<option value="NEPAL">NEPAL</option>
            	<option value="NETHERLANDS">NETHERLANDS</option>
            	<option value="NEW CALEDONIA">NEW CALEDONIA</option>
            	<option value="NEW ZEALAND">NEW ZEALAND</option>
            	<option value="NICARAGUA">NICARAGUA</option>
            	<option value="NIGER">NIGER</option>
            	<option value="NIGERIA">NIGERIA</option>
            	<option value="NIUE">NIUE</option>
            	<option value="NORFOLK ISLAND">NORFOLK ISLAND</option>
            	<option value="NORTHERN MARIANA ISLANDS">NORTHERN MARIANA ISLANDS</option>
            	<option value="NORWAY">NORWAY</option>
            	<option value="OMAN">OMAN</option>
            	<option value="PAKISTAN">PAKISTAN</option>
            	<option value="PALAU">PALAU</option>
            	<option value="PALESTINE">PALESTINE</option>
            	<option value="PANAMA">PANAMA</option>
            	<option value="PAPUA NEW GUINEA">PAPUA NEW GUINEA</option>
            	<option value="PARAGUAY">PARAGUAY</option>
            	<option value="PERU">PERU</option>
            	<option value="PHILIPPINES">PHILIPPINES</option>
            	<option value="PITCAIRN">PITCAIRN</option>
            	<option value="POLAND">POLAND</option>
            	<option value="PORTUGAL">PORTUGAL</option>
            	<option value="PUERTO RICO">PUERTO RICO</option>
            	<option value="QATAR">QATAR</option>
            	<option value="RÉUNION">RÉUNION</option>
            	<option value="ROMANIA">ROMANIA</option>
            	<option value="RUSSIA">RUSSIA</option>
            	<option value="RWANDA">RWANDA</option>
            	<option value="SAMOA">SAMOA</option>
            	<option value="SAN MARINO">SAN MARINO</option>
            	<option value="SAUDI ARABIA">SAUDI ARABIA</option>
            	<option value="SENEGAL">SENEGAL</option>
            	<option value="SEYCHELLES">SEYCHELLES</option>
            	<option value="SIERRA LEONE">SIERRA LEONE</option>
            	<option value="SINGAPORE">SINGAPORE</option>
            	<option value="SLOVAKIA">SLOVAKIA</option>
            	<option value="SLOVENIA">SLOVENIA</option>
            	<option value="SOLOMON ISLANDS">SOLOMON ISLANDS</option>
            	<option value="SOMALIA">SOMALIA</option>
            	<option value="SOUTH AFRICA">SOUTH AFRICA</option>
            	<option value="SPAIN">SPAIN</option>
            	<option value="SRI LANKA">SRI LANKA</option>
            	<option value="SUDAN">SUDAN</option>
            	<option value="SURINAME">SURINAME</option>
            	<option value="SWEDEN">SWEDEN</option>
            	<option value="SWITZERLAND">SWITZERLAND</option>
            	<option value="SYRIA">SYRIA</option>
            	<option value="TAIWAN">TAIWAN</option>
            	<option value="TAJIKISTAN">TAJIKISTAN</option>
            	<option value="TANZANIA">TANZANIA</option>
            	<option value="THAILAND">THAILAND</option>
            	<option value="TOGO">TOGO</option>
            	<option value="TOKELAU">TOKELAU</option>
            	<option value="TONGA">TONGA</option>
            	<option value="TUNISIA">TUNISIA</option>
            	<option value="TURKEY">TURKEY</option>
            	<option value="TURKMENISTAN">TURKMENISTAN</option>
            	<option value="TUVALU">TUVALU</option>
            	<option value="UGANDA">UGANDA</option>
            	<option value="UKRAINE">UKRAINE</option>
            	<option value="UAE">UAE</option>
            	<option value="UK">UK</option>
            	<option value="USA">USA</option>
            	<option value="URUGUAY">URUGUAY</option>
            	<option value="UZBEKISTAN">UZBEKISTAN</option>
            	<option value="VANUATU">VANUATU</option>
            	<option value="VENEZUELA">VENEZUELA</option>
            	<option value="VIETNAM">VIETNAM</option>
            	<option value="WALLIS AND FUTUNA">WALLIS AND FUTUNA</option>
            	<option value="WESTERN SAHARA">WESTERN SAHARA</option>
            	<option value="YEMEN">YEMEN</option>
            	<option value="ZAMBIA">ZAMBIA</option>
            	<option value="ZIMBABWE">ZIMBABWE</option>
           </select>
        </div>
          
</div>
    
    <div id="formPhone">
		
		<?php echo $form->textField($model,'country_register',array('size'=>60,'maxlength'=>200,'class' => 'register','placeholder'=>($this->{$_SESSION['lang']}['country']) )); ?>
		<?php echo $form->error($model,'country_register'); ?>

  
		<?php echo $form->textField($model,'areacode_register',array('size'=>60,'maxlength'=>200,'class' => 'register','placeholder'=>($this->{$_SESSION['lang']}['areacode']) )); ?>
		<?php echo $form->error($model,'areacode_register'); ?>

		<?php echo $form->textField($model,'phone_register',array('size'=>60,'maxlength'=>200,'class' => 'register','placeholder'=>($this->{$_SESSION['lang']}['phonenumber']) )); ?>
		<?php echo $form->error($model,'phone_register'); ?>
	
  	</div>

<!-------- ------------------>
<div style="clear: both;"></div>
<div id="formUpdates" style="display:table;margin-bottom: 16px;">
   <div id="updateCheckbox" >
      <input type="checkbox" name="Register[yesmail_register]" value="yes" />
   </div>
   <div id="checkText" >
      <?php print_r($this->{$_SESSION['lang']}['yessendmail']); ?>
   </div>
</div>
<!-------- ------------------>

   <div id="formCompanyAdd1">
        <div id="companyAdd1">
    	    <?php echo $form->textField($model,'companyaddress_register',array('size'=>60,'maxlength'=>200,'class' => 'register','placeholder'=>($this->{$_SESSION['lang']}['companyaddress']) )); ?>
    		<?php echo $form->error($model,'companyaddress_register'); ?>
    	</div>
    </div>
    <div id="formCity">
         <div id="city">
    	    <?php echo $form->textField($model,'city_register',array('size'=>60,'maxlength'=>200,'class' => 'register','placeholder'=>($this->{$_SESSION['lang']}['city']) )); ?>
    		<?php echo $form->error($model,'city_register'); ?>
        </div>
    </div>
    <div id="formZip">
        <div id="zipcode">
	    <?php echo $form->textField($model,'postcode_register',array('size'=>60,'maxlength'=>200,'class' => 'register','placeholder'=>($this->{$_SESSION['lang']}['postcode']) )); ?>
		<?php echo $form->error($model,'postcode_register'); ?>
	   </div>
    </div>
    <div id="formVIP">
         <div id="vipCode">
	    <?php echo $form->textField($model,'viplabel_register',array('size'=>60,'maxlength'=>200,'class' => 'register','placeholder'=>($this->{$_SESSION['lang']}['vipcode']) )); ?>
		<?php echo $form->error($model,'viplabel_register'); ?>
	    </div>
    </div>
    <div id="whereHear">
        <select name="Register[bannerad_register]" id="ctl00_Content_ctl02_data_WhereDidYouHearAboutUs">
        	<option selected="selected" value=""><?php print_r($this->{$_SESSION['lang']}['wheredid']); ?></option>
        	<option value="Advanced notice received by fax">Advanced notice received by fax</option>
        	<option value="Advanced notice received by post">Advanced notice received by post</option>
        	<option value="Banner Ad">Banner Ad</option>
        	<option value="Colleagues recommendation">Colleague's recommendation</option>
        	<option value="Email Newsletter">Email Newsletter</option>
        	<option value="Facebook">Facebook</option>
        	<option value="LinkedIn">LinkedIn</option>
        	<option value="Link from another site">Link from another site</option>
        	<option value="Magazine Ad">Magazine Ad</option>
        	<option value="Newsgroup post">Newsgroup post</option>
        	<option value="Phone invitation">Phone invitation</option>
        	<option value="Printed Brochure">Printed Brochure</option>
        	<option value="Search Engine">Search Engine</option>
        	<option value="Twitter">Twitter</option>
        
        </select>
         <div class="formValidators">
            <span id="ctl00_Content_ctl02_rfvHear" style="color:Red;visibility:hidden;">Where did you hear about us?</span>
        </div>    
    </div>

    <div id="httt">
        <ul>
                <li><input id="paymentn_1"  type="radio" name="Register[topay_register]" value="Secure online credit card payment" checked=""  /> <label for="paymentn_1"><?php print_r($this->{$_SESSION['lang']}['secureonline']); ?></label></li>
                <li><input id="paymentn_2" type="radio" name="Register[topay_register]" value="Electronic bank transfer"  /> <label for="paymentn_2"><?php print_r($this->{$_SESSION['lang']}['electronic']); ?></label></li>
                <li><input id="paymentn_3" type="radio" name="Register[topay_register]" value="Banker’s draft / Company cheque" /> <label for="paymentn_3"><?php print_r($this->{$_SESSION['lang']}['bankersdraft']); ?></label></li>
                <li><input id="paymentn_4" type="radio" name="Register[topay_register]" value="Cash on site(either US$ or AED)" /> <label for="paymentn_4"><?php print_r($this->{$_SESSION['lang']}['cashon']); ?></label></li>
                <li><input id="paymentn_5" type="radio" name="Register[topay_register]" value="Banker’s draft / Company cheque on site"/> <label for="paymentn_5"><?php print_r($this->{$_SESSION['lang']}['bankers']); ?></label></li>
               
        </ul>

    </div>
	<div id="captchas">
		   <?php $this->widget('CCaptcha',array(
		
			'clickableImage' => true,
			'imageOptions' => array('id' => 'captchaimgdl')
			)); ?>
		   <div>
		      <?php echo $form->textField($model,'code',array('size'=>60,'class' => 'cpbt')); ?>
	      	
		   </div>
		   
	</div>
		<?php echo $form->error($model,'code'); ?>
	
	
	<div id="ledie">
		<a href="/start-download/<?php echo $regisv->rewrite_url_news ?>.html"><?php echo CHtml::submitButton(($this->{$_SESSION['lang']}['send'])); ?></a>
		<?php echo CHtml::resetButton(($this->{$_SESSION['lang']}['reset'])) ?>
	</div>

<?php $this->endWidget(); ?>

</div>
<style type="text/css">
<!--
	.nbl input {
	   width: 49%;
	}
-->
</style>
    <div style="clear: both;"></div>
</div>
</div>
</div>
  <!-------------------- new ------------->
                        <!-------------------- new ------------->
                    <div class="comment_right">
                        <div class="comment_bg">
                            <div class="comment_right_title"><a><?php print_r($this->{$_SESSION['lang']}['comment']); ?></a> </div>       
                            <div class="eventh23 content mCustomScrollbar"> 
                             <?php 
		                      		                       
                               $comment = new CDbCriteria();
                               $comment->condition = 'cm_action=1';
                               $comment->limit=12;
                               $comment->order = 'cm_id DESC';
                               $comments = Comment::model()->findAll($comment);
                               foreach ($comments as $vals){
                           ?>
                           <div class="hi_comment">
                                <div class="nd_comment">
                                        <a> <?php echo $vals['cm_name_' . $_SESSION['lang']] ?></a>
                                </div>
                                <div class="td_comment">
                                       <a><?php echo $vals['cm_link_' . $_SESSION['lang']] ?></a>
                                </div>
                           </div>     
                         <?php } ?>
                         </div>
                          <div class="clickmore"><a href="/comment.html">Click see more...</a></div>
                        </div>
                       
                       <div class="videoscm">
                            <div class="comment_right_title" style="box-shadow: none;margin-bottom: 1px;"><a>Videos</a> </div>  
                             	 <script>
                                	$(function(){
                                		$('.video1').click(function(){
                                		var bien= $(this).attr('href');//alert(bien);
                                		
                                		$('.list').html(bien);
                                		});
                                		
                                	})
                                </script>
                        <?php 
		                       $video = new CDbCriteria();
                               $video->condition = 'showhide_links=1';
                               $video->order = 'id_links DESC';
                               $video->limit=3;
                               $videos = Links::model()->findAll($video);
                               foreach ($videos as $vc){
                           ?>     
                            <div class="list" style="margin-bottom: 6px;">
                    			<embed  class="video1"
                    			type="application/x-shockwave-flash" 
                    			src="<?php echo Yii::app()->request->baseUrl; ?>/images/home/images/player.swf" 
                    			width="100%" height="249" style="undefined" id="ply" name="ply" quality="high" allowfullscreen="true" allowscriptaccess="always" 
                    			flashvars="width=10&amp;height=249&amp;&amp;file=<?php echo $vc->url_links ?>&amp;feature=plcp"/>
                    		</div>
                          <?php } ?>  
                       </div>
                    </div>
               <div style="clear:both;">&nbsp;</div>   

                     
</div>
</div></div>