<?php Yii::app()->clientScript->registerMetaTag(CHtml::encode($YiiTypealbum->keywords_seo_album), 'keywords'); ?>
<?php  Yii::app()->clientScript->registerMetaTag(CHtml::encode($YiiTypealbum->description_seo_album), 'description'); ?>

<div id="content_type_bicycle">
<div class="items_contents">
                 <div id="block_right">
                 <div class="divPath">
					<?php $this->widget('application.components.BreadCrumb', array(
					  'crumbs' => array(
					    array('name' => '<i class="fa fa-home"></i>', 'url' => Yii::app()->getHomeUrl()),
					  	array('name' => 'Thư viện ảnh', 'url' => '/thu-vien-anh.html'),
					  	array('name' => $YiiTypealbum->name_typealbum),
					  ),
					  'delimiter' => ' <i class="fa fa-angle-right"></i> ', // if you want to change it
					)); ?>
				</div>
                 
                 <div id="detailimgs">
                    <div id="titlealbumsf"><?php echo $YiiTypealbum->name_typealbum ?></div>
                     <div class="row">
					 <?php foreach ($model as $val){ ?>
					  	<div class="col-xs-6 col-md-3 albumsfg">	    
					    <a class='highslide' href='<?php echo $val->img_album ?>' onclick="return hs.expand(this)">
		                  <img src='<?php echo $val->img_album ?>' alt='<?php echo $val->img_album ?>' width="100%"/></a>	
		                  </div>				  
					  <?php }?>
					 
					</div>
                 </div>
                     
                 </div>
                 </div>
                 </div>