<?php Yii::app()->clientScript->registerMetaTag(CHtml::encode($keywords), 'keywords'); ?>
<?php  Yii::app()->clientScript->registerMetaTag(CHtml::encode($description), 'description'); ?>
<div id="content_type_bicycle">
<div class="items_contents">
                 <div class="divPath">
					<?php $this->widget('application.components.BreadCrumb', array(
					  'crumbs' => array(
					    array('name' => '<i class="fa fa-home"></i>', 'url' => Yii::app()->getHomeUrl()),
					  		array('name' => 'Danh sách loại video', 'url' => '/list-video.html'),
					  	array('name' => $name__typevideo),
					  ),
					  'delimiter' => ' <i class="fa fa-angle-right"></i> ', // if you want to change it
					)); ?>
				</div>
				<div id="titlealbumsd"><?php echo $name__typevideo ?></div>
                     <div id="related" class="mainalbums">
                        
                        <div class="row">
                        <?php foreach ($a as $key=>$val){ ?>
                          
						  <div class="col-sm-6 col-md-4">
						    <div class="thumbnail albumsd youtubevideo">
						      <a href="/video/<?php echo $val['url_seo_video'] ?>.html" title="<?php echo $val['name_video'] ?>"><img border="0" src="http://i2.ytimg.com/vi/<?php echo $key ?>/0.jpg" alt="<?php echo $val['name_video'] ?>"  width="308" height="260">
						      <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/home/images/video.png" alt="video" id="iconvideoyou"/>
						      </a>
						      
						      <div class="caption">
						        <a href="/video/<?php echo $val['url_seo_video'] ?>.html" title="<?php echo $val['name_video'] ?>"><h5><?php echo $val['name_video'] ?></h5></a>
						      </div>
						    </div>
						  </div>
						  
						 <?php }?>
						</div>
                        
                        
                         <div>
								<div id="main_paginnation">
							     <div id="pagination">
							    <?php $this->widget('CLinkPager', array(
							    'pages'       => $pages,
							    'htmlOptions' => array('class' => 'pager'),
							    'header'      => '',
							    'nextPageLabel' => '<i class="fa fa-angle-right"></i>',
							    'prevPageLabel'=> '<i class="fa fa-angle-left"></i>',
					    		'firstPageLabel'=>'<i class="fa fa-angle-double-left"></i>',
					    		'lastPageLabel'=>'<i class="fa fa-angle-double-right"></i>'
							));
							    ?>
							</div>
				
						</div>
							</div>
                     </div>
                    
                     
                 </div>
                 </div>