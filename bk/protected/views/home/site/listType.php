<?php
$baseUrl = Yii::app()->request->baseUrl;
?>
        <div id="right"> 
        <div class="divPath">
					<?php $this->widget('application.components.BreadCrumb', array(
					  'crumbs' => array(
					    array('name' => '<i class="fa fa-home"></i>', 'url' => Yii::app()->getHomeUrl()),
					  	array('name' => 'Sản phẩm'),
					  ),
					  'delimiter' => ' <i class="fa fa-angle-right"></i> ', // if you want to change it
					)); ?>
				</div>
        <input type="hidden" id="sxep" value="<?php echo Yii::app()->session['sx'] ?>">
                       <div class="title_right">
                           <span class="sub_title"><i class="fa fa-star-half-o"></i>Sản phẩm</span>
                           <div id="sxsp_main">
                             <select name="sxsp" id="sxsp">
                                <option value="">Sắp xếp sản phẩm</option>
                                <option value="td">Giá tăng dần</option>
                                <option value="gd">Giá giảm dần</option>
                                <option value="new">Sản phẩm mới</option>
                             </select>
                         </div>
                       </div>
             
                <?php 
                
                if ($count != 0) {
                  ?>
                               
                       <div class="main_right">	
                       
 <div id="slsp">Có <?php echo $count ?> sản phẩm</div>
                        <?php 
                       foreach ($items as $vals){
                       ?>					  
							  <div class="items">
							    <a href="<?php echo '/san-pham/'.$vals->pr_url_news.'.html' ?>" class="title_items"><?php echo $vals->pr_name ?></a><br>
							    <a href="<?php echo '/san-pham/'.$vals->pr_url_news.'.html' ?>" class="img_items">
							      <img src="<?php  echo $vals->pr_img ?>" alt="<?php echo $vals->pr_name ?>" width="106" height="106">
							    </a>
							    <span class="code">Mã : <span class="numcode"><?php echo $vals->pr_code ?></span></span><br>
							    <span class="gia">Gía: <span class="price"><?php echo number_format($vals->pr_gia,0,"",".").' đ' ?></span></span>
							  </div>
							
							<?php }?>
							
						<div id="main_paginnation">
							     <div id="pagination">
							    <?php $this->widget('CLinkPager', array(
							    'pages'       => $pages,
							    'htmlOptions' => array('class' => 'pager'),
							    'header'      => '',
							    'nextPageLabel' => '<i class="fa fa-angle-right"></i>',
							    'prevPageLabel'=> '<i class="fa fa-angle-left"></i>',
					    		'firstPageLabel'=>'<i class="fa fa-angle-double-left"></i>',
					    		'lastPageLabel'=>'<i class="fa fa-angle-double-right"></i>'
							));
							    ?>
							</div>
				
						</div>
								
                       </div>
                       <?php }else{?>    
                        <div class="main_right">	
                           <span id="notitems">Không tồn tại sản phẩm.</span>
                        </div>
                       <?php }?>
                  </div>