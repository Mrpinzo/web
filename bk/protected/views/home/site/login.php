<?php
Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/css/admin/login.css');
?>
<div class="main">
<div id="com_main_login">
<div id="login_box">
			<span>Welcome Admin!</span>
<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableAjaxValidation'=>true,
)); ?>

	<div class="row">
		<?php echo $form->textField($model,'username',array('size'=>48,'maxlength'=>30,'encode'=>false,'value'=>'','placeholder'=>'Tên đăng  nhập')); ?>
		<?php echo $form->error($model,'cust_email'); ?>
	</div>

	<div class="row">
		<?php echo $form->passwordField($model,'password',array('size'=>48,'maxlength'=>30,'encode'=>false,'value'=>'','placeholder'=>'Mật khẩu')); ?>
		<?php echo $form->error($model,'cust_pass'); ?>
	</div>

	<div class="row rememberMe">
		<?php echo $form->checkBox($model,'rememberMe'); ?>
		<?php echo $form->label($model,'rememberMe'); ?>
		<?php echo $form->error($model,'rememberMe'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Đăng nhập'); ?>
		
	</div>

<?php $this->endWidget(); ?>
</div><!-- form -->
</div>
</div>
</div>