<?php
/**
 * Created by $Mitonios Editor.
 * User: $Mitonios
 * Date: 1/03/13
 * Time: 3:18 PM
 */
/* @var $item Product */
?>
<ul>
    <?php
    if (!empty($items)) {
        foreach ($items as $item) {
            ?>
            <li>
                <a class="preview" href="<?php echo Yii::app()->request->baseUrl . $item->avatar ?>"
                   onclick="window.location.href='<?php echo $this->createUrl('/site/detail', array('id' => $item->id)) ?>';return false;">
                    <div style="padding-bottom: 5px;">
                        <img width="135"
                             src="<?php echo Mitonios::phpThumb(Yii::app()->request->baseUrl . $item->avatar, 135) ?>">
                    </div>
                </a>

                <p>
                    Mã: <span class="code"><?php echo $item->id?></span><span class="space">|</span>Giá:
                    <span class="price"><?php echo number_format($item->price, 0, "", ".") ?></span>
                </p>
            </li>
        <?php
        }
    }
    ?>
</ul>