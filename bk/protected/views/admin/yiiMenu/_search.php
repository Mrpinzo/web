<?php
/* @var $this YiiMenuController */
/* @var $model YiiMenu */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_menu'); ?>
		<?php echo $form->textField($model,'id_menu'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'name_menu_en'); ?>
		<?php echo $form->textField($model,'name_menu_en',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'showhide_menu'); ?>
		<?php echo $form->textField($model,'showhide_menu'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'location_menu'); ?>
		<?php echo $form->textField($model,'location_menu'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'level_menu'); ?>
		<?php echo $form->textField($model,'level_menu'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'order_menu'); ?>
		<?php echo $form->textField($model,'order_menu'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'newwindow_mennu'); ?>
		<?php echo $form->textField($model,'newwindow_mennu'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->