<?php
/* @var $this NewsController */
/* @var $model News */

$this->breadcrumbs=array(
	'Tin tức'=>array('admin'),
	$model->new_title_en=>"/chi-tiet-tin-tuc/".$model->rewrite_url_news.".html",
	'Sửa',
);

$this->menu=array(
	array('label'=>'Danh sách tin tức', 'url'=>array('admin')),
);
?>

<h4>Sửa tin tức <?php echo $model->new_title_en; ?></h4>

<?php echo $this->renderPartial('_form', array('model'=>$model,'YiiMenu'=>$YiiMenu)); ?>