<?php
/* @var $this NewsController */
/* @var $model News */

$this->breadcrumbs=array(
	'Tin tức'=>array('admin'),
	$model->new_title_en,
);

$this->menu=array(
	array('label'=>'Danh sách tin tức', 'url'=>array('admin')),
);
?>

<h4>Chi tiết tin tức <?php echo $model->new_title_en; ?></h4>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'new_id',
			array(
					'name'  => 'new_title_en',
					'value' =>  $model->new_title_en.'<i class="fa fa-link links" id="/chi-tiet-tin-tuc/'.$model->rewrite_url_news.'.html"></i>',
					'type'  => 'raw',
			),
			array(
					'name'  => 'new_img',
					'value' => !empty($model->new_img)?'<img src="'.Yii::app()->request->baseUrl . $model->new_img.'" width="100" height="100px"/>':"",
					'type'  => 'raw',
			),
			array(
					'name'  => 'new_summary',
					'value' =>  $model->new_summary,
					'type'  => 'raw',
			),
			array(
					'name'  => 'new_content',
					'value' =>  $model->new_content,
					'type'  => 'raw',
			),
			array(
					'name'   => 'new_date',
					'value'  => date("d-m-Y",strtotime($model->new_date)),
			
			),
			'new_title_seo',
			'new_description_seo',
			'new_keywords_seo',
			array(
					'name'   => 'new_highlights',
					'value'  => $model->new_highlights==1?"<i class=\"fa fa-check-square-o csm\"></i>":"<i class=\"fa fa-times csm\"></i>",
					'filter' => array(1 => 'Nổi bật', 0 => 'Không nổi bật'),
					'type'   => 'raw',
			),
			array(
					'name'   => 'new_showhide',
					'value'  => $model->new_showhide==1?"<i class=\"fa fa-check-square-o csm\"></i>":"<i class=\"fa fa-times csm\"></i>",
					'filter' => array(1 => 'Hiện', 0 => 'Ẩn'),
					'type'   => 'raw',
			),
			'rewrite_url_news',
		
	),
)); ?>
