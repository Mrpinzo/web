<?php
/* @var $this RegisterController */
/* @var $model Register */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_register'); ?>
		<?php echo $form->textField($model,'id_register'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fullnam_register'); ?>
		<?php echo $form->textField($model,'fullnam_register',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'company_register'); ?>
		<?php echo $form->textField($model,'company_register',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'phone_register'); ?>
		<?php echo $form->textField($model,'phone_register',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'email_register'); ?>
		<?php echo $form->textField($model,'email_register',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'address_register'); ?>
		<?php echo $form->textField($model,'address_register',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'title_register'); ?>
		<?php echo $form->textField($model,'title_register',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'content_register'); ?>
		<?php echo $form->textArea($model,'content_register',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'mobile_register'); ?>
		<?php echo $form->textField($model,'mobile_register',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->