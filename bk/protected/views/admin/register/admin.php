<?php
/* @var $this RegisterController */
/* @var $model Register */

$this->breadcrumbs=array(
	'Register'=>array('admin'),
	'Danh sách',
);
/* 
$this->menu=array(
	array('label'=>'List Register', 'url'=>array('index')),
	array('label'=>'Create Register', 'url'=>array('create')),
); */

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#register-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h4>List Register</h4>
<?php $form = $this->beginWidget('CActiveForm', array(
    'enableAjaxValidation' => TRUE,
)); ?>
<input id="yt10" type="submit" value="Kích hoạt" name="yt10" style="display:none;" class="search-button">
<?php echo CHtml::ajaxSubmitButton('Xóa', array('Register/ajaxUpdate', 'act' => 'doDelete'), array('success' => 'reloadGrid', 'beforeSend' => 'function(){
            return confirm("Bạn có chắc chắn muốn xóa những thông tin đăng ký được chọn?")
        }',)); ?>


<?php 
$cs = Yii::app()->clientScript;
$css = 'ul.yiiPager .first, ul.yiiPager .last {display:inline;}';
$cs->registerCss('show_first_last_buttons', $css);
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'register-grid',
		'ajaxUpdate'=>false,
	'dataProvider'=>$model->search(),
	'filter'=>$model,
		'pager' => array('maxButtonCount' => 4,'pageSize'=>10,'firstPageLabel'=>'<i class="fa fa-angle-double-left"></i>','lastPageLabel'=>'<i class="fa fa-angle-double-right"></i>'
				,'nextPageLabel' => '<i class="fa fa-angle-right"></i>',
				'prevPageLabel'=> '<i class="fa fa-angle-left"></i>',
				'header'=> '',
					
		),
	'columns'=>array(
			array(
					'name'   => 'read',
					'value'  => '($data->read == 1)?"<img src=\"/images/home/images/new.gif\"/>":""',
					'type'   => 'raw',
			),
			array(
					'id'             => 'autoId',
					'class'          => 'CCheckBoxColumn',
					'selectableRows' => '50',
			),
		'id_register',
        'namexh_register',
		'fullnam_register',
        'lastnam_register',
        'namenew_register',
		'company_register',
		'email_register',
	   	'address_register',
		'title_register',
		/* 'country_register',
        'areacode_register', */
        'phone_register',
			array(
					'name'   => 'dates',
					'value'  => 'date("H:i:s d-m-Y",strtotime($data->dates))',
			),
		
		array(
			'class'=>'CButtonColumn',
				'header' => 'Hành động',
				'template'=>'{view}{delete}',
				'buttons'=>array
				(
						'view' => array(
								'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'View')),
								'label' => '<i class="fa fa-search"></i>',
								'imageUrl' => false,
						),
						'delete' => array(
								'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'Delete')),
								'label' => '<i class="fa fa-trash-o"></i>',
								'imageUrl' => false,
						)
				),
		),
	),
)); ?>
	<script>
    function reloadGrid(data) {
        $.fn.yiiGridView.update('register-grid');
    }
</script>

<?php $this->endWidget(); ?>