<?php
/* @var $this RegisterController */
/* @var $model Register */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'register-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'fullnam_register'); ?>
		<?php echo $form->textField($model,'fullnam_register',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'fullnam_register'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'company_register'); ?>
		<?php echo $form->textField($model,'company_register',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'company_register'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'phone_register'); ?>
		<?php echo $form->textField($model,'phone_register',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'phone_register'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email_register'); ?>
		<?php echo $form->textField($model,'email_register',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'email_register'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'address_register'); ?>
		<?php echo $form->textField($model,'address_register',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'address_register'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'title_register'); ?>
		<?php echo $form->textField($model,'title_register',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'title_register'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'content_register'); ?>
		<?php echo $form->textArea($model,'content_register',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'content_register'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'mobile_register'); ?>
		<?php echo $form->textField($model,'mobile_register',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'mobile_register'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->