<?php
/* @var $this RegisterController */
/* @var $data Register */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_register')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_register), array('view', 'id'=>$data->id_register)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fullnam_register')); ?>:</b>
	<?php echo CHtml::encode($data->fullnam_register); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('company_register')); ?>:</b>
	<?php echo CHtml::encode($data->company_register); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('phone_register')); ?>:</b>
	<?php echo CHtml::encode($data->phone_register); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email_register')); ?>:</b>
	<?php echo CHtml::encode($data->email_register); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('address_register')); ?>:</b>
	<?php echo CHtml::encode($data->address_register); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('title_register')); ?>:</b>
	<?php echo CHtml::encode($data->title_register); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('content_register')); ?>:</b>
	<?php echo CHtml::encode($data->content_register); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mobile_register')); ?>:</b>
	<?php echo CHtml::encode($data->mobile_register); ?>
	<br />

	*/ ?>

</div>