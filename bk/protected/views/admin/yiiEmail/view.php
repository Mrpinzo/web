<?php
/* @var $this YiiEmailController */
/* @var $model YiiEmail */

$this->breadcrumbs=array(
	'Yii Emails'=>array('index'),
	$model->id_email,
);

$this->menu=array(
	array('label'=>'List YiiEmail', 'url'=>array('index')),
	array('label'=>'Create YiiEmail', 'url'=>array('create')),
	array('label'=>'Update YiiEmail', 'url'=>array('update', 'id'=>$model->id_email)),
	array('label'=>'Delete YiiEmail', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_email),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage YiiEmail', 'url'=>array('admin')),
);
?>

<h1>View YiiEmail #<?php echo $model->id_email; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_email',
		'email',
		'date',
	),
)); ?>
