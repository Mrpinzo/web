<?php
/* @var $this OrdersController */
/* @var $model Orders */

$this->breadcrumbs=array(
	'Danh sách đơn hàng'=>array('admin'),
	'Quản lý',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#orders-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h4>Danh sách đơn hàng</h4>
<?php $form = $this->beginWidget('CActiveForm', array(
    'enableAjaxValidation' => TRUE,
)); ?>

<?php echo CHtml::ajaxSubmitButton('Xóa', array('Orders/ajaxUpdate', 'act' => 'doDelete'), array('success' => 'reloadGrid', 'beforeSend' => 'function(){
            return confirm("Bạn có chắc chắn muốn xóa những sản phẩm được chọn?")
        }',)); ?>

<?php 
$cs = Yii::app()->clientScript;
$css = 'ul.yiiPager .first, ul.yiiPager .last {display:inline;}';
$cs->registerCss('show_first_last_buttons', $css);
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'orders-grid',
		'ajaxUpdate'=>false,
	'dataProvider'=>$model->search(),
	'filter'=>$model,
		'pager' => array('maxButtonCount' => 4,'pageSize'=>10,'firstPageLabel'=>'<i class="fa fa-angle-double-left"></i>','lastPageLabel'=>'<i class="fa fa-angle-double-right"></i>'
				,'nextPageLabel' => '<i class="fa fa-angle-right"></i>',
				'prevPageLabel'=> '<i class="fa fa-angle-left"></i>',
				'header'=> '',
					
		),
	'columns'=>array(
			array(
					'id'             => 'autoId',
					'class'          => 'CCheckBoxColumn',
					'selectableRows' => '50',
			),
		'ord_id',
		'ord_email',
			array(
					'name'   => 'ord_Gender',
					'value'  => '$data->ord_Gender==1?"Nam":"Nữ"',
					'filter' => array(1 => 'Nam', 0 => 'Nữ'),
			),
			array(
					'name'   => 'ord_Dateofbirth',
					'value'  => '!empty($data->ord_Dateofbirth)?date("d-m-Y",strtotime($data->ord_Dateofbirth)):""',
					
			),
		'ord_name',
		
		'ord_from',
		'ord_phone',
			array(
					'name'   => 'ord_date',
					'value'  => 'date("d-m-Y",strtotime($data->ord_date))',
					
			),
			array(
					'name'   => 'ord_status',
					'value'  => '$data->ord_status==1?"<span class=\"hoanthanh\">Hoàn thành</span>":"<span class=\"dangcho\">Đang chờ</span>"',
					'filter' => array(1 => 'Hoàn thành', 0 => 'Đang chờ'),
					'type' => 'raw'
			),
		array(
			'class'=>'CButtonColumn',
				'header' => 'Hành động',
				'template'=>'{view}{delete}',
				'buttons'=>array
				(
				'view' => array(
                'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'View')),
                'label' => '<i class="fa fa-search"></i>',
                'imageUrl' => false,
            ),
            'delete' => array(
                'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'Delete')),
                'label' => '<i class="fa fa-trash-o"></i>',
                'imageUrl' => false,
               )
				),
		),
	),
)); ?>
<script>
    function reloadGrid(data) {
        $.fn.yiiGridView.update('orders-grid');
    }
</script>
<?php $this->endWidget(); ?>