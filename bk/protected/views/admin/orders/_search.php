<?php
/* @var $this OrdersController */
/* @var $model Orders */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'ord_id'); ?>
		<?php echo $form->textField($model,'ord_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ord_email'); ?>
		<?php echo $form->textField($model,'ord_email',array('size'=>60,'maxlength'=>150)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ord_Gender'); ?>
		<?php echo $form->textField($model,'ord_Gender'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ord_Dateofbirth'); ?>
		<?php echo $form->textField($model,'ord_Dateofbirth'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ord_payment'); ?>
		<?php echo $form->textField($model,'ord_payment'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ord_name'); ?>
		<?php echo $form->textField($model,'ord_name',array('size'=>60,'maxlength'=>150)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ord_from'); ?>
		<?php echo $form->textField($model,'ord_from',array('size'=>60,'maxlength'=>150)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ord_phone'); ?>
		<?php echo $form->textField($model,'ord_phone'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ord_date'); ?>
		<?php echo $form->textField($model,'ord_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ord_status'); ?>
		<?php echo $form->textField($model,'ord_status'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->