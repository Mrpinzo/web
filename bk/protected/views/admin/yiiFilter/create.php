<?php
/* @var $this YiiMenuController */
/* @var $model YiiMenu */

$this->breadcrumbs=array(
	'Bộ lọc tìm kiếm'=>array('admin'),
	'Tạo mới',
);

$this->menu=array(
	array('label'=>'Danh sách bộ lọc tìm kiếm', 'url'=>array('admin')),
);
?>

<h4>Tạo mới menu</h4>

<?php echo $this->renderPartial('_form', array('model'=>$model,'YiiMenue' =>$YiiMenue)); ?>