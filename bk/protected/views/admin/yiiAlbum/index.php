<?php
/* @var $this YiiAlbumController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Yii Albums',
);

$this->menu=array(
	array('label'=>'Create YiiAlbum', 'url'=>array('create')),
	array('label'=>'Manage YiiAlbum', 'url'=>array('admin')),
);
?>

<h1>Yii Albums</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
