<?php
/* @var $this YiiAlbumController */
/* @var $model YiiAlbum */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_album'); ?>
		<?php echo $form->textField($model,'id_album'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_typealbum'); ?>
		<?php echo $form->textField($model,'id_typealbum'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'img_album'); ?>
		<?php echo $form->textField($model,'img_album',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'showhide_album'); ?>
		<?php echo $form->textField($model,'showhide_album'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->