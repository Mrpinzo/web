<?php
/* @var $this YiiAlbumController */
/* @var $model YiiAlbum */

$this->breadcrumbs=array(
	'Albums'=>array('admin'),
	'Danh sách',
);

$this->menu=array(
	array('label'=>'Tạo mới', 'url'=>array('create')),
);

?>

<h4>Danh sách Albums</h4>
<?php $form = $this->beginWidget('CActiveForm', array(
    'enableAjaxValidation' => TRUE,
)); ?>

<?php echo CHtml::ajaxSubmitButton('Hiện', array('YiiAlbum/ajaxUpdate', 'act' => 'doActive'), array('success' => 'reloadGrid')); ?>
<?php echo CHtml::ajaxSubmitButton('Ẩn', array('YiiAlbum/ajaxUpdate', 'act' => 'doInactive'), array('success' => 'reloadGrid')); ?>
<?php echo CHtml::ajaxSubmitButton('Xóa', array('YiiAlbum/ajaxUpdate', 'act' => 'doDelete'), array('success' => 'reloadGrid', 'beforeSend' => 'function(){
            return confirm("Bạn có chắc chắn muốn xóa những ảnh được chọn?")
        }',)); ?>

<?php
$cs = Yii::app()->clientScript;
$css = 'ul.yiiPager .first, ul.yiiPager .last {display:inline;}';
$cs->registerCss('show_first_last_buttons', $css);
 $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'yii-album-grid',
 		'ajaxUpdate'=>false,
	'dataProvider'=>$model->search(),
	'filter'=>$model,
 		'pager' => array('maxButtonCount' => 4,'pageSize'=>10,'firstPageLabel'=>'<i class="fa fa-angle-double-left"></i>','lastPageLabel'=>'<i class="fa fa-angle-double-right"></i>'
 				,'nextPageLabel' => '<i class="fa fa-angle-right"></i>',
 				'prevPageLabel'=> '<i class="fa fa-angle-left"></i>',
 				'header'=> '',
 					
 		),
	'columns'=>array(
			array(
					'id'             => 'autoId',
					'class'          => 'CCheckBoxColumn',
					'selectableRows' => '50',
			),
		'id_album',

			array(
					'name'   => 'id_typealbum',
					'value'  => 'isset($data->id_typealbum)?$data->Albums->name_typealbum:""',
					'filter' => CHtml::activeDropDownList($model, 'id_typealbum', CHtml::listData(YiiTypealbum::model()->findAll(), "id_typealbum", "name_typealbum"), array("empty" => "Chọn loại Album")),
			),
			array(
					'name'   => 'img_album',
					'value'  =>  '!empty($data->img_album)?"<img src=\"".Yii::app()->request->baseUrl.$data->img_album."\" width=\"50\" height=\"50\"/>":""',
					'type'   => 'raw',
					'filter' => FALSE,
					'sortable'   => FALSE,
			),
			array(
					'name'   => 'showhide_album',
					'value'  => '$data->showhide_album==1?"<i class=\"fa fa-check-square-o csm\"></i>":"<i class=\"fa fa-times csm\"></i>"',
					'filter' => array(1 => 'Hiện', 0 => 'Ẩn'),
					'type'   => 'raw',
			),
		array(
			'class'=>'CButtonColumn',
				'header' => 'Hành động',
				'buttons'=>array
				(
						'view' => array(
								'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'View')),
								'label' => '<i class="fa fa-search"></i>',
								'imageUrl' => false,
						),
						'update' => array(
								'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'Update')),
								'label' => '<i class="fa fa-pencil-square-o"></i>',
								'imageUrl' => false,
						),
						'delete' => array(
								'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'Delete')),
								'label' => '<i class="fa fa-trash-o"></i>',
								'imageUrl' => false,
						)
				),
		),
	),
)); ?>
<script>
    function reloadGrid(data) {
        $.fn.yiiGridView.update('yii-album-grid');
    }
</script>
<?php $this->endWidget(); ?>