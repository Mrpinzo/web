<?php
/* @var $this YiiTypevideoController */
/* @var $model YiiTypevideo */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'yii-typevideo-form',
	'enableAjaxValidation'=>false,
)); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name__typevideo'); ?>
		<?php echo $form->textField($model,'name__typevideo',array('size'=>60,'maxlength'=>500)); ?>
		<?php echo $form->error($model,'name__typevideo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'showhide__typevideo'); ?>
		<?php echo $form->checkBox($model,'showhide__typevideo'); ?>
		<?php echo $form->error($model,'showhide__typevideo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'title_typevideo'); ?>
		<?php echo $form->textField($model,'title_typevideo',array('size'=>60,'maxlength'=>500)); ?>
		<?php echo $form->error($model,'title_typevideo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'keywords_typevideo'); ?>
		<?php echo $form->textField($model,'keywords_typevideo',array('size'=>60,'maxlength'=>500)); ?>
		<?php echo $form->error($model,'keywords_typevideo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'description_typevideo'); ?>
		<?php echo $form->textField($model,'description_typevideo',array('size'=>60,'maxlength'=>500)); ?>
		<?php echo $form->error($model,'description_typevideo'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Tạo mới' : 'Sửa'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->