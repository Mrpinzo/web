<?php
/* @var $this YiiTypevideoController */
/* @var $model YiiTypevideo */

$this->breadcrumbs=array(
	'Loại Video'=>array('admin'),
	$model->name__typevideo=>array('view','id'=>$model->id_typevideo),
	'Sửa',
);

$this->menu=array(
	array('label'=>'Danh sách Video', 'url'=>array('admin')),
);
?>

<h4>Sửa loại video <?php echo $model->name__typevideo; ?></h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>