<?php
/* @var $this YiiTypevideoController */
/* @var $model YiiTypevideo */

$this->breadcrumbs=array(
	'Loại Video'=>array('admin'),
	'Danh sách',
);

$this->menu=array(
	array('label'=>'Tạo mới', 'url'=>array('create')),
);

?>

<h4>Loại Video</h4>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'yii-typevideo-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_typevideo',
		'name__typevideo',
			array(
					'name'   => 'showhide__typevideo',
					'value'  => '$data->showhide__typevideo==1?"<i class=\'fa fa-check-square-o csm\'></i>":"<i class=\'fa fa-times csm\'></i>"',
					'filter' => array(1 => 'Hiện', 0 => 'Ẩn'),
					'type'  => 'raw'
			),
		'title_typevideo',
		'keywords_typevideo',		
		'description_typevideo',
		'date_typevideo',
		array(
			'class'=>'CButtonColumn',
				'header' => 'Hành động',
				'template'=>'{update}{delete}',
				'buttons'=>array
				(
				
						'update' => array(
								'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'Update')),
								'label' => '<i class="fa fa-pencil-square-o"></i>',
								'imageUrl' => false,
						),
						'delete' => array(
								'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'Delete')),
								'label' => '<i class="fa fa-trash-o"></i>',
								'imageUrl' => false,
						)
				),
		),
	),
)); ?>
