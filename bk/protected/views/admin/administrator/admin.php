<?php
/* @var $this AdministratorController */
/* @var $model Administrator */

$this->breadcrumbs=array(
	'Quản trị'=>array('admin'),
	'Danh sách',
);

 $this->menu = array(
        array('label' => 'Thêm mới', 'url' => array('create'))
    );
//var_dump($this->menu);
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#administrator-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>


<h4>Danh sách quản trị</h4>

<?php $form = $this->beginWidget('CActiveForm', array(
    'enableAjaxValidation' => TRUE,
)); ?>
<script>
    function reloadGrid(data) {
        $.fn.yiiGridView.update('administrator-grid');
    }
</script>
<input id="yt10" type="submit" value="Kích hoạt" name="yt10" style="display:none;" class="search-button">
<?php echo CHtml::ajaxSubmitButton('Xóa', array('Administrator/ajaxUpdate', 'act' => 'doDelete'), array('success' => 'reloadGrid', 'beforeSend' => 'function(){
            return confirm("Bạn có chắc chắn muốn xóa những tài khoản được chọn?")
        }',)); ?>
<?php
$cs = Yii::app()->clientScript;
$css = 'ul.yiiPager .first, ul.yiiPager .last {display:inline;}';
$cs->registerCss('show_first_last_buttons', $css);
 $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'administrator-grid',
 		'ajaxUpdate'=>false,
	'dataProvider'=>$model->search(),
	'filter'=>$model,
		'pager' => array('maxButtonCount' => 4,'pageSize'=>10,'firstPageLabel'=>'<i class="fa fa-angle-double-left"></i>','lastPageLabel'=>'<i class="fa fa-angle-double-right"></i>'
				,'nextPageLabel' => '<i class="fa fa-angle-right"></i>',
				'prevPageLabel'=> '<i class="fa fa-angle-left"></i>',
				'header'=> '',
				
		        ),
	'columns'=>array(
		array(
				'id'             => 'autoId',
				'class'          => 'CCheckBoxColumn',
				'selectableRows' => '50',
		),
		'ad_id',
		'username',

		'ad_fullname',
		'ad_phone',
		'ad_email',
			array(
					'name'   => 'id_role',
					'value'  => 'isset($data->id_role)?$data->YiiRoles->name_role:""',
					'filter' => CHtml::activeDropDownList($model, 'id_role', CHtml::listData(YiiRole::model()->findAll(), "id_role", "name_role"), array("empty" => "Chọn loại quyền")),
			),
		array(
			'class'=>'CButtonColumn',
				'header' => 'Hành động',
				'buttons'=>array
				(
						'view' => array(
								'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'View')),
								'label' => '<i class="fa fa-search"></i>',
								'imageUrl' => false,
						),
						'update' => array(
								'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'Update')),
								'label' => '<i class="fa fa-pencil-square-o"></i>',
								'imageUrl' => false,
						),
						'delete' => array(
								'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'Delete')),
								'label' => '<i class="fa fa-trash-o"></i>',
								'imageUrl' => false,
						)
				),
		),
	),
)); ?>
<?php $this->endWidget(); ?>
