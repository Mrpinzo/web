<?php
/* @var $this AdministratorController */
/* @var $model Administrator */

$this->breadcrumbs=array(
	'Quản trị'=>array('admin'),
	$model->username,
);

$this->menu=array(
	array('label'=>'Danh sách quản trị', 'url'=>array('admin')),
);
?>

<h4>Chi tiết - <?php echo $model->username; ?></h4>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ad_id',
		'username',
		'ad_fullname',
		'ad_phone',
		'ad_email',
	),
)); ?>
