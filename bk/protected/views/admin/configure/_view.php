<?php
/* @var $this ConfigureController */
/* @var $data Configure */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_configure')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_configure), array('view', 'id'=>$data->id_configure)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('logo_configure')); ?>:</b>
	<?php echo CHtml::encode($data->logo_configure); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('about_configure')); ?>:</b>
	<?php echo CHtml::encode($data->about_configure); ?>
	<br />


</div>