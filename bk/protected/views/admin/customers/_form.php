<?php
/* @var $this CustomersController */
/* @var $model Customers */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'customers-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'cust_email'); ?>
		<?php echo $form->textField($model,'cust_email',array('size'=>60,'maxlength'=>150)); ?>
		<?php echo $form->error($model,'cust_email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cust_pass'); ?>
		<?php echo $form->textField($model,'cust_pass',array('size'=>60,'maxlength'=>150)); ?>
		<?php echo $form->error($model,'cust_pass'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cust_from'); ?>
		<?php echo $form->textField($model,'cust_from',array('size'=>60,'maxlength'=>150)); ?>
		<?php echo $form->error($model,'cust_from'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cust_gender'); ?>
		<?php echo $form->textField($model,'cust_gender'); ?>
		<?php echo $form->error($model,'cust_gender'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cust_fullname'); ?>
		<?php echo $form->textField($model,'cust_fullname',array('size'=>60,'maxlength'=>80)); ?>
		<?php echo $form->error($model,'cust_fullname'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cust_active'); ?>
		<?php echo $form->textField($model,'cust_active'); ?>
		<?php echo $form->error($model,'cust_active'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cust_phone'); ?>
		<?php echo $form->textField($model,'cust_phone'); ?>
		<?php echo $form->error($model,'cust_phone'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cust_birth'); ?>
		<?php echo $form->textField($model,'cust_birth'); ?>
		<?php echo $form->error($model,'cust_birth'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->