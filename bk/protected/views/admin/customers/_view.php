<?php
/* @var $this CustomersController */
/* @var $data Customers */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('cust_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->cust_id), array('view', 'id'=>$data->cust_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cust_email')); ?>:</b>
	<?php echo CHtml::encode($data->cust_email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cust_pass')); ?>:</b>
	<?php echo CHtml::encode($data->cust_pass); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cust_from')); ?>:</b>
	<?php echo CHtml::encode($data->cust_from); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cust_gender')); ?>:</b>
	<?php echo CHtml::encode($data->cust_gender); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cust_fullname')); ?>:</b>
	<?php echo CHtml::encode($data->cust_fullname); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cust_active')); ?>:</b>
	<?php echo CHtml::encode($data->cust_active); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('cust_phone')); ?>:</b>
	<?php echo CHtml::encode($data->cust_phone); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cust_birth')); ?>:</b>
	<?php echo CHtml::encode($data->cust_birth); ?>
	<br />

	*/ ?>

</div>