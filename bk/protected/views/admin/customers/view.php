<?php
/* @var $this CustomersController */
/* @var $model Customers */

$this->breadcrumbs=array(
	'Danh sách khách hàng'=>array('admin'),
	$model->cust_fullname,
);

$this->menu=array(
	array('label'=>'Danh sách khách hàng', 'url'=>array('admin')),
);
?>

<h4>Chi tiết khách hàng <?php echo $model->cust_fullname; ?></h4>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'cust_id',
		'cust_email',
		'cust_pass',
		'cust_from',
	
			array(
					'name'   => 'cust_gender',
					'value'  => $model->cust_gender==1?"Nam":"Nữ",
					
			),
		'cust_fullname',
		
			array(
					'name'   => 'cust_active',
					'value'  => $model->cust_active==1?"Có":"Không",
			),
		'cust_phone',
		
			array(
					'name'   => 'cust_birth',
					'value'  => date('d-m-Y',strtotime($model->cust_birth )),
			),
	),
)); ?>
