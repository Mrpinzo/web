<?php
/* @var $this LinksController */
/* @var $model Links */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_links'); ?>
		<?php echo $form->textField($model,'id_links'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'name_links'); ?>
		<?php echo $form->textField($model,'name_links',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'showhide_links'); ?>
		<?php echo $form->textField($model,'showhide_links'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'url_links'); ?>
		<?php echo $form->textField($model,'url_links',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->