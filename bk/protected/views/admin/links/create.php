<?php
/* @var $this LinksController */
/* @var $model Links */

$this->breadcrumbs=array(
	'Liên kết'=>array('admin'),
	'Tạo mới',
);

$this->menu=array(
	array('label'=>'Danh sách liên kết', 'url'=>array('admin')),
);
?>

<h4>Tạo mới liên kết</h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>