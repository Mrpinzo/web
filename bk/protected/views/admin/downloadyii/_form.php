<?php
/* @var $this DownloadyiiController */
/* @var $model Downloadyii */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'downloadyii-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'fullnam_downloadyii'); ?>
		<?php echo $form->textField($model,'fullnam_downloadyii',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'fullnam_downloadyii'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'company_downloadyii'); ?>
		<?php echo $form->textField($model,'company_downloadyii',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'company_downloadyii'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'phone_downloadyii'); ?>
		<?php echo $form->textField($model,'phone_downloadyii',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'phone_downloadyii'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email_downloadyii'); ?>
		<?php echo $form->textField($model,'email_downloadyii',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'email_downloadyii'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'address_downloadyii'); ?>
		<?php echo $form->textField($model,'address_downloadyii',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'address_downloadyii'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'title_downloadyii'); ?>
		<?php echo $form->textField($model,'title_downloadyii',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'title_downloadyii'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'content_downloadyii'); ?>
		<?php echo $form->textArea($model,'content_downloadyii',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'content_downloadyii'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'mobile_downloadyii'); ?>
		<?php echo $form->textField($model,'mobile_downloadyii',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'mobile_downloadyii'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->