<?php
/* @var $this AdvertiseController */
/* @var $model Advertise */

$this->breadcrumbs=array(
	'Comment'=>array('admin'),
	'Create',
);

$this->menu=array(
	array('label'=>'Danh sách ', 'url'=>array('admin')),
);
?>

<h4>Create</h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>