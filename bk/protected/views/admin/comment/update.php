<?php
/* @var $this CmertiseController */
/* @var $model Cmertise */

$this->breadcrumbs=array(
	'Comment'=>array('admin'),
	$model->cm_link_en,
	'Sửa',
);

$this->menu=array(
	array('label'=>'Danh sách', 'url'=>array('admin')),
);
?>

<h4>Edit Comment <?php echo $model->cm_link_en; ?></h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>