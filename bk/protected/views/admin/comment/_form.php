
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'cmertise-form',
	'enableAjaxValidation'=>false,
		'htmlOptions'  => array(
				'enctype' => 'multipart/form-data',
		),
)); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'cm_link_en'); ?>
		<?php echo $form->textField($model,'cm_link_en',array('size'=>60,'maxlength'=>2000)); ?>
		<?php echo $form->error($model,'cm_link_en'); ?>
	</div>
    <div class="row">
		<?php echo $form->labelEx($model,'cm_link_fr'); ?>
		<?php echo $form->textField($model,'cm_link_fr',array('size'=>60,'maxlength'=>2000)); ?>
		<?php echo $form->error($model,'cm_link_fr'); ?>
	</div>
    <div class="row">
		<?php echo $form->labelEx($model,'cm_link_sp'); ?>
		<?php echo $form->textField($model,'cm_link_sp',array('size'=>60,'maxlength'=>2000)); ?>
		<?php echo $form->error($model,'cm_link_sp'); ?>
	</div>
  
  
    <div class="row">
		<?php echo $form->labelEx($model,'cm_name_en'); ?>		
		<?php echo CHtml::activeTextArea($model,'cm_name_en',array('rows'=>10, 'cols'=>40)); ?>
		<?php echo $form->error($model,'cm_name_en'); ?>
	</div>
	 <div class="row">
		<?php echo $form->labelEx($model,'cm_name_fr'); ?>		
		<?php echo CHtml::activeTextArea($model,'cm_name_fr',array('rows'=>10, 'cols'=>40)); ?>
		<?php echo $form->error($model,'cm_name_fr'); ?>
	</div>
     <div class="row">
		<?php echo $form->labelEx($model,'cm_name_sp'); ?>		
		<?php echo CHtml::activeTextArea($model,'cm_name_sp',array('rows'=>10, 'cols'=>40)); ?>
		<?php echo $form->error($model,'cm_name_sp'); ?>
	</div>

    <div class="row">
		<?php echo $form->labelEx($model,'cm_action'); ?>
		<?php echo $form->checkBox($model, 'cm_action'); ?>
		
		<?php echo $form->error($model,'cm_action'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'cm_order'); ?>
		<?php echo $form->textField($model,'cm_order',array('size'=>60,'maxlength'=>2000)); ?>
		<?php echo $form->error($model,'cm_order'); ?>
	</div>
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Tạo mới' : 'Sửa'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->