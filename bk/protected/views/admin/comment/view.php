<?php
/* @var $this cmertiseController */
/* @var $model cmertise */

$this->breadcrumbs=array(
	'View comment'=>array('admin'),
	$model->cm_link_en,
);

$this->menu=array(
	array('label'=>'Danh sách', 'url'=>array('admin')),
);
?>

<h4>Detail <?php echo $model->cm_link_en; ?></h4>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'cm_id',
		'cm_link_en',
        'cm_link_fr',
        'cm_link_sp',
		'cm_name_en',
        'cm_name_fr',
        'cm_name_sp',
			array(
					'name'  => 'cm_action',
					'value' =>  $model->cm_action == 0? 'Ẩn':'Hiện',
					'type'  => 'raw',
			),
			array(
					'name'  => 'cm_positions',
					'value' =>  $model->cm_positions == 0? 'Trái':'Phải',
					'type'  => 'raw',
			),
	),
)); ?>
