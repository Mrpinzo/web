<?php
/* @var $this YiiVideoController */
/* @var $model YiiVideo */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'yii-video-form',
	'enableAjaxValidation'=>false,
)); ?>

<div class="row">
		<?php echo $form->labelEx($model,'id_typevideo'); ?>
		<?php echo $form->dropDownList($model, 'id_typevideo',CHtml::listData(YiiTypevideo::model()->findAll(), "id_typevideo", "name__typevideo"), array("empty" => "Chọn loại Video")) ?>
		<?php echo $form->error($model,'id_typevideo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'name_video'); ?>
		<?php echo $form->textField($model,'name_video',array('size'=>60,'maxlength'=>5000)); ?>
		<?php echo $form->error($model,'name_video'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'url_video'); ?>
		<?php echo $form->textField($model,'url_video',array('size'=>60,'maxlength'=>5000)); ?>
		<?php echo $form->error($model,'url_video'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'title_video'); ?>
		<?php echo $form->textField($model,'title_video',array('size'=>60,'maxlength'=>5000)); ?>
		<?php echo $form->error($model,'title_video'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'keywords_video'); ?>
		<?php echo $form->textField($model,'keywords_video',array('size'=>60,'maxlength'=>5000)); ?>
		<?php echo $form->error($model,'keywords_video'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'description_video'); ?>
		<?php echo $form->textField($model,'description_video',array('size'=>60,'maxlength'=>5000)); ?>
		<?php echo $form->error($model,'description_video'); ?>
	</div>	

	<div class="row">
		<?php echo $form->labelEx($model,'highlights_video'); ?>
		<?php echo $form->checkBox($model,'highlights_video'); ?>
		<?php echo $form->error($model,'highlights_video'); ?>
	</div>
	
		<div class="row">
		<?php echo $form->labelEx($model,'showhide_video'); ?>
		<?php echo $form->checkBox($model,'showhide_video'); ?>
		<?php echo $form->error($model,'showhide_video'); ?>
	</div>
	
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Tạo mới' : 'Sửa'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->