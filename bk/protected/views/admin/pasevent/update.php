<?php
/* @var $this ProductController */
/* @var $model Product */

$this->breadcrumbs=array(
	'Hình ảnh'=>array('admin'),
	$model->pr_name=>"/san-pham/".$model->pr_url_news.".html",
	'Sửa',
);

$this->menu=array(
	array('label'=>'List images', 'url'=>array('admin')),
);
?>

<h4>Update  <?php echo $model->pr_name; ?></h4>

<?php echo $this->renderPartial('_form', array('model'=>$model,'YiiMenu' =>$YiiMenu,'imgproduct'=>$imgproduct,'join' => $join,'subarrFilter'=>$subarrFilter)); ?>