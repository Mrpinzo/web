<?php
/* @var $this ProductController */
/* @var $model Product */

$this->breadcrumbs=array(
	'Pasevent'=>array('admin'),
	'Tạo mới',
);

$this->menu=array(
	array('label'=>'Danh sách', 'url'=>array('admin')),
);
?>

<h4>Tạo mới Pasevent</h4>

<?php echo $this->renderPartial('_form', array('model'=>$model,'YiiMenu' =>$YiiMenu,'id'=>$id,'subarrFilter'=>$subarrFilter)); ?>