<?php
/* @var $this YiiRoleController */
/* @var $model YiiRole */

$this->breadcrumbs=array(
	'Nhóm quyền'=>array('admin'),
	$model->name_role,
	'Sửa',
);

$this->menu=array(
	array('label'=>'Danh sách nhóm quyền', 'url'=>array('admin')),
);
?>

<h4>Sửa nhóm quyền <?php echo $model->name_role; ?></h1>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'yii-role-form',
	'enableAjaxValidation'=>false,
)); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name_role'); ?>
		<?php echo $form->textField($model,'name_role',array('size'=>60,'maxlength'=>500)); ?>
		<?php echo $form->error($model,'name_role'); ?>
	</div>

	<div id="title_roles">Chọn quyền</div>
	<div class="title_roles_items">
	<div id="checkall"><input type="checkbox" name="checkall" id="checkallinput">Tất cả</div>
	<div class="itemsrole">
	 <div>Quản trị</div>
	   <ul>
	      <li><input type="checkbox" class="rolesd" name="Administrator_admin" <?php  if(!empty($Administrator_admin)){echo $Administrator_admin='Administrator_admin'?'checked=checked':'';} ?> id="Administrator_admin" value="Administrator_admin">Xem danh sách</li>
	      <li><input type="checkbox" class="rolesd" name="Administrator_view" <?php if(!empty($Administrator_view)){echo $Administrator_view='Administrator_view'?'checked=checked':'';} ?> id="Administrator_view" value="Administrator_view">Xem chi tiết</li>
	      <li><input type="checkbox" class="rolesd" name="Administrator_create" <?php if(!empty($Administrator_create)){echo $Administrator_create='Administrator_create'?'checked=checked':'';} ?> id="Administrator_create" value="Administrator_create">Tạo mới</li>
	      <li><input type="checkbox" class="rolesd" name="Administrator_update" <?php if(!empty($Administrator_update)){echo $Administrator_update='Administrator_update'?'checked=checked':'';} ?> id="Administrator_update" value="Administrator_update">Sửa</li>
	      <li><input type="checkbox" class="rolesd" name="Administrator_delete" <?php if(!empty($Administrator_delete)){echo $Administrator_delete='Administrator_delete'?'checked=checked':'';} ?> id="Administrator_delete" value="Administrator_delete">Xóa</li>
	   	      <li><input type="checkbox"  class="rolesd" name="Administrator_ajaxUpdate" <?php if(!empty($Administrator_ajaxUpdate)){echo $Administrator_ajaxUpdate='Administrator_ajaxUpdate'?'checked=checked':'';} ?>  id="Administrator_ajaxUpdate" value="Administrator_ajaxUpdate">Xóa tất cả</li>
	   
	   </ul>
	   </div>
	   <div class="itemsrole">
	   <div>Cấu hình</div>
	   <ul>
	      <li><input type="checkbox" class="rolesd"  name="Configure_view" <?php if(!empty($Configure_view)){echo $Configure_view='Configure_view'?'checked=checked':'';} ?> id="Configure_view" value="Configure_view">Xem cấu hình</li>
	      <li><input type="checkbox"  class="rolesd" name="Configure_create" <?php if(!empty($Configure_create)){echo $Configure_create='Configure_create'?'checked=checked':'';} ?> id="Configure_create" value="Configure_create">Tạo mới</li>
	      <li><input type="checkbox"  class="rolesd" name="Configure_update" <?php if(!empty($Configure_update)){echo $Configure_update='Configure_update'?'checked=checked':'';} ?> id="Configure_update" value="Configure_update">Sửa</li>
	   </ul>
	   </div>
	   <div class="itemsrole">
	   <div>Nhóm quyền</div>
	   <ul>
	      <li><input type="checkbox"  class="rolesd" name="YiiRole_admin" <?php if(!empty($YiiRole_admin)){echo $YiiRole_admin='YiiRole_admin'?'checked=checked':'';} ?> id="YiiRole_admin" value="YiiRole_admin">Xem danh sách</li>
	      <li><input type="checkbox"  class="rolesd" name="YiiRole_view" <?php if(!empty($YiiRole_view)){echo $YiiRole_view='YiiRole_view'?'checked=checked':'';} ?> id="YiiRole_view" value="YiiRole_view">Xem chi tiết</li>
	      <li><input type="checkbox"  class="rolesd" name="YiiRole_create" <?php if(!empty($YiiRole_create)){echo $YiiRole_create='YiiRole_create'?'checked=checked':'';} ?> id="YiiRole_create" value="YiiRole_create">Tạo mới</li>
	      <li><input type="checkbox"  class="rolesd" name="YiiRole_update" <?php if(!empty($YiiRole_update)){echo $YiiRole_update='YiiRole_update'?'checked=checked':'';} ?> id="YiiRole_update" value="YiiRole_update">Sửa</li>
	      <li><input type="checkbox"  class="rolesd" name="YiiRole_delete" <?php if(!empty($YiiRole_delete)){echo $YiiRole_delete='YiiRole_delete'?'checked=checked':'';} ?> id="YiiRole_delete" value="YiiRole_delete">Xóa</li>
	   </ul>
	   </div>
	   <div class="itemsrole">
	   <div>Tin tức</div>
	   <ul>
	      <li><input type="checkbox"  class="rolesd" name="News_admin" <?php if(!empty($News_admin)){echo $News_admin='News_admin'?'checked=checked':'';} ?> id="News_admin" value="News_admin">Xem danh sách</li>
	      <li><input type="checkbox"  class="rolesd" name="News_view" <?php if(!empty($News_view)){echo $News_view='News_view'?'checked=checked':'' ;}?> id="News_view" value="News_view">Xem chi tiết</li>
	      <li><input type="checkbox"  class="rolesd" name="News_create" <?php if(!empty($News_create)){echo $News_create='News_create'?'checked=checked':'';} ?> id="News_create" value="News_create">Tạo mới</li>
	      <li><input type="checkbox"  class="rolesd" name="News_update" <?php if(!empty($News_update)){echo $News_update='News_update'?'checked=checked':'' ;}?> id="News_update" value="News_update">Sửa</li>
	      <li><input type="checkbox"  class="rolesd" name="News_delete" <?php if(!empty($News_delete)){echo $News_delete='News_delete'?'checked=checked':'' ;}?> id="News_delete" value="News_delete">Xóa</li>
	      <li><input type="checkbox"  class="rolesd" name="News_ajaxUpdate" <?php if(!empty($News_ajaxUpdate)){echo $News_ajaxUpdate='News_ajaxUpdate'?'checked=checked':'' ;}?>  id="News_ajaxUpdate" value="News_ajaxUpdate">Hiện-Nổi bật-Xóa tất cả</li>
	      
	   </ul>
	   </div>
	   <div class="itemsrole">
	    <div>Sản phẩm</div>
	   <ul>
	      <li><input type="checkbox"  class="rolesd" name="Product_admin" <?php if(!empty($Product_admin)){echo $Product_admin='Product_admin'?'checked=checked':'' ;}?> id="Product_admin" value="Product_admin">Xem danh sách</li>
	      <li><input type="checkbox"  class="rolesd" name="Product_view" <?php if(!empty($Product_view)){echo $Product_view='Product_view'?'checked=checked':'' ;}?> id="Product_view" value="Product_view">Xem chi tiết</li>
	      <li><input type="checkbox"  class="rolesd" name="Product_create" <?php if(!empty($Product_create)){echo $Product_create='Product_create'?'checked=checked':'' ;}?> id="Product_create" value="Product_create">Tạo mới</li>
	      <li><input type="checkbox"  class="rolesd" name="Product_update" <?php if(!empty($Product_update)){echo $Product_update='Product_update'?'checked=checked':'';} ?> id="Product_update" value="Product_update">Sửa</li>
	      <li><input type="checkbox"  class="rolesd" name="Product_delete" <?php if(!empty($Product_delete)){echo $Product_delete='Product_delete'?'checked=checked':'' ;}?> id="Product_delete" value="Product_delete">Xóa</li>
	      <li><input type="checkbox"  class="rolesd" name="Product_ajaxUpdate" <?php if(!empty($Product_ajaxUpdate)){echo $Product_ajaxUpdate='Product_ajaxUpdate'?'checked=checked':'' ;}?>  id="Product_ajaxUpdate" value="Product_ajaxUpdate">Hiện - Ẩn - Xóa tất cả</li>
	      
	   </ul>
	    </div>
        
      <div class="itemsrole">
	    <div>Pass event</div>
	   <ul>
	      <li><input type="checkbox"  class="rolesd" name="Pasevent_admin" <?php if(!empty($Pasevent_admin)){echo $Pasevent_admin='Pasevent_admin'?'checked=checked':'' ;}?> id="Pasevent_admin" value="Pasevent_admin">Xem danh sách</li>
	      <li><input type="checkbox"  class="rolesd" name="Pasevent_view" <?php if(!empty($Pasevent_view)){echo $Pasevent_view='Pasevent_view'?'checked=checked':'' ;}?> id="Pasevent_view" value="Pasevent_view">Xem chi tiết</li>
	      <li><input type="checkbox"  class="rolesd" name="Pasevent_create" <?php if(!empty($Pasevent_create)){echo $Pasevent_create='Pasevent_create'?'checked=checked':'' ;}?> id="Pasevent_create" value="Pasevent_create">Tạo mới</li>
	      <li><input type="checkbox"  class="rolesd" name="Pasevent_update" <?php if(!empty($Pasevent_update)){echo $Pasevent_update='Pasevent_update'?'checked=checked':'';} ?> id="Pasevent_update" value="Pasevent_update">Sửa</li>
	      <li><input type="checkbox"  class="rolesd" name="Pasevent_delete" <?php if(!empty($Pasevent_delete)){echo $Pasevent_delete='Pasevent_delete'?'checked=checked':'' ;}?> id="Pasevent_delete" value="Pasevent_delete">Xóa</li>
	      <li><input type="checkbox"  class="rolesd" name="Pasevent_ajaxUpdate" <?php if(!empty($Pasevent_ajaxUpdate)){echo $Pasevent_ajaxUpdate='Pasevent_ajaxUpdate'?'checked=checked':'' ;}?>  id="Pasevent_ajaxUpdate" value="Pasevent_ajaxUpdate">Hiện - Ẩn - Xóa tất cả</li>
	      
	   </ul>
	    </div>
	    <div class="itemsrole">
	    <div>Quảng cáo</div>
	   <ul>
	      <li><input type="checkbox"  class="rolesd" name="Advertise_admin" <?php if(!empty($Advertise_admin)){echo $Advertise_admin='Advertise_admin'?'checked=checked':'';} ?> id="Advertise_admin" value="Advertise_admin">Xem danh sách</li>
	      <li><input type="checkbox"  class="rolesd" name="Advertise_view" <?php if(!empty($Advertise_view)){echo $Advertise_view='Advertise_view'?'checked=checked':'' ;}?> id="Advertise_view" value="Advertise_view">Xem chi tiết</li>
	      <li><input type="checkbox"  class="rolesd" name="Advertise_create" <?php if(!empty($Advertise_create)){echo $Advertise_create='Advertise_create'?'checked=checked':'' ;}?> id="Advertise_create" value="Advertise_create">Tạo mới</li>
	      <li><input type="checkbox"  class="rolesd" name="Advertise_update" <?php if(!empty($Advertise_update)){echo $Advertise_update='Advertise_update'?'checked=checked':'' ;}?> id="Advertise_update" value="Advertise_update">Sửa</li>
	      <li><input type="checkbox"  class="rolesd" name="Advertise_delete" <?php if(!empty($Advertise_delete)){echo $Advertise_delete='Advertise_delete'?'checked=checked':'' ;}?> id="Advertise_delete" value="Advertise_delete">Xóa</li>
	      	      <li><input type="checkbox"  class="rolesd" name="Advertise_ajaxUpdate" <?php if(!empty($Advertise_ajaxUpdate)){echo $Advertise_ajaxUpdate='Advertise_ajaxUpdate'?'checked=checked':'' ;}?> id="Advertise_ajaxUpdate" id="Advertise_ajaxUpdate" value="Advertise_ajaxUpdate">Hiện - Ẩn - Xóa tất cả</li>
	      
	   </ul>
	   </div>
      <div class="itemsrole">
	    <div>Comment</div>
	   <ul>
	      <li><input type="checkbox"  class="rolesd" name="Comment_admin" <?php if(!empty($Comment_admin)){echo $Comment_admin='Comment_admin'?'checked=checked':'';} ?> id="Comment_admin" value="Comment_admin">Xem danh sách</li>
	      <li><input type="checkbox"  class="rolesd" name="Comment_view" <?php if(!empty($Comment_view)){echo $Comment_view='Comment_view'?'checked=checked':'' ;}?> id="Comment_view" value="Comment_view">Xem chi tiết</li>
	      <li><input type="checkbox"  class="rolesd" name="Comment_create" <?php if(!empty($Comment_create)){echo $Comment_create='Comment_create'?'checked=checked':'' ;}?> id="Comment_create" value="Comment_create">Tạo mới</li>
	      <li><input type="checkbox"  class="rolesd" name="Comment_update" <?php if(!empty($Comment_update)){echo $Comment_update='Comment_update'?'checked=checked':'' ;}?> id="Comment_update" value="Comment_update">Sửa</li>
	      <li><input type="checkbox"  class="rolesd" name="Comment_delete" <?php if(!empty($Comment_delete)){echo $Comment_delete='Comment_delete'?'checked=checked':'' ;}?> id="Comment_delete" value="Comment_delete">Xóa</li>
	      	      <li><input type="checkbox"  class="rolesd" name="Comment_ajaxUpdate" <?php if(!empty($Comment_ajaxUpdate)){echo $Comment_ajaxUpdate='Comment_ajaxUpdate'?'checked=checked':'' ;}?> id="Comment_ajaxUpdate" id="Comment_ajaxUpdate" value="Comment_ajaxUpdate">Hiện - Ẩn - Xóa tất cả</li>
	      
	   </ul>
	   </div>
	   <div class="itemsrole">
	   <div>Danh sách menu</div>
	   <ul>
	      <li><input type="checkbox"  class="rolesd" name="YiiMenu_admin" <?php if(!empty($YiiMenu_admin)){echo $YiiMenu_admin='YiiMenu_admin'?'checked=checked':'';} ?> id="YiiMenu_admin" value="YiiMenu_admin">Xem danh sách</li>
	      <li><input type="checkbox"  class="rolesd" name="YiiMenu_view" <?php if(!empty($YiiMenu_view)){echo $YiiMenu_view='YiiMenu_view'?'checked=checked':'';} ?> id="YiiMenu_view" value="YiiMenu_view">Xem chi tiết</li>
	      <li><input type="checkbox"  class="rolesd" name="YiiMenu_create" <?php if(!empty($YiiMenu_create)){echo $YiiMenu_create='YiiMenu_create'?'checked=checked':'' ;}?> id="YiiMenu_create" value="YiiMenu_create">Tạo mới</li>
	      <li><input type="checkbox"  class="rolesd" name="YiiMenu_update" <?php if(!empty($YiiMenu_update)){echo $YiiMenu_update='YiiMenu_update'?'checked=checked':'' ;}?> id="YiiMenu_update" value="YiiMenu_update">Sửa</li>
	      <li><input type="checkbox"  class="rolesd" name="YiiMenu_delete" <?php if(!empty($YiiMenu_delete)){echo $YiiMenu_delete='YiiMenu_delete'?'checked=checked':'' ;}?> id="YiiMenu_delete" value="YiiMenu_delete">Xóa</li>
	       <li><input type="checkbox"  class="rolesd" name="YiiMenu_ajaxUpdate" <?php if(!empty($YiiMenu_ajaxUpdate)){echo $YiiMenu_ajaxUpdate='YiiMenu_ajaxUpdate'?'checked=checked':'';} ?> id="YiiMenu_ajaxUpdate" value="YiiMenu_ajaxUpdate">Hiện - Ẩn - Xóa tất cả</li>
	       <li><input type="checkbox"  class="rolesd" name="YiiMenu_updateAjax" <?php if(!empty($YiiMenu_updateAjax)){echo $YiiMenu_updateAjax='YiiMenu_updateAjax'?'checked=checked':'';} ?> id="YiiMenu_updateAjax" value="YiiMenu_updateAjax">Sắp xếp thứ tự menu</li>
	   </ul>
	   </div>
	   <div class="itemsrole">
	    <div>Liên kết</div>
	   <ul>
	      <li><input type="checkbox"  class="rolesd" name="Links_admin" <?php if(!empty($Links_admin)){echo $Links_admin='Links_admin'?'checked=checked':'';} ?> id="Links_admin" value="Links_admin">Xem danh sách</li>
	      <li><input type="checkbox"  class="rolesd" name="Links_view" <?php if(!empty($Links_view)){echo $Links_view='Links_view'?'checked=checked':'';} ?> id="Links_view" value="Links_view">Xem chi tiết</li>
	      <li><input type="checkbox"  class="rolesd" name="Links_create" <?php if(!empty($Links_create)){echo $Links_create='Links_create'?'checked=checked':'';} ?> id="Links_create" value="Links_create">Tạo mới</li>
	      <li><input type="checkbox"  class="rolesd" name="Links_update" <?php if(!empty($Links_update)){echo $Links_update='Links_update'?'checked=checked':'';} ?> id="Links_update" value="Links_update">Sửa</li>
	      <li><input type="checkbox"  class="rolesd" name="Links_delete" <?php if(!empty($Links_delete)){echo $Links_delete='Links_delete'?'checked=checked':'';} ?> id="Links_delete" value="Links_delete">Xóa</li>
	   </ul>
	   </div>
	   <div class="itemsrole">
	    <div>Liên hệ</div>
	   <ul>
	      <li><input type="checkbox"  class="rolesd" name="Contact_admin" <?php if(!empty($Contact_admin)){echo $Contact_admin='Contact_admin'?'checked=checked':'';} ?> id="Contact_admin" value="Contact_admin">Xem danh sách</li>
	      <li><input type="checkbox"  class="rolesd" name="Contact_view" <?php if(!empty($Contact_view)){echo $Contact_view='Contact_view'?'checked=checked':'' ;}?> id="Contact_view" value="Contact_view">Xem chi tiết</li>
	      <li><input type="checkbox"  class="rolesd" name="Contact_create" <?php if(!empty($Contact_create)){echo $Contact_create='Contact_create'?'checked=checked':'' ;}?> id="Contact_create" value="Contact_create">Tạo mới</li>
	      <li><input type="checkbox"  class="rolesd" name="Contact_update" <?php if(!empty($Contact_update)){echo $Contact_update='Contact_update'?'checked=checked':'' ;}?> id="Contact_update" value="Contact_update">Sửa</li>
	      <li><input type="checkbox"  class="rolesd" name="Contact_delete" <?php if(!empty($Contact_delete)){echo $Contact_delete='Contact_delete'?'checked=checked':'' ;}?> id="Contact_delete" value="Contact_delete">Xóa</li>
	      <li><input type="checkbox"  class="rolesd" name="Contact_ajaxUpdate" <?php if(!empty($Contact_ajaxUpdate)){echo $Contact_ajaxUpdate='Contact_ajaxUpdate'?'checked=checked':'' ;}?> id="Contact_ajaxUpdate" value="Contact_ajaxUpdate">Xóa tất cả</li>
	      
	   </ul>
	   </div>
       
       <div class="itemsrole">
    	    <div>Register</div>
    	   <ul>
    	      <li><input type="checkbox"  class="rolesd" name="Register_admin" <?php if(!empty($Register_admin)){echo $Register_admin='Register_admin'?'checked=checked':'';} ?> id="Register_admin" value="Register_admin">Xem danh sách</li>
    	      <li><input type="checkbox"  class="rolesd" name="Register_view" <?php if(!empty($Register_view)){echo $Register_view='Register_view'?'checked=checked':'' ;}?> id="Register_view" value="Register_view">Xem chi tiết</li>
    	      <li><input type="checkbox"  class="rolesd" name="Register_create" <?php if(!empty($Register_create)){echo $Register_create='Register_create'?'checked=checked':'' ;}?> id="Register_create" value="Register_create">Tạo mới</li>
    	      <li><input type="checkbox"  class="rolesd" name="Register_update" <?php if(!empty($Register_update)){echo $Register_update='Register_update'?'checked=checked':'' ;}?> id="Register_update" value="Register_update">Sửa</li>
    	      <li><input type="checkbox"  class="rolesd" name="Register_delete" <?php if(!empty($Register_delete)){echo $Register_delete='Register_delete'?'checked=checked':'' ;}?> id="Register_delete" value="Register_delete">Xóa</li>
    	      <li><input type="checkbox"  class="rolesd" name="Register_ajaxUpdate" <?php if(!empty($Register_ajaxUpdate)){echo $Register_ajaxUpdate='Register_ajaxUpdate'?'checked=checked':'' ;}?> id="Register_ajaxUpdate" value="Register_ajaxUpdate">Xóa tất cả</li>
    	      
    	   </ul>
	   </div>
       <div class="itemsrole">
    	    <div>Download</div>
    	   <ul>
    	      <li><input type="checkbox"  class="rolesd" name="Downloadyii_admin" <?php if(!empty($Downloadyii_admin)){echo $Downloadyii_admin='Downloadyii_admin'?'checked=checked':'';} ?> id="Downloadyii_admin" value="Downloadyii_admin">Xem danh sách</li>
    	      <li><input type="checkbox"  class="rolesd" name="Downloadyii_view" <?php if(!empty($Downloadyii_view)){echo $Downloadyii_view='Downloadyii_view'?'checked=checked':'' ;}?> id="Downloadyii_view" value="Downloadyii_view">Xem chi tiết</li>
    	      <li><input type="checkbox"  class="rolesd" name="Downloadyii_create" <?php if(!empty($Downloadyii_create)){echo $Downloadyii_create='Downloadyii_create'?'checked=checked':'' ;}?> id="Downloadyii_create" value="Downloadyii_create">Tạo mới</li>
    	      <li><input type="checkbox"  class="rolesd" name="Downloadyii_update" <?php if(!empty($Downloadyii_update)){echo $Downloadyii_update='Downloadyii_update'?'checked=checked':'' ;}?> id="Downloadyii_update" value="Downloadyii_update">Sửa</li>
    	      <li><input type="checkbox"  class="rolesd" name="Downloadyii_delete" <?php if(!empty($Downloadyii_delete)){echo $Downloadyii_delete='Downloadyii_delete'?'checked=checked':'' ;}?> id="Downloadyii_delete" value="Downloadyii_delete">Xóa</li>
    	      <li><input type="checkbox"  class="rolesd" name="Downloadyii_ajaxUpdate" <?php if(!empty($Downloadyii_ajaxUpdate)){echo $Downloadyii_ajaxUpdate='Downloadyii_ajaxUpdate'?'checked=checked':'' ;}?> id="Downloadyii_ajaxUpdate" value="Downloadyii_ajaxUpdate">Xóa tất cả</li>
    	      
    	   </ul>
	   </div>
	   <div class="itemsrole">
	    <div>Đơn hàng</div>
	   <ul>
	      <li><input type="checkbox"  class="rolesd" name="Orders_admin" <?php if(!empty($Orders_admin)){echo $Orders_admin='Orders_admin'?'checked=checked':'' ;}?> id="Orders_admin" value="Orders_admin">Xem danh sách</li>
	      <li><input type="checkbox"  class="rolesd" name="Orders_view" <?php if(!empty($Orders_view)){echo $Orders_view='Orders_view'?'checked=checked':'' ;}?> id="Orders_view" value="Orders_view">Xem chi tiết</li>
	      <li><input type="checkbox"  class="rolesd" name="Orders_create" <?php if(!empty($Orders_create)){echo $Orders_create='Orders_create'?'checked=checked':'' ;}?> id="Orders_create" value="Orders_create">Tạo mới</li>
	      <li><input type="checkbox"  class="rolesd" name="Orders_update" <?php if(!empty($Orders_update)){echo $Orders_update='Orders_update'?'checked=checked':'' ;}?> id="Orders_update" value="Orders_update">Sửa</li>
	      <li><input type="checkbox"  class="rolesd" name="Orders_delete" <?php if(!empty($Orders_delete)){echo $Orders_delete='Orders_delete'?'checked=checked':'' ;}?> id="Orders_delete" value="Orders_delete">Xóa</li>
	       <li><input type="checkbox"  class="rolesd" name="Orders_ajaxUpdate" <?php if(!empty($Orders_ajaxUpdate)){echo $Orders_ajaxUpdate='Orders_ajaxUpdate'?'checked=checked':'' ;}?> id="Orders_ajaxUpdate" value="Orders_ajaxUpdate">Xóa tất cả</li>
	        <li><input type="checkbox"  class="rolesd" name="Orders_StatusOrder" <?php if(!empty($Orders_StatusOrder)){echo $Orders_StatusOrder='Orders_StatusOrder'?'checked=checked':'' ;}?> id="Orders_StatusOrder" value="Orders_StatusOrder">Thay đổi tình trạng hóa đơn</li>
	     
	   </ul>
	   </div>
	   <div class="itemsrole">
	    <div>Loại Album</div>
	   <ul>
	      <li><input type="checkbox"  class="rolesd" name="YiiTypealbum_admin" <?php if(!empty($YiiTypealbum_admin)){echo $YiiTypealbum_admin='YiiTypealbum_admin'?'checked=checked':'' ;}?>  id="YiiTypealbum_admin" value="YiiTypealbum_admin">Xem danh sách</li>
	      <li><input type="checkbox"  class="rolesd" name="YiiTypealbum_create" <?php if(!empty($YiiTypealbum_create)){echo $YiiTypealbum_create='YiiTypealbum_create'?'checked=checked':'' ;}?>  id="YiiTypealbum_create" value="YiiTypealbum_create">Tạo mới</li>
	      <li><input type="checkbox"  class="rolesd" name="YiiTypealbum_update" <?php if(!empty($YiiTypealbum_update)){echo $YiiTypealbum_update='YiiTypealbum_update'?'checked=checked':'' ;}?>  id="YiiTypealbum_update" value="YiiTypealbum_update">Sửa</li>
	      <li><input type="checkbox"  class="rolesd" name="YiiTypealbum_delete" <?php if(!empty($YiiTypealbum_delete)){echo $YiiTypealbum_delete='YiiTypealbum_delete'?'checked=checked':'' ;}?>  id="YiiTypealbum_delete" value="YiiTypealbum_delete">Xóa</li>
	   
	   </ul>
	   </div>
	   <div class="itemsrole">
	    <div>Ảnh Album</div>
	   <ul>
          <li><input type="checkbox"  class="rolesd" name="YiiAlbum_admin" <?php if(!empty($YiiAlbum_admin)){echo $YiiAlbum_admin='YiiAlbum_admin'?'checked=checked':'' ;}?> id="YiiAlbum_admin" value="YiiAlbum_admin">Xem danh sach</li>       
	       <li><input type="checkbox"  class="rolesd" name="YiiAlbum_create" <?php if(!empty($YiiAlbum_create)){echo $YiiAlbum_create='YiiAlbum_create'?'checked=checked':'' ;}?> id="YiiAlbum_create" value="YiiAlbum_create">Tạo mới</li>
          <li><input type="checkbox"  class="rolesd" name="YiiAlbum_update" <?php if(!empty($YiiAlbum_update)){echo $YiiAlbum_update='YiiAlbum_update'?'checked=checked':'' ;}?> id="YiiAlbum_update" value="YiiAlbum_update">Hiện/Ẩn</li>
	      <li><input type="checkbox"  class="rolesd" name="YiiAlbum_delete" <?php if(!empty($YiiAlbum_delete)){echo $YiiAlbum_delete='YiiAlbum_delete'?'checked=checked':'' ;}?> id="YiiAlbum_delete" value="YiiAlbum_delete">Xóa</li>
	   </ul>
	   </div>
	   <div class="itemsrole">
	    <div>Loại Video</div>
	   <ul>
	      <li><input type="checkbox"  class="rolesd" name="YiiTypevideo_admin"  <?php if(!empty($YiiTypevideo_admin)){echo $YiiTypevideo_admin='YiiTypevideo_admin'?'checked=checked':'' ;}?> id="YiiTypevideo_admin" value="YiiTypevideo_admin">Xem danh sách</li>
	      <li><input type="checkbox"  class="rolesd" name="YiiTypevideo_create"  <?php if(!empty($YiiTypevideo_create)){echo $YiiTypevideo_create='YiiTypevideo_create'?'checked=checked':'' ;}?> id="YiiTypevideo_create" value="YiiTypevideo_create">Tạo mới</li>
	      <li><input type="checkbox"  class="rolesd" name="YiiTypevideo_update"  <?php if(!empty($YiiTypevideo_update)){echo $YiiTypevideo_update='YiiTypevideo_update'?'checked=checked':'' ;}?> id="YiiTypevideo_update" value="YiiTypevideo_update">Sửa</li>
	      <li><input type="checkbox"  class="rolesd" name="YiiTypevideo_delete"  <?php if(!empty($YiiTypevideo_delete)){echo $YiiTypevideo_delete='YiiTypevideo_delete'?'checked=checked':'' ;}?> id="YiiTypevideo_delete" value="YiiTypevideo_delete">Xóa</li>
	   </ul>
	   </div>
	    <div class="itemsrole">
	    <div>Video</div>
	   <ul>
	      <li><input type="checkbox"  class="rolesd" name="YiiVideo_admin" <?php if(!empty($YiiVideo_admin)){echo $YiiVideo_admin='YiiVideo_admin'?'checked=checked':'' ;}?> id="YiiVideo_admin" value="YiiVideo_admin">Xem danh sách</li>
	      <li><input type="checkbox"  class="rolesd" name="YiiVideo_view" <?php if(!empty($YiiVideo_view)){echo $YiiVideo_view='YiiVideo_view'?'checked=checked':'' ;}?> id="YiiVideo_view" value="YiiVideo_view">Xem chi tiết</li>
	      <li><input type="checkbox"  class="rolesd" name="YiiVideo_create" <?php if(!empty($YiiVideo_create)){echo $YiiVideo_create='YiiVideo_create'?'checked=checked':'' ;}?> id="YiiVideo_create" value="YiiVideo_create">Tạo mới</li>
	      <li><input type="checkbox"  class="rolesd" name="YiiVideo_update" <?php if(!empty($YiiVideo_update)){echo $YiiVideo_update='YiiVideo_update'?'checked=checked':'' ;}?> id="YiiVideo_update" value="YiiVideo_update">Sửa</li>
	      <li><input type="checkbox"  class="rolesd" name="YiiVideo_delete" <?php if(!empty($YiiVideo_delete)){echo $YiiVideo_delete='YiiVideo_delete'?'checked=checked':'' ;}?> id="YiiVideo_delete" value="YiiVideo_delete">Xóa</li>	      	  	      
	   </ul>
	</div>
	</div>
	
	<div class="row buttons">
		<?php echo CHtml::submitButton('Sửa'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->