<?php
/* @var $this ProductController */
/* @var $model Product */

$this->breadcrumbs=array(
	'Sản phẩm'=>array('admin'),
	$model->pr_name=>"/san-pham/".$model->pr_url_news.".html",
	'Sửa',
);

$this->menu=array(
	array('label'=>'Danh sách sản phẩm', 'url'=>array('admin')),
);
?>

<h4>Sửa sản phẩm  <?php echo $model->pr_name; ?></h4>

<?php echo $this->renderPartial('_form', array('model'=>$model,'YiiMenu' =>$YiiMenu,'imgproduct'=>$imgproduct,'join' => $join,'subarrFilter'=>$subarrFilter)); ?>