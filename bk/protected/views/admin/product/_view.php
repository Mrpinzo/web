<?php
/* @var $this ProductController */
/* @var $data Product */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('pr_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->pr_id), array('view', 'id'=>$data->pr_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_menu')); ?>:</b>
	<?php echo CHtml::encode($data->id_menu); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('st_id')); ?>:</b>
	<?php echo CHtml::encode($data->st_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pr_name')); ?>:</b>
	<?php echo CHtml::encode($data->pr_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pr_img')); ?>:</b>
	<?php echo CHtml::encode($data->pr_img); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pr_gia')); ?>:</b>
	<?php echo CHtml::encode($data->pr_gia); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pr_mota')); ?>:</b>
	<?php echo CHtml::encode($data->pr_mota); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('pr_number')); ?>:</b>
	<?php echo CHtml::encode($data->pr_number); ?>
	<br />

	*/ ?>

</div>