<?php
    return CMap::mergeArray(
        require(dirname(__FILE__) . '/main.php'),
        array(
            'name'     => 'Quản lý Admin',
            'language' => 'vi',
			'components' => array(
				'urlManager'=>array(
				//'showScriptName' => false,
				  'urlFormat'=>'path',
					'rules'=>array(
						'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
					),
				),
			),
        )
    );
?>