<?php

/**
 * This is the model class for table "yii_typevideo".
 *
 * The followings are the available columns in table 'yii_typevideo':
 * @property integer $id_typevideo
 * @property string $name__typevideo
 * @property integer $showhide__typevideo
 * @property string $url_typevideo
 * @property string $title_typevideo
 * @property string $keywords_typevideo
 * @property string $description_typevideo
 */
class YiiTypevideo extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return YiiTypevideo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'yii_typevideo';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name__typevideo', 'required'),
			array('showhide__typevideo', 'numerical', 'integerOnly'=>true),
			array('name__typevideo, url_typevideo, title_typevideo, keywords_typevideo, description_typevideo,date_typevideo', 'length', 'max'=>500),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_typevideo, name__typevideo, showhide__typevideo, url_typevideo, title_typevideo, keywords_typevideo, description_typevideo,date_typevideo', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_typevideo' => 'Id',
			'name__typevideo' => 'Tên loại video',
			'showhide__typevideo' => 'Hiện/Ẩn',
			'url_typevideo' => 'Url Typevideo',
			'title_typevideo' => 'Tiêu đề Seo',
			'keywords_typevideo' => 'Từ khóa Seo',
			'description_typevideo' => 'Miêu tả Seo',
				'date_typevideo' => 'Ngày đăng',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_typevideo',$this->id_typevideo);
		$criteria->compare('name__typevideo',$this->name__typevideo,true);
		$criteria->compare('showhide__typevideo',$this->showhide__typevideo);
		$criteria->compare('url_typevideo',$this->url_typevideo,true);
		$criteria->compare('title_typevideo',$this->title_typevideo,true);
		$criteria->compare('keywords_typevideo',$this->keywords_typevideo,true);
		$criteria->compare('description_typevideo',$this->description_typevideo,true);
		$criteria->compare('date_typevideo',$this->date_typevideo,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}