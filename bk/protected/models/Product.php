<?php

/**
 * This is the model class for table "product".
 *
 * The followings are the available columns in table 'product':
 * @property integer $pr_id
 * @property integer $id_menu
 * @property integer $st_id
 * @property string $pr_name
 * @property string $pr_img
 * @property integer $pr_gia
 * @property string $pr_mota
 * @property integer $pr_number
 */
class Product extends CActiveRecord
{
	public $pr_img_upload;
	public $rootPath;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Product the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'product';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
				
				array('pr_name,pr_code', 'required'),
				array('id_menu,pr_gia_cu,pr_tinhtrang, pr_gia, pr_showhide,pr_highlights,pr_type,pr_width,pr_height,pr_code', 'numerical', 'integerOnly'=>true),
				array('pr_number,pr_name,pr_mota,pr_title_seo,pr_description_seo,pr_keywords_seo,pr_url_news,pr_summary,pr_madein,pr_Manufacturer,pr_joinImg,pr_code,pr_khuyenmai,specifications,imgvideopr,filterpr', 'length', 'max'=>200000),
				array('id_menu','my_required'),
				/* array('pr_img', 'file',
						'allowEmpty'=>$this->isNewRecord ?false:true,
						'types'=>'jpg, gif, png',
						'wrongType'=>'Tập tin không hợp lệ',
						'maxSize'=>1 * 1024 * 1024,
						'tooLarge'=>'Dung lượng file < 1MB. Hãy tải lên một tập tin nhỏ hơn.',//Error Message
				), */
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('filterpr,imgvideopr,specifications,pr_gia_cu,pr_tinhtrang,pr_khuyenmai,pr_khuyenmai,pr_madein,pr_Manufacturer,pr_summary,pr_code,pr_id, id_menu, st_id, pr_name, pr_img, pr_gia, pr_mota, pr_number,pr_showhide,pr_highlights,pr_type,pr_title_seo,pr_description_seo,pr_keywords_seo,pr_url_news', 'safe', 'on'=>'search'),
		);
	}

	public function my_required($attribute_name,$params){
		if(empty($this->id_menu)){
			$this->addError('id_menu',
					'Vui lòng chọn loại sản phẩm');
		}
	}
	
	protected function beforeSave()
	{
		$this->pr_img_upload = CUploadedFile::getInstance($this, 'pr_img');
		if (isset($this->pr_img_upload)) {
			if(!$this->isNewRecord){
				if(!empty($this->pr_img)){
				if (file_exists(getcwd().'/'.$this->pr_img)) {
					unlink(getcwd().'/'.$this->pr_img);
				};
				}
			}
			$fileName     = rand().$this->pr_img_upload->name;

			$uploadFolder = Yii::getPathOfAlias('webroot') . DIRECTORY_SEPARATOR . 'upload' . DIRECTORY_SEPARATOR . 'sanpham' . DIRECTORY_SEPARATOR . date('Y-m-d');
			if (!is_dir($uploadFolder)) {
				mkdir($uploadFolder, 0755, TRUE);
			}
			$uploadFile = $uploadFolder . DIRECTORY_SEPARATOR . $fileName;
			if(empty($_POST['Product']['pr_width']))
				$_POST['Product']['pr_width'] = 0;
			if(empty($_POST['Product']['pr_height']))
				$_POST['Product']['pr_height'] = 0;
			
			$this->pr_img_upload->saveAs($uploadFile); //Upload file thong qua object CUploadedFile
			Mitonios::thumpimg($uploadFile,$uploadFolder . DIRECTORY_SEPARATOR .'thump_'.$_POST['Product']['pr_width'].'x'.$_POST['Product']['pr_height'].'_' .$fileName,$_POST['Product']['pr_width'],$_POST['Product']['pr_height']);
			unlink($uploadFile);
			$this->pr_img = '/upload/sanpham/' . date('Y-m-d') . '/'. 'thump_'.$_POST['Product']['pr_width'].'x'.$_POST['Product']['pr_height'].'_'.$fileName; //Lưu path vào csdl
		}
		return parent::beforeSave();
	}
	
	protected function beforeDelete() {
		if(!empty($this->pr_img)){
		if (file_exists(getcwd().'/'.$this->pr_img)) {
			unlink(getcwd().'/'.$this->pr_img);
		}		
		}
		return parent::beforeDelete();
	}
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
				'YiiMenus' => array(self::BELONGS_TO, 'YiiMenu', 'id_menu'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'pr_id' => 'Id',
			'id_menu' => 'Loại sản phẩm',
			'pr_name' => 'Tên sản phẩm',
			'pr_img' => 'Ảnh',
			'pr_gia' => 'Gía mới',
			'pr_mota' => 'Mô tả',
			'pr_number' => 'Công xuất',
				'pr_title_seo' => 'Tiêu đề Seo',
				'pr_description_seo' => 'Miêu tả Seo',
				'pr_keywords_seo' => 'Từ khóa Seo',
				'pr_highlights' => 'Nổi bật',
				'pr_showhide' => 'Hiện thị',
				'pr_url_news' => 'Định danh đường dẫn',
				'pr_type' => 'Loại',
				'pr_width' => 'Chiều rộng',
				'pr_height' => 'Chiều cao',
				'pr_summary' => 'Mô tả tóm tắt',
				'pr_code' => 'Số lượng',
				'pr_madein' => 'Xuất xứ',
				'pr_Manufacturer' => 'Quãng đường',
				'pr_gia_cu' => 'Gía cũ',
				'pr_tinhtrang' => 'Tình trạng',
				'pr_khuyenmai' => 'Khuyến mãi',
				'specifications' => 'Thông số kỹ thuật',
				'imgvideopr' => 'Ảnh và Video',
				'filterpr'  => 'Danh mục lọc',
				'id_highlight'=> 'Loại nổi bật',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('pr_id',$this->pr_id);
		$criteria->compare('id_menu',$this->id_menu);
		$criteria->compare('pr_name',$this->pr_name,true);
		$criteria->compare('pr_img',$this->pr_img,true);
		$criteria->compare('pr_gia',$this->pr_gia,true);
		$criteria->compare('pr_mota',$this->pr_mota,true);
		$criteria->compare('pr_number',$this->pr_number,true);

		$criteria->compare('pr_title_seo',$this->pr_title_seo,true);
		$criteria->compare('pr_description_seo',$this->pr_description_seo,true);
		$criteria->compare('pr_keywords_seo',$this->pr_keywords_seo,true);
		$criteria->compare('pr_highlights',$this->pr_highlights,true);
		$criteria->compare('pr_showhide',$this->pr_showhide);
		$criteria->compare('pr_url_news',$this->pr_url_news,true);
		$criteria->compare('pr_type',$this->pr_type);
		$criteria->compare('pr_code',$this->pr_code,true);
		$criteria->compare('pr_summary',$this->pr_summary,true);
		$criteria->compare('pr_madein',$this->pr_madein,true);
		$criteria->compare('pr_Manufacturer',$this->pr_Manufacturer,true);
		
		$criteria->compare('pr_gia_cu',$this->pr_gia_cu,true);
		$criteria->compare('pr_tinhtrang',$this->pr_tinhtrang);
		$criteria->compare('pr_khuyenmai',$this->pr_khuyenmai,true);
		$criteria->compare('specifications',$this->specifications,true);
		$criteria->compare('imgvideopr',$this->imgvideopr,true);
		$criteria->compare('filterpr',$this->filterpr,true);
		//$criteria->compare('id_highlight',$this->id_highlight);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function menuhighlight($highlight){
	   $criteriahl = new CDbCriteria();
		$criteriahl->condition = 'highlights_menu=1 AND showhide_menu=1 AND location_menu=4';
		$criteriahl->order = 'order_menu ASC';
		$YiiMenuhl = YiiMenu::model()->findAll($criteriahl);
		foreach ($YiiMenuhl as $val){
			$arrf[$val->id_menu] = $val->name_menu;
			if($val->id_menu == $highlight)
				return $val->name_menu;
		}

	}
	
	public function highlight($highlight){
		                     if($highlight == 1)
					         	return 'Mới';
					          else if($highlight == 2)
					         	return 'Bán chạy';
					         else  if($highlight == 3)
					         	 return 'Hot';
	}
	
	public function typeproduct($type){
		if($type == 1)
			return 'Bán chạy';
		else if ($type == 2 )
			return 'Mới';
	}
}