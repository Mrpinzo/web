<?php

/**
 * This is the model class for table "yii_typealbum".
 *
 * The followings are the available columns in table 'yii_typealbum':
 * @property integer $id_typealbum
 * @property integer $id_menu
 * @property string $name_typealbum
 * @property integer $showhide_typealbum
 * @property string $title_seo_album
 * @property string $description_seo_album
 * @property string $keywords_seo_album
 */
class YiiTypealbum extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return YiiTypealbum the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'yii_typealbum';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name_typealbum', 'required'),
				//array('id_menu', 'required','message'=> 'Vui lòng chọn loại menu.'),
			array('showhide_typealbum,highlights_typealbum', 'numerical', 'integerOnly'=>true),
			array('name_typealbum, title_seo_album, description_seo_album, keywords_seo_album,join_typeAlbum,url_seo_typealbum', 'length', 'max'=>2000),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('highlights_typealbum,id_typealbum, id_menu, name_typealbum, showhide_typealbum, title_seo_album, description_seo_album, keywords_seo_album', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
				'YiiMenualbum' => array(self::BELONGS_TO, 'YiiMenu', 'id_menu'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_typealbum' => 'Id',
			'id_menu' => 'Loại Menu',
			'name_typealbum' => 'Tên Album',
			'showhide_typealbum' => 'Hiện',
			'title_seo_album' => 'Tiêu đề Seo',
			'description_seo_album' => 'Miêu tả Seo',
			'keywords_seo_album' => 'Từ khóa Seo',
				'highlights_typealbum' => 'Nổi bật'
		);
	}
	
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_typealbum',$this->id_typealbum);
		$criteria->compare('id_menu',$this->id_menu);
		$criteria->compare('name_typealbum',$this->name_typealbum,true);
		$criteria->compare('showhide_typealbum',$this->showhide_typealbum);
		$criteria->compare('title_seo_album',$this->title_seo_album,true);
		$criteria->compare('description_seo_album',$this->description_seo_album,true);
		$criteria->compare('keywords_seo_album',$this->keywords_seo_album,true);
		$criteria->compare('highlights_typealbum',$this->highlights_typealbum);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}