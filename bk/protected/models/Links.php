<?php

/**
 * This is the model class for table "links".
 *
 * The followings are the available columns in table 'links':
 * @property integer $id_links
 * @property string $name_links
 * @property integer $showhide_links
 * @property string $url_links
 */
class Links extends CActiveRecord
{
	public $logo_img_upload;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Links the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'links';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name_links, url_links', 'required'),
				array('logo_links', 'file',
						'allowEmpty'=>$this->isNewRecord ?false:true,
						'allowEmpty'=>true,
						'types'=>'jpg, gif, png',
						'wrongType'=>'Tập tin không hợp lệ',
						'maxSize'=>1 * 1024 * 1024,
						'tooLarge'=>'Dung lượng file < 1MB. Hãy tải lên một tập tin nhỏ hơn.',//Error Message
				),
			array('showhide_links', 'numerical', 'integerOnly'=>true),
			array('name_links, url_links', 'length', 'max'=>2000),
				//array('url_links', 'url', 'defaultScheme' => 'http','message'=> 'Định dạng liên kết URL sai.'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_links, name_links, showhide_links, url_links,logo_links', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

protected function beforeSave()
	{
		$this->logo_img_upload = CUploadedFile::getInstance($this, 'logo_links');
		if (isset($this->logo_img_upload)) {
			if(!$this->isNewRecord){
				if(!empty($this->logo_links)){
					if (file_exists(getcwd().'/'.$this->logo_links)) {
						unlink(getcwd().'/'.$this->logo_links);
					};
				}
			}
			$fileName     = rand().$this->logo_img_upload->name;
			$uploadFolder = Yii::getPathOfAlias('webroot') . DIRECTORY_SEPARATOR . 'upload' . DIRECTORY_SEPARATOR .'doitac'.DIRECTORY_SEPARATOR . date('Y-m-d');
			if (!is_dir($uploadFolder)) {
				mkdir($uploadFolder, 0755, TRUE);
			}
			$uploadFile = $uploadFolder . DIRECTORY_SEPARATOR . $fileName;
			$this->logo_img_upload->saveAs($uploadFile); //Upload file thong qua object CUploadedFile
			$this->logo_links = '/upload/doitac/' . date('Y-m-d') . '/'. $fileName; //Lưu path vào csdl
		}
		return parent::beforeSave();
	}
	protected function beforeDelete() {
		if(!empty($this->logo_links)){
			if (file_exists(getcwd().'/'.$this->logo_links)) {
				unlink(getcwd().'/'.$this->logo_links);
			}
		}
		return parent::beforeDelete();
	}
	
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_links' => 'Id',
			'name_links' => 'Tên liên kết',
			'showhide_links' => 'Hiện',
			'url_links' => 'Đường dẫn URL',
				'logo_links' => 'Logo đối tác',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		
		$criteria->compare('id_links',$this->id_links);
		$criteria->compare('name_links',$this->name_links,true);
		$criteria->compare('showhide_links',$this->showhide_links);
		$criteria->compare('url_links',$this->url_links,true);
		$criteria->compare('logo_links',$this->logo_links);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}