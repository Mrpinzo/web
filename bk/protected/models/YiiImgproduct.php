<?php

/**
 * This is the model class for table "yii_imgproduct".
 *
 * The followings are the available columns in table 'yii_imgproduct':
 * @property integer $id_yii_imgproduct
 * @property string $url_yii_imgproduct
 * @property integer $showhide_yii_imgproduct
 * @property integer $pr_id
 */
class YiiImgproduct extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return YiiImgproduct the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'yii_imgproduct';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('url_yii_imgproduct, pr_id', 'required'),
			//array('showhide_yii_imgproduct, pr_id', 'numerical', 'integerOnly'=>true),
			array('url_yii_imgproduct,showhide_yii_imgproduct,pr_id,url_yii_imgproduct', 'length', 'max'=>5000),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_yii_imgproduct, url_yii_imgproduct, showhide_yii_imgproduct, pr_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	protected function beforeDelete() {
		if (file_exists(getcwd().'/'.$this->url_yii_imgproduct)) {
			unlink(getcwd().'/'.$this->url_yii_imgproduct);
		}
		return parent::beforeDelete();
	}
	
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_yii_imgproduct' => 'Id Yii Imgproduct',
			'url_yii_imgproduct' => 'Url Yii Imgproduct',
			'showhide_yii_imgproduct' => 'Showhide Yii Imgproduct',
			'pr_id' => 'Pr',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_yii_imgproduct',$this->id_yii_imgproduct);
		$criteria->compare('url_yii_imgproduct',$this->url_yii_imgproduct,true);
		$criteria->compare('showhide_yii_imgproduct',$this->showhide_yii_imgproduct);
		$criteria->compare('pr_id',$this->pr_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}