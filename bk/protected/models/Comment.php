<?php

/**
 * This is the model class for table "aomment".
 *
 * The followings are the available columns in table 'aomment':
 * @property integer $cm_id
 * @property string $cm_link_en
 * @property string $cm_img
 * @property integer $cm_action
 * @property integer $cm_positions
 */
class Comment extends CActiveRecord
{
	public $cm_img_upload;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Comment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'comment';
	}
	/* <?php echo CHtml::dropDownList($model, 'cm_positions',
			array($model->cm_positions => 'Phải', $model->cm_positions => 'Trái'),
					array('empty' => 'Chọn vị trí'));?> */
	/* protected function beforeSave()
	{
		$this->cm_img_upload = CUploadedFile::getInstance($this, 'cm_img');
		if (isset($this->cm_img_upload)) {
			if(!$this->isNewRecord){ 
				if (file_exists(getcwd().'/'.$this->cm_img)) {
					unlink(getcwd().'/'.$this->cm_img);
				};
			}
			$fileName     = rand().$this->cm_img_upload->name;
			$uploadFolder = Yii::getPathOfAlias('webroot') . DIRECTORY_SEPARATOR . 'upload' . DIRECTORY_SEPARATOR .'quangcao'.DIRECTORY_SEPARATOR . date('Y-m-d');
			if (!is_dir($uploadFolder)) {
				mkdir($uploadFolder, 0755, TRUE);
			}
			$uploadFile = $uploadFolder . DIRECTORY_SEPARATOR . $fileName;
			$this->cm_img_upload->saveAs($uploadFile); //Upload file thong qua object CUploadedFile
			$this->cm_img = '/upload/quangcao/' . date('Y-m-d') . '/'. $fileName; //Lưu path vào csdl
		}
		return parent::beforeSave();
	} */
/* 	protected function beforeDelete() {
		if (file_exists(getcwd().'/'.$this->cm_img)) {
			unlink(getcwd().'/'.$this->cm_img);
		}
		return parent::beforeDelete();
	} */
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cm_link_en,cm_name_en, cm_action,cm_order', 'required'),
				array('cm_order', 'numerical', 'integerOnly'=>true),
				//array('cm_order', 'ordermenu'),
			//array('cm_img', 'file', 'types' => 'jpg, gif, png', 'maxSize' => 1 * 1024 * 1024, 'tooLarge'=>'Dung lượng file < 1MB. Hãy tải lên một tập tin nhỏ hơn.', 'allowEmpty' => TRUE),				
			array('cm_link_en,cm_link_fr,cm_link_sp,cm_name_en,cm_name_fr,cm_name_sp', 'length', 'max'=>40000),
			//array('cm_link_en', 'url', 'defaultScheme' => 'http'),
			//	array('cm_img', 'file',
			//			'allowEmpty'=>$this->isNewRecord ?false:true,
			//			'types'=>'jpg, gif, png',
			//			'wrongType'=>'Tập tin không hợp lệ',
			//			'maxSize'=>1 * 1024 * 1024,
			//			'tooLarge'=>'Dung lượng file < 1MB. Hãy tải lên một tập tin nhỏ hơn.',//Error Message
			//	),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('cm_id,cm_order, cm_link_en, cm_action, cm_positions', 'safe', 'on'=>'search'),
		);
	}
	
	/* public function ordermenu($attribute_name,$params){
	
		$Criteria = new CDbCriteria();
		$Criteria->condition = "cm_order='".$this->cm_order."'";
		//$model=Posts::model()->findByPk($id, $Criteria);
		$model=$this->model()->findAll($Criteria);
		if(count($model))
			$this->addError('cm_order',
					'Vị trí đã tồn tại');
	} */
	
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'cm_id' => 'Id',
			'cm_link_en' => 'Title',
            'cm_link_fr' => 'Title French',
            'cm_link_sp' => 'Title Spanish',
            
            'cm_name_en' => 'Content',
            'cm_name_fr' => 'Content French',
            'cm_name_sp' => 'Content Spanish',
		
			'cm_action' => 'Hiện/Ẩn',
			'cm_positions' => 'Vị trí',
				'cm_order' => 'Thứ tự'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('cm_id',$this->cm_id);
		$criteria->compare('cm_link_en',$this->cm_link_en,true);
		$criteria->compare('cm_img',$this->cm_img,true);
		$criteria->compare('cm_action',$this->cm_action);
		$criteria->compare('cm_positions',$this->cm_positions);
		$criteria->compare('cm_order',$this->cm_order);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
				'sort'=>array(
						'defaultOrder'=>'cm_id DESC',
				),
		));
	}
}