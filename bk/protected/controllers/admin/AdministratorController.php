<?php

class AdministratorController extends AdminController
{


	/**
	 * @return array action filters
	 */
	/* public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	} */

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		$items2a = YiiRole::model()->findByPk(Yii::app()->user->getState('role'));
		
		if(!empty($items2a['type_role'])){
			
		   $arrs = CJSON::decode($items2a['type_role'],true);
		   
		$a = '';
		foreach ($arrs as $val){
			
			if(in_array('Administrator', $val)){
				$a .= implode('-',$val).'-';
			}
			
		};
			if(!empty($a)){
				$arrdsd = explode('-', $a);
				$arr1 = array_unique($arrdsd);
			     array_shift($arr1);
			     array_pop($arr1);	
			}else{
				$arr1 = array('');
			}	
		}else {
          $arr1 = array('');    
        }
        //var_dump($arr1);die;
		return array(
				
				array('allow', // allow authenticated user to perform 'create' and 'update' actions
						'actions'=>$arr1,
						'users'=>array('*'),
				),
		
				array('deny',  // deny all users
						'users'=>array('*'),
				),
		);
		
				
	}

	public function actions()
	{
		return array(
				// captcha action renders the CAPTCHA image displayed on the contact page
				'captcha'=>array(
						'class'=>'CCaptchaAction',
						'backColor'=>0xFFFFFF,
				),
		);
	}
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->layout = 'main';
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$this->layout = 'main';
		$model=new Administrator;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Administrator']))
		{
			$model->attributes=$_POST['Administrator'];
			$model->password = md5($_POST['Administrator']['password']);
			$model->creat_date = Yii::app()->dateFormatter->formatDateTime(time());
			$model->update_date = null;
			//var_dump(md5($_POST['Administrator']['password']));
			if($model->save())				
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$this->layout = 'main';
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		Yii::app()->session['password_admin'] = $model->password;
		Yii::app()->session['username_admin'] = $model->username;
		if(isset($_POST['Administrator']))
		{
			$model->attributes=$_POST['Administrator'];
			$model->update_date = Yii::app()->dateFormatter->formatDateTime(time());
			if($_POST['Administrator']['password'] != Yii::app()->session['password_admin'])
				$model->password = md5($_POST['Administrator']['password']);
			if($model->save())
				//$this->redirect(array('view','id'=>$model->ad_id));
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$this->layout = 'main';
		$dataProvider=new CActiveDataProvider('Administrator');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$this->layout = 'main';
		$model=new Administrator('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Administrator']))
			$model->attributes=$_GET['Administrator'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
	public function actionAjaxUpdate()
	{
		$act = $_GET['act'];
		$autoIdAll = $_POST['autoId'];
		
		if (count($autoIdAll) > 0) {
			foreach ($autoIdAll as $autoId) {
				$model = $this->loadModel($autoId);
				if ($act == 'doDelete') {
					$model->delete();
				}
			}
		}
	
	}
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Administrator the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Administrator::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Administrator $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='administrator-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
