<?php 
 class cartItem extends CWidget{
 	  public function init(){
 	  	
 	  }
 	  
 	  public function run(){
 	  	$proItem = Yii::app()->session['cart'];
 	  	$total = 0;
 	  	$items = 0;
 	    $arr = array();
 	  	if(!empty($proItem)){
	 	  	foreach ($proItem as $key=>$value){
	 	  		$items = $items + $value;
	 	  		$itemPro = Product::model()->findByPk($key);
	 	  		$total = $total + ($itemPro->pr_gia*$value);
	 	  		$arr[$key]['gia'] = $itemPro->pr_gia;
	 	  		$arr[$key]['anh'] = $itemPro->pr_img;
	 	  		$arr[$key]['soluong'] = $value;
	 	  		$arr[$key]['tensp'] = $itemPro->pr_name;
	 	  		$arr[$key]['pr_id'] = $itemPro->pr_id;
	 	  		$arr[$key]['tonggiasp'] = $itemPro->pr_gia*$value;
	 	  		$arr[$key]['pr_url_news'] = $itemPro->pr_url_news;
	 	  		$arr[$key]['pr_joinImg'] = $itemPro->pr_joinImg;
	 	  	};
 	  	};
 	  	Yii::app()->session['numbers'] = $items;
 	  	$this->render('cartItem',array('items'=>$items,'total'=>$total,'itemPro'=>$arr));
 	  }
 }
?>