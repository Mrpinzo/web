<?php 
 class Payorder extends CWidget{
 	  public function init(){
 	  	
 	  }
 	  
 	  public function run(){
 	  	$proItem = Yii::app()->session['cart'];
 	  	$total = 0;
 	  	$items = 0;
 	    $arr = array();
 	  	if(!empty($proItem)){
	 	  	foreach ($proItem as $key=>$value){
	 	  		$items = $items + $value;
	 	  		$itemPro = Product::model()->findByPk($key);
	 	  		$total = $total + ($itemPro->pr_gia*$value);
	 	  		$arr[$key]['gia'] = $itemPro->pr_gia;
	 	  		$arr[$key]['anh'] = $itemPro->pr_img;
	 	  		$arr[$key]['soluong'] = $value;
	 	  		$arr[$key]['tensp'] = $itemPro->pr_name;
	 	  		$arr[$key]['pr_id'] = $itemPro->pr_id;
	 	  		$arr[$key]['tonggiasp'] = $itemPro->pr_gia*$value;
	 	  		$arr[$key]['pr_url_news'] = $itemPro->pr_url_news;
	 	  		$arr[$key]['pr_joinImg'] = $itemPro->pr_joinImg;
	 	  	};
 	  	};
 	  	Yii::app()->session['numbers'] = $items;
 	  
 	  	
 	  	$model=new Orders;
 	  	
 	  	if(isset($_POST['Orders']))
 	  	{
 	  		$model->attributes=$_POST['Orders'];
 	  		$model->ord_date = date('d-m-Y');
 	  		if($model->save()){
 	  			foreach ($arr as $key=>$val){
	 	  			$command = Yii::app()->db->createCommand();
	 	  			$command->insert('cart', array(
	 	  					'car_summoney'=>$val['tonggiasp'],
	 	  					'ord_id'=>$model->ord_id,
	 	  					'cart_number'=>$val['soluong'],
	 	  					'pr_id'=>$key,
	 	  			));
 	  			} 
 	  			
 	  			//Yii::app()->request->redirect('index.php?r=site/index');
 	  			unset(Yii::app()->session['cart']); 
 	  			unset(Yii::app()->session['numbers']);
 	  			unset(Yii::app()->session['total']);
 	  			 
 	  			/* Yii::app()->clientScript->registerScript('helloscript',Yii::app()->request->baseUrl."/js/home/main.js"
 	  					,CClientScript::POS_HEAD); */
					$ourscript = "alert('Đặt hàng thành công.');window.location.href='/';";
					 Yii::app()->clientScript->registerScript('helloscript',$ourscript,CClientScript::POS_READY); 
					 //Yii::app()->request->redirect(Yii::app()->homeUrl);
 	  		}
 	  	}
 	  	$this->render('Payorder',array('items'=>$items,'total'=>$total,'itemPro'=>$arr,'model'=>$model));
 	  }
 }
?>