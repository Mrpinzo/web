<?php 
 class top extends CWidget{
     public $en = array(
             'contact' => 'Contact',
             'typeserch' => 'Type search text here...',
             
        );
      public $fr = array(
            'contact' => 'Connecté',
            'typeserch' => 'Tapez le texte de recherche ici ...',
        );   
        public $sp = array(
            'typeserch' => 'Escriba el texto de búsqueda aquí ...',
            'contact' => 'Conecta',
        );     
       
    
 	  public function init(){
 	  		//$_SESSION['lang'] = 'en';
 	  }
 	  
 	  public function run(){
 	      	
 	  	$criteria2  = new CDbCriteria();
 	  	$criteria2->condition = 'location_menu=1 AND showhide_menu=1';
 	  	$criteria2->order     = 'order_menu ASC';
 	  	$linktop = YiiMenu::model()->findAll($criteria2);
 	  	
 	  	$criteria2adv  = new CDbCriteria();
 	  	$criteria2adv->condition = 'adv_positions=1 AND adv_action=1';
 	  	$criteria2adv->order     = 'adv_id ASC';
 	  	$criteria2adv->limit = 2;
 	  	$Advertisetop = Advertise::model()->findAll($criteria2adv);
 	  	
 	  	$criteria2f  = new CDbCriteria();
 	  	$criteria2f->condition = 'location_menu=1 AND showhide_menu=1 AND level_menu=0';
 	  	$criteria2f->order     = 'order_menu ASC';
 	  	$menutop = YiiMenu::model()->findAll($criteria2f);
 	  	
 	  	$criterianew  = new CDbCriteria();
 	  	$criterianew->condition = 'new_highlights=1 AND new_showhide=1';
 	  	$criterianew->order     = 'new_id DESC';
 	  	$newhot = News::model()->findAll($criterianew);
 	  	
        $ad = new CDbCriteria();
    	$ad->condition = 'adv_positions=2';
    	$ad->order = 'adv_id DESC';
    	$ads = Advertise::model()->findAll($ad);
        
 	  	$Configure = Configure::model()->findAll();
 	  	
 	  	$time = $this->sw_get_current_weekday();
 	  	
 	  	$this->render('top',array('Configure'=>$Configure,'linktop'=>$linktop,'ads'=>$ads,'Advertisetop'=>$Advertisetop,
 	  			'menutop'=>$menutop,'newhot'=>$newhot,'time'=>$time
 	  	));
 	  }
 	  
 	  function sw_get_current_weekday() {
 	  	date_default_timezone_set('Asia/Ho_Chi_Minh');
 	  	$weekday = date("l");
 	  	$weekday = strtolower($weekday);
 	  	switch($weekday) {
 	  		case 'monday':
 	  			$weekday = 'Thứ 2';
 	  			break;
 	  		case 'tuesday':
 	  			$weekday = 'Thứ 3';
 	  			break;
 	  		case 'wednesday':
 	  			$weekday = 'Thứ 4';
 	  			break;
 	  		case 'thursday':
 	  			$weekday = 'Thứ 5';
 	  			break;
 	  		case 'friday':
 	  			$weekday = 'Thứ 6';
 	  			break;
 	  		case 'saturday':
 	  			$weekday = 'Thứ 7';
 	  			break;
 	  		default:
 	  			$weekday = 'Chủ nhật';
 	  			break;
 	  	}
 	  	
 	  	return $weekday.', '.date('d/m/Y | H:i').' GMT+7';
 	  }
 }
?>